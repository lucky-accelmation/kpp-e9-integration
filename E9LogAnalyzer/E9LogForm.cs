﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;

namespace E9LogAnalyzer
{
    public partial class E9LogForm : Form
    {
        public E9LogForm()
        {
            InitializeComponent();
        }
        private void E9LogForm_Load(object sender, EventArgs e)
        {
            worker = new BackgroundWorker();
            worker.DoWork += Worker_DoWork;
            lbParameters.Font = new Font("Consolas", 11, FontStyle.Regular);
            txDetails.Font = new Font("Consolas", 11, FontStyle.Regular);
            //txLogFilename.Text = @"C:\Users\devadmin\Downloads\traceBPM001_20215324_1006706\bpm001.log";
            //txLogFilename.Text = @"C:\Users\devadmin\Downloads\traceBPM001_20215324_1006706.zip";

            lbParameters.Items.Add("Service: ");
            lbParameters.Items.Add("Operation: ");
            lbParameters.Items.Add("DTO: ");
            lbParameters.Items.Add("Result: ");

            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                txLogFilename.Text = args[1];
                worker.RunWorkerAsync(txLogFilename.Text);
            }
        }

        private string getclassname(string fullname)
        {
            return Regex.Match(fullname, @"(?<=\.)[A-Za-z0-9\-]+$", RegexOptions.IgnoreCase).Value;
        }

        private string buildsoapparams(string pre, string[][] prm)
        {
            string f = "";
            foreach (string[] p in prm)
            {
                p[0] = char.ToLower(p[0][0]) + p[0].Substring(1); //lower first char
                f += "\r\n" + @"            <" + pre + p[0] + ">" + p[1] + "</" + pre + p[0] + ">";
            }
            return f;
        }

        private string buildsoap(string detail)
        {
            string[] d = parse(detail, true);
            if (d.Length <= 4)
            {
                return "";
            }
            string xml = @"
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:env=""http://envoygather.m3110.service.ellipse.mincom.com"" xmlns:con=""http://connectivity.service.ews.mincom.com"" xmlns:ser=""http://service.ellipse.mincom.com"" xmlns:att=""http://attribute.ellipse.mincom.com"" xmlns:ins=""http://instances.m3110.types.ellipse.mincom.com"">
   <soapenv:Header>
	<wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" soapenv:mustUnderstand=""1"">
	  <wsse:UsernameToken xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"" wsu:Id=""UsernameToken-1"">
	    <wsse:Username>bpm001</wsse:Username>
	    <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"">kpp0811</wsse:Password>
	  </wsse:UsernameToken>
	</wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <env:" + d[1] + @">
         <env:context>
            <con:district>KPHO</con:district>
         </env:context>
         <env:serviceDTO>";
            string[][] dtoparams = new JavaScriptSerializer().Deserialize<string[][]>(d[4]);
            if (d[2].EndsWith("-array", StringComparison.OrdinalIgnoreCase))
            {
                xml += buildsoapparams("ins:", dtoparams);
            }
            else
            {
                xml += buildsoapparams("ins:", dtoparams);
            }
            xml += @"
         </env:serviceDTO>
      </env:" + d[1] + @">
   </soapenv:Body>
</soapenv:Envelope>";

            return xml;
        }

        private string buildparams(string pre, string[][] prm)
        {
            string f = "";
            foreach (string[] p in prm)
            {
                p[0] = char.ToLower(p[0][0]) + p[0].Substring(1); //lower first char
                string val = "\"" + p[1].Trim() + "\"";
                if (Regex.Match(p[0], @"date", RegexOptions.IgnoreCase).Success && Regex.Match(p[1], @"\d{8}", RegexOptions.IgnoreCase).Success)
                {
                    try
                    {
                        val = string.Format("new DateTime({0},{1},{2})", p[1].Substring(0, 4), p[1].Substring(4, 2), p[1].Substring(6, 2));
                        f += pre + "\t" + p[0] + "Specified = true,\r\n";
                    }
                    catch { }
                }
                else if (Regex.Match(p[0], @"code", RegexOptions.IgnoreCase).Success)
                {
                    //string, not numbers
                }
                else if (Regex.Match(p[1], @"true|false", RegexOptions.IgnoreCase).Success)
                {
                    val = p[1];
                    f += pre + "\t" + p[0] + "Specified = true,\r\n";
                }
                else if (Regex.Match(p[1], @"^[\d\-]+$").Success) //number only
                {
                    val = p[1];
                    f += pre + "\t" + p[0] + "Specified = true,\r\n";
                }
                else if (Regex.Match(p[1], @"^[\d\.-]+$").Success) //decimal
                {
                    val = p[1] + "M";
                    f += pre + "\t" + p[0] + "Specified = true,\r\n";
                }
                f += pre + "\t" + p[0] + " = " + val + ",\r\n";
            }
            return f;
        }

        private string createfunction(string detail)
        {
            string[] d = parse(detail, true);
            if (d.Length <= 4)
            {
                return "";
            }

            string epbase = "http://ews-kpd.kppmining.net/ews/services/";
            string f = "//wsdl = " + epbase + d[0] + "?WSDL\r\n";
            f += "var ctx = GetContext<" + d[0] + ".OperationContext>(mp);\r\n";
            f += "var svc = new " + d[0] + "." + d[0] + "();\r\n";
            string[][] dtoparams = new JavaScriptSerializer().Deserialize<string[][]>(d[4]);
            if (d[2].EndsWith("-array", StringComparison.OrdinalIgnoreCase))
            {
                string dto = d[2].Substring(0, d[2].Length - "-array".Length);
                f += "var dto = new " + d[0] + "." + dto + "[]{\r\n";
                f += "\tnew " + d[0] + "." + dto + "()\r\n";
                f += "\t{\r\n";
                f += buildparams("\t", dtoparams);
                f += "\t}\r\n";
                f += "};\r\n";
            }
            else
            {
                f += "var dto = new " + d[0] + "." + d[2] + "()\r\n";
                f += "{\r\n";
                f += buildparams("", dtoparams);
                f += "};\r\n";
            }
            f += "var res = svc." + d[1] + "(ctx, dto);\r\n";
            if (d[5] != null)
            {
                string[][] resultparams = new JavaScriptSerializer().Deserialize<string[][]>(d[5]);
                foreach (string[] p in resultparams)
                {
                    p[0] = char.ToLower(p[0][0]) + p[0].Substring(1); //lower first char
                    if (d[3].EndsWith("-array", StringComparison.OrdinalIgnoreCase))
                    {
                        f += "Console.WriteLine(string.Format(\"" + p[0] + ": {0}\",res[0]." + p[0] + ")); // " + p[1] + "\r\n";
                    }
                    else
                    {
                        f += "Console.WriteLine(string.Format(\"" + p[0] + ": {0}\",res." + p[0] + ")); // " + p[1] + "\r\n";
                    }
                }
            }
            return f;
        }

        private string[] parse(string detail, bool parseparams)
        {
            //string xml = new Regex(@".*(?=\<)", RegexOptions.IgnoreCase).Replace(detail, "", 1);
            string xml = new Regex(@".*?(?=\<)", RegexOptions.IgnoreCase).Replace(detail, "", 1);
            if (!xml.StartsWith("<")) //not xml
            {
                return new string[]
                {
                    "","","",""
                };
            }
            try
            {
                XElement elm = XElement.Parse(xml);
                if(elm.Name.ToString()== "com.mincom.ria.data.Service")
                {
                    string cname = getclassname(elm.XPathSelectElement("/name").Value.ToString());
                    string opname = elm.XPathSelectElement("/operation").Value.ToString();
                    XElement dtoelm = elm.XPathSelectElement("/dto");
                    string dto = getclassname(dtoelm.Attribute("class").Value);
                    string dtoparamstx = null;
                    string result = null;
                    string resultparamstx = null;
                    XElement resultelm = elm.XPathSelectElement("/result");
                    if (resultelm != null)
                    {
                        result = getclassname(resultelm.Attribute("class").Value);
                    }
                    if (parseparams)
                    {
                        if (dto.EndsWith("-array", StringComparison.OrdinalIgnoreCase))
                        {
                            dtoelm = dtoelm.Elements().First();
                        }
                        string[][] dtoparams = dtoelm.Elements().Select(e => new string[] { e.Name.ToString(), e.Value }).ToArray();
                        dtoparamstx = new JavaScriptSerializer().Serialize(dtoparams);
                        if (resultelm != null)
                        {
                            //if (result.EndsWith("-array", StringComparison.OrdinalIgnoreCase))
                            //{
                            //    if (resultelm.Descendants().Count() > 0)
                            //    {
                            //        resultelm = resultelm.Descendants().First();
                            //    }
                            //}
                            string[][] resultparams = resultelm.Elements().Select(e => new string[] { e.Name.ToString(), e.Value }).ToArray();
                            resultparamstx = new JavaScriptSerializer().Serialize(resultparams);
                        }
                    }

                    return new string[]{
                        cname,
                        opname,
                        dto,
                        result,
                        dtoparamstx,
                        resultparamstx
                    };
                }
                else
                {
                    return new string[] //not service xml
                    {
                        "","","",""
                    };
                }
            }
            catch
            {
                //cannot parse
                return new string[]
                {
                    "","","",""
                };
            }
        }

        private void readFromStream(Stream stream)
        {
            string header = "";
            bool record = false;
            bool first = false;
            details = new List<string>();
            string detail = "";
            string headertype = "";

            int lineidx = 0;
            int headeridx = 0;
            lbLines.Invoke(new Action(
                () =>
                {
                    lbLines.Items.Clear();
                }));
            using (BufferedStream bs = new BufferedStream(stream))
            using (StreamReader sr = new StreamReader(bs))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Match m;
                    if ((m = Regex.Match(line, @"\[trace=true.*?]", RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        if (record) // end of last record
                        {
                            string h = string.Format("{0:000000}: {1} - {2}", headeridx, headertype, header);
                            if (headertype == "xml")
                            {
                                string[] d = parse(detail, false);
                                h = string.Format("{0:000000}: {1} {2} - {3}", headeridx, d[0], d[1], header);
                            }
                            if (headertype == "screensubmit")
                            {
                                string[] details = detail.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                                string json = string.Join("\r\n", details.Skip(1).ToArray());
                                detail = details[0] + "\r\n" + JsonConvert.SerializeObject(JsonConvert.DeserializeObject(json), Formatting.Indented);
                            }
                            if (headertype == "screenxml")
                            {
                                string dhdr = new Regex(@".*?(?=\<)", RegexOptions.IgnoreCase).Match(detail).Value;
                                string xml = new Regex(@".*?(?=\<)", RegexOptions.IgnoreCase).Replace(detail, "", 1);
                                XElement elm = XElement.Parse(xml);
                                detail = dhdr + "\r\n" + elm.ToString();
                            }
                            lbLines.Invoke(new Action(
                                () =>
                                {
                                    lbLines.Items.Add(h);
                                }));
                            details.Add(detail);
                            record = false;
                        }
                        // log line
                        detail = string.Format("{0:000000}: {1}", lineidx, line);
                        header = line;
                        headeridx = lineidx;
                    }

                    if ((m = Regex.Match(line, Regex.Escape(@"<com.mincom.ria.data.Service>"), RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        record = true;
                        first = true;
                        headertype = "xml";
                    }
                    if ((m = Regex.Match(line, Regex.Escape(@"c.a.v.service.ServiceInvokerImpl") + @".*?" + Regex.Escape("*** Service Messages ***"),
                        RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        lbLines.Invoke(new Action(
                            () =>
                            {
                                string ts = Regex.Match(line, @"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}").Value;
                                string h = string.Format("{0:000000}: {1} - {2}", headeridx, "Service Messages", ts);
                                lbLines.Items.Add(h);
                                details.Add(detail);
                            }));
                    }
                    if ((m = Regex.Match(line, Regex.Escape(@"c.m.e.e.EllipseScreenServiceImpl"), RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        record = true;
                        first = true;
                        headertype = "screenreply";
                    }
                    if ((m = Regex.Match(line, Regex.Escape(@"c.a.v.ellipse.LxScreenSubmitterImpl"), RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        record = true;
                        first = true;
                        headertype = "screensubmit";
                    }
                    if ((m = Regex.Match(line, Regex.Escape(@"c.m.m.r.screen.xml.LegacyXMLScreen"), RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        record = true;
                        first = true;
                        headertype = "screenxml";
                    }
                    if ((m = Regex.Match(line, Regex.Escape(@"c.m.m.r.s.x.t.matrix.MatrixHelper"), RegexOptions.IgnoreCase | RegexOptions.Multiline)).Success)
                    {
                        record = true;
                        first = true;
                        headertype = "screenmatrix";
                    }

                    if (record)
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            detail += "\r\n" + line;
                        }
                    }

                    lineidx++;
                    if (lineidx % 100000 == 0)
                        lbLogs.BeginInvoke(new Action(
                            () =>
                            {
                                lbLogs.Items.Add(string.Format("{0:N} lines loaded", lineidx));
                                lbLogs.SelectedIndex = lbLogs.Items.Count - 1;
                            }));
                    if (lineidx > 20000)
                    {
                        //break;
                    }
                }
                if (record) // end of last record
                {
                    string[] d = parse(detail, false);
                    string h = string.Format("{0:000000}: {1} - {2}", headeridx, d[0], header);
                    lbLines.Invoke(new Action(
                        () =>
                        {
                            lbLines.Items.Add(h);
                        }));
                    details.Add(detail);
                }
                lbLogs.BeginInvoke(new Action(
                    () =>
                    {
                        lbLogs.Items.Add(string.Format("{0:N} lines loaded - finished", lineidx));
                        lbLogs.SelectedIndex = lbLogs.Items.Count - 1;
                    }));
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string path = e.Argument as string;
            if (Path.GetExtension(path) == ".zip")
            {
                string[] exclude = new string[] { "Monitoring.csv", "Metadata.log" };
                using (ZipArchive archive = ZipFile.Open(path, ZipArchiveMode.Read))
                {
                    lbLogs.Invoke(new Action(() =>
                    {
                        lbLogs.Items.Add(string.Format("Archive size: {0:N} KB", new FileInfo(path).Length / 1024));
                    }));

                    ZipArchiveEntry entry = archive.Entries.First(
                        a => a.Name.EndsWith(".log", StringComparison.OrdinalIgnoreCase)
                        && !a.Name.EndsWith("ui.log", StringComparison.OrdinalIgnoreCase)
                        && !exclude.Contains(a.Name));
                    if (entry != null)
                    {
                        lbLogs.Invoke(new Action(() =>
                        {
                            lbLogs.Items.Add(string.Format("Found: {0} {1:N} KB", entry.Name, entry.Length / 1024));
                        }));
                        using (Stream fs = entry.Open())
                        {
                            readFromStream(fs);
                        }
                    }
                    else
                    {
                        lbLogs.Invoke(new Action(() =>
                        {
                            lbLogs.Items.Add("File not found, contents:");
                            lbLogs.Items.AddRange(archive.Entries.Select(ent => ent.FullName).ToArray());
                        }));
                    }
                }
            }
            else
            {
                using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    readFromStream(fs);
                }
            }
        }

        private void btBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Log files (*.log,*.zip)|*.log;*.zip";
            if (DialogResult.OK == fd.ShowDialog(this))
            {
                txLogFilename.Text = fd.FileName;
                worker.RunWorkerAsync(txLogFilename.Text);
            }

        }

        BackgroundWorker worker;
        List<string> details;

        private void lbLines_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txDetails.Text = lbLines.SelectedItem as string;
            string[] d = parse(details[lbLines.SelectedIndex], false);
            //lbParameters.Items.AddRange(d.Where(a => a != null).ToArray());
            lbParameters.Items[0] = "Service: " + d[0];
            lbParameters.Items[1] = "Operation: " + d[1];
            lbParameters.Items[2] = "DTO: " + d[2];
            lbParameters.Items[3] = "Result: " + d[3];
        }

        private void lbParameters_SelectedIndexChanged(object sender, EventArgs e)
        {
            showdetails();
        }

        private void showdetails()
        {
            if (lbLines.SelectedIndex == -1)
            {
                return;
            }
            switch (lbParameters.SelectedIndex)
            {
                case 0:
                    txDetails.Text = details[lbLines.SelectedIndex];
                    break;
                case 1:
                    txDetails.Text = createfunction(details[lbLines.SelectedIndex]);
                    break;
                case 2:
                    txDetails.Text = buildsoap(details[lbLines.SelectedIndex]);
                    break;
            }

        }
    }
}
