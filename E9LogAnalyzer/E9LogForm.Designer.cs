﻿
namespace E9LogAnalyzer
{
    partial class E9LogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txLogFilename = new System.Windows.Forms.TextBox();
            this.btBrowse = new System.Windows.Forms.Button();
            this.lbLines = new System.Windows.Forms.ListBox();
            this.lbParameters = new System.Windows.Forms.ListBox();
            this.txDetails = new System.Windows.Forms.TextBox();
            this.lbLogs = new System.Windows.Forms.ListBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Log File";
            // 
            // txLogFilename
            // 
            this.txLogFilename.Location = new System.Drawing.Point(64, 9);
            this.txLogFilename.Name = "txLogFilename";
            this.txLogFilename.ReadOnly = true;
            this.txLogFilename.Size = new System.Drawing.Size(604, 20);
            this.txLogFilename.TabIndex = 1;
            // 
            // btBrowse
            // 
            this.btBrowse.Location = new System.Drawing.Point(674, 7);
            this.btBrowse.Name = "btBrowse";
            this.btBrowse.Size = new System.Drawing.Size(75, 23);
            this.btBrowse.TabIndex = 2;
            this.btBrowse.Text = "Browse";
            this.btBrowse.UseVisualStyleBackColor = true;
            this.btBrowse.Click += new System.EventHandler(this.btBrowse_Click);
            // 
            // lbLines
            // 
            this.lbLines.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbLines.FormattingEnabled = true;
            this.lbLines.Location = new System.Drawing.Point(3, 5);
            this.lbLines.Name = "lbLines";
            this.lbLines.Size = new System.Drawing.Size(208, 563);
            this.lbLines.TabIndex = 3;
            this.lbLines.SelectedIndexChanged += new System.EventHandler(this.lbLines_SelectedIndexChanged);
            // 
            // lbParameters
            // 
            this.lbParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbParameters.FormattingEnabled = true;
            this.lbParameters.Location = new System.Drawing.Point(3, 5);
            this.lbParameters.Name = "lbParameters";
            this.lbParameters.Size = new System.Drawing.Size(802, 95);
            this.lbParameters.TabIndex = 4;
            this.lbParameters.SelectedIndexChanged += new System.EventHandler(this.lbParameters_SelectedIndexChanged);
            // 
            // txDetails
            // 
            this.txDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txDetails.Location = new System.Drawing.Point(3, 106);
            this.txDetails.Multiline = true;
            this.txDetails.Name = "txDetails";
            this.txDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txDetails.Size = new System.Drawing.Size(802, 356);
            this.txDetails.TabIndex = 5;
            // 
            // lbLogs
            // 
            this.lbLogs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbLogs.FormattingEnabled = true;
            this.lbLogs.Location = new System.Drawing.Point(3, 468);
            this.lbLogs.Name = "lbLogs";
            this.lbLogs.Size = new System.Drawing.Size(802, 108);
            this.lbLogs.TabIndex = 6;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 35);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lbLines);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lbParameters);
            this.splitContainer1.Panel2.Controls.Add(this.lbLogs);
            this.splitContainer1.Panel2.Controls.Add(this.txDetails);
            this.splitContainer1.Size = new System.Drawing.Size(1026, 579);
            this.splitContainer1.SplitterDistance = 214;
            this.splitContainer1.TabIndex = 7;
            // 
            // E9LogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 626);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btBrowse);
            this.Controls.Add(this.txLogFilename);
            this.Controls.Add(this.label1);
            this.Name = "E9LogForm";
            this.Text = "E9 Log Analyzer";
            this.Load += new System.EventHandler(this.E9LogForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txLogFilename;
        private System.Windows.Forms.Button btBrowse;
        private System.Windows.Forms.ListBox lbLines;
        private System.Windows.Forms.ListBox lbParameters;
        private System.Windows.Forms.TextBox txDetails;
        private System.Windows.Forms.ListBox lbLogs;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

