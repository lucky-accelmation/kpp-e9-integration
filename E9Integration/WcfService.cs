﻿using E9Integration.Classes;
using E9Integration.Libraries;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace E9Integration
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWcfService" in both code and config file together.
    [ServiceContract]
    public interface IWcfService
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetStatus")]
        ServiceResult GetStatus();

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "CreateJob/{operation}")]
        ServiceResult CreateJob(string operation, Stream data);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetJobInfo/{jobid}")]
        JobInfo GetJobInfo(string jobid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetJobList")]
        JobInfo[] GetJobList();
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WcfService" in both code and config file together.
    public class WcfService : IWcfService
    {
        private static readonly ILog logger =
            LogManager.GetLogger(typeof(WcfService));

        static JobProcess process;

        public bool Initiate()
        {
            return Initiate(false);
        }

        public bool Initiate(bool restart)
        {
            if (process == null)
            {
                process = JobProcess.GetJobProcess();
            }
            if (restart)
            {
                logger.InfoFormat("Restart Unfinished Jobs: {0} ", process.RestartUnfinishedJob());
            }
            return process.StartJob();
        }

        public ServiceResult GetStatus()
        {
            logger.Info("GetStatus");

            bool status = Initiate();
            int queue = process.GetJobCount();
            string statusString = "";
            if (status && queue == 0)
            {
                statusString = "process idle";
            }
            else if (status)
            {
                statusString = "process started";
            }
            else
            {
                statusString = "process is running";
            }
            ServiceResult result = new ServiceResult();

            result.message = string.Format("Status: {0} job(s) queued, {1} ({2}) - {3}", queue, statusString, process.GetRunningCount(), WindowsService.VERSION);

            return result;
        }

        public JobInfo GetJobInfo(string jobid)
        {
            Initiate();
            JobInfo result = process.GetJobInfo(jobid);

            return result;
        }

        public JobInfo[] GetJobList()
        {
            Initiate();
            JobInfo[] result = process.GetJobList();

            return result;
        }

        public ServiceResult CreateJob(string operation, Stream data)
        {
            bool status = Initiate();
            string statusString = status ? "process started" : "process is running";
            ServiceResult result = new ServiceResult();
            try
            {
                result.jobid = process.CreateJob(operation, ServiceHelper.GetStreamData(data));
                int queue = process.GetJobCount();
                result.message = string.Format("Job created: operation {0}, status: {1} job(s) queued, {2}",
                    operation, queue, statusString);
                process.StartJob();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
            }

            return result;
        }

    }
}