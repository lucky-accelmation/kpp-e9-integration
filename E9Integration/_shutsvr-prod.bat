@echo off

set svpf=10.14.101.
set svlist=9
set svname="E9 Integration"
set src=X:\VS\E9Integration\bin\Debug
set target=c$\E9I

echo .
echo prepare network
for %%G in (%svlist%) do net use \\%svpf%%%G\c$ /user:kppmining\dmadmin kpp0811
net use
pause

echo .
echo query service
for %%G in (%svlist%) do echo query %%G && sc \\%svpf%%%G query %svname% && timeout 2
rem pause

rem exit

echo .
echo stop service
for %%G in (%svlist%) do echo stop %%G && sc \\%svpf%%%G stop %svname% && timeout 2
pause

echo .
echo start service
for %%G in (%svlist%) do echo start %%G && sc \\%svpf%%%G start %svname% && timeout 2
rem pause

echo .
echo query service
for %%G in (%svlist%) do echo query %%G && sc \\%svpf%%%G query %svname% && timeout 2
rem pause

echo .
echo check
for %%G in (%svlist%) do echo check %%G && dir \\%svpf%%%G\%target%\*.exe
pause

