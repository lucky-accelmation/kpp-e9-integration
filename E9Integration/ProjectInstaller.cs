﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace E9Integration
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {

            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "E9 Integration";
            service.DisplayName = "E9 Integration";
            service.Description = "Ellipse 9 Integration";
            service.StartType = System.ServiceProcess.ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(service);
            InitializeComponent();
        }
    }
}
