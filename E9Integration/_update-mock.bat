@echo off

set svpf=10.14.101.
set svlist=5
set svname="E9 Integration"
set src=X:\VS\E9Integration\bin\Debug
set target=c$\E9I

echo .
echo prepare network
for %%G in (%svlist%) do net use \\%svpf%%%G\c$ /user:dmadmin Documentum1
net use
pause

echo .
set dt=%date:~10,4%%date:~4,2%%date:~7,2%
choice /c YN /M "Backup files to %dt% - DO THIS ONLY ONCE"
if %ERRORLEVEL% == 2 goto skipbackup
echo .
echo backup
for %%G in (%svlist%) do robocopy \\%svpf%%%G\%target%\ \\%svpf%%%G\%target%\bk%dt%\ *.exe *.dll *.pdb
pause
:skipbackup

echo .
echo query service
for %%G in (%svlist%) do echo query %%G && sc \\%svpf%%%G query %svname% && timeout 2
rem pause

rem exit

echo .
echo stop service
for %%G in (%svlist%) do echo stop %%G && sc \\%svpf%%%G stop %svname% && timeout 2
pause

echo .
echo updating
for %%G in (%svlist%) do echo updating %%G && robocopy %src% \\%svpf%%%G\%target%\ *.exe *.dll *.pdb && pause

rem echo updating config
rem for %%G in (%svlist%) do echo updating %%G && robocopy %src% \\%svpf%%%G\%target%\ *.config && pause
pause

echo .
echo start service
for %%G in (%svlist%) do echo start %%G && sc \\%svpf%%%G start %svname% && timeout 2
rem pause

echo .
echo query service
for %%G in (%svlist%) do echo query %%G && sc \\%svpf%%%G query %svname% && timeout 2
rem pause

echo .
echo check
for %%G in (%svlist%) do echo check %%G && dir \\%svpf%%%G\%target%\*.exe
pause

