﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Diagnostics;

namespace MincomWCF.Libraries
{
    public class LogHelper
    {
        static string LOG_PATH = "C:/inetpub/logs/MincomWCF/";
        static string LOG_SOURCE = "Mincom Web Service";
        static string LOG_NAME = "Web Service";
        static string LOG_EVENT = "Log";

        //public static void LogMessage(string message)
        //{
        //    try
        //    {
        //        LogMessageConsole(message);
        //        LogMessageFile(message);
        //    }
        //    catch(Exception ex)
        //    {
        //        try
        //        {
        //            LogMessageFileAlt(ex.Message);
        //            LogMessageFileAlt(ex.StackTrace);
        //            LogMessageFileAlt(message);
        //        }
        //        catch { }
        //    }
        //}

        public static void LogMessageConsole(string message)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} - {1}", DateTime.Now, message));
//            Console.WriteLine(string.Format("{0} - {1}", DateTime.Now, message));
        }

        public static void LogMessageEvent(string message)
        {
            if (!EventLog.SourceExists(LOG_SOURCE))
                EventLog.CreateEventSource(LOG_SOURCE, LOG_NAME);

            byte[] data = System.Text.UTF8Encoding.Default.GetBytes(message);
            EventLog.WriteEntry(LOG_SOURCE, LOG_EVENT, EventLogEntryType.Information, 0, 0, data);
        }

        public static void LogMessageFile(string message)
        {
            string logFile = LOG_PATH + string.Format("mws-{0:yyyy-MM-dd}.log", DateTime.Now);
            FileStream fs = new FileStream(logFile, FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(string.Format("{0} - {1}", DateTime.Now, message));
            sw.Flush();
            sw.Close();
            fs.Close();
        }

        public static void LogMessageFileAlt(string message)
        {
            string logFile = LOG_PATH + string.Format("mws-alt-{0:yyyy-MM-dd}.log", DateTime.Now);
            FileStream fs = new FileStream(logFile, FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(string.Format("{0} - {1}", DateTime.Now, message));
            sw.Flush();
            sw.Close();
            fs.Close();
        }
    }
}