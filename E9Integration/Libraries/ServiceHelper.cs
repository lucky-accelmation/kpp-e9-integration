﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using ActiveUp.Net.Mail;
using System.Runtime.InteropServices;

namespace E9Integration.Libraries
{
    public class ServiceHelper
    {
        public static string GetConfig(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public static string GetDefaultConnectionString()
        {
            return GetConnectionString(GetConfig("DefaultSqlConnection"));
        }

        public static NameValueCollection GetMincomLogin(string connectionName)
        {
            var res = new NameValueCollection();
            string cn = null;
            if (connectionName == null)
            {
                cn = GetConfig("DefaultMincomConnectionAdmin");
            }
            else
            {
                cn = GetConfig(connectionName);
            }
            res.Add("CN", cn);
            string cfgs = GetConnectionString(cn);
            string[] configs = cfgs.Split(';');
            foreach (string cfg in configs)
            {
                string[] cfgi = cfg.Split('=');
                res.Add(cfgi[0], cfgi[1]);
            }

            return res;
        }

        public static string GetStreamData(Stream data)
        {
            var reader = new StreamReader(data);
            string datatx = reader.ReadToEnd();
            reader.Close();
            return datatx;
        }

        public static void SendReport(string text)
        {
            var message = new Message();
            message.Subject = GetConfig("MailSubject");
            message.From = new Address(GetConfig("MailFrom"));
            string[] rcps = GetConfig("MailAdmin").Split(',');
            foreach (string rcp in rcps)
            {
                message.To.Add(rcp);
            }
            message.BodyText.Text = text;
            int port = 0;
            if (!int.TryParse(GetConfig("MailSmtpPort"), out port))
            {
                throw new Exception("Invalid SMTP mail port");
            }
            if (GetConfig("MailSsl").Equals("yes"))
            {
                if (GetConfig("MailUsername").Equals(""))
                {
                    SmtpClient.SendSsl(message, GetConfig("MailSmtpServer"), port);
                }
                else
                {
                    SmtpClient.SendSsl(message, GetConfig("MailSmtpServer"), port, GetConfig("MailUsername"),
                                       GetConfig("MailPassword"), SaslMechanism.Login);
                }
            }
            else
            {
                if (GetConfig("MailUsername").Equals(""))
                {
                    SmtpClient.Send(message, GetConfig("MailSmtpServer"), port);
                }
                else
                {
                    SmtpClient.Send(message, GetConfig("MailSmtpServer"), port, GetConfig("MailUsername"),
                                    GetConfig("MailPassword"), SaslMechanism.Login);
                }
            }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct TokPriv1Luid
        {
            public int Count;
            public long Luid;
            public int Attr;
        }

        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GetCurrentProcess();

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr
        phtok);

        [DllImport("advapi32.dll", SetLastError = true)]
        internal static extern bool LookupPrivilegeValue(string host, string name,
        ref long pluid);

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall,
        ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

        [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool ExitWindowsEx(int flg, int rea);

        [DllImport("kernel32.dll")]
        internal static extern IntPtr CreateThread([In] ref SECURITY_ATTRIBUTES
           SecurityAttributes, uint StackSize, IntPtr StartFunction,
           IntPtr ThreadParameter, uint CreationFlags, out uint ThreadId);

        [DllImport("kernel32.dll")]
        internal static extern bool TerminateThread(IntPtr hThread, uint dwExitCode);

        [DllImport("kernel32.dll")]
        internal static extern bool GetExitCodeThread(IntPtr hThread, out uint lpExitCode);

        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        internal const int SE_PRIVILEGE_ENABLED = 0x00000002;
        internal const int TOKEN_QUERY = 0x00000008;
        internal const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;
        internal const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
        internal const int EWX_LOGOFF = 0x00000000;
        internal const int EWX_SHUTDOWN = 0x00000001;
        internal const int EWX_REBOOT = 0x00000002;
        internal const int EWX_FORCE = 0x00000004;
        internal const int EWX_POWEROFF = 0x00000008;
        internal const int EWX_FORCEIFHUNG = 0x00000010;

        public static void DoExitWin(int flg)
        {
            bool ok;
            TokPriv1Luid tp;
            IntPtr hproc = GetCurrentProcess();
            IntPtr htok = IntPtr.Zero;
            ok = OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
            tp.Count = 1;
            tp.Luid = 0;
            tp.Attr = SE_PRIVILEGE_ENABLED;
            ok = LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref tp.Luid);
            ok = AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
            ok = ExitWindowsEx(flg, 0);
        }
    }
}