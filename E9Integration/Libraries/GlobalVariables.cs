using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;

namespace E9Integration.Libraries
{
    public static class GlobalVariables
    {
        //HOLDS GLOBAL APPLICATION WIDE INFORMATIONS AND PARAMETERS

        private static Dictionary<SqlConnection, SqlTransaction> sqlTransactions = new Dictionary<SqlConnection, SqlTransaction>();

        public static Dictionary<SqlConnection, SqlTransaction> SqlTransactions
        {
            get { return GlobalVariables.sqlTransactions; }
            set { GlobalVariables.sqlTransactions = value; }
        }

        private static Dictionary<string, object> config = new Dictionary<string, object>();

        public static Dictionary<string, object> Config
        {
            get { return GlobalVariables.config; }
            set { GlobalVariables.config = value; }
        }

        private static Dictionary<string, object> objectCache = new Dictionary<string, object>();

        public static Dictionary<string, object> ObjectCache
        {
            get { return GlobalVariables.objectCache; }
            set { GlobalVariables.objectCache = value; }
        }

    }
}
