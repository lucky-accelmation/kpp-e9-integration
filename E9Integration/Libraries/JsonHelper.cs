﻿using System;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using E9Integration.Classes;
using Newtonsoft.Json.Linq;

namespace E9Integration.Libraries
{
    public class JsonHelper
    {
        public static dynamic GetJsonObject(string json)
        {
            //if (json == null || json.Trim().Length == 0)
            //{
            //    throw new Exception("Input parameter is empty");
            //}

            //return JsonConvert.DeserializeObject(json);

            JavaScriptSerializer jss = new JavaScriptSerializer();
            jss.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });

            dynamic jsonobj = jss.Deserialize(json, typeof(object)) as dynamic;
            return jsonobj;
        }

        public static string GetJsonString(object obj)
        {
//            return JsonConvert.SerializeObject(obj);

//            Modified by James
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string result = jss.Serialize(obj);
            return result;
        }

        public static string GetProp(Newtonsoft.Json.Linq.JObject obj, string propName)
        {
            string res = null;
            try
            {
                res = obj.GetValue(propName).ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ": property not found " + propName);
            }
            return res;
        }

        public static string[] GetArrayProp(Newtonsoft.Json.Linq.JObject obj, string propName)
        {
            string[] res = null;
            try
            {
//                res = obj.GetValue(propName).ToObject() as string[];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ": property not found " + propName);
            }
            return res;
        }




    }
}