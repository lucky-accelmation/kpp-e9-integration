﻿using E9Integration.Classes;
using EllipseWebServicesClient;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace E9Integration.Libraries
{
    public class E9Functions
    {
        #region variables
        private readonly ILog logger =
            LogManager.GetLogger(typeof(E9Functions));
        private static readonly ILog dumper =
            LogManager.GetLogger("dumpfields");

        private object threadid = 0; //as child thread id
        private object result = null; //as child thread result

        //private bool skipcheckscreen = false;
        private void loggerinfo(string message)
        {
            string stringid = threadid.ToString();
            if (stringid != null && stringid.Length > 8) stringid = stringid.Substring(0, 8);
            logger.InfoFormat("#{0}: {1}", stringid, message);
        }

        //string DefaultLogin = "DefaultMincomConnectionAdmin";

        public delegate string OperationDelegate(StructOperation op, MincomParameter mp);
        public class StructOperation
        {
            public string login;
            public bool overrideDistrict;
            public int priority;
            public OperationDelegate operation;
            public string serviceType;

            public StructOperation(OperationDelegate operation, string serviceType, string login = "Admin", bool overrideDistrict = false, int priority = 0)
            {
                this.operation = operation;
                this.login = login;
                this.overrideDistrict = overrideDistrict;
                this.priority = priority;
                this.serviceType = serviceType;
            }
            public string baseurl;
            public string username;
            public string password;
            public string position;
            public StructOperation Clone()
            {
                return new StructOperation(operation, serviceType, login, overrideDistrict, priority)
                {
                    baseurl = this.baseurl,
                    username = this.username,
                    password = this.password,
                    position = this.position,
                };
            }
        }

        #endregion variables

        #region public functions

        public string GetOverrideDistrict(string operation, string input)
        {
            string district = null;

            if (Operations.ContainsKey(operation) && Operations[operation].overrideDistrict)
            {
                try
                {
                    //JToken tok = JToken.Parse(input);
                    //district = tok["overrideDistrict"].Value<string>();
                    MincomParameter mp = new MincomParameter(input);
                    district = mp.GetString("overrideDistrict");
                }
                catch (Exception ex)
                {
                    logger.Info("Override district failed, using default district: " + ex.Message);
                    //                    throw ex;
                }
            }

            return district;
        }

        public bool IsValidOperation(string operation)
        {
            if (operation != null && !Operations.ContainsKey(operation))
            {
                return false;
            }
            return true;
        }

        public int GetOperationPriority(string operation)
        {
            if (Operations.ContainsKey(operation))
            {
                return Operations[operation].priority;
            }
            return 0;
        }

        //public string GetLoginJob(string operation)
        //{
        //    if (IsValidOperation(operation))
        //    {
        //        return Operations[operation].login;
        //    }
        //    return DefaultLogin;
        //}


        public string ProcessJob(object threadid, string operation, string input)
        {
            loggerinfo("System CultureInfo: " + Thread.CurrentThread.CurrentCulture.Name);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");

            string result = null;
            this.threadid = threadid;

            if (!IsValidOperation(operation))
            {
                throw new Exception("Invalid operation.");
            }

            if (Operations[operation].operation == null)
            {
                throw new Exception("Operation is not set.");
            }

            StructOperation op = null;
            try
            {
                op = job.GetServerCredentials(Operations[operation]);
                result = op.operation(op, new MincomParameter(input));
            }
            catch (Exception ex)
            {
                loggerinfo(string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace));
                //throw new Exception(ex.Message + " at " + screen + " Field " + field, ex);
                throw ex;
            }
            finally
            {
                if (op != null)
                {
                    job.UnlockCredential(op);
                }
            }

            return result;
        }
        #endregion public functions

        static Dictionary<string, StructOperation> Operations = null;
        JobProcess job = JobProcess.GetJobProcess();

        public E9Functions()
        {
            Operations = new Dictionary<string, StructOperation>()
            {
                //{"GetPurchaseOrder",new StructOperation(GetPurchaseOrder)},
                {"PreloadInvoice",new StructOperation(PreloadInvoice_screen,"screen","User")},
                {"LoadInvoice",new StructOperation(LoadInvoice_screen,"screen","User")},
                {"ApproveInvoice",new StructOperation(ApproveInvoice,"screen")},
                {"ApproveInvoiceMulti",new StructOperation(ApproveInvoice,"screen")}, //merged with ApproveInvoice
                {"UploadPayment",new StructOperation(UploadPayment,"screen","Admin",true)},
                {"GetVendorInfo",new StructOperation(GetVendorInfo_repo,"repo")},
                //{"GetReportText",new StructOperation(GetReportText)},
                //{"UploadUnbudget",new StructOperation(UploadUnbudget)},
                {"GetBankAccounts",new StructOperation(GetBankAccount_screen,"screen")},
                {"LoadNonOrderInvoice",new StructOperation(LoadNonOrderInvoice_proxy,"screen","User")},
                {"LoadNonOrderInvoiceSingle",new StructOperation(LoadNonOrderInvoiceSingle,"screen","User",false,1)},
                {"CancelInvoice",new StructOperation(CancelInvoice_screen,"screen")},
                //{"CreatePurchaseOrder",new StructOperation(CreatePurchaseOrder,"Creator")},
                //{"ReceivePurchaseOrder",new StructOperation(ReceivePurchaseOrder,"Creator")},
                {"UploadSundries",new StructOperation(UploadSundries,"screen","Admin",true)},
                {"UploadMiscCashReceipt",new StructOperation(UploadMiscCashReceipt,"screen","Admin",true)},
                {"CreatePurchaseRequisition",new StructOperation(CreatePurchaseRequisition_repo,"repo","AdminPR",true)},
                {"CancelPurchaseRequisition",new StructOperation(CancelPurchaseRequisition_repo,"repo","AdminPR",true)},
                //{"ApprovePurchaseRequisition",new StructOperation(ApprovePurchaseRequisition,"Admin")},
            };
        }

        public void thread()
        {
            StructOperation op = job.GetServerCredentials(Operations["CancelInvoice"]);
            Thread.Sleep(5000);
            job.UnlockCredential(op);

        }

        public bool testfunction()
        {
            string testcase = "";
            try
            {
                MincomParameter mp = null;
                StructOperation op = null;
                if (testcase == "mytest")
                {
                    int cnt = 10;
                    Thread[] th = new Thread[cnt];
                    for (int i = 0; i < cnt; i++)
                    {
                        th[i] = new Thread(new ThreadStart(thread));
                        th[i].Start();
                    }
                    for (int i = 0; i < cnt; i++)
                    {
                        th[i].Join();
                    }

                }
                if (testcase == "mytest1")
                {
                    mp = new MincomParameter("{\"districtCode\":\"MASS\",\"supplierNo\":\"000375\",\"invoiceNo\":\"2102001010\"}");
                    op = job.GetServerCredentials(Operations["CancelInvoice"]);

                    E9Screen mobj = GetService<E9Screen>(op, mp);

                    string[] splitids = splitids = new string[] { mp.GetString("invoiceNo") };
                    List<string> results = new List<string>();
                    for (int i = 0; i < splitids.Length; i++)
                    {
                        mobj.GoHome();
                        //            LoadNonOrderResult result = new LoadNonOrderResult();

                        mobj.ExecuteMSO("MSO262");
                        mobj.CheckForScreen("MSM262A", true);

                        mobj.SetFieldValue("OPTION1I", "1");
                        mobj.SetFieldValue("DSTRCT_CODE1I", mp.GetString("districtCode"));
                        mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
                        mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
                        mobj.ExecuteCommand("OK");

                        mobj.CheckForScreen("MSM262B", true);
                        Console.WriteLine(mobj.DumpFields());
                    }
                    mp = new MincomParameter("{\"districtCode\":\"TOPB\",\"supplierNo\":\"000833\",\"invoiceNo\":\"2012000709.T2\"}");
                    splitids = splitids = new string[] { mp.GetString("invoiceNo") };
                    results = new List<string>();
                    for (int i = 0; i < splitids.Length; i++)
                    {
                        mobj.GoHome();
                        //            LoadNonOrderResult result = new LoadNonOrderResult();

                        mobj.ExecuteMSO("MSO262");
                        mobj.CheckForScreen("MSM262A", true);

                        mobj.SetFieldValue("OPTION1I", "1");
                        mobj.SetFieldValue("DSTRCT_CODE1I", mp.GetString("districtCode"));
                        mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
                        mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
                        mobj.ExecuteCommand("OK");

                        mobj.CheckForScreen("MSM262B", true);
                        Console.WriteLine(mobj.DumpFields());
                    }

                }
                if (testcase == "approval")
                {
                    mp = new MincomParameter("{\"authBy\":\"KB11013\",\"supplierNo\":\"001054\",\"positionId\":\"KP2AC014\",\"district\":\"INDE\",\"invoiceNo\":\"2012000613\"}");
                    op = job.GetServerCredentials(Operations["ApproveInvoice"]);
                    Console.WriteLine(ApproveInvoiceItemsDo_repo(op, mp));
                }
                if (testcase == "getvendorinfo")
                {
                    mp = new MincomParameter("{\"supplierNo\":\"000948\",\"district\":\"PDRO\"}");
                    Console.WriteLine(GetVendorInfo_repo(Operations["GetVendorInfo"], mp));
                    Console.WriteLine(GetBankAccount_screen(Operations["GetBankAccount"], mp));
                }
                if (testcase == "purchaserequisition")
                {
                    mp = new MincomParameter("{\"requestby\":\"KB15009\",\"position\":\"\",\"requestbydate\":\"9/21/2021\",\"suggestedsupplier\":\"toko buku\"," +
                        "\"accountcode\":\"470020\",\"woproject\":\"C20CA01A\",\"indicator\":\"P\",\"equipref\":\"\",\"items\":[{\"qty\":\"1\",\"uom\":\"EACH\",\"currency\":\"IDR\"," +
                        "\"price\":\"10000\",\"description\":\"pencil\"},{\"qty\":\"1\",\"uom\":\"EACH\",\"currency\":\"IDR\",\"price\":\"15000\",\"description\":\"ballpoint\"}]}");
                    //mp = new MincomParameter("{\"requestby\":\"KB15009\",\"position\":\"\",\"requestbydate\":\"9/21/2021\",\"suggestedsupplier\":\"toko buku\"," +
                    //    "\"accountcode\":\"\",\"woproject\":\"\",\"indicator\":\"\",\"equipref\":\"\",\"items\":[{\"qty\":\"1\",\"uom\":\"EACH\",\"currency\":\"IDR\"," +
                    //    "\"price\":\"10000\",\"description\":\"pencil\",\"accountcode\":\"888888201712003\"},{\"qty\":\"1\",\"uom\":\"EACH\",\"currency\":\"IDR\",\"price\":\"15000\"," +
                    //    "\"description\":\"ballpoint\",\"accountcode\":\"888888201712003\"}]}");
                    //mp = new MincomParameter("{\"indicator\":\"P\",\"requestbydate\":\"09/21/2021\",\"items\":[{\"price\":\"1000.0\",\"uom\":\"EACH\",\"description\":\"123\"," +
                    //    "\"accountcode\":\"\",\"qty\":\"1\",\"currency\":\"IDR\"}],\"docNo\":\"PR-21-026\",\"suggestedsupplier\":\"0401inde\",\"woproject\":\"C20CC02H\"," +
                    //    "\"accountcode\":\"\",\"overrideDistrict\":\"INDE\",\"equipref\":\"\",\"requestby\":\"KB17009\"}");
                    string prno = CreatePurchaseRequisition_repo(Operations["CreatePurchaseRequisition"], mp);
                    Console.WriteLine(prno);

                    mp = new MincomParameter("{\"prno\":\"" + prno + "\",\"itemno\":\"\"}");
                    Console.WriteLine(CheckPurchaseRequisition_repo(Operations["CheckPurchaseRequisition"], mp));

                    mp = new MincomParameter("{\"prno\":\"" + prno + "\",\"itemno\":\"2\"}");
                    Console.WriteLine(CancelPurchaseRequisition_repo(Operations["CancelPurchaseRequisition"], mp));
                    mp = new MincomParameter("{\"prno\":\"" + prno + "\",\"itemno\":\"2\"}");
                    Console.WriteLine(CancelPurchaseRequisition_repo(Operations["CancelPurchaseRequisition"], mp));
                    mp = new MincomParameter("{\"prno\":\"" + prno + "\",\"itemno\":\"\"}");
                    Console.WriteLine(CancelPurchaseRequisition_repo(Operations["CancelPurchaseRequisition"], mp));
                }
                if (testcase == "invoice")
                {
                    //start 23-25 mar 21
                    mp = new MincomParameter("{\"invoiceDate\":\"04/06/21\",\"purchaseNo\":\"H47650\",\"invoiceReceiptDate\":\"10/06/21\",\"invoiceAmount\":\"821090\",\"invoiceNo\":\"2106001407\",\"dueDate\":\"10/07/21\",\"currency\":\"IDR\"}");
                    Console.WriteLine(PreloadInvoice_repo(Operations["PreloadInvoice"], mp));
                    //Console.WriteLine(PreloadInvoice_screen(mp));

                    //mp = new MincomParameter("{\"invoiceDate\":\"11/02/21\",\"taxOrgInvoiceNo\":\" \",\"invoiceReceiptDate\":\"12/02/21\",\"invoiceAmount\":\"5544000\",\"invoiceNo\":\"2102001465\",\"refDate\":\" \",\"refOrgInvoiceNo\":\"0087/INV/TBN/II/2021\",\"accountant\":\"KB11013\",\"currency\":\"IDR\",\"purchaseNo\":\"H44589\",\"taxInvoiceDate\":\" \",\"refNo\":\" \",\"taxInvoiceNo\":\"\",\"dueDate\":\"14/03/21\",\"invoiceCommentType\":\"I\"}");
                    //Console.WriteLine(LoadInvoice(mp));
                }

            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("***** ERROR *****");
                Console.Out.WriteLine(ex.Message);
                Console.Out.WriteLine("***** STACK *****");
                Console.Out.WriteLine(ex.StackTrace);
            }
            if (testcase != "")
            {
                Console.ReadLine();
                return true;
            }
            return false;
        }

        IFormatProvider parserFormat = new CultureInfo("id-ID");

        private T GetContext<T>(StructOperation op, MincomParameter mp)
        {
            string district = mp.IsExist("overrideDistrict") ? mp.GetString("overrideDistrict") : "KPHO";
            loggerinfo("context district " + district);
            T ctx = (T)typeof(T).GetConstructor(new Type[0]).Invoke(null);
            typeof(T).GetProperty("district").SetValue(ctx, district);
            typeof(T).GetProperty("position").SetValue(ctx, op.position);
            ClientConversation.authenticate(op.username, op.password);
            return ctx;
        }

        private T GetService<T>(StructOperation op, MincomParameter mp)
        {
            if (typeof(T).FullName == "E9Integration.Classes.E9Screen")
            {
                //T e9s = (T)typeof(T).GetConstructors()[0].Invoke(new object[] {
                //    GetContext<ScreenService.OperationContext>(op,mp), op.url
                //});
                //return e9s;

                return (T)(object)new E9Screen(GetContext<ScreenService.OperationContext>(op, mp), op.baseurl);
                //ScreenService.OperationContext context = new ScreenService.OperationContext();
                //context.district = "KPHO";
                //context.position = op.position;
                //ClientConversation.authenticate(op.username, op.password);
                //return (T)(object)new E9Screen(context, op.baseurl);
            }
            T svc = (T)typeof(T).GetConstructor(new Type[0]).Invoke(null);
            if (op.baseurl != null)
            {
                var prop = typeof(T).GetProperty("Url");
                string url = prop.GetValue(svc) as string;
                url = Regex.Replace(url, @"https?://.*?(?=/)", op.baseurl);
                prop.SetValue(svc, url);
            }
            int timeout = int.Parse(ServiceHelper.GetConfig("ServiceTimeout"));
            if (timeout > 0)
            {
                var prop = typeof(T).GetProperty("Timeout");
                prop.SetValue(svc, timeout * 1000);
                loggerinfo("repo timeout " + timeout);
            }

            return svc;
        }

        private string CheckInvoice_repo(StructOperation op, MincomParameter mp)
        {
            var ctx = GetContext<InvoiceService.OperationContext>(op, mp);
            var svc = GetService<InvoiceService.InvoiceService>(op, mp);

            var prm = new InvoiceService.InvoiceSearchParam()
            {
                districtCode = mp.GetString("postingDistrict"),
                supplierNumber = mp.GetString("supplierNo"),
                invoiceNumber = mp.GetString("invoiceNo"),
            };

            var res = svc.search(ctx, prm, null);

            loggerinfo("Found " + res.Length + " invoices");

            if (res.Length > 0)
            {
                var resread = svc.read(ctx, res[0].invoiceDTO);
                var inv = resread.invoiceDTO;
                loggerinfo(string.Format("{0} {1} {2} {3}",
                    inv.districtCode, inv.supplierNo, inv.invoiceNo, inv.externalInvoiceNo, inv.commentType));
                //loggerinfo(string.Format("{0} {1}", //comments not shown
                //    inv.commentType, inv.comments));
            }
            return null;
        }

        private string LoadNonOrderInvoice_proxy(StructOperation op, MincomParameter mp)
        {
            string res = null;
            //check invoice item, < 0 screenservice, >= 0 repo
            if (decimal.Parse(mp.GetString("amount")) >= 0)
            {
                loggerinfo("Execute LoadNonOrderInvoice_repo");
                res = LoadNonOrderInvoice_repo(op, mp);
            }
            else
            {
                loggerinfo("Execute LoadNonOrderInvoice_screen");
                res = LoadNonOrderInvoice_screen(op, mp);
            }
            CheckInvoice_repo(op, mp);

            return res;
        }

        /// <summary>
        /// MSO265 - E9S
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="authBy"></param>
        /// <param name="authPosId"></param>
        /// <param name="accountant"></param>
        /// <param name="receivedDate"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="postingDistrict"></param>
        /// <param name="costingDistrict"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="description"></param>
        /// <param name="coa"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="invoiceCommentType"></param>
        /// <param name="taxInvoiceDate"></param>
        /// <param name="taxInvoiceNo"></param>
        /// <param name="taxOrgInvoiceNo"></param>
        /// <param name="refDate"></param>
        /// <param name="refNo"></param>
        /// <param name="refOrgInvoiceNo"></param>
        /// <returns></returns>
        private string LoadNonOrderInvoice_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);

            mp.CheckRequired(new string[]
            {
                "authBy","accountant","receivedDate","invoiceDate","dueDate","postingDistrict","costingDistrict",
                "supplierNo","invoiceNo","description","coa","amount","currency","invoiceCommentType"
            }, false);

            mobj.GoHome();

            mobj.ExecuteMSO("MSO265");
            mobj.CheckForScreen("MSM265A", true);
            loggerinfo("MSM265A");

            //key data MSM265A

            mobj.SetFieldValue("ACCOUNTANT1I", mp.IsExist("accountant") ? mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"));
            mobj.SetFieldValue("AUTH_BY1I1", mp.IsExist("authBy") ? mp.GetString("authBy") : ServiceHelper.GetConfig("Authorize"));
            //as of request 3 oct 2014 switch dates
            //mobj.SetFieldValue("INV_DATE1I", invoiceDate);
            //mobj.SetFieldValue("INV_RCPT_DATE1I", receivedDate);
            mobj.SetFieldValue("INV_DATE1I", mp.GetString("receivedDate"));
            mobj.SetFieldValue("INV_RCPT_DATE1I", mp.GetString("invoiceDate"));
            mobj.SetFieldValue("DUE_DATE1I", mp.GetString("dueDate"));
            mobj.SetFieldValue("DSTRCT_CODE1I", mp.GetString("postingDistrict"));
            mobj.SetFieldValue("ACCT_DSTRCT1I1", mp.GetString("costingDistrict"));
            mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
            mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
            mobj.SetFieldValue("INV_ITEM_DESC1I1", mp.GetStringTruncate("description", 40));
            mobj.SetFieldValue("ACCOUNT1I1", mp.GetString("coa"));
            mobj.SetFieldValue("INV_AMT1I", mp.GetString("amount"));
            mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));
            mobj.SetFieldValue("INV_COMM_TYPE1I", mp.GetString("invoiceCommentType"));
            mobj.SetFieldValue("INV_ITEM_VALUE1I1", mp.GetString("amount"));

            mobj.ExecuteCommand("OK");
            if (mobj.IsIdle())
            {
                throw new Exception("Unexpected idle state.");
            }

            while (mobj.CheckForScreen("MSM004A", false))
            {
                loggerinfo("MSM004A");
                string trans = mobj.GetFieldValue("TRANS_DETAIL1I").Trim();
                trans = trans.Substring(trans.Length - 3);
                int pos = 0;
                int.TryParse(trans, out pos);

                string positionId = mp.IsExist("positionId") ? mp.GetString("positionId") : ServiceHelper.GetConfig("AuthorizePositionID"); //get from config
                if (!positionId.Equals(""))
                {
                    List<string> posids = new List<string>();
                    for (int i = 1; i <= 13; i++)
                    {
                        string sp = "POSITION_ID1I" + i;
                        string cposid = mobj.GetFieldValue(sp).ToUpper().Trim();
                        posids.Add(cposid);
                        if (cposid.Equals(positionId.ToUpper().Trim()))
                        {
                            mobj.SetFieldValue("ITEM_NUM1I", "" + i);
                            mobj.ExecuteCommand("OK");
                            break;
                        }
                        if (i == 13)
                        {
                            string err = "No Position ID match MIMS Position ID found on screen MSM004A\r\n";
                            err += "Available: " + string.Join(",", posids.ToArray()) + "\r\n";
                            err += "Needed: " + positionId;
                            throw new Exception(err);
                        }
                    }
                }
                else
                {
                    throw new Exception("No Position ID supplied");
                }
            }
            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetErrorMessage() + " - Active Field: " + mobj.GetActiveField());
            }

            string fillingno = null;
            while (mobj.GetErrorMessage().StartsWith("W2"))
            {
                if (mobj.GetActiveField().Equals("CURRENCY_TYPE1I"))
                {
                    mobj.SetFieldValue(mobj.GetActiveField(), mp.GetString("currency"));
                }
                if (mobj.CheckForScreen("MSM265A", false))
                {
                    fillingno = mobj.GetFieldValue("VCHR_NO1I");
                }
                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                if (mobj.CheckForScreen("MSM265A", false))
                {
                    fillingno = mobj.GetFieldValue("VCHR_NO1I");
                }
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.CheckForScreen("MSM096B", false))
            {
                loggerinfo("MSM096B");
                //key data MSM096B
                if (mobj.IsCommand("Edit Mode"))
                {
                    mobj.ExecuteCommand("Edit Mode");
                }
                else
                {
                    throw new Exception("E9I: MSM096B No Edit Mode");
                }
                mobj.CheckForScreen("MSM096B", true);

                mobj.SetFieldValue("EDITCODE2I1", "COPY");

                string taxInvoiceDate = mp.IsNotEmpty("taxInvoiceDate") ? mp.GetString("taxInvoiceDate").Trim() : "";
                string taxInvoiceNo = mp.IsNotEmpty("taxInvoiceNo") ? mp.GetString("taxInvoiceNo").Trim() : "";
                string taxOrgInvoiceNo = mp.IsNotEmpty("taxOrgInvoiceNo") ? mp.GetString("taxOrgInvoiceNo").Trim() : "";
                string refDate = mp.IsNotEmpty("refDate") ? mp.GetString("refDate").Trim() : "";
                string refNo = mp.IsNotEmpty("refNo") ? mp.GetString("refNo").Trim() : "";
                string refOrgInvoiceNo = mp.IsNotEmpty("refOrgInvoiceNo") ? mp.GetString("refOrgInvoiceNo").Trim() : "";
                if (!taxInvoiceDate.Equals("") || !taxInvoiceNo.Equals("") || !taxOrgInvoiceNo.Equals(""))
                {
                    //ppn
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPN");
                    mobj.ExecuteCommand("OK");

                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " " + taxInvoiceDate);
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " " + taxInvoiceNo);
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + taxOrgInvoiceNo);
                }
                else if (!refDate.Equals("") || !refNo.Equals("") || !refOrgInvoiceNo.Equals(""))
                {
                    //pph
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");
                    mobj.ExecuteCommand("OK");

                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " " + refDate);
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " " + refNo);
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + refOrgInvoiceNo);
                }
                else if (!refOrgInvoiceNo.Equals(""))
                {
                    //invoice
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");
                    mobj.ExecuteCommand("OK");

                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " " + refDate);
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " " + refNo);
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + refOrgInvoiceNo);
                }
                mobj.ExecuteCommand("OK");
                Console.WriteLine(mobj.GetFieldValue("STD_TEXT_C2I1"));
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Exit");
                }
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    //mobj.ExecuteCommand("Revert");
                    mobj.ExecuteCommand("Exit");
                }
            }
            object result = new
            {
                FillingNo = fillingno,
                Text = mobj.GetErrorMessage(),
            };
            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO265 - Invoice service
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="authBy"></param>
        /// <param name="authPosId"></param>
        /// <param name="accountant"></param>
        /// <param name="receivedDate"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="postingDistrict"></param>
        /// <param name="costingDistrict"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="description"></param>
        /// <param name="coa"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="invoiceCommentType"></param>
        /// <param name="taxInvoiceDate"></param>
        /// <param name="taxInvoiceNo"></param>
        /// <param name="taxOrgInvoiceNo"></param>
        /// <param name="refDate"></param>
        /// <param name="refNo"></param>
        /// <param name="refOrgInvoiceNo"></param>
        /// <returns></returns>
        private string LoadNonOrderInvoice_repo(StructOperation op, MincomParameter mp)
        {
            mp.CheckRequired(new string[]
            {
                "authBy","accountant","receivedDate","invoiceDate","dueDate","postingDistrict","costingDistrict",
                "supplierNo","invoiceNo","description","coa","amount","currency","invoiceCommentType"
            }, false);


            var ctx = GetContext<InvoiceService.OperationContext>(op, mp);
            var svc = GetService<InvoiceService.InvoiceService>(op, mp);

            string[] cppn = new string[]{
                "Tgl. Faktur Pajak: ",
                "No. Faktur Pajak: ",
                "No. Inv.(Khusus A/P): ",
            };
            string[] cpph = new string[]
            {
                "Tgl. Bukti Potong: ",
                "No. Bukti Potong: ",
                "No. Original Invoice: ",
                "Nilai DPP (Khusus A/R Invoice): ",
                "!!!(nilai DPP sesuai currency invoice)!!!",
            };
            string comment = "";
            if (mp.IsNotEmpty("taxInvoiceDate") || mp.IsNotEmpty("taxInvoiceNo") || mp.IsNotEmpty("taxOrgInvoiceNo"))
            {
                string[] cmt = cppn;
                cmt[0] = string.Format("{0,-79}", cmt[0] + mp.GetString("taxInvoiceDate"));
                cmt[1] = string.Format("{0,-79}", cmt[1] + mp.GetString("taxInvoiceNo"));
                cmt[2] = string.Format("{0,-79}", cmt[2] + mp.GetString("taxOrgInvoiceNo"));
                comment = string.Join("\r\n", cmt);
            }
            else if (mp.IsNotEmpty("refDate") || mp.IsNotEmpty("refNo") || mp.IsNotEmpty("refOrgInvoiceNo"))
            {
                string[] cmt = cpph;
                cmt[0] = string.Format("{0,-79}", cmt[0] + mp.GetString("refDate"));
                cmt[1] = string.Format("{0,-79}", cmt[1] + mp.GetString("refNo"));
                cmt[2] = string.Format("{0,-79}", cmt[2] + mp.GetString("refOrgInvoiceNo"));
                comment = string.Join("\r\n", cmt);
            }

            var dto = new InvoiceService.InvoiceDTO()
            {
                accountant = mp.IsNotEmpty("accountant") ? mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"),
                authorisedBy = mp.IsNotEmpty("authBy") ? mp.GetString("authBy") : ServiceHelper.GetConfig("Authorize"),
                invoiceDate = DateTime.Parse(mp.GetString("receivedDate"), parserFormat),
                invoiceDateSpecified = true,
                receiptDate = DateTime.Parse(mp.GetString("invoiceDate"), parserFormat),
                receiptDateSpecified = true,
                districtCode = mp.GetString("postingDistrict"),
                supplierNo = mp.GetString("supplierNo"),
                //invoiceNo = mp.GetStringTruncate("invoiceNo",10), //max 10 chars??
                externalInvoiceNo = mp.GetString("invoiceNo"),
                //invoiceAmount = decimal.Parse(mp.GetString("amount")),
                //invoiceAmountSpecified = true,
                currencyType = mp.GetString("currency"),
                commentType = mp.GetString("invoiceCommentType"),

                authorisedPosition = mp.IsNotEmpty("positionId") ? mp.GetString("positionId") : ServiceHelper.GetConfig("AuthorizePositionID"),
                comments = comment,
                invoiceItem = new InvoiceService.InvoiceItemDTO[]
                {
                    new InvoiceService.InvoiceItemDTO()
                    {
                        acctDistrictCode=mp.GetString("costingDistrict"),
                        invoiceItemDesc=mp.GetStringTruncate("description", 40),
                        account=mp.GetString("coa"),
                        itemAmountSpecified=true,
                        itemAmount=decimal.Parse(mp.GetString("amount")),
                    }
                }
            };
            try // due date may be empty
            {
                dto.dueDate = DateTime.Parse(mp.GetString("dueDate"), parserFormat);
                dto.dueDateSpecified = true;
            }
            catch { }

            var res = svc.create(ctx, dto);
            var msg = PrintErrors(res);
            if (res.errors.Length > 0)
            {
                throw new Exception(string.Join(";", msg));
            }
            object result = new
            {
                Text = res.informationalMessages[0].messageText,
                //FillingNo = res.invoiceDTO.voucherNo,
                FillingNo = res.invoiceDTO.invoiceNo,
            };
            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO260 *used
        /// match an order with invoice each item's action
        /// e9s
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <param name="accountant"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="invoiceReceiptDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmount"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        private string LoadInvoice_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);

            //all date field format dd/mm/yy
            string district = null;
            string supplier = null;
            string payment = null;
            string loaded = null;

            mp.CheckRequired(new string[]
            {
                "purchaseNo",
                "invoiceDate",
                "invoiceReceiptDate",
                "dueDate",
                "invoiceNo",
                "invoiceAmount",
                "currency"
            }, true);

            mobj.GoHome();

            mobj.ExecuteMSO("MSO260");
            if (!mobj.CheckForScreen("MSM260A", false))
            {
                throw new Exception("Screen not match: " + mobj.GetScreenName());
            }

            mobj.SetFieldValue("PO_NO1I", mp.GetStringTruncate("purchaseNo", 6));
            mobj.ExecuteCommand("OK");
            if ((mobj.GetErrorMessage().StartsWith("X2:0011 - INPUT REQUIRED") && mobj.GetActiveField().Equals("INV_NO1I")))
            {
                district = mobj.GetFieldValue("DSTRCT_CODE1I");
                supplier = mobj.GetFieldValue("SUPPLIER_NO1I");
                payment = mobj.GetFieldValue("PAYMENT_NAME1I");
            }
            else
            {
                throw new Exception("PreMSM260A: " + mobj.GetErrorMessage());
            }
            if (!mobj.GetFieldValue("CURRENCY_TYPE1I").Trim().Equals(mp.GetString("currency")))
            {
                throw new Exception("PreMSM260A: PO Currency is in " + mobj.GetFieldValue("CURRENCY_TYPE1I"));
            }

            mobj.SetFieldValue("ACCOUNTANT1I", mp.IsExist("accountant") ? mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"));
            mobj.SetFieldValue("DSP_ITEMS1I", "Y");
            //as of request 3 oct 2014 switch dates
            //mobj.SetFieldValue("INV_DATE1I", invoiceDate); //dd/mm/yyyy
            //mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceReceiptDate);
            mobj.SetFieldValue("INV_DATE1I", mp.GetString("invoiceReceiptDate")); //dd/mm/yyyy
            mobj.SetFieldValue("INV_RCPT_DATE1I", mp.GetString("invoiceDate"));
            mobj.SetFieldValue("DUE_DATE1I", mp.GetString("dueDate"));
            mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
            mobj.SetFieldValue("INV_AMT1I", mp.GetString("invoiceAmount"));
            if (mp.GetString("invoiceCommentType") != null)
            {
                if (mp.IsExist("items"))
                {
                    mobj.SetFieldValue("INV_COMM_TYPE1I", mp.GetString("invoiceCommentType"));
                }
                else
                {
                    loggerinfo("Items not found, three-way-matching, ignore invoice comment type");
                }
            }
            //mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency")); //not writable

            mobj.ExecuteCommand("OK");

            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception("MSM260A: " + mobj.GetErrorMessage() + " - Active field: " + mobj.GetActiveField());
            }
            if (mobj.GetErrorMessage().StartsWith("W2"))
            {
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM260A: " + mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            //          loggerinfo(mobj.DumpFields());

            if (mobj.CheckForScreen("MSM260B", false))
            {
                mobj.ExecuteCommand("Match with order");
            }

            //if (mobj.CheckForScreen("MSM260B", false)) //still here? something wrong
            //{
            //    loggerinfo("MSM260B Invoice: " + mobj.GetFieldValue("INV_AMT2I"));
            //    loggerinfo("MSM260B Receipt: " + mobj.GetFieldValue("RCPT_TOT_VAL2I"));
            //    loggerinfo("MSM260B Invoice Total: " + mobj.GetFieldValue("INV_TOT_VAL2I"));
            //    loggerinfo("MSM260B Variance: " + mobj.GetFieldValue("VARIANCE2I"));
            //}

            //if (!mobj.CheckForScreen("MSM26CA", false))
            //{
            //    throw new Exception("Screen not match: " + mobj.GetScreenName());
            //}

            mobj.CheckForScreen("MSM26CA", true);

            bool next = true;
            int counter = 0;
            List<object> resitems = new List<object>();
            while (next)
            {
                if (!mobj.CheckForScreen("MSM26CA", false))
                {
                    break;
                }
                loggerinfo("PAGE: " + mobj.GetFieldValue("SKLDISPLAY1I"));
                for (int i = 1; i <= 3; i++)
                {
                    object resitem = new
                    {
                        PoItemNo = mobj.GetFieldValue("PO_ITEM1I" + i),
                        Description = mobj.GetFieldValue("DESCRIPTION1I" + i),
                        Qty = mobj.GetFieldValue("QTY_NOT_INV_P1I" + i),
                        Price = mobj.GetFieldValue("GROSS_PRICE_P1I" + i),
                        Value = mobj.GetFieldValue("RCPT_ITM_VALUE1I" + i),
                        Tax = mobj.GetFieldValue("ITM_TAX_LIT1I" + i),
                        Discount = mobj.GetFieldValue("DISCOUNT1I" + i),
                        Uop = mobj.GetFieldValue("UOP1I" + i),
                        ItemType = mobj.GetFieldValue("PO_ITEM_TYPE1I" + i),
                        TaxCode = mobj.GetFieldValue("ATAX_CODE_ITM1I" + i),
                    };
                    resitems.Add(resitem);

                    string itemno = mobj.GetFieldValue("PO_ITEM1I" + i).Trim();
                    if (!itemno.Equals(""))
                    {
                        //loggerinfo("Item: " + mobj.GetFieldValue("PO_ITEM1I" + i));
                        //loggerinfo("Desc: " + mobj.GetFieldValue("DESCRIPTION1I" + i));
                        //loggerinfo("Qty: " + mobj.GetFieldValue("QTY_NOT_INV_P1I" + i));
                        //loggerinfo("Price: " + mobj.GetFieldValue("GROSS_PRICE_P1I" + i));

                        if (!mp.IsExist("items"))
                        {
                            //loggerinfo("Qty: " + mobj.GetFieldValue("QTY_NOT_INV_P1I" + i));
                            //loggerinfo("Price: " + mobj.GetFieldValue("GROSS_PRICE_P1I" + i));
                            mobj.SetFieldValue("ACTION1I" + i, "M");
                            loggerinfo("Items not found, Auto Set Item: " + mobj.GetFieldValue("PO_ITEM1I" + i).Trim() + " -Action: M");
                        }
                        else if (mp.GetArray("items").First(x => x.GetString("PoItemNo") == itemno) != null)
                        {
                            MincomParameter mpi = mp.GetArray("items").First(x => x.GetString("PoItemNo").Trim() == itemno);

                            if (mpi.GetString("Action").Trim().ToUpper().Equals("M"))
                            {
                                if (mpi.GetString("Qty").Trim().Equals("0"))
                                {
                                    loggerinfo("Qty is 0, ignore value");
                                }
                                else if (mpi.GetString("ItemType").ToUpper().Equals("SERVICE"))
                                {
                                    //"ItemType": "SERVICE",
                                    loggerinfo("ItemType SERVICE, Qty ignore value");
                                }
                                else
                                {
                                    mobj.SetFieldValue("QTY_NOT_INV_P1I" + i, mpi.GetString("Qty"));
                                }
                                mobj.SetFieldValue("ACTION1I" + i, mpi.GetString("Action"));
                                mobj.SetFieldValue("DISCOUNT1I" + i, mpi.GetString("Discount"));
                                loggerinfo(string.Format("Set Item: {0} - Qty: {1} - Discount: {2} - Action: {3}",
                                    itemno, mpi.GetString("Qty"), mpi.GetString("Discount"), mpi.GetString("Action")));
                            }
                            else
                            {
                                loggerinfo("Skip Item: " + itemno);
                            }
                        }
                        else
                        {
                            loggerinfo("Skip Item: " + itemno);
                        }
                    }
                    else
                    {
                        next = false;
                    }
                    counter++;
                }
                mobj.ExecuteCommand("OK");
                if (mobj.CheckForScreen("MSM26CA", false))
                {
                    loggerinfo("Invoice: " + mobj.GetFieldValue("INV_AMT1I"));
                    loggerinfo("Receipt: " + mobj.GetFieldValue("RCPT_TOT_VAL1I"));
                    loggerinfo("Invoice Total: " + mobj.GetFieldValue("INV_TOT_VAL1I"));
                    loggerinfo("Variance: " + mobj.GetFieldValue("VARIANCE1I"));
                    if (mobj.IsCommand("Confirm"))
                    {
                        loggerinfo("Confirm");
                        mobj.ExecuteCommand("Confirm");
                    }
                    if (next)
                    {
                        loggerinfo("Next");
                        if (!mobj.GetErrorMessage().Trim().Equals(""))
                        {
                            break;
                        }
                        //continue;
                    }
                }

                if (mobj.CheckForScreen("MSM096B", false))
                {
                    //key data MSM096B
                    mobj.ExecuteCommand("Edit Mode");
                    mobj.CheckForScreen("MSM096B", true);

                    mobj.SetFieldValue("EDITCODE2I1", "COPY");
                    //set ppn/pph, but neither, type PPH anyway
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");

                    mobj.ExecuteCommand("OK");
                    mobj.CheckForScreen("MSM096B", true);
                    //set detail
                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " ");
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " ");
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + mp.GetString("refOrgInvoiceNo"));

                    mobj.ExecuteCommand("OK");
                    if (mobj.CheckForScreen("MSM096B", false))
                    {
                        mobj.ExecuteCommand("Exit");
                    }
                    if (mobj.CheckForScreen("MSM096B", false))
                    {
                        mobj.ExecuteCommand("Revert");
                    }
                    break;
                }
            }

            if (mobj.CheckForScreen("MSM26CA", false))
            {
                mobj.ExecuteCommand("OK");
            }
            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetScreenName() + ": " + mobj.GetErrorMessage() + " - Active Field: " + mobj.GetActiveField());
            }
            if (mobj.GetErrorMessage().StartsWith("W2"))
            {
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM260CA: " + mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }
            if (mobj.GetErrorMessage().StartsWith("Invoice"))
            {
                loaded = mobj.GetErrorMessage();
            }

            mobj.ExecuteCommand("Home");
            object res = new
            {
                PurchaseNo = mp.GetString("purchaseNo"),
                InvoiceNo = mp.GetString("invoiceNo"),
                Message = loaded,
                District = district,
                Supplier = supplier,
                Payment = payment,
                Items = resitems.ToArray(),

            };

            return JsonHelper.GetJsonString(res);
        }

        private string PreloadInvoice_screentest(StructOperation op, MincomParameter mp)
        {
            var svc = GetService<ScreenService.ScreenService>(op, mp);
            var ctx = GetContext<ScreenService.OperationContext>(op, mp);

            svc.positionToMenu(ctx);
            var scr = svc.executeScreen(ctx, "MSO260");

            var fields = new ScreenService.ScreenNameValueDTO[] {
                new ScreenService.ScreenNameValueDTO(){fieldName = "PO_NO1I",
                    value = mp.GetString("purchaseNo"),},
            };
            var req = new ScreenService.ScreenSubmitRequestDTO()
            {
                screenFields = fields,
                screenKey = "1", //ok
            };
            scr = svc.submit(ctx, req);
            loggerinfo(scr.message);
            loggerinfo(scr.currentCursorFieldName);

            fields = new ScreenService.ScreenNameValueDTO[]
            {
                new ScreenService.ScreenNameValueDTO(){fieldName = "ACCOUNTANT1I",
                    value = mp.IsExist("accountant") ? mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"),},
                new ScreenService.ScreenNameValueDTO(){fieldName = "DSP_ITEMS1I",value = "Y"},
                new ScreenService.ScreenNameValueDTO(){fieldName = "INV_DATE1I",
                    value = mp.GetString("invoiceDate"),},
                new ScreenService.ScreenNameValueDTO(){fieldName = "INV_RCPT_DATE1I",
                    value = mp.GetString("invoiceReceiptDate"),},
                new ScreenService.ScreenNameValueDTO(){fieldName = "DUE_DATE1I",
                    value = mp.GetString("dueDate"),},
                new ScreenService.ScreenNameValueDTO(){fieldName = "INV_NO1I",
                    value = mp.GetString("invoiceNo"),},
                new ScreenService.ScreenNameValueDTO(){fieldName = "INV_AMT1I",
                    value = mp.GetString("invoiceAmount"),},
            };
            req = new ScreenService.ScreenSubmitRequestDTO()
            {
                screenFields = fields,
                screenKey = "1", //ok
            };
            scr = svc.submit(ctx, req);
            loggerinfo(scr.message);
            loggerinfo(scr.currentCursorFieldName);

            req = new ScreenService.ScreenSubmitRequestDTO()
            {
                screenFields = fields,
                screenKey = "1", //confirm
            };
            scr = svc.submit(ctx, req);
            loggerinfo(scr.message);
            loggerinfo(scr.currentCursorFieldName);

            loggerinfo(scr.mapName);

            var fieldslist = new List<ScreenService.ScreenNameValueDTO>();
            for (int i = 1; i <= 10; i++)
            {
                if (scr.screenFields.Where(f => f.fieldName == "RECEIPT_DATE2I" + i).First().value.Trim() != "")
                {
                    fieldslist.Add(new ScreenService.ScreenNameValueDTO()
                    {
                        fieldName = "ACTN2I" + i,
                        value = "M",
                    });
                }
            }
            req = new ScreenService.ScreenSubmitRequestDTO()
            {
                screenFields = fieldslist.ToArray(),
                screenKey = "2", //match with order
            };
            scr = svc.submit(ctx, req);
            loggerinfo(scr.message);
            loggerinfo(scr.currentCursorFieldName);

            return null;

        }

        /// <summary>
        /// MSO260 Preload invoice for returning item details - screenservice
        /// same as loadinvoice, except this doesnt match each invoice item's action
        /// E9Screen
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <param name="accountant"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="invoiceReceiptDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmount"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        private string PreloadInvoice_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);

            //    result = PreloadInvoiceDo(mobj, jsd.purchaseNo, jsd.accountant, jsd.invoiceDate, jsd.invoiceReceiptDate, jsd.dueDate,
            //        jsd.invoiceNo, jsd.invoiceAmount, jsd.currency);
            //all date field format dd/mm/yy
            mp.CheckRequired(new string[]
            {
                "purchaseNo",
                "invoiceDate",
                "invoiceReceiptDate",
                "dueDate",
                "invoiceNo",
                "invoiceAmount",
                "currency"
            }, true);

            mobj.GoHome();
            mobj.ExecuteMSO("MSO260");
            if (!mobj.CheckForScreen("MSM260A", false))
            {
                throw new Exception("Screen not match: " + mobj.GetScreenName());
            }
            mobj.SetFieldValue("PO_NO1I", mp.GetString("purchaseNo"));
            mobj.ExecuteCommand("OK");
            if (mobj.GetErrorMessage().StartsWith("X2:0011 - INPUT REQUIRED") && mobj.GetActiveField().Equals("INV_NO1I"))
            {
                //expected
            }
            else
            {
                throw new Exception("PreMSM260A: " + mobj.GetErrorMessage() + " Field " + mobj.GetActiveField());
            }
            if (!mobj.GetFieldValue("CURRENCY_TYPE1I").Trim().Equals(mp.GetString("currency")))
            {
                throw new Exception("PreMSM260A: PO Currency is in " + mobj.GetFieldValue("CURRENCY_TYPE1I"));
            }

            mobj.SetFieldValue("ACCOUNTANT1I", mp.IsExist("accountant") ? mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"));
            mobj.SetFieldValue("DSP_ITEMS1I", "Y");
            mobj.SetFieldValue("INV_DATE1I", mp.GetString("invoiceDate")); //dd/mm/yyyy
            mobj.SetFieldValue("INV_RCPT_DATE1I", mp.GetString("invoiceReceiptDate"));
            mobj.SetFieldValue("DUE_DATE1I", mp.GetString("dueDate"));
            mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
            mobj.SetFieldValue("INV_AMT1I", mp.GetString("invoiceAmount"));
            //mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));

            mobj.ExecuteCommand("OK");

            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception("MSM260A: " + mobj.GetErrorMessage() + " - Active field: " + mobj.GetActiveField());
            }
            if (mobj.GetErrorMessage().StartsWith("W2"))
            {
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM260A: " + mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.CheckForScreen("MSM260B", false))
            {
                mobj.ExecuteCommand("Match with order");
            }

            //          loggerinfo(mobj.DumpFields());
            //if (!mobj.CheckForScreen("MSM26CA", false))
            //{
            //    throw new Exception("Screen not match: " + mobj.GetScreenName());
            //}
            mobj.CheckForScreen("MSM26CA", true);

            List<object> items = new List<object>();
            bool next = true;
            while (next)
            {
                for (int i = 1; i <= 3; i++)
                {
                    if (!mobj.GetFieldValue("PO_ITEM1I" + i).Trim().Equals(""))
                    {
                        object item = new
                        {
                            PoItemNo = mobj.GetFieldValue("PO_ITEM1I" + i),
                            Description = mobj.GetFieldValue("DESCRIPTION1I" + i),
                            Qty = mobj.GetFieldValue("QTY_NOT_INV_P1I" + i),
                            Price = mobj.GetFieldValue("GROSS_PRICE_P1I" + i),
                            Value = mobj.GetFieldValue("RCPT_ITM_VALUE1I" + i),
                            Tax = mobj.GetFieldValue("ITM_TAX_LIT1I" + i),
                            Discount = mobj.GetFieldValue("DISCOUNT1I" + i),
                            Uop = mobj.GetFieldValue("UOP1I" + i),
                            ItemType = mobj.GetFieldValue("PO_ITEM_TYPE1I" + i),
                            TaxCode = mobj.GetFieldValue("ATAX_CODE_ITM1I" + i),
                        };
                        items.Add(item);
                    }
                    else
                    {
                        next = false;
                    }
                }
                if (next)
                {
                    mobj.ExecuteCommand("OK");
                    if (!mobj.GetErrorMessage().Trim().Equals(""))
                    {
                        break;
                    }
                }
            }
            object result = new
            {
                Items = items.ToArray()
            };
            mobj.GoHome();

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO260 Preload invoice for returning item details
        /// same as loadinvoice, except this doesnt match each invoice item's action
        /// InvoiceService - not working, must supply items data
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <param name="accountant"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="invoiceReceiptDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmount"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        private string PreloadInvoice_repo(StructOperation op, MincomParameter mp)
        {
            //    result = PreloadInvoiceDo(mobj, jsd.purchaseNo, jsd.accountant, jsd.invoiceDate, jsd.invoiceReceiptDate, jsd.dueDate,
            //        jsd.invoiceNo, jsd.invoiceAmount, jsd.currency);

            //string district = mp.IsExist("district") ? mp.GetString("district") : "KPHO";
            var ctx = GetContext<InvoiceService.OperationContext>(op, mp);
            var svc = GetService<InvoiceService.InvoiceService>(op, mp);
            //svc.Url = "http://localhost:9999/test";

            var dto = new InvoiceService.InvoiceDTO()
            {
                poNo = mp.GetString("purchaseNo"),
                accountant = mp.GetString("accountant"),
                invoiceDateSpecified = true,
                invoiceDate = DateTime.Parse(mp.GetString("invoiceDate"), parserFormat),
                receiptDateSpecified = true,
                receiptDate = DateTime.Parse(mp.GetString("invoiceReceiptDate"), parserFormat),
                dueDateSpecified = true,
                dueDate = DateTime.Parse(mp.GetString("dueDate"), parserFormat),
                invoiceNo = mp.GetString("invoiceNo"),
                invoiceAmountSpecified = true,
                invoiceAmount = decimal.Parse(mp.GetString("invoiceAmount")),
                currencyType = mp.GetString("currency"),
                noOfItems = "1"
            };
            var res = svc.create(ctx, dto);
            PrintErrors(res);


            return null;
        }

        private void ResetDistrict(StructOperation op, MincomParameter mp)
        {
            //workaround bug e9: set district from context
            E9Screen mobj = GetService<E9Screen>(op, mp);
            mobj.GoHome();
            mobj.ExecuteMSO("MSO262");
            mobj.SetFieldValue("OPTION1I", "1");
            mobj.SetFieldValue("DSTRCT_CODE1I", mobj.ctx.district);
            mobj.ExecuteCommand("OK");
        }

        /// <summary>
        /// MSO262 - Cancel Invoice
        /// screen service
        /// </summary>
        /// <param name="mp"></param>
        /// <returns></returns>
        private string CancelInvoice_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);

            JobSplit[] splitids = job.GetJobSplit("LoadNonOrderInvoice", (dynamic)mp.GetString("invoiceNo"));
            if (splitids == null)
            {
                splitids = new JobSplit[] { new JobSplit(mp.GetString("invoiceNo"), "0") };
            }
            List<string> results = new List<string>();
            for (int i = 0; i < splitids.Length; i++)
            {
                mobj.GoHome();
                //            LoadNonOrderResult result = new LoadNonOrderResult();

                mobj.ExecuteMSO("MSO262");
                mobj.CheckForScreen("MSM262A", true);

                mobj.SetFieldValue("OPTION1I", "1");
                mobj.SetFieldValue("DSTRCT_CODE1I", mp.GetString("districtCode"));
                mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
                mobj.SetFieldValue("INV_NO1I", splitids[i].id);
                mobj.ExecuteCommand("OK");

                mobj.CheckForScreen("MSM262B", true);
                if (!mobj.IsFieldProtected("REMOVE_INV2I"))
                {
                    mobj.SetFieldValue("REMOVE_INV2I", "Y");
                }
                else
                {
                    loggerinfo("Field REMOVE_INV2I read-only, not set");
                }
                mobj.SetFieldValue("CONFIRM2I", "Y");
                mobj.ExecuteCommand("OK");

                string result = "";
                if (!mobj.CheckForScreen("MSM262A", false))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                else
                {
                    result = mobj.GetErrorMessage().Trim();
                    if (result.EndsWith("CANCELLED & REMOVED off file")
                        || result.EndsWith("CANCELLED"))
                    {
                        result = "Invoice successfully cancelled: " + mp.GetString("invoiceNo");
                        loggerinfo(result);
                    }
                    else
                    {
                        throw new Exception(result);
                    }
                }
                results.Add(result);
            }

            return string.Join(";", results.ToArray());
        }

        /// <summary>
        /// MSO262 - Cancel Invoice
        /// repo service ** not working - for Load invoice with po? only for invoice loaded with repo?
        /// </summary>
        /// <param name="mp"></param>
        /// <returns></returns>
        private string CancelInvoice_repo(StructOperation op, MincomParameter mp)
        {
            string result = null;
            var ctx = GetContext<InvoiceService.OperationContext>(op, mp);
            var svc = GetService<InvoiceService.InvoiceService>(op, mp);

            JobSplit[] splitids = job.GetJobSplit("LoadNonOrderInvoice", (dynamic)mp.GetString("invoiceNo"));
            if (splitids == null)
            {
                splitids = new JobSplit[] { new JobSplit(mp.GetString("invoiceNo"), "0") };
            }
            for (int i = 0; i < splitids.Length; i++)
            {
                //todo: invoice should searched first svc.search() otherwise it cant find externalinvoiceno
                var prm = new InvoiceService.InvoiceSearchParam()
                {
                    districtCode = mp.GetString("districtCode"),
                    supplierNumber = mp.GetString("supplierNo"),
                    invoiceNumber = splitids[i].id,
                };
                var ressrc = svc.search(ctx, prm, null);

                if (ressrc.Length <= 0)
                {
                    throw new Exception("Invoice not found: " + splitids[i].id);
                }
                var res = svc.delete(ctx, ressrc[0].invoiceDTO);
                if (res.errors.Length > 0)
                {
                    throw new Exception("E9: " + res.errors[0].messageText
                        + " invoice no: " + splitids[i]);
                }
                result = "Invoice successfully cancelled: " + splitids[i];
            }

            return result;
        }

        /// <summary>
        /// MSO201 - SupplierBusinessService
        /// </summary>
        /// <param name="supplierNo"></param>
        /// <param name="district"></param>
        /// <returns></returns>
        private string GetVendorInfo_repo(StructOperation op, MincomParameter mp)
        {
            var ctx = GetContext<SupplierBusinessService.OperationContext>(op, mp);
            var svc = GetService<SupplierBusinessService.SupplierBusinessService>(op, mp);
            //svc.Url = "http://localhost:9999/test";
            //var dto = new SupplierBusinessService.SupplierBusinessServiceReadRequestDTO()
            //{
            //    supplierNo = mp.GetString("supplierNo"),
            //    //districtCode = "KPHO"
            //};
            //var res = svc.read(ctx, dto);

            var dto = new SupplierBusinessService.SupplierBusinessServiceRetrieveRequestDTO()
            {
                searchMethod = "E",
                supplierNo = mp.GetString("supplierNo"),
                districtCode = mp.GetString("district"),
            };
            var dtorr = new SupplierBusinessService.SupplierBusinessServiceRetrieveRequiredAttributesDTO()
            {
                returnAccountNumber = true
            };
            var resarr = svc.retrieve(ctx, dto, dtorr, "");
            if (resarr.replyElements.Length == 0)
            {
                throw new Exception("E9: supplier info not found");
            }
            var res = resarr.replyElements[0];

            JObject ret = JObject.FromObject(new
            {
                Name = res.supplierName,
                Status = res.supplierStatus,
                StatusLit = res.supplierStatusDescription,
                DistrictName = res.districtName,
                DaysPayment = ((int)res.noDaysToPay).ToString(),
                Statement = res.invoiceStatementInd,
                StatementLit = res.invoiceStatementIndDescription
            });

            return ret.ToString(Formatting.None);
        }

        private string DumpObject(object obj, bool includeEmpty)
        {
            Type type = obj.GetType();
            StringBuilder sb = new StringBuilder();
            sb.Append("---Dump Object---\r\n");
            sb.Append("Object\t: ").Append(type.FullName).Append("\r\n");
            List<string> props = new List<string>();
            foreach (PropertyInfo prop in type.GetProperties())
            {
                object val = prop.GetValue(obj);
                if (includeEmpty)
                {
                    props.Add(prop.Name + "\t: " + val == null ? "(null)" : Convert.ToString(val));
                }
                else if (val != null)
                {
                    string str = Convert.ToString(val);
                    if (!str.Trim().Equals(""))
                    {
                        props.Add(prop.Name + "\t: " + Convert.ToString(val));
                    }
                }
            }
            sb.Append(string.Join("\r\n", props.ToArray()));
            sb.Append("\r\n---End Dump---");
            return sb.ToString();
        }

        /// <summary>
        /// create pr - EnvoyGatherService
        /// </summary>
        /// <param name="requestby"></param>
        /// <param name="position"></param>
        /// <param name="requestbydate"></param>
        /// <param name="suggestedsupplier"></param>
        /// <param name="accountcode">left empty to input account code per item</param>
        /// <param name="woproject"></param>
        /// <param name="indicator"></param>
        /// <param name="equipref"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        ///        private string CreatePurchaseRequisition(MincomObject mobj, string requestby, string position, string requestbydate, string suggestedsupplier, string accountcode, string woproject, string indicator, string equipref, PurchaseRequisitionItem[] items)
        private string CreatePurchaseRequisition_repo(StructOperation op, MincomParameter mp)
        {
            mp.CheckRequired(new string[] {
                "docNo","warehouse","deliveryinstruction","requestbydate","requestby"
            }, true);

            //check db for existing PR number
            List<SqlParameter> prm = new List<SqlParameter>();
            prm.Add(new SqlParameter("prno", mp.GetString("docNo")));
            DataTable dt = job.QueryDB("SELECT DSTRCT_CODE,PREQ_NO FROM [dbo].[MSF230] WHERE PR_DOCUMENTUM=@prno", prm);
            if (dt.Rows.Count > 0)
            {
                if (!dt.Rows[0]["DSTRCT_CODE"].Equals(mp.GetString("overrideDistrict")))
                {
                    loggerinfo(string.Format("Check DB district mismatch, request {} got {}",
                        mp.GetString("overrideDistrict"), dt.Rows[0]["DSTRCT_CODE"]));
                }
                return dt.Rows[0]["PREQ_NO"].ToString();
            }

            //string district = mp.IsExist("overrideDistrict") ? mp.GetString("overrideDistrict") : "KPHO";
            var ctx = GetContext<EnvoyGatherService.OperationContext>(op, mp);
            ctx.trace = true;
            ctx.traceSpecified = true;
            string district = ctx.district;
            //loggerinfo("context district createpurchaserequisition: " + ctx.district);

            //workaround bug e9: set district by context
            ResetDistrict(op, mp);

            var svc = GetService<EnvoyGatherService.EnvoyGatherService>(op, mp);
            var dto = new EnvoyGatherService.GatheringDTO();
            var res = svc.readDefault(ctx, dto);

            DateTime requestByDate = DateTime.Parse(mp.GetString("requestbydate"), parserFormat);
            if (requestByDate.CompareTo(DateTime.Today) < 0)
            {
                loggerinfo("Required by date must be greater than or equal curent date -> fix");
                requestByDate = DateTime.Today;
            }

            var dtomod = res.gatheringDTO;
            dtomod.categoryManagedFlagSpecified = true;
            dtomod.categoryManagedFlag = false;
            dtomod.districtCode = district;
            dtomod.issueHdrCostingDefaultedSpecified = true;
            dtomod.issueHdrCostingDefaulted = false;
            dtomod.issueItemsExistSpecified = true;
            dtomod.issueItemsExist = false;
            //dtomod.purchaseWarehouseId = mp.GetString("warehouse");
            dtomod.originalWarehouseId = mp.GetString("warehouse");
            dtomod.priorityCode = mp.GetString("priority");
            dtomod.purchDeliveryInstruction = mp.GetString("docNo") + "; " + mp.GetStringTruncate("deliveryinstruction", 48);
            dtomod.purchDeliveryLocation = mp.GetString("deliverylocation");
            dtomod.purchPriorityCode = mp.GetString("priority");
            dtomod.purchaseHdrCostingDefaultedSpecified = true;
            dtomod.purchaseHdrCostingDefaulted = false;
            dtomod.purchaseItemsExistSpecified = true;
            dtomod.purchaseItemsExist = false;
            dtomod.purchaseRequiredByDateSpecified = true;
            dtomod.purchaseRequiredByDate = requestByDate;
            dtomod.requestedBy = mp.GetString("requestby");
            //dtomod.requestedByName = "MAYA REGINA SEPTIANA,";
            dtomod.requestedByPosition = mp.GetString("position");
            dtomod.requiredByDateSpecified = true;
            dtomod.requiredByDate = requestByDate;
            dtomod.deliveryLocation = mp.GetString("deliverylocation");
            dtomod.gatheringId = res.gatheringDTO.gatheringId;
            dtomod.gatheringType = "geo.shoppingcart";
            dtomod.headerDistrictCode = district;
            dtomod.isItemsExistSpecified = true;
            dtomod.isItemsExist = false;
            dtomod.isPartIssueAcceptableSpecified = true;
            dtomod.isPartIssueAcceptable = false;
            dtomod.issueCostCodeA = mp.GetString("accountcode");
            dtomod.issueDistrictA = district;
            dtomod.issueProjectA = mp.GetString("project");
            dtomod.issueWorkOrderA = mp.GetString("workorder");
            //dtomod.issuePercentageASpecified = true;
            //dtomod.issuePercentageA = 100.00M;
            dtomod.matGroupCode = mp.GetString("materialgroupcode");
            dtomod.protectedIndSpecified = true;
            dtomod.protectedInd = false;
            dtomod.purchCostCodeA = mp.GetString("accountcode");
            dtomod.purchProjectA = mp.GetString("project");
            dtomod.purchWorkOrderA = mp.GetString("workorder");
            //dtomod.purchPercentageASpecified = true;
            //dtomod.purchPercentageA = 100.00M;
            dtomod.requestedByProtectSpecified = true;
            dtomod.requestedByProtect = false;
            dtomod.suggestedSupplier = mp.GetString("suggestedsupplier");
            dtomod.purchasingOfficer = mp.GetString("purchasingofficer");
            if (mp.GetString("accountcode") == "") // costing per item
            {
                MincomParameter mpi = mp.Get("items[0]");// get costing from first item
                dtomod.purchCostCodeA = mpi.GetString("accountcode");
                //dtomod.costCentreA = mpi.GetString("accountcode");
                dtomod.issueCostCodeA = mpi.GetString("accountcode");
            }
            dumper.Debug(DumpObject(dtomod, false));
            var resmod = svc.modify(ctx, dtomod);
            string[] messages = PrintErrors(resmod);
            if (messages.Length > 0)
            {
                throw new Exception("E9: " + string.Join(";", messages));
            }

            List<EnvoyGatherService.GatherableDTO> items = new List<EnvoyGatherService.GatherableDTO>();
            for (int i = 0; i < mp.Count("items"); i++)
            {
                MincomParameter mpi = mp.Get("items[" + i + "]");
                //string desc = mpi.GetString("description");
                //int chunk = 40;
                //string[] descarr = Enumerable.Range(0, (int)Math.Ceiling(((decimal)desc.Length / chunk))).Select(ix => desc.Substring((ix * chunk) < desc.Length ? (ix * chunk) : desc.Length, (ix * chunk + chunk) < desc.Length ? 40 : desc.Length - (ix * chunk))).ToArray();
                string[] descriptions = mpi.GetStringChunk("description", 40, 4);

                EnvoyGatherService.GatherableDTO item = new EnvoyGatherService.GatherableDTO()
                {
                    isStockedInDifferentWarehouseFlagSpecified = true,
                    isStockedInDifferentWarehouseFlag = false,
                    repairableItemSpecified = true,
                    repairableItem = false,
                    catMgtFlagSpecified = true,
                    catMgtFlag = false,
                    createRepairRequestSpecified = true,
                    createRepairRequest = false,
                    currencyType = mpi.GetString("currency"),
                    descriptionCatalogueItem = mpi.GetStringTruncate("description", 160),
                    disableRepairRequestFlagSpecified = true,
                    disableRepairRequestFlag = true,
                    districtCode = district,
                    estimatePriceSpecified = true,
                    estimatePrice = decimal.Parse(mpi.GetString("price")),
                    gatherableType = "PurchaseRequisitionItem",
                    gatheringId = resmod.gatheringDTO.gatheringId.Trim(),
                    isCalculateTotalPriceSpecified = true,
                    isCalculateTotalPrice = false,
                    isPartNumberMatchesMultipleStockCodeSpecified = true,
                    isPartNumberMatchesMultipleStockCode = false,
                    isPartNumberMatchesSingleStockCodeSpecified = true,
                    isPartNumberMatchesSingleStockCode = false,
                    isPersistItemsSpecified = true,
                    isPersistItems = true,
                    isStockCodeOnMultipleWarehouseFlagSpecified = true,
                    isStockCodeOnMultipleWarehouseFlag = false,
                    itemType = mpi.GetString("itemtype"), //D GOODS, V SERVICE
                    partIssueSpecified = true,
                    partIssue = false,
                    purchaseItemDescriptionA = descriptions[0],
                    purchaseItemDescriptionB = descriptions[1],
                    purchaseItemDescriptionC = descriptions[2],
                    purchaseItemDescriptionD = descriptions[3],
                    quantitySpecified = true,
                    quantity = mpi.GetString("itemtype") == "D" ? decimal.Parse(mpi.GetString("qty")) : 0,
                    showItemCostingSpecified = true,
                    showItemCosting = false,
                    totalPriceSpecified = true,
                    totalPrice = decimal.Parse(mpi.GetString("qty")) * decimal.Parse(mpi.GetString("price")),
                    unitOfMeasure = mpi.GetString("uom"),
                    //warehouseCode = mp.GetString("warehouse"),
                };
                if (mp.GetString("accountcode") == "") // costing per item
                {
                    item.costDistrictA = district;
                    item.costCentreA = mpi.GetString("accountcode");
                    //item.equipmentReferenceA = mpi.GetString("equipref");
                    item.allocationPercentageA = 100;
                }
                items.Add(item);
                dumper.Debug(DumpObject(item, false));
            }
            var dtoitem = items.ToArray();
            var resitem = svc.multipleCreateItem(ctx, dtoitem);
            messages = PrintErrors(resitem);

            dumper.Debug(DumpObject(resmod.gatheringDTO, false));
            //force field requestedbyposition
            resmod.gatheringDTO.requestedByPosition = dtomod.requestedByPosition;
            //ctx.returnWarnings = true;
            //ctx.returnWarningsSpecified = true;
            var rescommit = svc.preCommit(ctx, resmod.gatheringDTO);
            messages = messages.Concat(PrintErrors(rescommit)).ToArray();

            if (rescommit.informationalMessages.Length > 0)
            {
                Match m = Regex.Match(rescommit.informationalMessages[0].messageText,
                    @"Purchase requisition (?<prno>\d{6}) has been saved and finalised");
                if (m.Success)
                {
                    return m.Groups["prno"].Value;
                }
            }
            throw new Exception("E9: " + string.Join(";", messages));
        }

        private string[] PrintErrors(object res)
        {
            List<string> messages = new List<string>();
            if (res.GetType().IsArray)
            {
                object obj = ((Array)res).GetValue(0);
                return PrintErrors(obj);
            }
            Array err = (Array)res.GetType().GetProperty("errors").GetValue(res);
            for (int i = 0; i < err.Length; i++)
            {
                object elm = err.GetValue(i);
                messages.Add(string.Format("Err: ID {0} Instance {1} Text {2} Field {3} {4}",
                    elm.GetType().GetProperty("messageId").GetValue(elm),
                    elm.GetType().GetProperty("messageInstance").GetValue(elm),
                    elm.GetType().GetProperty("messageText").GetValue(elm),
                    elm.GetType().GetProperty("fieldId").GetValue(elm),
                    elm.GetType().GetProperty("fieldIndex").GetValue(elm)));
            }
            Array info = (Array)res.GetType().GetProperty("informationalMessages").GetValue(res);
            for (int i = 0; i < info.Length; i++)
            {
                object elm = info.GetValue(i);
                messages.Add(string.Format("Info: {0} Field: {1}",
                    elm.GetType().GetProperty("messageText").GetValue(elm),
                    elm.GetType().GetProperty("fieldId").GetValue(elm)));
            }
            Console.WriteLine(string.Join("\r\n", messages.ToArray()));
            return messages.ToArray();
        }

        /// <summary>
        /// create pr - requisition service - deprecated - no currency support
        /// </summary>
        /// <param name="requestby"></param>
        /// <param name="position"></param>
        /// <param name="requestbydate"></param>
        /// <param name="suggestedsupplier"></param>
        /// <param name="accountcode">left empty to input account code per item</param>
        /// <param name="woproject"></param>
        /// <param name="indicator"></param>
        /// <param name="equipref"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        ///        private string CreatePurchaseRequisition(MincomObject mobj, string requestby, string position, string requestbydate, string suggestedsupplier, string accountcode, string woproject, string indicator, string equipref, PurchaseRequisitionItem[] items)
        private string CreatePurchaseRequisition_dep(StructOperation op, MincomParameter mp)
        {
            string district = mp.IsExist("overrideDistrict") ? mp.GetString("overrideDistrict") : "KPHO";
            var ctx = GetContext<RequisitionService.OperationContext>(op, mp);
            var svc = GetService<RequisitionService.RequisitionService>(op, mp);
            //svc.Url = "http://localhost:9999/test";
            var dto = new RequisitionService.RequisitionServiceCreateRequestDTO()
            {
                purchaseOrdNo = "PUR001",
                requestedBy = mp.GetString("requestby"),
                requiredByDate = mp.GetString("requestbydate"),
                suggestedSupp = mp.GetString("suggestedsupplier"),
                delivInstrA = mp.GetString("docNo"),
                districtCode = district, //new e9
                ireqType = "PR", //new e9
                origWhouseId = mp.GetString("warehouse"), //new e9, need different each site
                matGroupCode = "0000", //new e9
            };
            if (mp.GetString("indicator") == "P" || mp.GetString("accountcode") != "") // costing per pr
            {
                dto.costDistrictA = district;
                dto.costCentreA = mp.GetString("accountcode");
                dto.projectA = mp.GetString("woproject");
                //dto.workProjIndA = mp.GetString("indicator"); //e9 not required??
                dto.equipmentRefA = mp.GetString("equipref");
                dto.allocPcA = "100";
            }

            List<RequisitionService.RequisitionItemDTO> items = new List<RequisitionService.RequisitionItemDTO>();
            for (int i = 0; i < mp.Count("items"); i++)
            {
                MincomParameter mpi = mp.Get("items[" + i + "]");
                RequisitionService.RequisitionItemDTO item = new RequisitionService.RequisitionItemDTO()
                {
                    itemType = "D", //D GOODS
                    quantityRequired = decimal.Parse(mpi.GetString("qty")),
                    quantityRequiredSpecified = true,
                    unitOfMeasure = mpi.GetString("uom"),
                    //currency field not found?
                    estimatedPrice = decimal.Parse(mpi.GetString("price")),
                    estimatedPriceSpecified = true,
                    itemDescriptionA = mpi.GetString("description"),
                    //classType = "0000"
                };
                if (mp.GetString("accountcode") == "") // costing per item
                {
                    item.costDistrictA = district;
                    item.costCentreA = mpi.GetString("accountcode");
                    item.equipmentRefA = mpi.GetString("equipref");
                    item.allocationPercentA = "100";
                }
                items.Add(item);
            }

            //input items not working, use separated method
            //dto.numOfItems = items.Count;
            //dto.numOfItemsSpecified = true;
            //dto.requisitionItems = items.ToArray();

            var res = svc.create(ctx, dto);
            svc.createItem(ctx, new RequisitionService.RequisitionServiceCreateItemRequestDTO()
            {
                preqNo = res.preqNo,
                districtCode = res.districtCode,
                ireqType = "PR",
                requisitionItems = items.ToArray()
            });
            svc.finalise(ctx, new RequisitionService.RequisitionServiceFinaliseRequestDTO()
            {
                preqNo = res.preqNo,
                districtCode = res.districtCode,
                ireqType = "PR",
            });

            //res2.hdr140StatusDesc //header status = Unauthorised

            return res.preqNo.Trim();
        }

        /// <summary>
        /// check PR status - requisitionservice - deprecated (included in cancel pr)
        /// </summary>
        /// <param name="mp"></param>
        /// <returns></returns>
        private string CheckPurchaseRequisition_repo(StructOperation op, MincomParameter mp)
        {
            var ctx = GetContext<RequisitionService.OperationContext>(op, mp);
            var svc = GetService<RequisitionService.RequisitionService>(op, mp);
            //svc.Url = "http://localhost:9999/test";
            var dto = new RequisitionService.RequisitionServiceReadRequestDTO()
            {
                preqNo = mp.GetString("prno"),
                ireqType = "PR"
            };
            var res = svc.read(ctx, dto);

            JObject ret = null;
            if (mp.GetString("itemno") == "")
            {
                ret = JObject.FromObject(new
                {
                    Status = res.authsdStatusDesc,
                    ReqBy = res.requestedBy
                });
            }
            else
            {
                for (int i = 0; i < res.requisitionItems.Length; i++)
                {
                    if (i >= res.numOfItems)
                        throw new Exception("E9: Item not found");
                    if (res.requisitionItems[i].issueRequisitionItem.ToString("0") == mp.GetString("itemno"))
                    {
                        ret = JObject.FromObject(new
                        {
                            Status = res.requisitionItems[i].orderStatusDesc,
                            ReqBy = res.requestedBy
                        });
                        break;
                    }
                }
            }
            return ret.ToString(Formatting.None);
        }

        /// <summary>
        /// Cancel PR - RequisitionService
        /// </summary>
        /// <param name="mp"></param>
        /// <returns></returns>
        private string CancelPurchaseRequisition_repo(StructOperation op, MincomParameter mp)
        {
            //check PR
            var ctx = GetContext<RequisitionService.OperationContext>(op, mp);
            var svc = GetService<RequisitionService.RequisitionService>(op, mp);
            ctx.trace = true;
            ctx.traceSpecified = true;

            //workaround bug e9: set district by context
            ResetDistrict(op, mp);

            var dto = new RequisitionService.RequisitionServiceReadRequestDTO()
            {
                districtCode = mp.GetString("overrideDistrict"),
                preqNo = mp.GetString("prno"),
                ireqType = "PR"
            };
            var res = svc.read(ctx, dto);
            dumper.Debug("Read PR");
            dumper.Debug(DumpObject(res, false));

            string ret = null;
            if (mp.GetString("itemno") == "")
            {
                string stat = res.authsdStatusDesc;
                if (!stat.StartsWith("Authorised", StringComparison.OrdinalIgnoreCase)
                    && !stat.StartsWith("Unauthor", StringComparison.OrdinalIgnoreCase))
                {
                    throw new Exception("Error status: " + stat);
                }
                //cancel
                var dtocancel = new RequisitionService.RequisitionServiceCancelRequestDTO()
                {
                    districtCode = mp.GetString("overrideDistrict"),
                    preqNo = res.preqNo,
                    ireqType = res.ireqType,
                };
                dumper.Debug("Cancel Request");
                dumper.Debug(DumpObject(dtocancel, false));
                var rescancel = svc.cancel(ctx, dtocancel);
                if (rescancel.warningsAndInformation.Length == 0)
                {
                    ret = "success";
                }
                else
                {
                    ret = res.warningsAndInformation[0].message;
                }
            }
            else
            {
                for (int i = 0; i < res.requisitionItems.Length; i++)
                {
                    if (res.requisitionItems[i].issueRequisitionItem.ToString("0") == mp.GetString("itemno"))
                    {
                        string stat = res.requisitionItems[i].orderStatusDesc;
                        if (!stat.Equals("Not Ordered", StringComparison.OrdinalIgnoreCase)
                            && !stat.Equals("Not Authorised", StringComparison.OrdinalIgnoreCase))
                        {
                            throw new Exception("Error status: " + stat);
                        }
                        var dtodelete = new RequisitionService.RequisitionServiceDeleteItemRequestDTO()
                        {
                            districtCode = res.districtCode,
                            preqNo = res.preqNo,
                            ireqType = res.ireqType,
                            requisitionItems = new RequisitionService.RequisitionItemDTO[]
                                {
                                    res.requisitionItems[i]
                                }
                        };
                        dumper.Debug("Delete Item Request");
                        dumper.Debug(DumpObject(dtodelete, false));
                        var resdelete = svc.multipleDeleteItem(ctx, new RequisitionService.RequisitionServiceDeleteItemRequestDTO[]
                        {
                            dtodelete
                        });
                        if (resdelete.replyElements[0].warningsAndInformation.Length == 0)
                        {
                            ret = "success";
                        }
                        else
                        {
                            ret = res.warningsAndInformation[0].message;
                        }
                        break;
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// wrap approveinvoicemulti and join its results
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="mp"></param>
        /// <returns></returns>
        private string ApproveInvoice(StructOperation op, MincomParameter mp)
        {
            JobSplit[] ids = job.GetJobSplit("LoadNonOrderInvoice", mp.GetString("invoiceNo"));
            if (ids == null)
            {
                ids = new JobSplit[] { new JobSplit(mp.GetString("invoiceNo"), null) };
            }
            //ApproveInvoiceResult resjoin = new ApproveInvoiceResult();
            Dictionary<string, List<string>> resjoin = new Dictionary<string, List<string>>();
            resjoin.Add("AccountantCode", new List<string>());
            resjoin.Add("AccountantName", new List<string>());
            resjoin.Add("AccountName", new List<string>());
            resjoin.Add("ApprovalStatus", new List<string>());
            resjoin.Add("ApprovalStatusDesc", new List<string>());
            resjoin.Add("ApproveItemsResult", new List<string>());
            resjoin.Add("BankAccount", new List<string>());
            resjoin.Add("Currency", new List<string>());
            resjoin.Add("DueDate", new List<string>());
            resjoin.Add("InvoiceAmountAmend", new List<string>());
            resjoin.Add("InvoiceAmountOrig", new List<string>());
            resjoin.Add("InvoiceDate", new List<string>());
            resjoin.Add("InvoiceType", new List<string>());
            resjoin.Add("InvoiceTypeDesc", new List<string>());
            resjoin.Add("Message", new List<string>());
            resjoin.Add("PaymentStatus", new List<string>());
            resjoin.Add("PaymentStatusDesc", new List<string>());
            resjoin.Add("PurchaseOrderNo", new List<string>());
            resjoin.Add("VoucherNo", new List<string>());

            for (int i = 0; i < ids.Length; i++)
            {
                loggerinfo("Approve Invoice: " + ids[i].id);
                //ApproveInvoiceResult res = ApproveInvoiceDo(mobj, mp.GetString("district"), mp.GetString("supplierNo"), ids[i].id,
                //    mp.GetString("authBy"), mp.GetString("positionId"));
                MincomParameter mps = new MincomParameter(mp.token.ToString());
                mps.SetString("invoiceNo", ids[i].id);
                Dictionary<string, string> res = ApproveInvoiceMultiDo(op, mps);
                foreach (var item in resjoin)
                {
                    resjoin[item.Key].Add(res[item.Key]);
                }
            }
            Dictionary<string, string> res1 = new Dictionary<string, string>();
            foreach (var item in resjoin)
            {
                res1.Add(item.Key, string.Join(";", item.Value.ToArray()));
            }

            return JsonConvert.SerializeObject(res1);
        }

        /// <summary>
        /// MSO261 internal method
        /// e9s
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="district"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="authBy"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        private Dictionary<string, string> ApproveInvoiceMultiDo(StructOperation op, MincomParameter mp)
        {
            //string[] district, string supplierNo, string invoiceNo, string authBy, string positionId
            //new string[] { mp.GetString("district") }, mp.GetString("supplierNo"),
            //ids[i].id, mp.GetString("authBy"), mp.GetString("positionId")
            E9Screen mobj = GetService<E9Screen>(op, mp);

            string approveItems = "";
            string district;
            if (mp.IsArray("district")) //multi
            {
                string[] districts = mp.GetStringArray("district", null);
                if (mp.IsExist("approvalDistrict"))
                {
                    district = mp.GetString("approvalDistrict");
                }
                else
                {
                    district = districts[0];
                }
                foreach (string d in districts)
                {
                    approveItems += ApproveInvoiceItemsDo_repo(op, new MincomParameter(JObject.FromObject(new
                    {
                        district = d,
                        authBy = mp.GetString("authBy"),
                        positionId = mp.GetString("positionId")
                    })));
                }
            }
            else //single
            {
                district = mp.GetString("district");
                approveItems += ApproveInvoiceItemsDo_repo(op, new MincomParameter(JObject.FromObject(new
                {
                    district = district,
                    authBy = mp.GetString("authBy"),
                    positionId = mp.GetString("positionId")
                })));
            }

            mobj.GoHome();

            mobj.ExecuteMSO("MSO261");
            mobj.CheckForScreen("MSM261A", true);
            mobj.SetFieldValue("OPTION1I", "1");
            mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
            mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
            mobj.SetFieldValue("DSTRCT_CODE1I", district);
            //            mobj.SetFieldValue("VCHR_NO1I", "");
            mobj.ExecuteCommand("OK");
            mobj.CheckForScreen("MSM261B", true);

            Dictionary<string, string> res = new Dictionary<string, string>();
            if (mobj.IsCommand("Approve Invoice"))
            {
                mobj.ExecuteCommand("Approve Invoice");
                string msg = mobj.GetErrorMessage();
                res.Add("BankAccount", mobj.GetFieldValue("BANK_ACCT_NO2I"));
                res.Add("AccountName", mobj.GetFieldValue("ACCT_NAME2I"));
                res.Add("AccountantCode", mobj.GetFieldValue("ACCT2I"));
                res.Add("AccountantName", mobj.GetFieldValue("ACCTNT_NAME2I"));
                res.Add("Currency", mobj.GetFieldValue("CURRENCY_TYPE2I"));
                res.Add("InvoiceDate", mobj.GetFieldValue("INV_DATE2I"));
                res.Add("DueDate", mobj.GetFieldValue("DUE_DATE2I"));
                res.Add("InvoiceType", mobj.GetFieldValue("INV_TYPE2I"));
                res.Add("InvoiceTypeDesc", mobj.GetFieldValue("INV_TYPE_DESC2I"));
                res.Add("ApprovalStatus", mobj.GetFieldValue("APPR_STATUS2I"));
                res.Add("ApprovalStatusDesc", mobj.GetFieldValue("APPR_STAT_DESC2I"));
                res.Add("PaymentStatus", mobj.GetFieldValue("PMT_STATUS2I"));
                res.Add("PaymentStatusDesc", mobj.GetFieldValue("PMT_STAT_DESC2I"));
                res.Add("VoucherNo", mobj.GetFieldValue("VCHR_NO2I"));
                res.Add("PurchaseOrderNo", mobj.GetFieldValue("CONT_PO2I"));
                res.Add("InvoiceAmountOrig", mobj.GetFieldValue("INV_AMT_ORIG2I"));
                res.Add("InvoiceAmountAmend", mobj.GetFieldValue("INV_AMT_AMD2I"));
                res.Add("ApproveItemsResult", approveItems);
                res.Add("Message", msg);

                loggerinfo(msg);
                //UNAUTHORISED items, NOT approved
                //MISMATCH detected, NOT approved
                //INVOICE STATUS DOES NOT ALLOW APPROVAL
                //if (!msg.ToUpper().Trim().EndsWith("APPROVED OK"))
                //{
                //    throw new Exception(msg);
                //}
                if (!msg.ToUpper().Trim().EndsWith("MODIFICATIONS MADE TO INVOICE"))
                {
                    throw new Exception(msg);
                }
                if (mobj.GetFieldValue("PMT_STATUS2I").Trim() != "30")
                {
                    throw new Exception("E9: Payment status expected 30 FULLY APPROVED, got "
                        + mobj.GetFieldValue("PMT_STATUS2I") + " " + mobj.GetFieldValue("PMT_STAT_DESC2I"));
                }
            }
            else
            {
                throw new Exception("Failed to approve invoice");
            }
            mobj.ExecuteCommand("Home");

            return res;
            //            return JsonHelper.GetJsonString(res);
        }

        private string ApproveInvoiceItemsDo_repo(StructOperation op, MincomParameter mp)
        {
            var ctx = GetContext<ApprovalsManagerService.OperationContext>(op, mp);
            //ctx.district = mp.GetString("district");
            ctx.maxInstances = 100;
            ctx.maxInstancesSpecified = true;
            var svc = GetService<ApprovalsManagerService.ApprovalsManagerService>(op, mp);

            var prm = new ApprovalsManagerService.TransactionRetrievalCriteriaSearchParam()
            {
                districtCode = mp.GetString("district"),
                employeeHistoryFlag = false,
                employeeHistoryFlagSpecified = true,
                employeeId = mp.GetString("authBy"),
                positionId = mp.GetString("positionId"),
                retrieveNotification = false,
                retrieveNotificationSpecified = true,
                retrieveUnauthorisedTransactions = true,
                retrieveUnauthorisedTransactionsSpecified = true,
                tran877Type = "IN",
            };
            //var cnt = svc.retrieveApprovalCounts(ctx, prm, null);
            //loggerinfo("Found items: " + cnt[0].transactionCountsDTO.count);

            ApprovalsManagerService.TransactionDTO last = null;
            List<ApprovalsManagerService.TransactionServiceResult> results = new List<ApprovalsManagerService.TransactionServiceResult>();
            bool finished = false;
            for (int i = 0; i < 100; i++)
            {
                var res = svc.retrieveApprovals(ctx, prm, last);
                if (res.Length == 0)
                {
                    finished = true;
                    break;
                }
                if (res[0].errors.Length > 0)
                {
                    throw new Exception(res[0].errors[0].messageText);
                }
                results.AddRange(res);
                string[] keys = res.Where(t => t.transactionDTO != null)
                    .Select(t => t.transactionDTO.transactionKey.Trim()).ToArray();
                loggerinfo("Found items [" + keys.Length + "]: " + string.Join(";", keys));
                last = res[res.Length - 1].transactionDTO;
            }
            if (results.Count > 0)
                loggerinfo("Total transaction items: " + results.Count);
            string ret = "Approve All - Items Found: " + results.Count;
            if (!finished)
            {
                loggerinfo("Warning: Limit items count reached");
                ret = "Approve All - Limit items count reached - Items: " + results.Count;
            }
            for (int i = 0; i < results.Count; i++)
            {
                var dto = results[i].transactionDTO;
                loggerinfo("approve item " + (i + 1) + "/" + results.Count + ": " + dto.transactionKey);
                var tranres = svc.approve(ctx, dto);
                string[] err = tranres.errors.Select(e => e.messageText).ToArray();
                string[] info = tranres.informationalMessages.Select(e => e.messageText).ToArray();
                loggerinfo(string.Join("; ", err.Concat(info).ToArray()));
                //if (i == 3) break;
            }

            return ret;
        }

        /// <summary>
        /// MSO877 internal method ** deprecated, use MSEAPM repo service
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="district"></param>
        /// <param name="authBy"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        private string ApproveInvoiceItemsDo_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);
            //string district, string authBy, string positionId
            string result = "";
            mobj.GoHome();

            mobj.ExecuteMSO("MSO877");
            mobj.CheckForScreen("MSM877A", true);
            if (mp.GetString("authBy") == null)
            {
                mp.SetString("authBy", ServiceHelper.GetConfig("Authorize"));
            }
            if (mp.GetString("positionId") == null)
            {
                mp.SetString("positionId", ServiceHelper.GetConfig("AuthorizePositionID"));
            }
            mobj.SetFieldValue("TYPE_FILTER1I", "IN");
            mobj.SetFieldValue("STATUS_FILTER1I", "U");
            mobj.SetFieldValue("DISTRICT1I", mp.GetString("district"));
            mobj.SetFieldValue("EMPLOYEE_ID1I", mp.GetString("authBy"));
            mobj.SetFieldValue("POSITION_ID1I", mp.GetString("positionId"));

            mobj.ExecuteCommand("OK");

            if (mobj.GetErrorMessage().ToUpper().Contains("POSITION HAS NO TRANSACTIONS TO AUTHORIZE IN"))
            {
                result = "No Transactions";
            }
            else
            {
                int items = 0;
                for (int i = 1; i <= 3; i++)
                {
                    if (mobj.GetFieldValue("TRAN_TYPE1I" + i).Trim().ToUpper().Equals("IN"))
                    {
                        mobj.SetFieldValue("ACTION1I" + i, "I");
                        items++;
                    }
                }
                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().Trim().Equals(""))
                {
                    result = "Approve All - Items Found: " + items;
                }
                else
                {
                    result = mobj.GetErrorMessage();
                }
            }

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO003 - ScreenService via e9screen
        /// </summary>
        /// <returns></returns>
        private string GetBankAccount_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);
            mobj.GoHome();
            mobj.ExecuteMSO("MSO003");

            ArrayList accts = new ArrayList();

            int maxpage = 100; //max page=100
            for (int page = 1; page <= maxpage; page++)
            {
                if (mobj.GetFieldValue("SKLDISPLAY1I").Trim().Equals(string.Format("{0:000}/{0:000}", page)))
                {
                    //last page detected
                    maxpage = page;
                }
                else if (!mobj.GetFieldValue("SKLDISPLAY1I").Trim().Equals(string.Format("{0:000}", page)))
                {
                    throw new Exception("SKLDISPLAY1I not match");
                }
                for (int i = 1; i <= 5; i++)
                {
                    if (mobj.GetFieldValue("BRANCH_CODE1I" + i).Trim().Equals(""))
                    {
                        maxpage = page;
                        break;
                    }

                    accts.Add(new
                    {
                        BranchCode = mobj.GetFieldValue("BRANCH_CODE1I" + i),
                        BranchName = mobj.GetFieldValue("BRANCH_NAME1I" + i),
                        OwnerDistrict = mobj.GetFieldValue("OWNER_DSTRCT1I" + i),
                        CurrencyType = mobj.GetFieldValue("CURRENCY_TYPE1I" + i),
                        AccountNo = mobj.GetFieldValue("BANK_ACCT_NO1I" + i),
                        AccountName = mobj.GetFieldValue("ACCOUNT_NAME1I" + i)
                    });
                }
                if (maxpage > page)
                {
                    //check next page
                    mobj.ExecuteCommand("OK");
                }
            }

            JObject ret = JObject.FromObject(new
            {
                Accounts = accts.ToArray()
            });
            mobj.GoHome();

            return ret.ToString(Formatting.None);
        }

        //private string UploadPaymentTemp(StructOperation op, MincomParameter mp)
        //{
        //    //check district on uploadpayment mso275
        //    ScreenService.OperationContext context = new ScreenService.OperationContext();
        //    context.district = "KPHO";
        //    context.position = "KP2SD03";
        //    ClientConversation.authenticate("dailykpp", "kpp12345");
        //    //List<ScreenService.ScreenNameValueDTO> f = new List<ScreenService.ScreenNameValueDTO>();
        //    ScreenService.ScreenService svc = new ScreenService.ScreenService();
        //    svc.Url = Regex.Replace(svc.Url, @"http://.*?(?=/)", op.baseurl);
        //    var scr = svc.executeScreen(context, "MSO275");
        //    string[] fields = scr.screenFields
        //        .Where(f => f.value.Trim().Length > 0)
        //        .Select(f => string.Format("{0}{1}\t: {2}", f.fieldProtected ? "*" : "", f.fieldName, f.value)).ToArray();
        //    Console.WriteLine(string.Join("\r\n", fields));

        //    return null;
        //}

        /// <summary>
        /// MSO275
        /// e9s
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="bankBranch"></param>
        /// <param name="bankAccount"></param>
        /// <param name="supplierNo"></param>
        /// <param name="totalAmount"></param>
        /// <param name="date"></param>
        /// <param name="currency"></param>
        /// <param name="invoices"></param>
        /// <returns></returns>
        private string UploadPayment(StructOperation op, MincomParameter mp)
        {
            //UploadPaymentTemp(op, mp);
            E9Screen mobj = GetService<E9Screen>(op, mp);
            CultureInfo[] ci = new CultureInfo[] {
                CultureInfo.CreateSpecificCulture("en-US"),
                CultureInfo.CreateSpecificCulture("id-ID")
            };

            //string result = null;
            //List<UploadPaymentItem> invoices = new List<UploadPaymentItem>();
            var invoices = new JArray();

            foreach (MincomParameter inv in mp.GetArray("invoiceItems"))
            {
                JobSplit[] splitids = job.GetJobSplit("LoadNonOrderInvoice", inv.GetString("invoiceNo"));
                if (splitids == null || splitids.Length == 1)
                {
                    splitids = new JobSplit[] { new JobSplit(inv.GetString("invoiceNo"), string.Format("{0}", inv.GetString("amount"))) };
                }
                else
                {
                    loggerinfo(string.Format("Job split found, invoice: {0} count: {1}", inv.GetString("invoiceNo"), splitids.Length));
                }
                foreach (JobSplit split in splitids)
                {
                    //UploadPaymentItem item = new UploadPaymentItem();
                    double amt = 0;
                    try
                    {
                        amt = double.Parse(split.amount, NumberStyles.Any, ci[0]);
                    }
                    catch
                    {
                        amt = double.Parse(split.amount, NumberStyles.Any, ci[1]);
                    }
                    var item = new
                    {
                        invoiceNo = split.id,
                        district = inv.GetString("district"),
                        accountCode = inv.GetString("accountCode"),
                        amount = amt,
                    };

                    //item.invoiceNo = split.id;
                    //item.district = inv.GetString("district");
                    //item.accountCode = inv.GetString("accountCode");

                    //double.TryParse(split.amount, System.Globalization.NumberStyles.Any, cien, out item.amount);
                    //if (item.amount == 0)
                    //{//try id format
                    //    CultureInfo ciid = CultureInfo.CreateSpecificCulture("id-ID");
                    //    double.TryParse(split.amount, System.Globalization.NumberStyles.Any, ciid, out item.amount);
                    //}
                    invoices.Add(JObject.FromObject(item));
                }
            }

            //all date field format dd/mm/yy
            mp.CheckRequired(new string[] {
                "bankBranch","bankAccount","supplierNo","totalAmount","date","currency"
            }, true);


            //UploadPaymentResult res = new UploadPaymentResult();

            //workaround bug e9: set district by context
            ResetDistrict(op, mp);

            mobj.GoHome();
            mobj.ExecuteMSO("MSO275");
            mobj.CheckForScreen("MSM275A", true);
            loggerinfo("Current district: " + mobj.GetFieldValue("DSTRCT_CODE1I"));

            mobj.SetFieldValue("BRANCH_CODE1I", mp.GetString("bankBranch"));
            mobj.SetFieldValue("BANK_ACCT_NO1I", mp.GetString("bankAccount"));
            mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
            mobj.SetFieldValue("SELECT_INVS1I", "Y");
            mobj.SetFieldValue("CHEQUE_AMT1I", mp.GetString("totalAmount"));
            mobj.SetFieldValue("CHEQUE_DATE1I", mp.GetString("date"));
            //mobj.SetFieldValue("CHEQUE_DATE1I", "01/09/21"); // todo: temp uat

            mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));

            List<string> amtstotal = new List<string>();
            int distcounter = 0;
            string lastskl = null;
            string[] districts = invoices.GroupBy(inv => inv["district"])
                .Select(inv => (string)inv.First()["district"]).ToArray();
            while (distcounter < districts.Length)
            {
                string skl = mobj.GetFieldValue("SKLDISPLAY1I").Trim();
                loggerinfo("Page: " + skl);
                if (lastskl != null && lastskl.Equals(skl))
                {
                    throw new Exception("SKLDISPLAY error, last: " + lastskl + ", current: " + skl);
                }
                lastskl = skl;
                for (int i = 1; i <= 4; i++)
                {
                    string dist = districts[distcounter];
                    loggerinfo("Dist: " + dist);
                    //List<UploadPaymentItem> items = sum[dist];
                    JToken[] items = invoices.Where(inv => (string)inv["district"] == dist).ToArray();
                    double tamt = 0;
                    for (int j = 0; j < items.Length; j++)
                    {
                        loggerinfo("X " + items[j]["invoiceNo"] + " " + items[j]["amount"]);
                        tamt += (double)items[j]["amount"];
                    }
                    loggerinfo(string.Format("T {0} {1:N2}", dist, tamt));
                    //                    double tamt = items.Sum(itm => itm.amount);
                    mobj.SetFieldValue("ACCT_DSTRCT1I" + i, dist);
                    mobj.SetFieldValue("DESCRIPTION1I" + i, mp.GetStringTruncate("description", 38));
                    mobj.SetFieldValue("AMOUNT1I" + i, string.Format("{0:N2}", tamt));
                    mobj.SetFieldValue("GL_ACCOUNT1I" + i, (string)items[0]["accountCode"]);
                    distcounter++;
                    if (distcounter >= districts.Length)
                    {
                        break;
                    }
                }

                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM275A: " + mobj.GetErrorMessage() + " Field " + mobj.GetActiveField());
                }
                else if (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    while (mobj.GetErrorMessage().StartsWith("W2"))
                    {
                        if (mobj.GetActiveField().Equals("CURRENCY_TYPE1I"))
                        {
                            mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));
                        }
                        mobj.ExecuteCommand("OK");
                    }
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                    }
                }

                if (!mobj.GetFieldValue("CURRENCY_TYPE1I").Equals(mp.GetString("currency")))
                {
                    mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));
                }
                if (!mobj.GetFieldValue("EXCHANGE_RATE1I").Equals(""))
                {
                    mobj.SetFieldValue("EXCHANGE_RATE1I", "");
                }
                if (mobj.IsCommand("Confirm") && mobj.CheckForScreen("MSM275A", false))
                {
                    mobj.ExecuteCommand("Confirm");
                }

                mobj.CheckForScreen("MSM275B", true);

                string lastdist = null;
                int index = 0;
                int maxpages = 30;
                while (mobj.CheckForScreen("MSM275B", false))
                {
                    string dist = mobj.GetFieldValue("ACCT_DSTRCT2I").Trim();
                    if (lastdist == null || !lastdist.Equals(dist))
                    {
                        index = 0;
                    }
                    lastdist = dist;
                    //List<UploadPaymentItem> items = sum[dist];
                    JToken[] items = invoices.Where(inv => (string)inv["district"] == dist).ToArray();
                    double[] amtsinput = new double[12];
                    for (int i = 1; i <= 11; i++)
                    {
                        if (index >= items.Length)
                        {
                            break;
                        }
                        mobj.SetFieldValue("INV_NO2I" + i, (string)items[index]["invoiceNo"]);
                        amtsinput[i] = (double)items[index]["amount"];
                        index++;
                    }
                    mobj.ExecuteCommand("OK");
                    //StringBuilder sbmismatch = new StringBuilder();
                    List<string> mismatches = new List<string>();
                    for (int i = 1; i <= 11; i++)
                    {
                        if (!mobj.CheckForScreen("MSM275B", false))
                        {
                            break;
                        }
                        string inv = mobj.GetFieldValue("INV_NO2I" + i);
                        string amt = mobj.GetFieldValue("INV_AMT2I" + i);
                        if (inv == null || amt == null)
                        {
                            break;
                        }
                        inv = inv.Trim();
                        amt = amt.Trim();
                        double amtn = 0;
                        if (!double.TryParse(amt, out amtn))
                        {
                            break;
                        }
                        if (amtsinput[i] != 0 && amtsinput[i] != amtn)
                        {
                            string mismatch = string.Format("Invoice Mismatch {0} - Input: {1} - System: {2}",
                                inv, amtsinput[i], amt);
                            loggerinfo(mismatch);
                            mismatches.Add(mismatch);
                        }
                    }

                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                    }

                    List<string> amts = new List<string>();
                    int retry = 3;
                    while (mobj.IsCommand("Confirm") && retry > 0)
                    {
                        if (mobj.CheckForScreen("MSM275B", false))
                        {
                            amts.Clear();
                            for (int i = 1; i <= 11; i++)
                            {
                                amts.Add(mobj.GetFieldValue("INV_AMT2I" + i));
                            }
                        }
                        mobj.ExecuteCommand("Confirm");
                        if (mobj.GetErrorMessage().StartsWith("X2"))
                        {
                            string message = mobj.GetErrorMessage();
                            if (message != null
                                && message.ToUpper().Trim().Contains("X2:7375 - TOTAL VALUE OF INVOICES NOT EQUAL AMOUNT PAYABLE")
                                && mismatches.Count > 0)
                            {
                                throw new Exception("MSM275A: " + mobj.GetErrorMessage()
                                    + Environment.NewLine + string.Join(Environment.NewLine, mismatches.ToArray()));
                            }
                            else
                            {
                                throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                            }
                        }
                        retry--;
                    }
                    amtstotal.AddRange(amts);
                    if (maxpages > 0)
                    {
                        maxpages--;
                    }
                    else
                    {
                        throw new Exception("Unlimited loop detected, terminating...");
                    }
                }
            }
            //res.InvoiceAmounts = amtstotal.ToArray();

            string pvno = Regex.Match(mobj.GetErrorMessage(), @"Cheque\s*:\s*(.*?)\s").Groups[1].Value.Trim();
            if (mobj.CheckForScreen("MSM275A", false) && mobj.GetErrorMessage().Trim().Equals(""))
            {
                //patch for district count/4==0
                int count = 5;
                //while (mobj.GetFieldValue("CHEQUE_NO1I").Trim().StartsWith("XXXX"))
                while (pvno == "")
                {
                    loggerinfo(string.Join(";", mobj.GetCommands()));
                    if (mobj.IsCommand("OK"))
                    {
                        mobj.ExecuteCommand("OK");
                    }
                    else if (mobj.IsCommand("Validate"))
                    {
                        mobj.ExecuteCommand("Validate");
                    }
                    else if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }
                    if (--count <= 0) break;
                    pvno = Regex.Match(mobj.GetErrorMessage(), @"Cheque\s*:\s*(.*?)\s").Groups[1].Value.Trim();
                }
            }
            //res.Result = mobj.GetErrorMessage();
            if (pvno == "")
            {
                throw new Exception("PV Number not found");
            }
            //translation
            var res = new
            {
                InvoiceAmounts = amtstotal.ToArray(),
                Result = "PV Number : " + pvno,
            };

            return JsonHelper.GetJsonString(res);
        }

        /// <summary>
        /// mso27a
        /// e9s
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="bankBranch"></param>
        /// <param name="bankAccount"></param>
        /// <param name="totalAmount"></param>
        /// <param name="date"></param>
        /// <param name="district"></param>
        /// <param name="description"></param>
        /// <param name="amount"></param>
        /// <param name="glAccount"></param>
        /// <returns></returns>
        private string UploadSundries(StructOperation op, MincomParameter mp)
        {
            //string[] district = mp.GetStringArray("items", "district");
            //string[] description = mp.GetStringArray("items", "description");
            //string[] amount = mp.GetStringArray("items", "amount");
            //string[] glAccount = mp.GetStringArray("items", "glAccount");

            //mp.SetString("overrideDistrict", district[0]);
            //string ovr = mp.GetStringArray("items", "district")[0];
            //mp.SetString("overrideDistrict", ovr);

            E9Screen mobj = GetService<E9Screen>(op, mp);

            //all date field format dd/mm/yy
            mp.CheckRequired(new string[]
            {
                "bankBranch","bankAccount","totalAmount","date",
            }, true);


            //workaround bug e9: set district by context
            ResetDistrict(op, mp);

            mobj.GoHome();
            mobj.ExecuteMSO("MSO27A");
            mobj.CheckForScreen("MSM27AA", true);

            mobj.SetFieldValue("OPTION1I", "2");
            mobj.SetFieldValue("BRANCH_CODE1I", mp.GetString("bankBranch"));
            mobj.SetFieldValue("BANK_ACCT_NO1I", mp.GetString("bankAccount"));
            mobj.SetFieldValue("TRANAMT1I", mp.GetString("totalAmount"));
            mobj.SetFieldValue("TRANDT1I", mp.GetString("date"));
            //mobj.SetFieldValue("TRANDT1I", "01/09/21"); // todo: temp uat

            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM275A", true);
            MincomParameter[] mpi = mp.GetArray("items");
            for (int i = 0; i < mpi.Length; i++)
            {
                int x = (i % 4) + 1;
                mobj.SetFieldValue("ACCT_DSTRCT1I" + x, mpi[i].GetString("district"));
                mobj.SetFieldValue("DESCRIPTION1I" + x, mpi[i].GetStringTruncate("description", 38));
                mobj.SetFieldValue("AMOUNT1I" + x, mpi[i].GetString("amount"));
                mobj.SetFieldValue("GL_ACCOUNT1I" + x, mpi[i].GetString("glAccount"));
                //mobj.SetFieldValue("WORK_ORDER1I" + i, "");
                //mobj.SetFieldValue("PROJECT_IND1I" + i, "");
                //mobj.SetFieldValue("PLANT_NO1I" + i, "");
                if (i % 4 == 3)
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }

                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetScreenName() + ": " + mobj.GetErrorMessage());
                    }
                }
            }
            mobj.ExecuteCommand("OK");
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetScreenName() + ": " + mobj.GetErrorMessage());
            }

            string pvno = Regex.Match(mobj.GetErrorMessage(), @"Sundry:\s*(.*?)\s").Groups[1].Value;
            if (pvno == "")
            {
                throw new Exception(mobj.GetScreenName() + " invalid message: " + mobj.GetErrorMessage());
            }
            var result = new
            {
                //translation
                Result = "PV Number : " + pvno,
            };
            //UploadPaymentResult result = new UploadPaymentResult();
            //result.Result = mobj.GetErrorMessage();

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO572
        /// e9s
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="totalReceived"></param>
        /// <param name="date"></param>
        /// <param name="branchCode"></param>
        /// <param name="bankAccount"></param>
        /// <param name="currency"></param>
        /// <param name="accountCode"></param>
        /// <param name="amtReceived"></param>
        /// <param name="district"></param>
        /// <returns></returns>
        private string UploadMiscCashReceipt(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);
            //string receivedBy, string date, string totalReceived, string bankBranch, string bankAccount, string currency,
            //string[] accountCode, string[] amtReceived, string[] district, string[] description

            //string[] accountCode = mp.GetStringArray("items", "accountCode");
            //string[] amtReceived = mp.GetStringArray("items", "amtReceived");
            //string[] district = mp.GetStringArray("items", "district");
            //string[] description = mp.GetStringArray("items", "description");
            MincomParameter[] mpi = mp.GetArray("items");
            //result = UploadMiscCashReceiptDo(mobj, jsd.receivedBy, jsd.date, jsd.totalReceived, jsd.bankBranch, jsd.bankAccount, jsd.currency,
            //    accountCode.ToArray(), amtReceived.ToArray(), district.ToArray(), description.ToArray());

            string res = null;

            //workaround bug e9: set district by context
            ResetDistrict(op, mp);

            mobj.GoHome();
            mobj.ExecuteMSO("MSO572");
            mobj.CheckForScreen("MSM572A", true);

            mobj.SetFieldValue("RECEIVED_BY1I", mp.GetString("receivedBy"));
            mobj.SetFieldValue("RECEIPT_DATE1I", mp.GetString("date"));
            mobj.SetFieldValue("TOT_RECEIVED1I", mp.GetString("totalReceived"));
            mobj.SetFieldValue("BRANCH_CODE1I", mp.GetString("bankBranch"));
            mobj.SetFieldValue("BANK_ACCT_NO1I", mp.GetString("bankAccount"));
            mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));

            int n = 0;
            while (n < mpi.Length)
            {
                loggerinfo(mobj.GetFieldValue("SKLDISPLAY1I"));
                for (int i = 1; i <= 5; i++)
                {
                    if (n >= mpi.Length) break;

                    mobj.SetFieldValue("ACCOUNT_CODE1I" + i, mpi[n].GetString("accountCode"));
                    mobj.SetFieldValue("AMT_RECEIVED1I" + i, mpi[n].GetString("amtReceived"));
                    mobj.SetFieldValue("DSTRCT_CODE1I" + i, mpi[n].GetString("district"));
                    mobj.SetFieldValue("RECEIPT_DESC1I" + i, mpi[n].GetStringTruncate("description", 40));
                    n++;
                }
                mobj.ExecuteCommand("OK");
                mobj.DumpFields();

                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM572A: " + mobj.GetErrorMessage());
                }

                //mobj.DumpFields();
                res = mobj.GetErrorMessage();

                if (mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");

                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception("MSM572A: " + mobj.GetErrorMessage());
                    }

                    res = mobj.GetErrorMessage();
                }
            }
            if (mobj.IsCommand("Confirm")) //last confirm needed for multi page
            {
                mobj.ExecuteCommand("Confirm");

                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM572A: " + mobj.GetErrorMessage());
                }

                res = mobj.GetErrorMessage();
            }
            dumper.Debug(mobj.DumpFields());

            string rcpno = Regex.Match(res, "ReceiptNo (.*)").Groups[1].Value;
            //            res += "1959 - TRANSACTION COMPLETED - RA151473";
            if (rcpno.Trim() == "")
            {
                throw new Exception("Unexpected result: " + res);
            }
            //translation
            string rest = "W2:1959 - TRANSACTION COMPLETED - " + rcpno;

            //"W2:1959 - TRANSACTION COMPLETED - RN200358"
            //I-1:000 - ReceiptNo RN210059

            //            mso572
            //check mso572a
            //RECEIVED_BY1I
            //REC_BY_DESC1I
            //RECEIPT_DATE1I
            //TOT_RECEIVED1I
            //TOT_APPLIED1I
            //BRANCH_CODE1I
            //BANK_ACCT_NO1I
            //CURRENCY_TYPE1I

            //ACCOUNT_CODE1I1
            //ACCOUNT_NAME1I1
            //AMT_RECEIVED1I1
            //DSTR_RCT1I1
            //RECEIPT_DESC1I1

            return JsonHelper.GetJsonString(rest);
        }

        private string LoadNonOrderInvoiceSingle(StructOperation op, MincomParameter mp)
        {
            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");

            //split if item > 500
            int splitSize = 500;
            int.TryParse(ServiceHelper.GetConfig("ChildThreadMaxItem"), out splitSize);

            List<Thread> threads = new List<Thread>();
            List<E9Functions> childs = new List<E9Functions>();
            JArray prms = new JArray();
            int tcnt = 0;
            var itemList = mp.GetArray("items");
            for (int i = 0; i < itemList.Length; i += splitSize)
            {
                int size = (itemList.Length >= i + splitSize) ? splitSize : itemList.Length - i;
                var items = itemList.Skip(i).Take(size).Select(itm => itm.token).ToArray();
                string invoiceNo = mp.GetString("invoiceNo");
                string invoiceAmt = mp.GetString("invoiceAmt");
                if (itemList.Length > splitSize)
                {
                    invoiceNo = string.Format("{0}-{1}", invoiceNo, (i / splitSize) + 1);
                    double sum = items.Sum(itm => double.Parse((string)itm["value"]));
                    invoiceAmt = string.Format(ci, "{0:N2}", sum);
                    loggerinfo(string.Format("Split {0}: {1} amount {2}", i, invoiceNo, invoiceAmt));
                }
                Thread t = new Thread(new ParameterizedThreadStart(ProcessLoadNonOrderInvoiceThread));
                threads.Add(t);
                JObject prm = JObject.FromObject(new
                {
                    districtCode = mp.GetString("districtCode"),
                    supplierNo = mp.GetString("supplierNo"),
                    invoiceNo = invoiceNo,
                    invoiceAmt = invoiceAmt,
                    branchCode = mp.GetString("branchCode"),
                    bankAccountNo = mp.GetString("bankAccountNo"),
                    accountant = mp.GetString("accountant"),
                    authBy = mp.GetString("authBy"),
                    positionId = mp.GetString("positionId"),
                    currency = mp.GetString("currency"),
                    invoiceCommentType = mp.GetString("invoiceCommentType"),
                    refOrgInvoiceNo = mp.GetString("refOrgInvoiceNo"),
                    handlingCode = mp.GetString("handlingCode"),
                    invoiceReceived = mp.GetString("invoiceReceived"),
                    invoiceDate = mp.GetString("invoiceDate"),
                    dueDate = mp.GetString("dueDate"),
                    invoiceItems = items,
                });

                prms.Add(prm);
                string threadid = this.threadid.ToString() + "_" + (i / splitSize).ToString();
                E9Functions child = new E9Functions()
                {
                    threadid = this.threadid.ToString() + "_" + (i / splitSize).ToString()
                };
                childs.Add(child);
                //start the thread 1 by 1
                t.Start(new object[]{
                        child,
                        op,
                        prm
                    });
                if (ServiceHelper.GetConfig("DisableMultiThreadConsignment") == "true")
                {
                    t.Join(); //e9: wait 1 by 1 until finished
                }
                tcnt++;
            }
            List<string> textResult = new List<string>();
            List<string> fillingNoResult = new List<string>();
            List<string> errormessage = new List<string>();
            int[] tstatus = new int[tcnt];
            for (int i = 0; i < tcnt; i++)
            {
                tstatus[i] = -1;
                //wait thread 1 by 1
                threads[i].Join();
                Dictionary<string, object> childout = (Dictionary<string, object>)childs[i].result;
                Exception e = null;
                //childout 0 result obj, 1 error obj
                if (childout.ContainsKey("result"))
                {
                    //success
                    JObject r = (JObject)childout["result"];
                    fillingNoResult.Add((string)r["FillingNo"]);
                    textResult.Add((string)r["Text"]);
                    tstatus[i] = 1;
                }
                if (childout.ContainsKey("error"))
                {
                    //error
                    e = (Exception)childout["error"];
                    errormessage.Add(e.Message);
                    tstatus[i] = 0;
                }
            }

            //errormessage = null;
            string result;
            if (errormessage.Count == 0)
            {
                //everything ok, no error
                //now create job split
                JobSplit[] ids = new JobSplit[tcnt];
                for (int i = 0; i < tcnt; i++)
                {
                    JObject prm = (JObject)prms[i];
                    ids[i] = new JobSplit((string)prm["invoiceNo"], (string)prm["invoiceAmt"]);
                    loggerinfo(string.Format("Jobsplit {0} {1}", prm["invoiceNo"], prm["invoiceAmt"]));
                }
                job.CreateJobSplit("", "LoadNonOrderInvoice", mp.GetString("invoiceNo"), ids);
                var rs = new
                {
                    Text = textResult,
                    FillingNo = fillingNoResult,
                };
                result = JsonHelper.GetJsonString(rs);
            }
            else
            {
                //there is error
                //cancel all loaded invoices
                for (int i = 0; i < tcnt; i++)
                {
                    //if (tstatus[i] == 1)
                    if (true) //cancel all including error loaded
                    {
                        //cancel loaded
                        JObject prm = (JObject)prms[i];
                        loggerinfo("Cancel invoice #" + prm["invoiceNo"]);

                        try
                        {
                            string cancelresult = Operations["CancelInvoice"].operation(op, new MincomParameter(JObject.FromObject(new
                            {
                                districtCode = prm["districtCode"],
                                supplierNo = prm["supplierNo"],
                                invoiceNo = prm["invoiceNo"],
                            })));
                            //string cancelresult = CancelInvoiceDo(mobj, (string)prm[0], (string)prm[1], (string)prm[2]);
                            loggerinfo(cancelresult);
                        }
                        catch (Exception ex1)
                        {
                            loggerinfo("Error cancel invoice #" + prm["invoiceNo"]);
                            loggerinfo(ex1.Message);
                        }
                    }
                }

                //skipcheckscreen = true; //check screen causing system crash
                throw new Exception(string.Join(";", errormessage));
            }
            return result;
        }

        /// <summary>
        /// loadnoi child process, called by parrent
        /// </summary>
        /// <param name="param"></param>
        private void ProcessLoadNonOrderInvoiceThread(object param)
        {
            object[] prm = (object[])param;
            E9Functions child = (E9Functions)prm[0];
            StructOperation op = (StructOperation)prm[1];
            JObject mfprm = (JObject)prm[2];

            Dictionary<string, object> result = new Dictionary<string, object>();
            //0 result object
            //1 exception object
            try
            {
                //check if negative values exist, use screen service
                decimal[] amounts = mfprm.SelectToken("invoiceItems")
                    .Select(itm => decimal.Parse(itm.SelectToken("value").ToString())).ToArray();
                int negamts = amounts.Where(amt => amt < 0).Count();
                if (negamts > 0)
                {
                    loggerinfo("Negative amounts found: " + negamts + ", Execute LoadNonOrderInvoiceSingleDo_screen");
                    result.Add("result", child.LoadNonOrderInvoiceSingleDo_screen(op, new MincomParameter(mfprm)));
                }
                else
                {
                    loggerinfo("Execute LoadNonOrderInvoiceSingleDo_repo");
                    result.Add("result", child.LoadNonOrderInvoiceSingleDo_repo(op, new MincomParameter(mfprm)));
                }
            }
            catch (Exception ex)
            {
                logger.InfoFormat("#{0}: Error {1}", child.threadid, ex.Message);
                logger.InfoFormat("#{0}: {1}", child.threadid, ex.StackTrace);
                result.Add("error", ex);
            }

            child.result = result;
        }

        private JObject LoadNonOrderInvoiceSingleDo_repo(StructOperation op, MincomParameter mp)
        {
            mp.CheckRequired(new string[]
            {
                "districtCode","supplierNo","invoiceNo","invoiceAmt","bankAccountNo","positionId","currency",
                "invoiceReceived","invoiceDate","invoiceItems"
            }, false);

            string[] cppn = new string[]{
                "Tgl. Faktur Pajak:",
                "No. Faktur Pajak:",
                "No. Inv.(Khusus A/P):",
            };
            string[] cpph = new string[]
            {
                "Tgl. Bukti Potong:",
                "No. Bukti Potong:",
                "No. Original Invoice:",
                "Nilai DPP (Khusus A/R Invoice):",
                "!!!(nilai DPP sesuai currency invoice)!!!",
            };
            string comment = "";
            if (mp.IsNotEmpty("taxInvoiceDate") || mp.IsNotEmpty("taxInvoiceNo") || mp.IsNotEmpty("taxOrgInvoiceNo"))
            {
                string[] cmt = cppn;
                cmt[0] += mp.GetString("taxInvoiceDate");
                cmt[1] += mp.GetString("taxInvoiceNo");
                cmt[2] += mp.GetString("taxOrgInvoiceNo");
                comment = string.Join("\r\n", cmt);
            }
            else if (mp.IsNotEmpty("refDate") || mp.IsNotEmpty("refNo") || mp.IsNotEmpty("refOrgInvoiceNo"))
            {
                string[] cmt = cpph;
                cmt[0] += mp.GetString("refDate");
                cmt[1] += mp.GetString("refNo");
                cmt[2] += mp.GetString("refOrgInvoiceNo");
                comment = string.Join("\r\n", cmt);
            }
            string authBy = mp.IsNotEmpty("authBy") ? mp.GetString("authBy") : ServiceHelper.GetConfig("Authorize");
            string authPos = mp.IsNotEmpty("positionId") ? mp.GetString("positionId") : ServiceHelper.GetConfig("AuthorizePositionID");
            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
            var ctx = GetContext<InvoiceService.OperationContext>(op, mp);
            var svc = GetService<InvoiceService.InvoiceService>(op, mp);
            //svc.Url = Regex.Replace(svc.Url, @"kpt", "kpd");
            //loggerinfo("url " + svc.Url);

            MincomParameter[] invoiceItems = mp.GetArray("invoiceItems");
            decimal sumdet = invoiceItems.Sum(mpi => decimal.Parse(mpi.GetString("value")));
            decimal invamt = decimal.Parse(mp.GetString("invoiceAmt"));
            if (invamt != sumdet)
            {
                throw new Exception(string.Format("E9 Error: Inconsistent field invoiceAmt {0:N2} != sum of detail values {1:N2}", invamt, sumdet));
            }
            InvoiceService.InvoiceItemDTO[] items = invoiceItems.Select(mpi => new InvoiceService.InvoiceItemDTO()
            {
                invoiceItemDesc = mpi.GetStringTruncate("description", 40),
                itemAmountSpecified = true,
                itemAmount = decimal.Parse(mpi.GetString("value"), ci),
                acctDistrictCode = mpi.GetString("district"),
                authorisedBy = authBy,
                authorisedPosition = authPos,
                account = mpi.GetString("account"),
                //workOrder = mpi.GetString("workOrder"),
                //projectNo = mpi.GetString("project"),
                //equipmentNo = mpi.GetString("equipNo")
            }).ToArray();
            var dto = new InvoiceService.InvoiceDTO()
            {
                districtCode = mp.GetString("districtCode"),
                supplierNo = mp.GetString("supplierNo"),
                //invoiceNo = mp.GetString("invoiceNo").Substring(0, 10), //max 10 chars??
                externalInvoiceNo = mp.GetString("invoiceNo"),
                //invoiceAmountSpecified = true,
                //invoiceAmount = decimal.Parse(mp.GetString("invoiceAmt")),
                //branchCode = mp.IsNotEmpty("branchCode") ? mp.GetString("branchCode") : "",
                //bankAccountNumber = mp.IsNotEmpty("bankAccountNo") ? mp.GetString("bankAccountNo") : "",
                //authorisedBy
                //authorisedPosition
                accountant = mp.IsNotEmpty("accountant") ? mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"),
                currencyType = mp.GetString("currency"),
                commentType = mp.IsNotEmpty("invoiceCommentType") ? mp.GetString("invoiceCommentType") : "",
                //chequeHandleCode = mp.IsNotEmpty("handlingCode") ? mp.GetString("handlingCode") : "",
                invoiceDate = DateTime.Parse(mp.GetString("invoiceReceived"), parserFormat),
                invoiceDateSpecified = true,
                receiptDate = DateTime.Parse(mp.GetString("invoiceDate"), parserFormat),
                receiptDateSpecified = true,
                comments = comment,
                invoiceItem = items,
            };
            try // due date may be empty
            {
                dto.dueDate = DateTime.Parse(mp.GetString("dueDate"), parserFormat);
                dto.dueDateSpecified = true;
            }
            catch { }

            var res = svc.create(ctx, dto);
            var msg = PrintErrors(res);
            loggerinfo(string.Join(";", msg));
            if (res.errors.Length > 0)
            {
                throw new Exception(string.Join(";", msg));
            }
            if (msg.Length == 0 || !msg[0].Contains("was created"))
            {
                if (msg.Length > 0)
                {
                    throw new Exception("Unexpected message: " + msg[0]);
                }
                else
                {
                    throw new Exception("Expected message not found");
                }
            }

            var dtoread = new InvoiceService.InvoiceDTO()
            {
                districtCode = res.invoiceDTO.districtCode,
                supplierNo = res.invoiceDTO.supplierNo,
                invoiceNo = res.invoiceDTO.invoiceNo,
            };
            var resread = svc.read(ctx, dtoread);
            msg = PrintErrors(resread);
            if (res.errors.Length > 0)
            {
                throw new Exception(string.Join(";", msg));
            }
            return JObject.FromObject(new
            {
                Text = res.informationalMessages[0].messageText,
                FillingNo = resread.invoiceDTO.voucherNo,
                //FillingNo = res.invoiceDTO.invoiceNo,
            });
        }

        /// <summary>
        /// MSO265
        /// e9s
        /// internal function called by processloadnoithread
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="districtCode"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmt"></param>
        /// <param name="branchCode"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="accountant"></param>
        /// <param name="authBy"></param>
        /// <param name="positionId"></param>
        /// <param name="currency"></param>
        /// <param name="invoiceCommentType"></param>
        /// <param name="handlingCode"></param>
        /// <param name="invoiceReceived"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceItems"></param>
        /// <returns></returns>
        private JObject LoadNonOrderInvoiceSingleDo_screen(StructOperation op, MincomParameter mp)
        {
            E9Screen mobj = GetService<E9Screen>(op, mp);
            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
            //string districtCode, string supplierNo, string invoiceNo, string invoiceAmt,
            //string branchCode, string bankAccountNo, string accountant, string authBy, string positionId, string currency, string invoiceCommentType, string refOrgInvoiceNo, string handlingCode,
            //string invoiceReceived, string invoiceDate, string dueDate, LoadNonOrderItems[] invoiceItems
            mobj.GoHome();
            //LoadNonOrderResult result = new LoadNonOrderResult();

            mobj.ExecuteMSO("MSO265");
            mobj.CheckForScreen("MSM265A", true);
            string fillingNo = mobj.GetFieldValue("VCHR_NO1I");

            //key data MSM265A
            mobj.SetFieldValue("DSTRCT_CODE1I", mp.GetString("districtCode"));
            mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
            mobj.SetFieldValue("INV_NO1I", mp.GetString("invoiceNo"));
            mobj.SetFieldValue("INV_AMT1I", mp.GetString("invoiceAmt"));
            if (mp.GetString("branchCode").Trim().Length > 0)
            {
                mobj.SetFieldValue("BRANCH_CODE1I", mp.GetString("branchCode"));
            }
            if (mp.GetString("bankAccountNo").Trim().Length > 0)
            {
                mobj.SetFieldValue("BANK_ACCT_NO1I", mp.GetString("bankAccountNo"));
            }
            //if (accountant == null)
            //{
            //    accountant = ServiceHelper.GetConfig("Accountant");
            //}
            mobj.SetFieldValue("ACCOUNTANT1I", mp.IsNotEmpty("accountant") ?
                mp.GetString("accountant") : ServiceHelper.GetConfig("Accountant"));
            mobj.SetFieldValue("CURRENCY_TYPE1I", mp.GetString("currency"));
            if (mp.GetString("invoiceCommentType").Trim().Length > 0)
            {
                mobj.SetFieldValue("INV_COMM_TYPE1I", mp.GetString("invoiceCommentType"));
            }
            if (mp.GetString("handlingCode").Trim().Length > 0)
            {
                mobj.SetFieldValue("HANDLE_CDE1I", mp.GetString("handlingCode"));
            }
            //as of request 3 oct 2014 switch dates
            //mobj.SetFieldValue("INV_DATE1I", invoiceDate);
            //mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceReceived);
            mobj.SetFieldValue("INV_DATE1I", mp.GetString("invoiceReceived"));
            mobj.SetFieldValue("INV_RCPT_DATE1I", mp.GetString("invoiceDate"));
            mobj.SetFieldValue("DUE_DATE1I", mp.GetString("dueDate"));

            loggerinfo("Found item records: " + mp.Count("invoiceItems"));
            double sum = mp.GetArray("invoiceItems")
                .Sum(inv => double.Parse(inv.GetString("value"), NumberStyles.Any, ci));
            double amt = double.Parse(mp.GetString("invoiceAmt"), NumberStyles.Any, ci);
            string amountsum = string.Format("Invoice Amount: {0:N2} - Sum Values: {1:N2}", amt, sum);
            if (Math.Abs(amt - sum) < 0.01)
            {
                loggerinfo("Invoice amount match: " + amountsum);
            }
            else
            {
                throw new Exception("Invoice amount not match: " + amountsum);
            }

            int counter = 0;
            //if (authBy == null)
            //{
            //    authBy = ServiceHelper.GetConfig("Authorize");
            //}
            MincomParameter[] invoiceItems = mp.GetArray("invoiceItems");
            while (counter < invoiceItems.Length)
            {
                mobj.CheckForScreen("MSM265A", true);
                List<string> sblog = new List<string>();
                for (int i = 1; i <= 3; i++)
                {
                    if (counter + i > invoiceItems.Length)
                        break;
                    MincomParameter invoiceItem = invoiceItems[counter + i - 1];
                    sblog.Add("Set item " + mobj.GetFieldValue("INV_ITEM_NO1I" + i) + ":" + invoiceItem.GetString("description"));
                    mobj.SetFieldValue("INV_ITEM_DESC1I" + i, invoiceItem.GetString("description"));
                    mobj.SetFieldValue("INV_ITEM_VALUE1I" + i, invoiceItem.GetString("value"));
                    mobj.SetFieldValue("ACCT_DSTRCT1I" + i, invoiceItem.GetString("district"));
                    //mobj.SetFieldValue("AUTH_BY1I" + i, invoiceItem.authBy);
                    mobj.SetFieldValue("AUTH_BY1I" + i, mp.IsNotEmpty("authBy") ?
                        mp.GetString("authBy") : ServiceHelper.GetConfig("Authorize"));
                    mobj.SetFieldValue("ACCOUNT1I" + i, invoiceItem.GetString("account"));
                    mobj.SetFieldValue("WORK_ORDER1I" + i, invoiceItem.GetString("workOrder"));
                    mobj.SetFieldValue("PROJECT_IND1I" + i, invoiceItem.GetString("project"));
                    mobj.SetFieldValue("PLANT_NO1I" + i, invoiceItem.GetString("equipNo"));
                }
                loggerinfo(string.Join("\r\n", sblog.ToArray()));

                mobj.ExecuteCommand("OK");
                //                loggerinfo(mobj.DumpFields());
                while (mobj.CheckForScreen("MSM004A", false))
                {
                    string trans = mobj.GetFieldValue("TRANS_DETAIL1I").Trim();
                    trans = trans.Substring(trans.Length - 3);
                    int pos = 0;
                    int.TryParse(trans, out pos);
                    //if (positionId == null)
                    //{
                    //    positionId = ServiceHelper.GetConfig("AuthorizePositionID");
                    //}
                    string positionId = mp.IsNotEmpty("positionId") ?
                        mp.GetString("positionId") : ServiceHelper.GetConfig("AuthorizePositionID");
                    loggerinfo("Position ID:" + positionId);
                    //                    string authPosId = invoiceItems[pos].positionId.Trim();

                    if (!positionId.Equals(""))
                    {
                        List<string> posids = new List<string>();
                        for (int i = 1; i <= 13; i++)
                        {
                            string sp = "POSITION_ID1I" + i;
                            string cposid = mobj.GetFieldValue(sp).ToUpper().Trim();
                            posids.Add(cposid);
                            if (cposid.Equals(positionId.ToUpper().Trim()))
                            {
                                mobj.SetFieldValue("ITEM_NUM1I", "" + i);
                                mobj.ExecuteCommand("OK");
                                break;
                            }
                            if (i == 13)
                            {
                                string err = "No Position ID match MIMS Position ID found on screen MSM004A\r\n";
                                err += "Available: " + string.Join(",", posids.ToArray()) + "\r\n";
                                err += "Needed: " + positionId;
                                throw new Exception(err);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("No Position ID supplied");
                    }
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage() + " - Active Field: " + mobj.GetActiveField());
                }
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    if (mobj.GetActiveField().Equals("CURRENCY_TYPE1I"))
                    {
                        mobj.SetFieldValue(mobj.GetActiveField(), mp.GetString("currency"));
                    }
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                }
                if (mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                }

                counter += 3;
            }//end loop while
            mobj.ExecuteCommand("OK");

            if (mobj.CheckForScreen("MSM096B", false))
            {
                //key data MSM096B
                if (mobj.IsCommand("Edit Mode"))
                {
                    mobj.ExecuteCommand("Edit Mode");
                }
                else if (mobj.IsCommand("Entry Mode"))
                {
                    mobj.ExecuteCommand("Entry Mode");
                }
                else
                {
                    throw new Exception("No Edit/Entry Mode");
                }
                mobj.CheckForScreen("MSM096B", true);

                mobj.SetFieldValue("EDITCODE2I1", "COPY");
                //set ppn/pph, but is not, type PPH anyway
                mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");

                mobj.ExecuteCommand("OK");
                mobj.CheckForScreen("MSM096B", true);
                //set detail
                mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " ");
                mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " ");
                mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " "
                    + mp.GetString("refOrgInvoiceNo"));

                mobj.ExecuteCommand("OK");
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Exit");
                }
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Revert");
                }
            }

            JObject result = null;

            if (mobj.CheckForScreen("MSM265A", false))
            {
                for (int i = 0; i < 4; i++)
                {
                    if (mobj.GetErrorMessage().Trim().Length > 0)
                    {
                        if (Regex.IsMatch(mobj.GetErrorMessage(), "invoice.*?loaded\\s+(un)?approved",
                            RegexOptions.IgnoreCase | RegexOptions.Singleline))
                        {
                            result = JObject.FromObject(new
                            {
                                Text = mobj.GetErrorMessage(),
                                FillingNo = fillingNo,
                            });
                            break;
                        }
                        else
                        {
                            throw new Exception("Error: " + mobj.GetErrorMessage());
                        }
                    }
                    else if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }
                    else if (mobj.IsCommand("OK"))
                    {
                        mobj.ExecuteCommand("OK");
                    }
                }//end loop for
            }
            else
            {
                throw new Exception("Error: Invoice not loaded, screen: " + mobj.GetScreenName());
            }
            //            loggerinfo(mobj.DumpFields());

            return result;
        }


        /// <summary>
        /// MSO003 - ScreenService - deprecated, direct call
        /// </summary>
        /// <returns></returns>
        private string GetBankAccount_dep(StructOperation op, MincomParameter mp)
        {
            var ctx = GetContext<ScreenService.OperationContext>(op, mp);
            var svc = GetService<ScreenService.ScreenService>(op, mp);
            svc.positionToMenu(ctx);
            var scr = svc.executeScreen(ctx, "MSO003");
            Dictionary<string, string> fields = scr.screenFields.ToDictionary(a => a.fieldName, a => a.value);

            ArrayList accts = new ArrayList();

            int maxpage = 100; //max page=100
            for (int page = 1; page <= maxpage; page++)
            {
                if (fields["SKLDISPLAY1I"].Trim().Equals(string.Format("{0:000}/{0:000}", page)))
                {
                    //last page detected
                    maxpage = page;
                }
                else if (!fields["SKLDISPLAY1I"].Trim().Equals(string.Format("{0:000}", page)))
                {
                    throw new Exception("SKLDISPLAY1I not match");
                }
                for (int i = 1; i <= 5; i++)
                {
                    if (fields["BRANCH_CODE1I" + i].Trim().Equals(""))
                    {
                        maxpage = page;
                        break;
                    }

                    accts.Add(new
                    {
                        BranchCode = fields["BRANCH_CODE1I" + i],
                        BranchName = fields["BRANCH_NAME1I" + i],
                        OwnerDistrict = fields["OWNER_DSTRCT1I" + i],
                        CurrencyType = fields["CURRENCY_TYPE1I" + i],
                        AccountNo = fields["BANK_ACCT_NO1I" + i],
                        AccountName = fields["ACCOUNT_NAME1I" + i]
                    });
                }
                if (maxpage > page)
                {
                    //check next page
                    var dto = new ScreenService.ScreenSubmitRequestDTO()
                    {
                        screenKey = "1" //ok
                    };
                    scr = svc.submit(ctx, dto);
                    fields = scr.screenFields.ToDictionary(a => a.fieldName, a => a.value);
                }
            }

            JObject ret = JObject.FromObject(new
            {
                Accounts = accts.ToArray()
            });
            svc.positionToMenu(ctx);

            return ret.ToString(Formatting.None);
        }

    }
}
