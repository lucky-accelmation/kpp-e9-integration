﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using E9Integration.Classes;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using log4net;
using System.Threading;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace E9Integration.Libraries
{
    public class MincomFunctions
    {
        #region variables
        private static readonly ILog logger =
            LogManager.GetLogger(typeof(MincomFunctions));

        object threadid = 0;
        private object result = null; //child thread result
        private bool skipcheckscreen = false;
        private void loggerinfo(string message)
        {
            string stringid = threadid.ToString();
            if (stringid != null && stringid.Length > 8) stringid = stringid.Substring(0, 8);
            logger.InfoFormat("#{0}: {1}", stringid, message);
        }

        string DefaultLogin = "DefaultMincomConnectionAdmin";
        delegate string OperationDelegate(MincomObject mobj, MincomParameter mp);

        struct StructOperation
        {
            public string login;
            public bool overrideDistrict;
            public int priority;
            public OperationDelegate operation;
            public StructOperation(OperationDelegate operation, string login = "DefaultMincomConnectionAdmin", bool overrideDistrict = false, int priority = 0)
            {
                this.operation = operation;
                this.login = login;
                this.overrideDistrict = overrideDistrict;
                this.priority = priority;
            }
        }

        static Dictionary<string, StructOperation> Operations = null;
        JobProcess job = JobProcess.GetJobProcess();

        public MincomFunctions()
        {
            Operations = new Dictionary<string, StructOperation>()
            {
                {"GetPurchaseOrder",new StructOperation(GetPurchaseOrder)},
                {"PreloadInvoice",new StructOperation(PreloadInvoice,"DefaultMincomConnectionUser")},
                {"LoadInvoice",new StructOperation(LoadInvoice,"DefaultMincomConnectionUser")},
                {"ApproveInvoice",new StructOperation(ApproveInvoice)},
                {"ApproveInvoiceMulti",new StructOperation(ApproveInvoiceMulti)},
                {"UploadPayment",new StructOperation(UploadPayment,"DefaultMincomConnectionAdmin",true)},
                {"GetVendorInfo",new StructOperation(GetVendorInfo)},
                {"GetReportText",new StructOperation(GetReportText)},
                {"UploadUnbudget",new StructOperation(UploadUnbudget)},
                {"GetBankAccounts",new StructOperation(GetBankAccount)},
                {"LoadNonOrderInvoice",new StructOperation(LoadNonOrderInvoice,"DefaultMincomConnectionUser")},
                {"LoadNonOrderInvoiceSingle",new StructOperation(LoadNonOrderInvoiceSingle,"DefaultMincomConnectionUser",false,1)},
                {"CancelInvoice",new StructOperation(CancelInvoice)},
                {"CreatePurchaseOrder",new StructOperation(CreatePurchaseOrder,"DefaultMincomConnectionCreator")},
                {"ReceivePurchaseOrder",new StructOperation(ReceivePurchaseOrder,"DefaultMincomConnectionCreator")},
                {"UploadSundries",new StructOperation(UploadSundries,"DefaultMincomConnectionAdmin",true)},
                {"UploadMiscCashReceipt",new StructOperation(UploadMiscCashReceipt,"DefaultMincomConnectionAdmin",true)},
                {"CreatePurchaseRequisition",new StructOperation(CreatePurchaseRequisition,"DefaultMincomConnectionAdmin",true)},
                {"CancelPurchaseRequisition",new StructOperation(CancelPurchaseRequisition,"DefaultMincomConnectionAdmin",true)},
                {"ApprovePurchaseRequisition",new StructOperation(ApprovePurchaseRequisition,"DefaultMincomConnectionAdmin")},
            };
        }

        #endregion variables

        #region public functions

        public string GetOverrideDistrict(string operation, string input)
        {
            string district = null;

            if (Operations.ContainsKey(operation) && Operations[operation].overrideDistrict)
            {
                try
                {
                    //JToken tok = JToken.Parse(input);
                    //district = tok["overrideDistrict"].Value<string>();
                    MincomParameter mp = new MincomParameter(input);
                    district = mp.GetString("overrideDistrict");
                }
                catch (Exception ex)
                {
                    logger.Info("Override district failed, using default district: " + ex.Message);
                    //                    throw ex;
                }
            }

            return district;
        }

        public bool IsValidOperation(string operation)
        {
            if (operation != null && !Operations.ContainsKey(operation))
            {
                return false;
            }
            return true;
        }

        public int GetOperationPriority(string operation)
        {
            if (Operations.ContainsKey(operation))
            {
                return Operations[operation].priority;
            }
            return 0;
        }

        public string GetLoginJob(string operation)
        {
            if (IsValidOperation(operation))
            {
                return Operations[operation].login;
            }
            return DefaultLogin;
        }


        public string ProcessJob(MincomObject mobj, object threadid, string operation, string input)
        {
            string result = null;
            this.threadid = threadid;

            if (!IsValidOperation(operation))
            {
                throw new Exception("Invalid operation.");
            }

            if (Operations[operation].operation == null)
            {
                //throw new Exception("Operation is not set.");
                ProcessJob_deprecated(mobj, threadid, operation, input);
            }

            try
            {
                result = Operations[operation].operation(mobj, new MincomParameter(input));
            }
            catch (Exception ex)
            {
                string screen = "-";
                string field = "-";
                if (!skipcheckscreen)
                {
                    loggerinfo("Checking screen, may cause system crash");
                    try
                    {
                        if (mobj.IsConnected())
                        {
                            screen = mobj.GetScreenName();
                            field = mobj.GetActiveField();
                        }
                    }
                    catch (Exception exx)
                    {
                        loggerinfo("Error checking screen " + exx.Message);
                    }
                }
                else
                {
                    loggerinfo("Skip check screen");
                }
                loggerinfo("Error at " + screen);
                throw new Exception(ex.Message + " at " + screen + " Field " + field, ex);
                //throw ex;
            }

            return result;
        }

        public string ProcessJob_deprecated(MincomObject mobj, object threadid, string operation, string input)
        {
            string result = null;
            this.threadid = threadid;

            if (!IsValidOperation(operation))
            {
                throw new Exception("Invalid operation.");
            }

            dynamic jsd = null;
            try
            {
                jsd = JsonHelper.GetJsonObject(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //if (operation.Equals("ApproveInvoice"))
            //{
            //    JobSplit[] ids = SqlJobProcess.GetJobSplit("LoadNonOrderInvoice", jsd.invoiceNo);
            //    if (ids == null)
            //    {
            //        ids = new JobSplit[] { new JobSplit(jsd.invoiceNo, null) };
            //    }
            //    ApproveInvoiceResult resjoin = new ApproveInvoiceResult();
            //    for (int i = 0; i < ids.Length; i++)
            //    {
            //        loggerinfo("Approve Invoice: " + ids[i].id);
            //        ApproveInvoiceResult res = ApproveInvoiceDo(mobj, jsd.district, jsd.supplierNo, ids[i].id, jsd.authBy, jsd.positionId);
            //        if (resjoin.Message != null)
            //        {
            //            resjoin.AccountantCode += ";";
            //            resjoin.AccountantName += ";";
            //            resjoin.AccountName += ";";
            //            resjoin.ApprovalStatus += ";";
            //            resjoin.ApprovalStatusDesc += ";";
            //            resjoin.ApproveItemsResult += ";";
            //            resjoin.BankAccount += ";";
            //            resjoin.Currency += ";";
            //            resjoin.DueDate += ";";
            //            resjoin.InvoiceAmountAmend += ";";
            //            resjoin.InvoiceAmountOrig += ";";
            //            resjoin.InvoiceDate += ";";
            //            resjoin.InvoiceType += ";";
            //            resjoin.InvoiceTypeDesc += ";";
            //            resjoin.Message += ";";
            //            resjoin.PaymentStatus += ";";
            //            resjoin.PaymentStatusDesc += ";";
            //            resjoin.PurchaseOrderNo += ";";
            //            resjoin.VoucherNo += ";";
            //        }
            //        resjoin.AccountantCode += res.AccountantCode;
            //        resjoin.AccountantName += res.AccountantName;
            //        resjoin.AccountName += res.AccountName;
            //        resjoin.ApprovalStatus += res.ApprovalStatus;
            //        resjoin.ApprovalStatusDesc += res.ApprovalStatusDesc;
            //        resjoin.ApproveItemsResult += res.ApproveItemsResult;
            //        resjoin.BankAccount += res.BankAccount;
            //        resjoin.Currency += res.Currency;
            //        resjoin.DueDate += res.DueDate;
            //        resjoin.InvoiceAmountAmend += res.InvoiceAmountAmend;
            //        resjoin.InvoiceAmountOrig += res.InvoiceAmountOrig;
            //        resjoin.InvoiceDate += res.InvoiceDate;
            //        resjoin.InvoiceType += res.InvoiceType;
            //        resjoin.InvoiceTypeDesc += res.InvoiceTypeDesc;
            //        resjoin.Message += res.Message;
            //        resjoin.PaymentStatus += res.PaymentStatus;
            //        resjoin.PaymentStatusDesc += res.PaymentStatusDesc;
            //        resjoin.PurchaseOrderNo += res.PurchaseOrderNo;
            //        resjoin.VoucherNo += res.VoucherNo;
            //    }
            //    result = JsonHelper.GetJsonString(resjoin);
            //}
            //if (operation.Equals("ApproveInvoiceMulti"))
            //{
            //    //multi district
            //    JobSplit[] ids = SqlJobProcess.GetJobSplit("LoadNonOrderInvoice", jsd.invoiceNo);
            //    if (ids == null)
            //    {
            //        ids = new JobSplit[] { new JobSplit(jsd.invoiceNo, null) };
            //    }

            //    List<string> lsdistrict = new List<string>();
            //    foreach (string d in jsd.district)
            //    {
            //        lsdistrict.Add(d);
            //    }
            //    ApproveInvoiceResult resjoin = new ApproveInvoiceResult();
            //    for (int i = 0; i < ids.Length; i++)
            //    {
            //        loggerinfo("Approve Invoice Multi: " + ids[i].id);
            //        ApproveInvoiceResult res = ApproveInvoiceMultiDo(mobj, lsdistrict.ToArray(), jsd.supplierNo, ids[i].id, jsd.authBy, jsd.positionId);
            //        if (resjoin.Message != null)
            //        {
            //            resjoin.AccountantCode += ";";
            //            resjoin.AccountantName += ";";
            //            resjoin.AccountName += ";";
            //            resjoin.ApprovalStatus += ";";
            //            resjoin.ApprovalStatusDesc += ";";
            //            resjoin.ApproveItemsResult += ";";
            //            resjoin.BankAccount += ";";
            //            resjoin.Currency += ";";
            //            resjoin.DueDate += ";";
            //            resjoin.InvoiceAmountAmend += ";";
            //            resjoin.InvoiceAmountOrig += ";";
            //            resjoin.InvoiceDate += ";";
            //            resjoin.InvoiceType += ";";
            //            resjoin.InvoiceTypeDesc += ";";
            //            resjoin.Message += ";";
            //            resjoin.PaymentStatus += ";";
            //            resjoin.PaymentStatusDesc += ";";
            //            resjoin.PurchaseOrderNo += ";";
            //            resjoin.VoucherNo += ";";
            //        }
            //        resjoin.AccountantCode += res.AccountantCode;
            //        resjoin.AccountantName += res.AccountantName;
            //        resjoin.AccountName += res.AccountName;
            //        resjoin.ApprovalStatus += res.ApprovalStatus;
            //        resjoin.ApprovalStatusDesc += res.ApprovalStatusDesc;
            //        resjoin.ApproveItemsResult += res.ApproveItemsResult;
            //        resjoin.BankAccount += res.BankAccount;
            //        resjoin.Currency += res.Currency;
            //        resjoin.DueDate += res.DueDate;
            //        resjoin.InvoiceAmountAmend += res.InvoiceAmountAmend;
            //        resjoin.InvoiceAmountOrig += res.InvoiceAmountOrig;
            //        resjoin.InvoiceDate += res.InvoiceDate;
            //        resjoin.InvoiceType += res.InvoiceType;
            //        resjoin.InvoiceTypeDesc += res.InvoiceTypeDesc;
            //        resjoin.Message += res.Message;
            //        resjoin.PaymentStatus += res.PaymentStatus;
            //        resjoin.PaymentStatusDesc += res.PaymentStatusDesc;
            //        resjoin.PurchaseOrderNo += res.PurchaseOrderNo;
            //        resjoin.VoucherNo += res.VoucherNo;
            //    }
            //    result = JsonHelper.GetJsonString(resjoin);
            //}
            //if (operation.Equals("UploadPayment"))
            //{
            //    List<UploadPaymentItem> invoices = new List<UploadPaymentItem>();
            //    foreach (dynamic invoice in jsd.invoiceItems)
            //    {
            //        JobSplit[] splitids = SqlJobProcess.GetJobSplit("LoadNonOrderInvoice", invoice["invoiceNo"]);
            //        if (splitids == null || splitids.Length == 1)
            //        {
            //            splitids = new JobSplit[] { new JobSplit(invoice["invoiceNo"], string.Format("{0}", invoice["amount"])) };
            //        }
            //        else
            //        {
            //            loggerinfo(string.Format("Job split found, invoice: {0} count: {1}", invoice["invoiceNo"], splitids.Length));
            //        }
            //        foreach (JobSplit split in splitids)
            //        {
            //            UploadPaymentItem item = new UploadPaymentItem();
            //            item.invoiceNo = split.id;
            //            item.district = invoice["district"];
            //            item.accountCode = invoice["accountCode"];
            //            CultureInfo cien = CultureInfo.CreateSpecificCulture("en-US");
            //            double.TryParse(split.amount, System.Globalization.NumberStyles.Any, cien, out item.amount);
            //            if (item.amount == 0)
            //            {//try id format
            //                CultureInfo ciid = CultureInfo.CreateSpecificCulture("id-ID");
            //                double.TryParse(split.amount, System.Globalization.NumberStyles.Any, ciid, out item.amount);
            //            }
            //            invoices.Add(item);
            //        }
            //    }

            //    result = UploadPaymentDo(mobj, jsd.bankBranch, jsd.bankAccount, jsd.supplierNo, string.Format("{0}", jsd.totalAmount),
            //        jsd.date, jsd.currency, jsd.description, invoices.ToArray());
            //}
            //if (operation.Equals("LoadNonOrderInvoiceSingle"))
            //{
            //    List<LoadNonOrderItems> itemList = new List<LoadNonOrderItems>();
            //    try
            //    {
            //        foreach (dynamic item in jsd.items)
            //        {
            //            LoadNonOrderItems itm = new LoadNonOrderItems();
            //            itm.description = item["description"];
            //            itm.value = item["value"];
            //            itm.district = item["district"];
            //            itm.authBy = item["authBy"];
            //            itm.account = item["account"];
            //            itm.workOrder = item["workOrder"];
            //            itm.project = item["project"];
            //            itm.equipNo = item["equipNo"];
            //            itm.positionId = item["positionId"];
            //            itemList.Add(itm);
            //        }
            //    }
            //    catch (Exception ex) { loggerinfo("Error parsing: " + ex.Message); }

            //    //split if item > 500
            //    int splitSize = 500;
            //    int.TryParse(ServiceHelper.GetConfig("ChildThreadMaxItem"), out splitSize);

            //    List<Thread> threads = new List<Thread>();
            //    List<MincomFunctions> childs = new List<MincomFunctions>();
            //    List<object> prms = new List<object>();
            //    int tcnt = 0;
            //    for (int i = 0; i < itemList.Count; i += splitSize)
            //    {
            //        int size = (itemList.Count >= i + splitSize) ? splitSize : itemList.Count - i;
            //        List<LoadNonOrderItems> itemsTemp = itemList.GetRange(i, size);
            //        LoadNonOrderItems[] items = itemsTemp.ToArray();
            //        string invoiceNo = jsd.invoiceNo;
            //        string invoiceAmt = jsd.invoiceAmt;
            //        if (itemList.Count > splitSize)
            //        {
            //            invoiceNo = string.Format("{0}-{1}", invoiceNo, (i / splitSize) + 1);
            //            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
            //            double sum = 0;
            //            for (int j = 0; j < items.Length; j++)
            //            {
            //                double value = 0;
            //                double.TryParse(items[j].value, System.Globalization.NumberStyles.Any, ci, out value);
            //                sum += value;
            //            }
            //            invoiceAmt = string.Format(ci, "{0:N2}", sum);
            //            //invoiceAmt = sum.ToString(ci);
            //            loggerinfo(string.Format("Split {0}: {1} amount {2}", i, invoiceNo, invoiceAmt));
            //        }
            //        Thread t = new Thread(new ParameterizedThreadStart(ProcessLoadNonOrderInvoiceThread));
            //        threads.Add(t);
            //        object[] prm ={jsd.districtCode, jsd.supplierNo, invoiceNo, invoiceAmt,
            //            jsd.branchCode, jsd.bankAccountNo, jsd.accountant, jsd.authBy, jsd.positionId, jsd.currency, jsd.invoiceCommentType, jsd.refOrgInvoiceNo, jsd.handlingCode,
            //            jsd.invoiceReceived, jsd.invoiceDate, jsd.dueDate, items};
            //        //LoadNonOrderInvoiceSingle(mobj, jsd.supplierNo, invoiceNo, invoiceAmt,
            //        //    jsd.branchCode, jsd.bankAccountNo, jsd.accountant, jsd.authBy, jsd.positionId, jsd.currency, jsd.invoiceCommentType, jsd.refOrgInvoiceNo, jsd.handlingCode,
            //        //    jsd.invoiceReceived, jsd.invoiceDate, jsd.dueDate, items);
            //        prms.Add(prm);
            //        MincomFunctions child = new MincomFunctions();
            //        childs.Add(child);
            //        child.threadid = this.threadid.ToString() + "_" + (i / splitSize).ToString();
            //        //start the thread 1 by 1
            //        t.Start(new object[]{
            //            child,
            //            prm
            //        });
            //        tcnt++;
            //    }
            //    string textResult = "";
            //    string fillingNoResult = "";
            //    string errormessage = null;
            //    int[] tstatus = new int[tcnt];
            //    for (int i = 0; i < tcnt; i++)
            //    {
            //        tstatus[i] = -1;
            //        //wait thread 1 by 1
            //        threads[i].Join();
            //        object[] childout = (object[])childs[i].result;
            //        LoadNonOrderResult r = null;
            //        Exception e = null;
            //        //childout 0 result obj, 1 error obj
            //        if (childout != null && childout[0] != null)
            //        {
            //            //success
            //            r = (LoadNonOrderResult)childout[0];
            //            if (i > 0)
            //            {
            //                fillingNoResult += ";";
            //                textResult += ";";
            //            }
            //            fillingNoResult += r.FillingNo;
            //            textResult += r.Text;
            //            tstatus[i] = 1;
            //        }
            //        if (childout != null && childout[1] != null)
            //        {
            //            //error
            //            e = (Exception)childout[1];
            //            errormessage = e.Message;
            //            tstatus[i] = 0;
            //        }
            //    }

            //    //errormessage = null;

            //    if (errormessage == null)
            //    {
            //        //everything ok, no error
            //        //now create job split
            //        JobSplit[] ids = new JobSplit[tcnt];
            //        for (int i = 0; i < tcnt; i++)
            //        {
            //            object[] prm = (object[])prms[i];
            //            ids[i] = new JobSplit((string)prm[2], (string)prm[3]);
            //            loggerinfo(string.Format("Jobsplit {0} {1}", prm[2], prm[3]));
            //        }
            //        SqlJobProcess.CreateJobSplit("", "LoadNonOrderInvoice", jsd.invoiceNo, ids);
            //        LoadNonOrderResult rs = new LoadNonOrderResult();
            //        rs.Text = textResult;
            //        rs.FillingNo = fillingNoResult;
            //        result = JsonHelper.GetJsonString(rs);
            //    }
            //    else
            //    {
            //        //there is error
            //        //cancel all loaded invoices
            //        for (int i = 0; i < tcnt; i++)
            //        {
            //            if (tstatus[i] == 1)
            //            {
            //                //cancel loaded
            //                object[] prm = (object[])prms[i];
            //                string cancelresult = CancelInvoiceDo(mobj, (string)prm[0], (string)prm[1], (string)prm[2]);
            //                loggerinfo("Cancel invoice #" + prm[2]);
            //                loggerinfo(cancelresult);
            //            }
            //        }
            //        throw new Exception(errormessage);
            //    }
            //}
            if (operation.Equals("LoadNonOrderInvoiceSingleNoSplit")) //is this used?
            {
                List<LoadNonOrderItems> itemList = new List<LoadNonOrderItems>();
                try
                {
                    foreach (dynamic item in jsd.items)
                    {
                        LoadNonOrderItems itm = new LoadNonOrderItems();
                        itm.description = item["description"];
                        itm.value = item["value"];
                        itm.district = item["district"];
                        itm.authBy = item["authBy"];
                        itm.account = item["account"];
                        itm.workOrder = item["workOrder"];
                        itm.project = item["project"];
                        itm.equipNo = item["equipNo"];
                        itm.positionId = item["positionId"];
                        itemList.Add(itm);
                    }
                }
                catch (Exception ex) { loggerinfo("Error parsing: " + ex.Message); }
                LoadNonOrderItems[] items = itemList.ToArray();
                LoadNonOrderResult r = LoadNonOrderInvoiceSingleDo(mobj, jsd.districtCode, jsd.supplierNo, jsd.invoiceNo, jsd.invoiceAmt,
                    jsd.branchCode, jsd.bankAccountNo, jsd.accountant, jsd.authBy, jsd.positionId, jsd.currency, jsd.invoiceCommentType, jsd.refOrgInvoiceNo, jsd.handlingCode,
                    jsd.invoiceReceived, jsd.invoiceDate, jsd.dueDate, items);
                result = JsonHelper.GetJsonString(r);
            }

            return result;
        }

        #endregion public functions

        /// <summary>
        /// MSO263
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <returns></returns>
        private static string GetPurchaseOrder(MincomObject mobj, MincomParameter mp)
        {
            string purchaseNo = mp.GetString("purchaseNo");
            if (purchaseNo == null || purchaseNo.Trim().Length == 0)
            {
                throw new Exception("Purchase number is empty.");
            }

            mobj.GoHome();

            mobj.ExecuteMSO("MSO263");
            mobj.CheckForScreen("MSM263A", true);

            mobj.SetFieldValue("OPTION1I", "5");
            mobj.SetFieldValue("PO_NO1I", purchaseNo);
            mobj.ExecuteCommand("OK");
            mobj.CheckForScreen("MSM263D", true);

            JObject res = JObject.FromObject(new
            {
                InvoiceNo = mobj.GetFieldValue("INV_NO4I1").Trim(),
                InvoiceDate = mobj.GetFieldValue("INV_DATE4I1").Trim(),
                PurchaseOrderNo = mobj.GetFieldValue("PO_NO4I1").Trim(),
                InvoiceValue = mobj.GetFieldValue("LOC_VAL_INVD4I1").Trim()
            });
            mobj.ExecuteCommand("Home");

            return res.ToString(Formatting.None);
        }

        /// <summary>
        /// MSO201
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="supplierNo"></param>
        /// <param name="district"></param>
        /// <returns></returns>
        private static string GetVendorInfo(MincomObject mobj, MincomParameter mp)
        {
            mobj.GoHome();

            mobj.ExecuteMSO("MSO201");
            mobj.CheckForScreen("MSM201A", true);
            mobj.SetFieldValue("OPTION1I", "3");
            mobj.SetFieldValue("SUPPLIER_NO1I", mp.GetString("supplierNo"));
            mobj.SetFieldValue("DSTRCT_CODE1I", mp.GetString("district"));
            mobj.ExecuteCommand("OK");
            mobj.CheckForScreen("MSM201D", true);

            JObject res = JObject.FromObject(new
            {
                Name = mobj.GetFieldValue("SUPPLIER_NAME4I"),
                Status = mobj.GetFieldValue("SUP_STATUS4I"),
                StatusLit = mobj.GetFieldValue("SUP_STATUS_LIT4I"),
                DistrictName = mobj.GetFieldValue("DSTRCT_NAME4I"),
                DaysPayment = mobj.GetFieldValue("NO_OF_DAYS_PAY4I"),
                Statement = mobj.GetFieldValue("INV_STMT_IND4I"),
                StatementLit = mobj.GetFieldValue("L_INV_STMT_IND4I")
            });

            mobj.ExecuteCommand("OK");

            return res.ToString(Formatting.None);
        }

        private void ProcessLoadNonOrderInvoiceThread(object param)
        {
            object[] prm = (object[])param;
            MincomFunctions child = (MincomFunctions)prm[0];
            object[] mfprm = (object[])prm[1];

            object[] result = new object[2];
            //0 result object
            //1 exception object
            MincomObject MObj = null;
            try
            {
                string cn = GetLoginJob("LoadNonOrderInvoice");
                MObj = MincomWorker.DoLogin(child.threadid, cn);
                result[0] = child.LoadNonOrderInvoiceSingleDo(MObj,
                    (string)mfprm[0], (string)mfprm[1], (string)mfprm[2], (string)mfprm[3], (string)mfprm[4],
                    (string)mfprm[5], (string)mfprm[6], (string)mfprm[7], (string)mfprm[8], (string)mfprm[9],
                    (string)mfprm[10], (string)mfprm[11], (string)mfprm[12], (string)mfprm[13], (string)mfprm[14],
                    (string)mfprm[15], (LoadNonOrderItems[])mfprm[16]);

                MincomWorker.DoDisconnect(MObj);
            }
            catch (Exception ex)
            {
                logger.InfoFormat("#{0}: Error {1}", child.threadid, ex.Message);
                logger.InfoFormat("#{0}: {1}", child.threadid, ex.StackTrace);
                result[1] = ex;
            }
            finally
            {
                MincomWorker.DoDisconnect(MObj);
            }

            child.result = result;
        }


        public class InvoiceItem
        {
            //public string ReceiptNo;
            //public string ReceiptDate;
            //public string ReceiptReference;
            //public string ItemValue;
            //public string TaxValue;
            public string PoItemNo;
            public string Description;
            public string Qty;
            public string Price;
            public string Value;
            public string Tax;
            public string Discount;
            public string Uop;
            public string ItemType;
            public string TaxCode;
            public string Action;
        }

        public class PreloadInvoiceResult
        {
            public InvoiceItem[] Items = null;
        }

        /// <summary>
        /// MSO260 Preload invoice for returning item details
        /// same as loadinvoice, except this doesnt match each invoice item's action
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <param name="accountant"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="invoiceReceiptDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmount"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        private string PreloadInvoice(MincomObject mobj, MincomParameter mp)
        {
            //    result = PreloadInvoiceDo(mobj, jsd.purchaseNo, jsd.accountant, jsd.invoiceDate, jsd.invoiceReceiptDate, jsd.dueDate,
            //        jsd.invoiceNo, jsd.invoiceAmount, jsd.currency);
            string purchaseNo = mp.GetString("purchaseNo");
            string accountant = mp.GetString("accountant");
            string invoiceDate = mp.GetString("invoiceDate");
            string invoiceReceiptDate = mp.GetString("invoiceReceiptDate");
            string dueDate = mp.GetString("dueDate");
            string invoiceNo = mp.GetString("invoiceNo");
            string invoiceAmount = mp.GetString("invoiceAmount");
            string currency = mp.GetString("currency");

            //all date field format dd/mm/yy
            string district = null;
            string supplier = null;
            string payment = null;

            if (purchaseNo == null || invoiceDate == null || invoiceReceiptDate == null
                || dueDate == null || invoiceNo == null || invoiceAmount == null
                || currency == null)
            {
                throw new Exception("One or more parameter is empty.");
            }

            if (purchaseNo.Trim().Equals("") || invoiceDate.Trim().Equals("") || invoiceReceiptDate.Trim().Equals("")
                || dueDate.Trim().Equals("") || invoiceNo.Trim().Equals("") || invoiceAmount.Trim().Equals("")
                || currency.Trim().Equals(""))
            {
                throw new Exception("One or more parameter is empty.");
            }

            mobj.GoHome();

            mobj.ExecuteMSO("MSO260");
            if (!mobj.CheckForScreen("MSM260A", false))
            {
                throw new Exception("Screen not match: " + mobj.GetScreenName());
            }
            mobj.SetFieldValue("PO_NO1I", purchaseNo);
            mobj.ExecuteCommand("OK");
            if (mobj.GetErrorMessage().StartsWith("X2:0011 - INPUT REQUIRED") && mobj.GetActiveField().Equals("INV_NO1I"))
            {
                district = mobj.GetFieldValue("DSTRCT_CODE1I");
                supplier = mobj.GetFieldValue("SUPPLIER_NO1I");
                payment = mobj.GetFieldValue("PAYMENT_NAME1I");
            }
            else
            {
                throw new Exception("PreMSM260A: " + mobj.GetErrorMessage());
            }
            if (!mobj.GetFieldValue("CURRENCY_TYPE1I").Trim().Equals(currency))
            {
                throw new Exception("PreMSM260A: PO Currency is in " + mobj.GetFieldValue("CURRENCY_TYPE1I"));
            }

            if (accountant == null)
            {
                accountant = ServiceHelper.GetConfig("Accountant");
            }

            mobj.SetFieldValue("ACCOUNTANT1I", accountant);
            mobj.SetFieldValue("DSP_ITEMS1I", "Y");
            mobj.SetFieldValue("INV_DATE1I", invoiceDate); //dd/mm/yyyy
            mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceReceiptDate);
            mobj.SetFieldValue("DUE_DATE1I", dueDate);
            mobj.SetFieldValue("INV_NO1I", invoiceNo);
            mobj.SetFieldValue("INV_AMT1I", invoiceAmount);
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);

            mobj.ExecuteCommand("OK");

            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception("MSM260A: " + mobj.GetErrorMessage() + " - Active field: " + mobj.GetActiveField());
            }
            if (mobj.GetErrorMessage().StartsWith("W2"))
            {
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM260A: " + mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.CheckForScreen("MSM260B", false))
            {
                mobj.ExecuteCommand("Match with order");
            }

            //          loggerinfo(mobj.DumpFields());
            PreloadInvoiceResult result = null;
            if (!mobj.CheckForScreen("MSM26CA", false))
            {
                throw new Exception("Screen not match: " + mobj.GetScreenName());
            }
            result = new PreloadInvoiceResult();
            List<InvoiceItem> items = new List<InvoiceItem>();

            bool next = true;
            while (next)
            {
                for (int i = 1; i <= 3; i++)
                {
                    InvoiceItem item = new InvoiceItem();
                    //item.ReceiptNo = mobj.GetFieldValue("RECEIPT_NO2I" + i);
                    //item.ReceiptDate = mobj.GetFieldValue("RECEIPT_DATE2I" + i);
                    //item.ReceiptReference = mobj.GetFieldValue("RECEIPT_REF2I" + i);
                    //item.ItemValue = mobj.GetFieldValue("VALUE_NOT_INV2I" + i);
                    //item.TaxValue = mobj.GetFieldValue("ATAX_VALUE2I" + i);

                    if (!mobj.GetFieldValue("PO_ITEM1I" + i).Trim().Equals(""))
                    {
                        item.PoItemNo = mobj.GetFieldValue("PO_ITEM1I" + i);
                        item.Description = mobj.GetFieldValue("DESCRIPTION1I" + i);
                        item.Qty = mobj.GetFieldValue("QTY_NOT_INV_P1I" + i);
                        item.Price = mobj.GetFieldValue("GROSS_PRICE_P1I" + i);
                        item.Value = mobj.GetFieldValue("RCPT_ITM_VALUE1I" + i);
                        item.Tax = mobj.GetFieldValue("ITM_TAX_LIT1I" + i);
                        item.Discount = mobj.GetFieldValue("DISCOUNT1I" + i);
                        item.Uop = mobj.GetFieldValue("UOP1I" + i);
                        item.ItemType = mobj.GetFieldValue("PO_ITEM_TYPE1I" + i);
                        item.TaxCode = mobj.GetFieldValue("ATAX_CODE_ITM1I" + i);
                        items.Add(item);
                    }
                    else
                    {
                        next = false;
                    }
                }
                if (next)
                {
                    mobj.ExecuteCommand("OK");
                    if (!mobj.GetErrorMessage().Trim().Equals(""))
                    {
                        break;
                    }
                }
            }
            result.Items = items.ToArray();
            mobj.ExecuteCommand("Home");

            return JsonHelper.GetJsonString(result);
        }

        public class LoadInvoiceResult
        {
            public string PurchaseNo;
            public string InvoiceNo;
            public string Message;
            public string District;
            public string Supplier;
            public string Payment;
            public InvoiceItem[] Items = null;
        }

        /// <summary>
        /// MSO260 *used
        /// match an order with invoice each item's action
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <param name="accountant"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="invoiceReceiptDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmount"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        private string LoadInvoice(MincomObject mobj, MincomParameter mp)
        {
            List<InvoiceItem> itemAction = new List<InvoiceItem>();
            try
            {
                for (int i = 0; i < mp.Count("items"); i++)
                {
                    MincomParameter mpi = mp.Get("items[" + i + "]");
                    InvoiceItem invitm = new InvoiceItem();
                    invitm.PoItemNo = mpi.GetString("PoItemNo");
                    invitm.Description = mpi.GetString("Description");
                    invitm.Qty = mpi.GetString("Qty");
                    invitm.Price = mpi.GetString("Price");
                    invitm.Value = mpi.GetString("Value");
                    invitm.Tax = mpi.GetString("Tax");
                    invitm.Discount = mpi.GetString("Discount");
                    invitm.Uop = mpi.GetString("Uop");
                    invitm.ItemType = mpi.GetString("ItemType");
                    invitm.TaxCode = mpi.GetString("TaxCode");
                    invitm.Action = mpi.GetString("Action");
                    itemAction.Add(invitm);
                }
            }
            catch
            {
                loggerinfo("Items not found: three-way matching");
            }

            string purchaseNo = mp.GetString("purchaseNo");
            string accountant = mp.GetString("accountant");
            string invoiceDate = mp.GetString("invoiceDate");
            string invoiceReceiptDate = mp.GetString("invoiceReceiptDate");
            string dueDate = mp.GetString("dueDate");
            string invoiceNo = mp.GetString("invoiceNo");
            string invoiceAmount = mp.GetString("invoiceAmount");
            string currency = mp.GetString("currency");
            string invoiceCommentType = mp.GetString("invoiceCommentType");
            string refOrgInvoiceNo = mp.GetString("refOrgInvoiceNo");
            InvoiceItem[] items = itemAction.ToArray();

            //all date field format dd/mm/yy
            string district = null;
            string supplier = null;
            string payment = null;
            string loaded = null;

            if (purchaseNo == null || invoiceDate == null || invoiceReceiptDate == null
                || dueDate == null || invoiceNo == null || invoiceAmount == null
                || currency == null)
            {
                throw new Exception("One or more parameter is empty.");
            }

            if (purchaseNo.Trim().Equals("") || invoiceDate.Trim().Equals("") || invoiceReceiptDate.Trim().Equals("")
                || dueDate.Trim().Equals("") || invoiceNo.Trim().Equals("") || invoiceAmount.Trim().Equals("")
                || currency.Trim().Equals(""))
            {
                throw new Exception("One or more parameter is empty.");
            }

            mobj.GoHome();

            mobj.ExecuteMSO("MSO260");
            if (!mobj.CheckForScreen("MSM260A", false))
            {
                throw new Exception("Screen not match: " + mobj.GetScreenName());
            }

            mobj.SetFieldValue("PO_NO1I", purchaseNo);
            mobj.ExecuteCommand("OK");
            if (mobj.GetErrorMessage().StartsWith("X2:0011 - INPUT REQUIRED") && mobj.GetActiveField().Equals("INV_NO1I"))
            {
                district = mobj.GetFieldValue("DSTRCT_CODE1I");
                supplier = mobj.GetFieldValue("SUPPLIER_NO1I");
                payment = mobj.GetFieldValue("PAYMENT_NAME1I");
            }
            else
            {
                throw new Exception("PreMSM260A: " + mobj.GetErrorMessage());
            }
            if (!mobj.GetFieldValue("CURRENCY_TYPE1I").Trim().Equals(currency))
            {
                throw new Exception("PreMSM260A: PO Currency is in " + mobj.GetFieldValue("CURRENCY_TYPE1I"));
            }

            if (accountant == null)
            {
                accountant = ServiceHelper.GetConfig("Accountant");
            }
            mobj.SetFieldValue("ACCOUNTANT1I", accountant);
            mobj.SetFieldValue("DSP_ITEMS1I", "Y");
            //as of request 3 oct 2014 switch dates
            //mobj.SetFieldValue("INV_DATE1I", invoiceDate); //dd/mm/yyyy
            //mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceReceiptDate);
            mobj.SetFieldValue("INV_DATE1I", invoiceReceiptDate); //dd/mm/yyyy
            mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceDate);
            mobj.SetFieldValue("DUE_DATE1I", dueDate);
            mobj.SetFieldValue("INV_NO1I", invoiceNo);
            mobj.SetFieldValue("INV_AMT1I", invoiceAmount);
            if (invoiceCommentType != null)
            {
                if (items != null && items.Length > 0)
                {
                    mobj.SetFieldValue("INV_COMM_TYPE1I", invoiceCommentType);
                }
                else
                {
                    loggerinfo("Items not found, three-way-matching, ignore invoice comment type");
                }
            }
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);

            mobj.ExecuteCommand("OK");

            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception("MSM260A: " + mobj.GetErrorMessage() + " - Active field: " + mobj.GetActiveField());
            }
            if (mobj.GetErrorMessage().StartsWith("W2"))
            {
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM260A: " + mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            //          loggerinfo(mobj.DumpFields());

            if (mobj.CheckForScreen("MSM260B", false))
            {
                mobj.ExecuteCommand("Match with order");
            }

            //            if (mobj.CheckForScreen("MSM260B", false))
            //            {
            //                for (int i = 1; i <= 10; i++)
            //                {
            //                    string f1 = "RECEIPT_DATE2I" + i;
            //                    string f2 = "RECEIPT_REF2I" + i;
            //                    string f3 = "VALUE_NOT_INV2I" + i;
            //                    string f4 = "ATAX_VALUE2I" + i;
            //                    string field = "ACTN2I" + i;
            //                    if (mobj.GetFieldValue(f1).Trim().Equals("") ||
            //                        mobj.GetFieldValue(f2).Trim().Equals("") ||
            //                        mobj.GetFieldValue(f3).Trim().Equals("") ||
            //                        mobj.GetFieldValue(f4).Trim().Equals(""))
            //                    {
            ////                        mobj.SetFieldValue(field, "M");
            //                        mobj.SetFieldValue(field, actions[i - 1]);
            //                    }
            //                }
            //                mobj.ExecuteCommand("OK");
            //                if (mobj.GetErrorMessage().StartsWith("X2"))
            //                {
            //                    throw new Exception("MSM260A: " + mobj.GetErrorMessage());
            //                }
            //                if (mobj.GetErrorMessage().StartsWith("W2"))
            //                {
            //                    while (mobj.GetErrorMessage().StartsWith("W2"))
            //                    {
            //                        mobj.ExecuteCommand("OK");
            //                    }
            //                    if (mobj.GetErrorMessage().StartsWith("X2"))
            //                    {
            //                        throw new Exception("MSM260A: " + mobj.GetErrorMessage());
            //                    }
            //                }
            //                if (mobj.IsCommand("Confirm"))
            //                {
            //                    mobj.ExecuteCommand("Confirm");
            //                }
            //                if (mobj.GetErrorMessage().StartsWith("Invoice"))
            //                {
            //                    loaded = mobj.GetErrorMessage();
            //                }
            //            }


            if (!mobj.CheckForScreen("MSM26CA", false))
            {
                throw new Exception("Screen not match: " + mobj.GetScreenName());
            }

            Dictionary<string, InvoiceItem> itemsdct = new Dictionary<string, InvoiceItem>();
            for (int i = 0; i < items.Length; i++)
            {
                itemsdct.Add(items[i].PoItemNo.Trim(), items[i]);
            }

            bool next = true;
            int counter = 0;
            List<InvoiceItem> resitems = new List<InvoiceItem>();
            while (next)
            {
                loggerinfo("PAGE: " + mobj.GetFieldValue("SKLDISPLAY1I"));
                for (int i = 1; i <= 3; i++)
                {
                    InvoiceItem resitem = new InvoiceItem();
                    resitem.PoItemNo = mobj.GetFieldValue("PO_ITEM1I" + i);
                    resitem.Description = mobj.GetFieldValue("DESCRIPTION1I" + i);
                    resitem.Qty = mobj.GetFieldValue("QTY_NOT_INV_P1I" + i);
                    resitem.Price = mobj.GetFieldValue("GROSS_PRICE_P1I" + i);
                    resitem.Value = mobj.GetFieldValue("RCPT_ITM_VALUE1I" + i);
                    resitem.Tax = mobj.GetFieldValue("ITM_TAX_LIT1I" + i);
                    resitem.Discount = mobj.GetFieldValue("DISCOUNT1I" + i);
                    resitem.Uop = mobj.GetFieldValue("UOP1I" + i);
                    resitem.ItemType = mobj.GetFieldValue("PO_ITEM_TYPE1I" + i);
                    resitem.TaxCode = mobj.GetFieldValue("ATAX_CODE_ITM1I" + i);
                    resitems.Add(resitem);

                    string itemno = mobj.GetFieldValue("PO_ITEM1I" + i).Trim();
                    if (!itemno.Equals(""))
                    {
                        //loggerinfo("Item: " + mobj.GetFieldValue("PO_ITEM1I" + i));
                        //loggerinfo("Desc: " + mobj.GetFieldValue("DESCRIPTION1I" + i));
                        //loggerinfo("Qty: " + mobj.GetFieldValue("QTY_NOT_INV_P1I" + i));
                        //loggerinfo("Price: " + mobj.GetFieldValue("GROSS_PRICE_P1I" + i));
                        if (itemsdct.Count == 0)
                        {
                            mobj.SetFieldValue("ACTION1I" + i, "M");
                            loggerinfo("Items not found, Auto Set Item: " + mobj.GetFieldValue("PO_ITEM1I" + i).Trim() + " -Action: M");
                        }
                        else if (itemsdct.ContainsKey(itemno))
                        {
                            InvoiceItem item = itemsdct[itemno];
                            if (item.Action.Trim().ToUpper().Equals("M"))
                            {
                                if (!item.Qty.Trim().Equals("0"))
                                {
                                    mobj.SetFieldValue("QTY_NOT_INV_P1I" + i, item.Qty);
                                }
                                else
                                {
                                    loggerinfo("Qty is 0, ignore value");
                                }
                                mobj.SetFieldValue("ACTION1I" + i, item.Action);
                                mobj.SetFieldValue("DISCOUNT1I" + i, item.Discount);
                                loggerinfo(string.Format("Set Item: {0} - Qty: {1} - Discount: {2} - Action: {3}",
                                    itemno, item.Qty, item.Discount, item.Action));
                            }
                            else
                            {
                                loggerinfo("Skip Item: " + itemno);
                            }
                        }
                        else
                        {
                            loggerinfo("Skip Item: " + itemno);
                        }
                    }
                    else
                    {
                        next = false;
                    }
                    counter++;
                }
                mobj.ExecuteCommand("OK");
                loggerinfo("Invoice: " + mobj.GetFieldValue("INV_AMT1I"));
                loggerinfo("Receipt: " + mobj.GetFieldValue("RCPT_TOT_VAL1I"));
                loggerinfo("Invoice Total: " + mobj.GetFieldValue("INV_TOT_VAL1I"));
                loggerinfo("Variance: " + mobj.GetFieldValue("VARIANCE1I"));
                if (mobj.IsCommand("Confirm"))
                {
                    loggerinfo("Confirm");
                    mobj.ExecuteCommand("Confirm");
                }
                if (next)
                {
                    loggerinfo("Next");
                    if (!mobj.GetErrorMessage().Trim().Equals(""))
                    {
                        break;
                    }
                    //continue;
                }
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    //key data MSM096B
                    mobj.ExecuteCommand("Edit Mode");
                    mobj.CheckForScreen("MSM096B", true);

                    mobj.SetFieldValue("EDITCODE2I1", "COPY");
                    //set ppn/pph, but neither, type PPH anyway
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");

                    mobj.ExecuteCommand("OK");
                    mobj.CheckForScreen("MSM096B", true);
                    //set detail
                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " ");
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " ");
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + refOrgInvoiceNo);

                    mobj.ExecuteCommand("OK");
                    if (mobj.CheckForScreen("MSM096B", false))
                    {
                        mobj.ExecuteCommand("Exit");
                    }
                    if (mobj.CheckForScreen("MSM096B", false))
                    {
                        mobj.ExecuteCommand("Revert");
                    }
                    break;
                }
            }

            if (mobj.CheckForScreen("MSM26CA", false))
            {
                mobj.ExecuteCommand("OK");
            }
            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetScreenName() + ": " + mobj.GetErrorMessage() + " - Active Field: " + mobj.GetActiveField());
            }
            if (mobj.GetErrorMessage().StartsWith("W2"))
            {
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM260CA: " + mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }
            if (mobj.GetErrorMessage().StartsWith("Invoice"))
            {
                loaded = mobj.GetErrorMessage();
            }

            mobj.ExecuteCommand("Home");
            LoadInvoiceResult res = new LoadInvoiceResult();
            res.PurchaseNo = purchaseNo;
            res.InvoiceNo = invoiceNo;
            res.Message = loaded;
            res.District = district;
            res.Supplier = supplier;
            res.Payment = payment;
            res.Items = resitems.ToArray();

            return JsonHelper.GetJsonString(res);
        }

        public class ApproveInvoiceResult
        {
            public string BankAccount;
            public string AccountName;
            public string AccountantCode;
            public string AccountantName;
            public string Currency;
            public string InvoiceDate;
            public string DueDate;
            public string InvoiceType;
            public string InvoiceTypeDesc;
            public string ApprovalStatus;
            public string ApprovalStatusDesc;
            public string PaymentStatus;
            public string PaymentStatusDesc;
            public string VoucherNo;
            public string PurchaseOrderNo;
            public string InvoiceAmountOrig;
            public string InvoiceAmountAmend;
            public string Message;
            public string ApproveItemsResult;
        }

        private string ApproveInvoice(MincomObject mobj, MincomParameter mp)
        {
            string result = null;

            JobSplit[] ids = job.GetJobSplit("LoadNonOrderInvoice", mp.GetString("invoiceNo"));
            if (ids == null)
            {
                ids = new JobSplit[] { new JobSplit(mp.GetString("invoiceNo"), null) };
            }
            ApproveInvoiceResult resjoin = new ApproveInvoiceResult();
            for (int i = 0; i < ids.Length; i++)
            {
                loggerinfo("Approve Invoice: " + ids[i].id);
                //ApproveInvoiceResult res = ApproveInvoiceDo(mobj, mp.GetString("district"), mp.GetString("supplierNo"), ids[i].id,
                //    mp.GetString("authBy"), mp.GetString("positionId"));
                ApproveInvoiceResult res = ApproveInvoiceMultiDo(mobj, new string[] { mp.GetString("district") }, mp.GetString("supplierNo"),
                    ids[i].id, mp.GetString("authBy"), mp.GetString("positionId"));

                if (resjoin.Message != null)
                {
                    resjoin.AccountantCode += ";";
                    resjoin.AccountantName += ";";
                    resjoin.AccountName += ";";
                    resjoin.ApprovalStatus += ";";
                    resjoin.ApprovalStatusDesc += ";";
                    resjoin.ApproveItemsResult += ";";
                    resjoin.BankAccount += ";";
                    resjoin.Currency += ";";
                    resjoin.DueDate += ";";
                    resjoin.InvoiceAmountAmend += ";";
                    resjoin.InvoiceAmountOrig += ";";
                    resjoin.InvoiceDate += ";";
                    resjoin.InvoiceType += ";";
                    resjoin.InvoiceTypeDesc += ";";
                    resjoin.Message += ";";
                    resjoin.PaymentStatus += ";";
                    resjoin.PaymentStatusDesc += ";";
                    resjoin.PurchaseOrderNo += ";";
                    resjoin.VoucherNo += ";";
                }
                resjoin.AccountantCode += res.AccountantCode;
                resjoin.AccountantName += res.AccountantName;
                resjoin.AccountName += res.AccountName;
                resjoin.ApprovalStatus += res.ApprovalStatus;
                resjoin.ApprovalStatusDesc += res.ApprovalStatusDesc;
                resjoin.ApproveItemsResult += res.ApproveItemsResult;
                resjoin.BankAccount += res.BankAccount;
                resjoin.Currency += res.Currency;
                resjoin.DueDate += res.DueDate;
                resjoin.InvoiceAmountAmend += res.InvoiceAmountAmend;
                resjoin.InvoiceAmountOrig += res.InvoiceAmountOrig;
                resjoin.InvoiceDate += res.InvoiceDate;
                resjoin.InvoiceType += res.InvoiceType;
                resjoin.InvoiceTypeDesc += res.InvoiceTypeDesc;
                resjoin.Message += res.Message;
                resjoin.PaymentStatus += res.PaymentStatus;
                resjoin.PaymentStatusDesc += res.PaymentStatusDesc;
                resjoin.PurchaseOrderNo += res.PurchaseOrderNo;
                resjoin.VoucherNo += res.VoucherNo;
            }
            result = JsonHelper.GetJsonString(resjoin);
            return result;
        }

        ///// <summary>
        ///// MSO261
        ///// </summary>
        ///// <param name="mobj"></param>
        ///// <param name="supplierNo"></param>
        ///// <param name="invoiceNo"></param>
        ///// <param name="authBy"></param>
        ///// <param name="positionId"></param>
        ///// <returns></returns>
        //private ApproveInvoiceResult ApproveInvoiceDo(MincomObject mobj, string district, string supplierNo, string invoiceNo, string authBy, string positionId)
        //{
        //    return ApproveInvoiceMultiDo(mobj, new string[] { district }, supplierNo, invoiceNo, authBy, positionId);
        //}

        private string ApproveInvoiceMulti(MincomObject mobj, MincomParameter mp)
        {
            string result = null;

            //multi district
            JobSplit[] ids = job.GetJobSplit("LoadNonOrderInvoice", mp.GetString("invoiceNo"));
            if (ids == null)
            {
                ids = new JobSplit[] { new JobSplit(mp.GetString("invoiceNo"), null) };
            }

            List<string> lsdistrict = new List<string>();
            foreach (string d in mp.GetStringArray("district", null))
            {
                lsdistrict.Add(d);
            }
            ApproveInvoiceResult resjoin = new ApproveInvoiceResult();
            for (int i = 0; i < ids.Length; i++)
            {
                loggerinfo("Approve Invoice Multi: " + ids[i].id);
                ApproveInvoiceResult res = ApproveInvoiceMultiDo(mobj, lsdistrict.ToArray(), mp.GetString("supplierNo"), ids[i].id,
                    mp.GetString("authBy"), mp.GetString("positionId"));
                if (resjoin.Message != null)
                {
                    resjoin.AccountantCode += ";";
                    resjoin.AccountantName += ";";
                    resjoin.AccountName += ";";
                    resjoin.ApprovalStatus += ";";
                    resjoin.ApprovalStatusDesc += ";";
                    resjoin.ApproveItemsResult += ";";
                    resjoin.BankAccount += ";";
                    resjoin.Currency += ";";
                    resjoin.DueDate += ";";
                    resjoin.InvoiceAmountAmend += ";";
                    resjoin.InvoiceAmountOrig += ";";
                    resjoin.InvoiceDate += ";";
                    resjoin.InvoiceType += ";";
                    resjoin.InvoiceTypeDesc += ";";
                    resjoin.Message += ";";
                    resjoin.PaymentStatus += ";";
                    resjoin.PaymentStatusDesc += ";";
                    resjoin.PurchaseOrderNo += ";";
                    resjoin.VoucherNo += ";";
                }
                resjoin.AccountantCode += res.AccountantCode;
                resjoin.AccountantName += res.AccountantName;
                resjoin.AccountName += res.AccountName;
                resjoin.ApprovalStatus += res.ApprovalStatus;
                resjoin.ApprovalStatusDesc += res.ApprovalStatusDesc;
                resjoin.ApproveItemsResult += res.ApproveItemsResult;
                resjoin.BankAccount += res.BankAccount;
                resjoin.Currency += res.Currency;
                resjoin.DueDate += res.DueDate;
                resjoin.InvoiceAmountAmend += res.InvoiceAmountAmend;
                resjoin.InvoiceAmountOrig += res.InvoiceAmountOrig;
                resjoin.InvoiceDate += res.InvoiceDate;
                resjoin.InvoiceType += res.InvoiceType;
                resjoin.InvoiceTypeDesc += res.InvoiceTypeDesc;
                resjoin.Message += res.Message;
                resjoin.PaymentStatus += res.PaymentStatus;
                resjoin.PaymentStatusDesc += res.PaymentStatusDesc;
                resjoin.PurchaseOrderNo += res.PurchaseOrderNo;
                resjoin.VoucherNo += res.VoucherNo;
            }
            result = JsonHelper.GetJsonString(resjoin);
            return result;
        }

        /// <summary>
        /// MSO261
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="district"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="authBy"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        private ApproveInvoiceResult ApproveInvoiceMultiDo(MincomObject mobj, string[] district, string supplierNo, string invoiceNo, string authBy, string positionId)
        {
            string approveItems = "";
            for (int i = 0; i < district.Length; i++)
            {
                approveItems += ApproveInvoiceItemsDo(mobj, district[i], authBy, positionId);
            }

            mobj.GoHome();

            mobj.ExecuteMSO("MSO261");
            mobj.CheckForScreen("MSM261A", true);
            mobj.SetFieldValue("OPTION1I", "1");
            mobj.SetFieldValue("SUPPLIER_NO1I", supplierNo);
            mobj.SetFieldValue("INV_NO1I", invoiceNo);
            mobj.SetFieldValue("DSTRCT_CODE1I", district[0]);
            //            mobj.SetFieldValue("VCHR_NO1I", "");
            mobj.ExecuteCommand("OK");
            mobj.CheckForScreen("MSM261B", true);

            ApproveInvoiceResult res = new ApproveInvoiceResult();
            res.BankAccount = mobj.GetFieldValue("BANK_ACCT_NO2I");
            res.AccountName = mobj.GetFieldValue("ACCT_NAME2I");
            res.AccountantCode = mobj.GetFieldValue("ACCT2I");
            res.AccountantName = mobj.GetFieldValue("ACCTNT_NAME2I");
            res.Currency = mobj.GetFieldValue("CURRENCY_TYPE2I");
            res.InvoiceDate = mobj.GetFieldValue("INV_DATE2I");
            res.DueDate = mobj.GetFieldValue("DUE_DATE2I");
            res.InvoiceType = mobj.GetFieldValue("INV_TYPE2I");
            res.InvoiceTypeDesc = mobj.GetFieldValue("INV_TYPE_DESC2I");
            res.ApprovalStatus = mobj.GetFieldValue("APPR_STATUS2I");
            res.ApprovalStatusDesc = mobj.GetFieldValue("APPR_STAT_DESC2I");
            res.PaymentStatus = mobj.GetFieldValue("PMT_STATUS2I");
            res.PaymentStatusDesc = mobj.GetFieldValue("PMT_STAT_DESC2I");
            res.VoucherNo = mobj.GetFieldValue("VCHR_NO2I");
            res.PurchaseOrderNo = mobj.GetFieldValue("CONT_PO2I");
            res.InvoiceAmountOrig = mobj.GetFieldValue("INV_AMT_ORIG2I");
            res.InvoiceAmountAmend = mobj.GetFieldValue("INV_AMT_AMD2I");

            res.ApproveItemsResult = approveItems;

            if (mobj.IsCommand("Approve Invoice"))
            {
                mobj.ExecuteCommand("Approve Invoice");
                string msg = mobj.GetErrorMessage();
                res.Message = msg;
                //UNAUTHORISED items, NOT approved
                //MISMATCH detected, NOT approved
                //INVOICE STATUS DOES NOT ALLOW APPROVAL
                if (!msg.ToUpper().Trim().EndsWith("APPROVED OK"))
                {
                    throw new Exception(msg);
                }
            }
            else
            {
                throw new Exception("Failed to approve invoice");
            }
            mobj.ExecuteCommand("Home");

            return res;
            //            return JsonHelper.GetJsonString(res);
        }

        /// <summary>
        /// MSO572
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="totalReceived"></param>
        /// <param name="date"></param>
        /// <param name="branchCode"></param>
        /// <param name="bankAccount"></param>
        /// <param name="currency"></param>
        /// <param name="accountCode"></param>
        /// <param name="amtReceived"></param>
        /// <param name="district"></param>
        /// <returns></returns>
        private string UploadMiscCashReceipt(MincomObject mobj, MincomParameter mp)
        {
            //string receivedBy, string date, string totalReceived, string bankBranch, string bankAccount, string currency,
            //string[] accountCode, string[] amtReceived, string[] district, string[] description

            List<string> accountCodeL = new List<string>();
            List<string> amtReceivedL = new List<string>();
            List<string> districtL = new List<string>();
            List<string> descriptionL = new List<string>();
            for (int i = 0; i < mp.Count("items"); i++)
            {
                MincomParameter mpi = mp.Get("items[" + i + "]");
                accountCodeL.Add(mpi.GetString("accountCode"));
                amtReceivedL.Add(mpi.GetString("amtReceived"));
                districtL.Add(mpi.GetString("district"));
                descriptionL.Add(mpi.GetString("description"));
            }
            string receivedBy = mp.GetString("receivedBy");
            string date = mp.GetString("date");
            string totalReceived = mp.GetString("totalReceived");
            string bankBranch = mp.GetString("bankBranch");
            string bankAccount = mp.GetString("bankAccount");
            string currency = mp.GetString("currency");
            string[] accountCode = accountCodeL.ToArray();
            string[] amtReceived = amtReceivedL.ToArray();
            string[] district = districtL.ToArray();
            string[] description = descriptionL.ToArray();
            //result = UploadMiscCashReceiptDo(mobj, jsd.receivedBy, jsd.date, jsd.totalReceived, jsd.bankBranch, jsd.bankAccount, jsd.currency,
            //    accountCode.ToArray(), amtReceived.ToArray(), district.ToArray(), description.ToArray());

            string res = null;

            mobj.GoHome();

            mobj.ExecuteMSO("MSO572");
            mobj.CheckForScreen("MSM572A", true);

            mobj.SetFieldValue("RECEIVED_BY1I", receivedBy);
            mobj.SetFieldValue("RECEIPT_DATE1I", date);
            mobj.SetFieldValue("TOT_RECEIVED1I", totalReceived);
            mobj.SetFieldValue("BRANCH_CODE1I", bankBranch);
            mobj.SetFieldValue("BANK_ACCT_NO1I", bankAccount);
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);

            int n = 0;
            while (n < accountCode.Length)
            {
                loggerinfo(mobj.GetFieldValue("SKLDISPLAY1I"));
                for (int i = 1; i <= 5; i++)
                {
                    if (n >= accountCode.Length) break;

                    mobj.SetFieldValue("ACCOUNT_CODE1I" + i, accountCode[n]);
                    mobj.SetFieldValue("AMT_RECEIVED1I" + i, amtReceived[n]);
                    mobj.SetFieldValue("DSTR_RCT1I" + i, district[n]);
                    mobj.SetFieldValue("RECEIPT_DESC1I" + i, description[n]);
                    n++;
                }
                mobj.ExecuteCommand("OK");
                mobj.DumpFields();

                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception("MSM572A: " + mobj.GetErrorMessage());
                }

                //mobj.DumpFields();
                res = mobj.GetErrorMessage();

                if (mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");

                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception("MSM572A: " + mobj.GetErrorMessage());
                    }

                    res = mobj.GetErrorMessage();
                }
            }

            //            res += "1959 - TRANSACTION COMPLETED - RA151473";
            if (!res.Contains("- TRANSACTION COMPLETED -"))
            {
                throw new Exception("Unexpected result: " + res);
            }

            //            mso572
            //check mso572a
            //RECEIVED_BY1I
            //REC_BY_DESC1I
            //RECEIPT_DATE1I
            //TOT_RECEIVED1I
            //TOT_APPLIED1I
            //BRANCH_CODE1I
            //BANK_ACCT_NO1I
            //CURRENCY_TYPE1I

            //ACCOUNT_CODE1I1
            //ACCOUNT_NAME1I1
            //AMT_RECEIVED1I1
            //DSTR_RCT1I1
            //RECEIPT_DESC1I1

            return JsonHelper.GetJsonString(res);
        }

        public class UploadPaymentResult
        {
            public string Result;
            public string[] InvoiceAmounts = null;
        }

        public class UploadPaymentItem
        {
            public string invoiceNo;
            public string district;
            public string accountCode;
            public double amount;
        }

        private string UploadPayment(MincomObject mobj, MincomParameter mp)
        {
            string result = null;
            List<UploadPaymentItem> invoices = new List<UploadPaymentItem>();
            foreach (MincomParameter inv in mp.GetArray("invoiceItems"))
            {
                JobSplit[] splitids = job.GetJobSplit("LoadNonOrderInvoice", inv.GetString("invoiceNo"));
                if (splitids == null || splitids.Length == 1)
                {
                    splitids = new JobSplit[] { new JobSplit(inv.GetString("invoiceNo"), string.Format("{0}", inv.GetString("amount"))) };
                }
                else
                {
                    loggerinfo(string.Format("Job split found, invoice: {0} count: {1}", inv.GetString("invoiceNo"), splitids.Length));
                }
                foreach (JobSplit split in splitids)
                {
                    UploadPaymentItem item = new UploadPaymentItem();
                    item.invoiceNo = split.id;
                    item.district = inv.GetString("district");
                    item.accountCode = inv.GetString("accountCode");
                    CultureInfo cien = CultureInfo.CreateSpecificCulture("en-US");
                    double.TryParse(split.amount, System.Globalization.NumberStyles.Any, cien, out item.amount);
                    if (item.amount == 0)
                    {//try id format
                        CultureInfo ciid = CultureInfo.CreateSpecificCulture("id-ID");
                        double.TryParse(split.amount, System.Globalization.NumberStyles.Any, ciid, out item.amount);
                    }
                    invoices.Add(item);
                }
            }

            result = UploadPaymentDo(mobj, mp.GetString("bankBranch"), mp.GetString("bankAccount"), mp.GetString("supplierNo"),
                string.Format("{0}", mp.GetString("totalAmount")),
                mp.GetString("date"), mp.GetString("currency"), mp.GetString("description"), invoices.ToArray());
            return result;
        }

        /// <summary>
        /// MSO275
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="bankBranch"></param>
        /// <param name="bankAccount"></param>
        /// <param name="supplierNo"></param>
        /// <param name="totalAmount"></param>
        /// <param name="date"></param>
        /// <param name="currency"></param>
        /// <param name="invoices"></param>
        /// <returns></returns>
        private string UploadPaymentDo(MincomObject mobj, string bankBranch, string bankAccount, string supplierNo,
            string totalAmount, string date, string currency, string description, UploadPaymentItem[] invoiceItems)
        {
            //all date field format dd/mm/yy
            if (bankBranch == null || bankAccount == null || supplierNo == null || totalAmount == null
                || date == null || currency == null)
            {
                throw new Exception("One or more parameter is empty.");
            }
            if (bankBranch.Trim().Equals("") || bankAccount.Trim().Equals("") || supplierNo.Trim().Equals("") || totalAmount.Trim().Equals("")
                || date.Trim().Equals("") || currency.Trim().Equals(""))
            {
                throw new Exception("One or more parameter is empty.");
            }

            UploadPaymentResult res = new UploadPaymentResult();

            mobj.GoHome();

            Dictionary<string, List<UploadPaymentItem>> sum = new Dictionary<string, List<UploadPaymentItem>>();
            for (int i = 0; i < invoiceItems.Length; i++)
            {
                //loggerinfo(invoiceItems[i].invoiceNo + " " + invoiceItems[i].amount);
                if (!sum.ContainsKey(invoiceItems[i].district.Trim()))
                {
                    sum.Add(invoiceItems[i].district.Trim(), new List<UploadPaymentItem>());
                }
                sum[invoiceItems[i].district].Add(invoiceItems[i]);
            }
            //if (sum.Count > 4)
            //{
            //    throw new Exception("District list more than 4");
            //}

            mobj.ExecuteMSO("MSO275");
            mobj.CheckForScreen("MSM275A", true);

            mobj.SetFieldValue("BRANCH_CODE1I", bankBranch);
            mobj.SetFieldValue("BANK_ACCT_NO1I", bankAccount);
            mobj.SetFieldValue("SUPPLIER_NO1I", supplierNo);
            mobj.SetFieldValue("SELECT_INVS1I", "Y");
            mobj.SetFieldValue("CHEQUE_AMT1I", totalAmount);
            mobj.SetFieldValue("CHEQUE_DATE1I", date);
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);

            List<string> amtstotal = new List<string>();
            int distcounter = 0;
            string lastskl = null;
            while (distcounter < sum.Count)
            {
                string skl = mobj.GetFieldValue("SKLDISPLAY1I").Trim();
                if (lastskl != null && lastskl.Equals(skl))
                {
                    throw new Exception("SKLDISPLAY error, last: " + lastskl + ", current: " + skl);
                }
                lastskl = skl;
                for (int i = 1; i <= 4; i++)
                {
                    string dist = sum.Keys.ElementAt(distcounter);
                    List<UploadPaymentItem> items = sum[dist];
                    double tamt = 0;
                    for (int j = 0; j < items.Count; j++)
                    {
                        loggerinfo("X " + items[j].invoiceNo + " " + items[j].amount);
                        tamt += items[j].amount;
                    }
                    loggerinfo(string.Format("T {0} {1:N2}", dist, tamt));
                    //                    double tamt = items.Sum(itm => itm.amount);
                    mobj.SetFieldValue("ACCT_DSTRCT1I" + i, dist);
                    mobj.SetFieldValue("DESCRIPTION1I" + i, description);
                    mobj.SetFieldValue("AMOUNT1I" + i, string.Format("{0:N2}", tamt));
                    mobj.SetFieldValue("GL_ACCOUNT1I" + i, items[0].accountCode);
                    distcounter++;
                    if (distcounter >= sum.Count)
                    {
                        break;
                    }
                }

                if (!mobj.IsIdle())
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                    }
                    else if (mobj.GetErrorMessage().StartsWith("W2"))
                    {
                        while (mobj.GetErrorMessage().StartsWith("W2"))
                        {
                            if (mobj.GetActiveField().Equals("CURRENCY_TYPE1I"))
                            {
                                mobj.SetFieldValue("CURRENCY_TYPE1I", currency);
                            }
                            mobj.ExecuteCommand("OK");
                        }
                        if (mobj.GetErrorMessage().StartsWith("X2"))
                        {
                            throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                        }
                    }

                    if (!mobj.GetFieldValue("CURRENCY_TYPE1I").Equals(currency))
                    {
                        mobj.SetFieldValue("CURRENCY_TYPE1I", currency);
                    }
                    if (!mobj.GetFieldValue("EXCHANGE_RATE1I").Equals(""))
                    {
                        mobj.SetFieldValue("EXCHANGE_RATE1I", "");
                    }
                    if (mobj.IsCommand("Confirm") && mobj.CheckForScreen("MSM275A", false))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }

                    mobj.CheckForScreen("MSM275B", true);

                    string lastdist = null;
                    int index = 0;
                    int maxpages = 30;
                    while (mobj.CheckForScreen("MSM275B", false))
                    {
                        string dist = mobj.GetFieldValue("ACCT_DSTRCT2I").Trim();
                        if (lastdist == null || !lastdist.Equals(dist))
                        {
                            index = 0;
                        }
                        lastdist = dist;
                        List<UploadPaymentItem> items = sum[dist];
                        double[] amtsinput = new double[12];
                        for (int i = 1; i <= 11; i++)
                        {
                            if (index >= items.Count)
                            {
                                break;
                            }
                            mobj.SetFieldValue("INV_NO2I" + i, items[index].invoiceNo);
                            amtsinput[i] = items[index].amount;
                            index++;
                        }
                        mobj.ExecuteCommand("OK");
                        StringBuilder sbmismatch = new StringBuilder();
                        for (int i = 1; i <= 11; i++)
                        {
                            string inv = mobj.GetFieldValue("INV_NO2I" + i);
                            string amt = mobj.GetFieldValue("INV_AMT2I" + i);
                            if (inv == null || amt == null)
                            {
                                break;
                            }
                            inv = inv.Trim();
                            amt = amt.Trim();
                            double amtn = 0;
                            if (!double.TryParse(amt, out amtn))
                            {
                                break;
                            }
                            if (amtsinput[i] != 0 && amtsinput[i] != amtn)
                            {
                                string mismatch = string.Format("Invoice Mismatch {0} - Input: {1} - System: {2}",
                                    inv,
                                    amtsinput[i],
                                    amt
                                    );
                                loggerinfo(mismatch);
                                sbmismatch.Append(mismatch).Append(Environment.NewLine);
                            }
                        }

                        if (mobj.GetErrorMessage().StartsWith("X2"))
                        {
                            throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                        }

                        List<string> amts = new List<string>();
                        int retry = 3;
                        while (mobj.IsCommand("Confirm") && retry > 0)
                        {
                            if (mobj.CheckForScreen("MSM275B", false))
                            {
                                amts.Clear();
                                for (int i = 1; i <= 11; i++)
                                {
                                    amts.Add(mobj.GetFieldValue("INV_AMT2I" + i));
                                }
                            }
                            mobj.ExecuteCommand("Confirm");
                            if (mobj.GetErrorMessage().StartsWith("X2"))
                            {
                                string message = mobj.GetErrorMessage();
                                if (message != null
                                    && message.ToUpper().Trim().Contains("X2:7375 - TOTAL VALUE OF INVOICES NOT EQUAL AMOUNT PAYABLE")
                                    && sbmismatch.Length > 0)
                                {
                                    throw new Exception("MSM275A: " + mobj.GetErrorMessage()
                                        + Environment.NewLine + sbmismatch.ToString());
                                }
                                else
                                {
                                    throw new Exception("MSM275A: " + mobj.GetErrorMessage());
                                }
                            }
                            retry--;
                        }
                        amtstotal.AddRange(amts);
                        if (maxpages > 0)
                        {
                            maxpages--;
                        }
                        else
                        {
                            throw new Exception("Unlimited loop detected, terminating...");
                        }
                    }
                }
            }
            res.InvoiceAmounts = amtstotal.ToArray();

            if (mobj.CheckForScreen("MSM275A", false) && mobj.GetErrorMessage().Trim().Equals(""))
            {
                //patch for district count/4==0
                if (mobj.GetFieldValue("CHEQUE_NO1I").Trim().Equals("XXXXXXXXXXXX"))
                {
                    if (mobj.IsCommand("OK"))
                    {
                        mobj.ExecuteCommand("OK");
                    }
                    if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }
                }
            }
            res.Result = mobj.GetErrorMessage();

            return JsonHelper.GetJsonString(res);
        }

        /// <summary>
        /// mso27a
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="bankBranch"></param>
        /// <param name="bankAccount"></param>
        /// <param name="totalAmount"></param>
        /// <param name="date"></param>
        /// <param name="district"></param>
        /// <param name="description"></param>
        /// <param name="amount"></param>
        /// <param name="glAccount"></param>
        /// <returns></returns>
        public string UploadSundries(MincomObject mobj, MincomParameter mp)
        {
            string bankBranch = mp.GetString("bankBranch");
            string bankAccount = mp.GetString("bankAccount");
            string totalAmount = mp.GetString("totalAmount");
            string date = mp.GetString("date");
            string[] district = mp.GetStringArray("items", "district");
            string[] description = mp.GetStringArray("items", "description");
            string[] amount = mp.GetStringArray("items", "amount");
            string[] glAccount = mp.GetStringArray("items", "glAccount");
            //all date field format dd/mm/yy
            if (bankBranch == null || bankAccount == null || totalAmount == null
                || date == null)
            {
                throw new Exception("One or more parameter is empty.");
            }
            if (bankBranch.Trim().Equals("") || bankAccount.Trim().Equals("") || totalAmount.Trim().Equals("")
                || date.Trim().Equals(""))
            {
                throw new Exception("One or more parameter is empty.");
            }

            //UploadPaymentResult res = new UploadPaymentResult();

            mobj.GoHome();

            mobj.ExecuteMSO("MSO27A");
            mobj.CheckForScreen("MSM27AA", true);

            mobj.SetFieldValue("OPTION1I", "2");
            mobj.SetFieldValue("BRANCH_CODE1I", bankBranch);
            mobj.SetFieldValue("BANK_ACCT_NO1I", bankAccount);
            mobj.SetFieldValue("TRANAMT1I", totalAmount);
            mobj.SetFieldValue("TRANDT1I", date);

            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM275A", true);
            for (int i = 0; i < description.Length; i++)
            {
                int x = (i % 4) + 1;
                mobj.SetFieldValue("ACCT_DSTRCT1I" + x, district[i]);
                mobj.SetFieldValue("DESCRIPTION1I" + x, description[i]);
                mobj.SetFieldValue("AMOUNT1I" + x, amount[i]);
                mobj.SetFieldValue("GL_ACCOUNT1I" + x, glAccount[i]);
                //mobj.SetFieldValue("WORK_ORDER1I" + i, "");
                //mobj.SetFieldValue("PROJECT_IND1I" + i, "");
                //mobj.SetFieldValue("PLANT_NO1I" + i, "");
                if (i % 4 == 3)
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }

                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetScreenName() + ": " + mobj.GetErrorMessage());
                    }
                }
            }
            mobj.ExecuteCommand("OK");
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetScreenName() + ": " + mobj.GetErrorMessage());
            }

            UploadPaymentResult result = new UploadPaymentResult();
            result.Result = mobj.GetErrorMessage();
            if (!result.Result.StartsWith("PV Number"))
            {
                throw new Exception(mobj.GetScreenName() + " invalid message: " + mobj.GetErrorMessage());
            }

            return JsonHelper.GetJsonString(result);
        }

        public class UploadUnbudgetResult
        {
            public string TotalYearEstimates;
            public string Result;
        }

        /// <summary>
        /// MSO660
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="projectNumber"></param>
        /// <param name="budgetCode"></param>
        /// <param name="projectDescription"></param>
        /// <param name="authorisedBy"></param>
        /// <param name="authorisedDate"></param>
        /// <param name="originator"></param>
        /// <param name="dateRaised"></param>
        /// <param name="assignedTo"></param>
        /// <param name="capitalProject"></param>
        /// <param name="costAccount"></param>
        /// <param name="type"></param>
        /// <param name="department"></param>
        /// <param name="projectStructure"></param>
        /// <param name="classification"></param>
        /// <param name="estimator"></param>
        /// <param name="spreadCode"></param>
        /// <param name="estimateExpense"></param>
        /// <param name="date"></param>
        /// <param name="estimateForOth"></param>
        /// <param name="plannedStartDate"></param>
        /// <param name="plannedFinishDate"></param>
        /// <param name="financialYear"></param>
        /// <param name="totalYearEstimates"></param>
        /// <param name="estimatePeriod1"></param>
        /// <param name="estimatePeriod2"></param>
        /// <param name="estimatePeriod3"></param>
        /// <param name="estimatePeriod4"></param>
        /// <param name="estimatePeriod5"></param>
        /// <param name="estimatePeriod6"></param>
        /// <param name="estimatePeriod7"></param>
        /// <param name="estimatePeriod8"></param>
        /// <param name="estimatePeriod9"></param>
        /// <param name="estimatePeriod10"></param>
        /// <param name="estimatePeriod11"></param>
        /// <param name="estimatePeriod12"></param>
        /// <returns></returns>
        private string UploadUnbudget(MincomObject mobj, MincomParameter mp)
        {
            //if (operation.Equals("UploadUnbudget"))
            //{
            //    result = UploadUnbudgetDo(mobj, jsd.projectNumber, jsd.budgetCode, jsd.projectDescription,
            //        jsd.authorisedBy, jsd.authorisedDate, jsd.originator, jsd.dateRaised, jsd.assignedTo, jsd.capitalProject,
            //        jsd.costAccount, jsd.type, jsd.department, jsd.projectStructure, jsd.classification, jsd.estimator,
            //        jsd.spreadCode, jsd.estimateExpense, jsd.date, jsd.estimateForOth, jsd.plannedStartDate, jsd.plannedFinishDate,
            //        jsd.financialYear, jsd.totalYearEstimates, jsd.estimatePeriod1, jsd.estimatePeriod2, jsd.estimatePeriod3,
            //        jsd.estimatePeriod4, jsd.estimatePeriod5, jsd.estimatePeriod6, jsd.estimatePeriod7, jsd.estimatePeriod8,
            //        jsd.estimatePeriod9, jsd.estimatePeriod10, jsd.estimatePeriod11, jsd.estimatePeriod12);
            //}

            string projectNumber = mp.GetString("projectNumber");
            string budgetCode = mp.GetString("budgetCode");
            string projectDescription = mp.GetString("projectDescription");
            string authorisedBy = mp.GetString("authorisedBy");
            string authorisedDate = mp.GetString("authorisedDate");
            string originator = mp.GetString("originator");
            string dateRaised = mp.GetString("dateRaised");
            string assignedTo = mp.GetString("assignedTo");
            string capitalProject = mp.GetString("capitalProject");
            string costAccount = mp.GetString("costAccount");
            string type = mp.GetString("type");
            string department = mp.GetString("department");
            string projectStructure = mp.GetString("projectStructure");
            string classification = mp.GetString("classification");
            string estimator = mp.GetString("estimator");
            string spreadCode = mp.GetString("spreadCode");
            //string estimateExpense=mp.GetString("estimateExpense");
            //string date=mp.GetString("date");
            //string estimateForOth=mp.GetString("estimateForOth");
            string plannedStartDate = mp.GetString("plannedStartDate");
            string plannedFinishDate = mp.GetString("plannedFinishDate");
            string financialYear = mp.GetString("financialYear");
            string totalYearEstimates = mp.GetString("totalYearEstimates");

            string[] estimatePeriod = new string[13];
            for (int i = 1; i <= 12; i++)
            {
                estimatePeriod[i] = mp.GetString("estimatePeriod" + i);

            }
            //string estimatePeriod1, string estimatePeriod2, string estimatePeriod3,
            //string estimatePeriod4, string estimatePeriod5, string estimatePeriod6, string estimatePeriod7, string estimatePeriod8,
            //string estimatePeriod9, string estimatePeriod10, string estimatePeriod11, string estimatePeriod12

            if (projectNumber.Trim().Length == 0 || budgetCode.Trim().Length == 0 || projectDescription.Trim().Length == 0 ||
                authorisedBy.Trim().Length == 0 || authorisedDate.Trim().Length == 0 || originator.Trim().Length == 0 ||
                dateRaised.Trim().Length == 0 || assignedTo.Trim().Length == 0 || classification.Trim().Length == 0 || estimator.Trim().Length == 0 ||
                spreadCode.Trim().Length == 0 || plannedStartDate.Trim().Length == 0 || plannedFinishDate.Trim().Length == 0 ||
                financialYear.Trim().Length == 0 || totalYearEstimates.Trim().Length == 0)
            {
                throw new Exception("Required parameters: projectNumber, budgetCode, projectDescription, authorisedBy, authorisedDate, " +
                    "originator, dateRaised, assignedTo, classification, estimator, spreadCoce, plannedStartDate, plannedFinishDate, " +
                    "financialYear, totalYearEstimates.");
            }

            UploadUnbudgetResult res = new UploadUnbudgetResult();

            mobj.GoHome();

            mobj.ExecuteMSO("MSO660");
            mobj.CheckForScreen("MSM660A", true);

            //key data MSM660A
            mobj.SetFieldValue("OPTION1I", "1");
            mobj.SetFieldValue("PROJECT_NO1I", projectNumber);
            mobj.SetFieldValue("BUDGET_CDE1I", budgetCode);

            mobj.ExecuteCommand("OK");
            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetErrorMessage());
            }

            if (mobj.CheckForScreen("MSM660B", false))
            {
                //key data MSM660B
                mobj.SetFieldValue("PROJECT_NO2I", projectNumber);
                mobj.SetFieldValue("PROJ_DESC2I", projectDescription);
                mobj.SetFieldValue("AUTHSD_BY2I", authorisedBy);
                mobj.SetFieldValue("AUTHSD_DATE2I", authorisedDate);
                mobj.SetFieldValue("ORIGINATOR_ID2I", originator);
                mobj.SetFieldValue("RAISED_DATE2I", dateRaised);
                mobj.SetFieldValue("ASSIGN_PERSON2I", assignedTo);
                mobj.SetFieldValue("CAP_PROJ_IND2I", capitalProject);
                mobj.SetFieldValue("ACCOUNT_CODE2I", costAccount);
                mobj.SetFieldValue("PROJ_CLASSIF2I1", type);
                mobj.SetFieldValue("PROJ_CLASSIF2I2", department);
                mobj.SetFieldValue("PROJ_CLASSIF2I3", projectStructure);

                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                }
                if (mobj.CheckForScreen("MSM660B", false) && mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                }
            }

            if (mobj.CheckForScreen("MSM660C", false))
            {
                //key data MSM660C
                mobj.SetFieldValue("BUDGET_CDE3I", budgetCode);
                mobj.SetFieldValue("CATEGORY_CODA3I1", "OTH");
                mobj.SetFieldValue("ACTION_CODE13I", "S");
                mobj.SetFieldValue("ACTION_CODE23I", "U");
                mobj.SetFieldValue("CLASSIFICATION3I", projectNumber);
                mobj.SetFieldValue("ESTIMATOR3I", projectNumber);
                mobj.SetFieldValue("SPREAD_CODE3I", projectNumber);
                mobj.SetFieldValue("EST_EXP_LOC3I", projectNumber);
                mobj.SetFieldValue("ESTIMATE_DATE3I", projectNumber);
                mobj.SetFieldValue("ESTIMATEA3I1", projectNumber);

                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                }
                if (mobj.CheckForScreen("MSM660C", false) && mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                }
            }

            if (mobj.CheckForScreen("MSM661A", false))
            {
                //key data MSM660B
                mobj.SetFieldValue("PLAN_STR_DATE1I", plannedStartDate);
                mobj.SetFieldValue("PLAN_FIN_DATE1I", plannedFinishDate);

                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                }
                if (mobj.CheckForScreen("MSM661A", false) && mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                }
            }

            if (mobj.CheckForScreen("MSM66LA", false))
            {
                //key data MSM66LA
                mobj.SetFieldValue("PROJECT_NO1I", projectNumber);
                mobj.SetFieldValue("BUDGET_CODE1I", budgetCode);
                mobj.SetFieldValue("CATEGORY_CODE1I", "OTH");
                mobj.SetFieldValue("FIN_YR1I", financialYear);

                mobj.SetFieldValue("ESTIMATE1I1", estimatePeriod[1]);
                mobj.SetFieldValue("ESTIMATE1I2", estimatePeriod[2]);
                mobj.SetFieldValue("ESTIMATE1I3", estimatePeriod[3]);
                mobj.SetFieldValue("ESTIMATE1I4", estimatePeriod[4]);
                mobj.SetFieldValue("ESTIMATE1I5", estimatePeriod[5]);
                mobj.SetFieldValue("ESTIMATE1I6", estimatePeriod[6]);
                mobj.SetFieldValue("ESTIMATE1I7", estimatePeriod[7]);
                mobj.SetFieldValue("ESTIMATE1I8", estimatePeriod[8]);
                mobj.SetFieldValue("ESTIMATE1I9", estimatePeriod[9]);
                mobj.SetFieldValue("ESTIMATE1I10", estimatePeriod[10]);
                mobj.SetFieldValue("ESTIMATE1I11", estimatePeriod[11]);
                mobj.SetFieldValue("ESTIMATE1I12", estimatePeriod[12]);

                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                if (mobj.CheckForScreen("MSM66LA", false) && mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                }

                res.TotalYearEstimates = mobj.GetFieldValue("TOT_YR_EST1I");

                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                }
                if (mobj.CheckForScreen("MSM66LA", false) && mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                    res.Result = "Success";
                }

                if (mobj.CheckForScreen("MSM660C", false))
                {
                    mobj.ExecuteCommand("OK");
                }

                if (mobj.CheckForScreen("MSM660B", false))
                {
                    mobj.ExecuteCommand("OK");
                }

                if (mobj.CheckForScreen("MSM660A", false))
                {
                    mobj.ExecuteCommand("Home");
                }
            }

            return JsonHelper.GetJsonString(res);
        }

        public class GetBankAccountResult
        {
            public class BankAccount
            {
                public string BranchCode;
                public string BranchName;
                public string OwnerDistrict;
                public string CurrencyType;
                public string AccountNo;
                public string AccountName;
            }
            public BankAccount[] Accounts;
        }

        /// <summary>
        /// MSO003
        /// </summary>
        /// <param name="mobj"></param>
        /// <returns></returns>
        private string GetBankAccount(MincomObject mobj, MincomParameter mp)
        {
            mobj.GoHome();

            mobj.ExecuteMSO("MSO003");
            mobj.CheckForScreen("MSM003A", true);

            GetBankAccountResult res = new GetBankAccountResult();

            string[] fields = mobj.GetFields();
            int cnt = 1;
            while (fields.Contains("ITEM_NO1I" + cnt))
            {
                cnt++;
            }
            GetBankAccountResult.BankAccount[] accounts = new GetBankAccountResult.BankAccount[--cnt];
            for (int i = 0; i < cnt; i++)
            {
                accounts[i] = new GetBankAccountResult.BankAccount();
                accounts[i].BranchCode = mobj.GetFieldValue("BRANCH_CODE1I" + (i + 1));
                accounts[i].BranchName = mobj.GetFieldValue("BRANCH_NAME1I" + (i + 1));
                accounts[i].OwnerDistrict = mobj.GetFieldValue("OWNER_DSTRCT1I" + (i + 1));
                accounts[i].CurrencyType = mobj.GetFieldValue("CURRENCY_TYPE1I" + (i + 1));
                accounts[i].AccountNo = mobj.GetFieldValue("BANK_ACCT_NO1I" + (i + 1));
                accounts[i].AccountName = mobj.GetFieldValue("ACCOUNT_NAME1I" + (i + 1));
            }

            res.Accounts = accounts;
            mobj.ExecuteCommand("Home");

            return JsonHelper.GetJsonString(res);
        }

        /// <summary>
        /// MSO086
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="reportName"></param>
        /// <returns></returns>
        private string GetReportText(MincomObject mobj, MincomParameter mp)
        {
            //if (operation.Equals("GetReportText"))
            //{
            //    result = GetReportTextDo(mobj, jsd.reportName);
            //}

            mobj.GoHome();

            mobj.ExecuteMSO("MSO086");
            mobj.CheckForScreen("MSM086A", true);

            bool found = false;
            int rc = 0;
            mobj.SetFieldValue("PROGREP_ID1I", mp.GetString("reportName"));
            mobj.ExecuteCommand("OK");

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    string field = mobj.GetFieldValue("REPORT_ID1I" + (j + 1));
                    if (field != null && field.Trim().Equals(mp.GetString("reportName")))
                    {
                        rc = j + 1;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    mobj.ExecuteCommand("OK");
                }
                else
                {
                    break;
                }
            }
            if (!found)
            {
                throw new Exception("Report not found, search result:" + mobj.GetFieldValue("REPORT1I1"));
            }
            mobj.SetFieldValue("ACTION1I" + rc, "R");
            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM086B", true);

            int screen = 0;
            int page = 0;
            List<string> lines = new List<string>();
            string lpage = mobj.GetFieldValue("PAGE_NO2I");
            string tpage = mobj.GetFieldValue("PAGE_OF2I");
            List<string> pages = new List<string>();

            while (true)
            {
                for (int i = 0; i < 14; i++)
                {
                    lines.Add(mobj.GetFieldValue("REPORT_LINE2I" + (i + 1)));
                }
                if (mobj.IsCommand("Right Screen"))
                {
                    mobj.ExecuteCommand("Right Screen");
                    for (int i = 0; i < 14; i++)
                    {
                        string r = mobj.GetFieldValue("REPORT_LINE2I" + (i + 1));
                        r = r.Substring(24);
                        lines[screen * 14 + i] += r;
                    }
                    mobj.ExecuteCommand("Left Screen");
                }
                mobj.ExecuteCommand("OK");
                if (mobj.CheckForScreen("MSM086B", false))
                {
                    if (lpage.Equals(mobj.GetFieldValue("PAGE_NO2I")))
                    {
                        screen++;
                    }
                    else
                    {
                        screen = 0;
                        page++;
                        lpage = mobj.GetFieldValue("PAGE_NO2I");

                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < lines.Count; i++)
                        {
                            sb.Append(lines[i]).Append("\r\n");
                        }
                        lines = new List<string>();
                        pages.Add(sb.ToString());
                    }
                }
                else
                {
                    break;
                }
            }

            mobj.ExecuteCommand("Home");

            StringBuilder pagelines = new StringBuilder();
            for (int i = 0; i < pages.Count; i++)
            {
                lines.Add("PAGE: " + (page + 1));
                pagelines.Append(pages[i]);
            }

            return pagelines.ToString();
        }

        public class LoadNonOrderItems
        {
            public string description;
            public string value;
            public string district;
            public string authBy;
            public string account;
            public string workOrder;
            public string project;
            public string equipNo;
            public string positionId;
        }

        public class LoadNonOrderResult
        {
            public string Text;
            public string FillingNo;
        }

        /// <summary>
        /// MSO265
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="authBy"></param>
        /// <param name="authPosId"></param>
        /// <param name="accountant"></param>
        /// <param name="receivedDate"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="postingDistrict"></param>
        /// <param name="costingDistrict"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="description"></param>
        /// <param name="coa"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="invoiceCommentType"></param>
        /// <param name="taxInvoiceDate"></param>
        /// <param name="taxInvoiceNo"></param>
        /// <param name="taxOrgInvoiceNo"></param>
        /// <param name="refDate"></param>
        /// <param name="refNo"></param>
        /// <param name="refOrgInvoiceNo"></param>
        /// <returns></returns>
        private string LoadNonOrderInvoice(MincomObject mobj, MincomParameter mp)
        {
            //if (operation.Equals("LoadNonOrderInvoice"))
            //{
            //    result = LoadNonOrderInvoiceDo(mobj, jsd.authBy, jsd.accountant, jsd.positionId, jsd.receivedDate,
            //        jsd.invoiceDate, jsd.dueDate, jsd.postingDistrict, jsd.costingDistrict, jsd.supplierNo, jsd.invoiceNo,
            //        jsd.description, jsd.coa, jsd.amount, jsd.currency, jsd.invoiceCommentType, jsd.taxInvoiceDate, jsd.taxInvoiceNo, jsd.taxOrgInvoiceNo,
            //        jsd.refDate, jsd.refNo, jsd.refOrgInvoiceNo);
            //}

            string authBy = mp.GetString("authBy");
            string accountant = mp.GetString("accountant");
            string positionId = mp.GetString("positionId");
            string receivedDate = mp.GetString("receivedDate");
            string invoiceDate = mp.GetString("invoiceDate");
            string dueDate = mp.GetString("dueDate");
            string postingDistrict = mp.GetString("postingDistrict");
            string costingDistrict = mp.GetString("costingDistrict");
            string supplierNo = mp.GetString("supplierNo");
            string invoiceNo = mp.GetString("invoiceNo");
            string description = mp.GetString("description");
            string coa = mp.GetString("coa");
            string amount = mp.GetString("amount");
            string currency = mp.GetString("currency");
            string invoiceCommentType = mp.GetString("invoiceCommentType");
            string taxInvoiceDate = mp.GetString("taxInvoiceDate");
            string taxInvoiceNo = mp.GetString("taxInvoiceNo");
            string taxOrgInvoiceNo = mp.GetString("taxOrgInvoiceNo");
            string refDate = mp.GetString("refDate");
            string refNo = mp.GetString("refNo");
            string refOrgInvoiceNo = mp.GetString("refOrgInvoiceNo");

            mobj.GoHome();
            LoadNonOrderResult result = new LoadNonOrderResult();

            mobj.ExecuteMSO("MSO265");
            mobj.CheckForScreen("MSM265A", true);
            loggerinfo("MSM265A");

            //key data MSM265A

            //loggerinfo("authBy: " + authBy);
            if (authBy == null)
            {
                authBy = ServiceHelper.GetConfig("Authorize"); // override by config
            }
            if (accountant == null)
            {
                accountant = ServiceHelper.GetConfig("Accountant");
            }
            mobj.SetFieldValue("ACCOUNTANT1I", accountant);
            mobj.SetFieldValue("AUTH_BY1I1", authBy);
            //as of request 3 oct 2014 switch dates
            //mobj.SetFieldValue("INV_DATE1I", invoiceDate);
            //mobj.SetFieldValue("INV_RCPT_DATE1I", receivedDate);
            mobj.SetFieldValue("INV_DATE1I", receivedDate);
            mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceDate);
            mobj.SetFieldValue("DUE_DATE1I", dueDate);
            mobj.SetFieldValue("DSTRCT_CODE1I", postingDistrict);
            mobj.SetFieldValue("ACCT_DSTRCT1I1", costingDistrict);
            mobj.SetFieldValue("SUPPLIER_NO1I", supplierNo);
            mobj.SetFieldValue("INV_NO1I", invoiceNo);
            mobj.SetFieldValue("INV_ITEM_DESC1I1", description);
            mobj.SetFieldValue("ACCOUNT1I1", coa);
            mobj.SetFieldValue("INV_AMT1I", amount);
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);
            mobj.SetFieldValue("INV_COMM_TYPE1I", invoiceCommentType);
            mobj.SetFieldValue("INV_ITEM_VALUE1I1", amount);

            mobj.ExecuteCommand("OK");
            if (mobj.IsIdle())
            {
                throw new Exception("Unexpected idle state.");
            }

            if (positionId == null)
            {
                positionId = ServiceHelper.GetConfig("AuthorizePositionID"); //get from config
            }

            while (mobj.CheckForScreen("MSM004A", false))
            {
                loggerinfo("MSM004A");
                string trans = mobj.GetFieldValue("TRANS_DETAIL1I").Trim();
                trans = trans.Substring(trans.Length - 3);
                int pos = 0;
                int.TryParse(trans, out pos);

                if (!positionId.Equals(""))
                {
                    List<string> posids = new List<string>();
                    for (int i = 1; i <= 13; i++)
                    {
                        string sp = "POSITION_ID1I" + i;
                        string cposid = mobj.GetFieldValue(sp).ToUpper().Trim();
                        posids.Add(cposid);
                        if (cposid.Equals(positionId.ToUpper().Trim()))
                        {
                            mobj.SetFieldValue("ITEM_NUM1I", "" + i);
                            mobj.ExecuteCommand("OK");
                            break;
                        }
                        if (i == 13)
                        {
                            string err = "No Position ID match MIMS Position ID found on screen MSM004A\r\n";
                            err += "Available: " + string.Join(",", posids.ToArray()) + "\r\n";
                            err += "Needed: " + positionId;
                            throw new Exception(err);
                        }
                    }
                }
                else
                {
                    throw new Exception("No Position ID supplied");
                }
            }
            if (mobj.GetErrorMessage().StartsWith("X2"))
            {
                throw new Exception(mobj.GetErrorMessage() + " - Active Field: " + mobj.GetActiveField());
            }
            while (mobj.GetErrorMessage().StartsWith("W2"))
            {
                if (mobj.GetActiveField().Equals("CURRENCY_TYPE1I"))
                {
                    mobj.SetFieldValue(mobj.GetActiveField(), currency);
                }
                if (mobj.CheckForScreen("MSM265A", false))
                {
                    result.FillingNo = mobj.GetFieldValue("VCHR_NO1I");
                }
                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
            }
            if (mobj.IsCommand("Confirm"))
            {
                if (mobj.CheckForScreen("MSM265A", false))
                {
                    result.FillingNo = mobj.GetFieldValue("VCHR_NO1I");
                }
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.CheckForScreen("MSM096B", false))
            {
                loggerinfo("MSM096B");
                //key data MSM096B
                if (mobj.IsCommand("Edit Mode"))
                {
                    mobj.ExecuteCommand("Edit Mode");
                }
                else if (mobj.IsCommand("Entry Mode"))
                {
                    mobj.ExecuteCommand("Entry Mode");
                }
                else
                {
                    throw new Exception("No Edit/Entry Mode");
                }
                mobj.CheckForScreen("MSM096B", true);

                mobj.SetFieldValue("EDITCODE2I1", "COPY");

                taxInvoiceDate = (taxInvoiceDate == null) ? "" : taxInvoiceDate.Trim();
                taxInvoiceNo = (taxInvoiceNo == null) ? "" : taxInvoiceNo.Trim();
                taxOrgInvoiceNo = (taxOrgInvoiceNo == null) ? "" : taxOrgInvoiceNo.Trim();
                refDate = (refDate == null) ? "" : refDate.Trim();
                refNo = (refNo == null) ? "" : refNo.Trim();
                refOrgInvoiceNo = (refOrgInvoiceNo == null) ? "" : refOrgInvoiceNo.Trim();

                if (!taxInvoiceDate.Equals("") || !taxInvoiceNo.Equals("") || !taxOrgInvoiceNo.Equals(""))
                {
                    //ppn
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPN");
                    mobj.ExecuteCommand("OK");

                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " " + taxInvoiceDate);
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " " + taxInvoiceNo);
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + taxOrgInvoiceNo);
                }
                else if (!refDate.Equals("") || !refNo.Equals("") || !refOrgInvoiceNo.Equals(""))
                {
                    //pph
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");
                    mobj.ExecuteCommand("OK");

                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " " + refDate);
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " " + refNo);
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + refOrgInvoiceNo);
                }
                else if (!refOrgInvoiceNo.Equals(""))
                {
                    //invoice
                    mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");
                    mobj.ExecuteCommand("OK");

                    mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " " + refDate);
                    mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " " + refNo);
                    mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + refOrgInvoiceNo);
                }
                mobj.ExecuteCommand("OK");
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Exit");
                }
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Revert");
                }
            }
            result.Text = mobj.GetErrorMessage();

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO877
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="district"></param>
        /// <param name="authBy"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        public static string ApproveInvoiceItemsDo(MincomObject mobj, string district, string authBy, string positionId)
        {
            string result = "";
            mobj.GoHome();

            mobj.ExecuteMSO("MSO877");
            mobj.CheckForScreen("MSM877A", true);
            if (authBy == null)
            {
                authBy = ServiceHelper.GetConfig("Authorize");
            }
            if (positionId == null)
            {
                positionId = ServiceHelper.GetConfig("AuthorizePositionID");
            }
            mobj.SetFieldValue("TYPE_FILTER1I", "IN");
            mobj.SetFieldValue("STATUS_FILTER1I", "U");
            mobj.SetFieldValue("DISTRICT1I", district);
            mobj.SetFieldValue("EMPLOYEE_ID1I", authBy);
            mobj.SetFieldValue("POSITION_ID1I", positionId);

            mobj.ExecuteCommand("OK");

            if (mobj.GetErrorMessage().ToUpper().Contains("POSITION HAS NO TRANSACTIONS TO AUTHORIZE IN"))
            {
                result = "No Transactions";
            }
            else
            {
                int items = 0;
                for (int i = 1; i <= 3; i++)
                {
                    if (mobj.GetFieldValue("TRAN_TYPE1I" + i).Trim().ToUpper().Equals("IN"))
                    {
                        mobj.SetFieldValue("ACTION1I" + i, "I");
                        items++;
                    }
                }
                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().Trim().Equals(""))
                {
                    result = "Approve All - Items Found: " + items;
                }
                else
                {
                    result = mobj.GetErrorMessage();
                }
            }

            return JsonHelper.GetJsonString(result);
        }

        private string LoadNonOrderInvoiceSingle(MincomObject mobj, MincomParameter mp)
        {
            string result = null;
            List<LoadNonOrderItems> itemList = new List<LoadNonOrderItems>();
            try
            {
                foreach (MincomParameter item in mp.GetArray("items"))
                {
                    LoadNonOrderItems itm = new LoadNonOrderItems();
                    itm.description = item.GetString("description");
                    itm.value = item.GetString("value");
                    itm.district = item.GetString("district");
                    itm.authBy = item.GetString("authBy");
                    itm.account = item.GetString("account");
                    itm.workOrder = item.GetString("workOrder");
                    itm.project = item.GetString("project");
                    itm.equipNo = item.GetString("equipNo");
                    itm.positionId = item.GetString("positionId");
                    itemList.Add(itm);
                }
            }
            catch (Exception ex) { loggerinfo("Error parsing: " + ex.Message); }

            //split if item > 500
            int splitSize = 500;
            int.TryParse(ServiceHelper.GetConfig("ChildThreadMaxItem"), out splitSize);

            List<Thread> threads = new List<Thread>();
            List<MincomFunctions> childs = new List<MincomFunctions>();
            List<object> prms = new List<object>();
            int tcnt = 0;
            for (int i = 0; i < itemList.Count; i += splitSize)
            {
                int size = (itemList.Count >= i + splitSize) ? splitSize : itemList.Count - i;
                List<LoadNonOrderItems> itemsTemp = itemList.GetRange(i, size);
                LoadNonOrderItems[] items = itemsTemp.ToArray();
                string invoiceNo = mp.GetString("invoiceNo");
                string invoiceAmt = mp.GetString("invoiceAmt");
                if (itemList.Count > splitSize)
                {
                    invoiceNo = string.Format("{0}-{1}", invoiceNo, (i / splitSize) + 1);
                    CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
                    double sum = 0;
                    for (int j = 0; j < items.Length; j++)
                    {
                        double value = 0;
                        double.TryParse(items[j].value, System.Globalization.NumberStyles.Any, ci, out value);
                        sum += value;
                    }
                    invoiceAmt = string.Format(ci, "{0:N2}", sum);
                    //invoiceAmt = sum.ToString(ci);
                    loggerinfo(string.Format("Split {0}: {1} amount {2}", i, invoiceNo, invoiceAmt));
                }
                Thread t = new Thread(new ParameterizedThreadStart(ProcessLoadNonOrderInvoiceThread));
                threads.Add(t);
                object[] prm ={mp.GetString("districtCode"), mp.GetString("supplierNo"), invoiceNo, invoiceAmt,
                        mp.GetString("branchCode"), mp.GetString("bankAccountNo"), mp.GetString("accountant"), mp.GetString("authBy"),
                    mp.GetString("positionId"), mp.GetString("currency"), mp.GetString("invoiceCommentType"), mp.GetString("refOrgInvoiceNo"),
                    mp.GetString("handlingCode"),mp.GetString("invoiceReceived"), mp.GetString("invoiceDate"), mp.GetString("dueDate"), items};
                //LoadNonOrderInvoiceSingle(mobj, jsd.supplierNo, invoiceNo, invoiceAmt,
                //    jsd.branchCode, jsd.bankAccountNo, jsd.accountant, jsd.authBy, jsd.positionId, jsd.currency, jsd.invoiceCommentType, jsd.refOrgInvoiceNo, jsd.handlingCode,
                //    jsd.invoiceReceived, jsd.invoiceDate, jsd.dueDate, items);
                prms.Add(prm);
                MincomFunctions child = new MincomFunctions();
                childs.Add(child);
                child.threadid = this.threadid.ToString() + "_" + (i / splitSize).ToString();
                //start the thread 1 by 1
                t.Start(new object[]{
                        child,
                        prm
                    });
                tcnt++;
            }
            string textResult = "";
            string fillingNoResult = "";
            string errormessage = null;
            int[] tstatus = new int[tcnt];
            for (int i = 0; i < tcnt; i++)
            {
                tstatus[i] = -1;
                //wait thread 1 by 1
                threads[i].Join();
                object[] childout = (object[])childs[i].result;
                LoadNonOrderResult r = null;
                Exception e = null;
                //childout 0 result obj, 1 error obj
                if (childout != null && childout[0] != null)
                {
                    //success
                    r = (LoadNonOrderResult)childout[0];
                    if (i > 0)
                    {
                        fillingNoResult += ";";
                        textResult += ";";
                    }
                    fillingNoResult += r.FillingNo;
                    textResult += r.Text;
                    tstatus[i] = 1;
                }
                if (childout != null && childout[1] != null)
                {
                    //error
                    e = (Exception)childout[1];
                    errormessage = e.Message;
                    tstatus[i] = 0;
                }
            }

            //errormessage = null;

            if (errormessage == null)
            {
                //everything ok, no error
                //now create job split
                JobSplit[] ids = new JobSplit[tcnt];
                for (int i = 0; i < tcnt; i++)
                {
                    object[] prm = (object[])prms[i];
                    ids[i] = new JobSplit((string)prm[2], (string)prm[3]);
                    loggerinfo(string.Format("Jobsplit {0} {1}", prm[2], prm[3]));
                }
                job.CreateJobSplit("", "LoadNonOrderInvoice", mp.GetString("invoiceNo"), ids);
                LoadNonOrderResult rs = new LoadNonOrderResult();
                rs.Text = textResult;
                rs.FillingNo = fillingNoResult;
                result = JsonHelper.GetJsonString(rs);
            }
            else
            {
                //there is error
                //cancel all loaded invoices
                for (int i = 0; i < tcnt; i++)
                {
                    if (tstatus[i] == 1)
                    {
                        //cancel loaded
                        object[] prm = (object[])prms[i];
                        loggerinfo("Cancel invoice #" + prm[2]);
                        string cancelresult = CancelInvoiceDo(mobj, (string)prm[0], (string)prm[1], (string)prm[2]);
                        loggerinfo(cancelresult);
                    }
                }

                skipcheckscreen = true; //check screen causing system crash
                throw new Exception(errormessage);
            }
            return result;
        }

        /// <summary>
        /// MSO265
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="districtCode"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <param name="invoiceAmt"></param>
        /// <param name="branchCode"></param>
        /// <param name="bankAccountNo"></param>
        /// <param name="accountant"></param>
        /// <param name="authBy"></param>
        /// <param name="positionId"></param>
        /// <param name="currency"></param>
        /// <param name="invoiceCommentType"></param>
        /// <param name="handlingCode"></param>
        /// <param name="invoiceReceived"></param>
        /// <param name="invoiceDate"></param>
        /// <param name="dueDate"></param>
        /// <param name="invoiceItems"></param>
        /// <returns></returns>
        private LoadNonOrderResult LoadNonOrderInvoiceSingleDo(MincomObject mobj, string districtCode, string supplierNo, string invoiceNo, string invoiceAmt,
            string branchCode, string bankAccountNo, string accountant, string authBy, string positionId, string currency, string invoiceCommentType, string refOrgInvoiceNo, string handlingCode,
            string invoiceReceived, string invoiceDate, string dueDate, LoadNonOrderItems[] invoiceItems)
        {
            mobj.GoHome();
            LoadNonOrderResult result = new LoadNonOrderResult();

            mobj.ExecuteMSO("MSO265");
            mobj.CheckForScreen("MSM265A", true);
            string fillingNo = mobj.GetFieldValue("VCHR_NO1I");

            //key data MSM265A
            mobj.SetFieldValue("DSTRCT_CODE1I", districtCode);
            mobj.SetFieldValue("SUPPLIER_NO1I", supplierNo);
            mobj.SetFieldValue("INV_NO1I", invoiceNo);
            mobj.SetFieldValue("INV_AMT1I", invoiceAmt);
            if (branchCode.Trim().Length > 0)
            {
                mobj.SetFieldValue("BRANCH_CODE1I", branchCode);
            }
            if (bankAccountNo.Trim().Length > 0)
            {
                mobj.SetFieldValue("BANK_ACCT_NO1I", bankAccountNo);
            }
            if (accountant == null)
            {
                accountant = ServiceHelper.GetConfig("Accountant");
            }
            mobj.SetFieldValue("ACCOUNTANT1I", accountant);
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);
            if (invoiceCommentType.Trim().Length > 0)
            {
                mobj.SetFieldValue("INV_COMM_TYPE1I", invoiceCommentType);
            }
            if (handlingCode.Trim().Length > 0)
            {
                mobj.SetFieldValue("HANDLE_CDE1I", handlingCode);
            }
            //as of request 3 oct 2014 switch dates
            //mobj.SetFieldValue("INV_DATE1I", invoiceDate);
            //mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceReceived);
            mobj.SetFieldValue("INV_DATE1I", invoiceReceived);
            mobj.SetFieldValue("INV_RCPT_DATE1I", invoiceDate);
            mobj.SetFieldValue("DUE_DATE1I", dueDate);

            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
            loggerinfo("Found item records: " + invoiceItems.Length);
            double sum = 0;
            for (int i = 0; i < invoiceItems.Length; i++)
            {
                double value = 0;
                double.TryParse(invoiceItems[i].value, System.Globalization.NumberStyles.Any, ci, out value);
                sum += value;
            }
            double amt = 0;
            double.TryParse(invoiceAmt, System.Globalization.NumberStyles.Any, ci, out amt);
            string amountsum = string.Format("Invoice Amount: {0:N2} - Sum Values: {1:N2}", amt, sum);
            if (Math.Abs(amt - sum) < 0.01)
            {
                loggerinfo("Invoice amount match: " + amountsum);
            }
            else
            {
                throw new Exception("Invoice amount not match: " + amountsum);
            }

            int counter = 0;
            if (authBy == null)
            {
                authBy = ServiceHelper.GetConfig("Authorize");
            }
            while (counter < invoiceItems.Length)
            {
                mobj.CheckForScreen("MSM265A", true);
                StringBuilder sblog = new StringBuilder();
                for (int i = 1; i <= 3; i++)
                {
                    if (counter + i > invoiceItems.Length)
                        break;
                    LoadNonOrderItems invoiceItem = invoiceItems[counter + i - 1];
                    sblog.Append("\r\n");
                    sblog.Append("Set item " + mobj.GetFieldValue("INV_ITEM_NO1I" + i) + ":" + invoiceItem.description);
                    mobj.SetFieldValue("INV_ITEM_DESC1I" + i, invoiceItem.description);
                    mobj.SetFieldValue("INV_ITEM_VALUE1I" + i, invoiceItem.value);
                    mobj.SetFieldValue("ACCT_DSTRCT1I" + i, invoiceItem.district);
                    //mobj.SetFieldValue("AUTH_BY1I" + i, invoiceItem.authBy);
                    mobj.SetFieldValue("AUTH_BY1I" + i, authBy);
                    mobj.SetFieldValue("ACCOUNT1I" + i, invoiceItem.account);
                    mobj.SetFieldValue("WORK_ORDER1I" + i, invoiceItem.workOrder);
                    mobj.SetFieldValue("PROJECT_IND1I" + i, invoiceItem.project);
                    mobj.SetFieldValue("PLANT_NO1I" + i, invoiceItem.equipNo);
                }
                loggerinfo(sblog.ToString());

                mobj.ExecuteCommand("OK");
                //                loggerinfo(mobj.DumpFields());
                while (mobj.CheckForScreen("MSM004A", false))
                {
                    string trans = mobj.GetFieldValue("TRANS_DETAIL1I").Trim();
                    trans = trans.Substring(trans.Length - 3);
                    int pos = 0;
                    int.TryParse(trans, out pos);
                    if (positionId == null)
                    {
                        positionId = ServiceHelper.GetConfig("AuthorizePositionID");
                    }
                    loggerinfo("Position ID:" + positionId);
                    //                    string authPosId = invoiceItems[pos].positionId.Trim();

                    if (!positionId.Equals(""))
                    {
                        List<string> posids = new List<string>();
                        for (int i = 1; i <= 13; i++)
                        {
                            string sp = "POSITION_ID1I" + i;
                            string cposid = mobj.GetFieldValue(sp).ToUpper().Trim();
                            posids.Add(cposid);
                            if (cposid.Equals(positionId.ToUpper().Trim()))
                            {
                                mobj.SetFieldValue("ITEM_NUM1I", "" + i);
                                mobj.ExecuteCommand("OK");
                                break;
                            }
                            if (i == 13)
                            {
                                string err = "No Position ID match MIMS Position ID found on screen MSM004A\r\n";
                                err += "Available: " + string.Join(",", posids.ToArray()) + "\r\n";
                                err += "Needed: " + positionId;
                                throw new Exception(err);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("No Position ID supplied");
                    }
                }
                if (mobj.GetErrorMessage().StartsWith("X2"))
                {
                    throw new Exception(mobj.GetErrorMessage() + " - Active Field: " + mobj.GetActiveField());
                }
                while (mobj.GetErrorMessage().StartsWith("W2"))
                {
                    if (mobj.GetActiveField().Equals("CURRENCY_TYPE1I"))
                    {
                        mobj.SetFieldValue(mobj.GetActiveField(), currency);
                    }
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().StartsWith("X2"))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                }
                if (mobj.IsCommand("Confirm"))
                {
                    mobj.ExecuteCommand("Confirm");
                }

                counter += 3;
            }//end loop while
            mobj.ExecuteCommand("OK");

            if (mobj.CheckForScreen("MSM096B", false))
            {
                //key data MSM096B
                if (mobj.IsCommand("Edit Mode"))
                {
                    mobj.ExecuteCommand("Edit Mode");
                }
                else if (mobj.IsCommand("Entry Mode"))
                {
                    mobj.ExecuteCommand("Entry Mode");
                }
                else
                {
                    throw new Exception("No Edit/Entry Mode");
                }
                mobj.CheckForScreen("MSM096B", true);

                mobj.SetFieldValue("EDITCODE2I1", "COPY");
                //set ppn/pph, but is not, type PPH anyway
                mobj.SetFieldValue("STD_TEXT_C2I1", "PPH");

                mobj.ExecuteCommand("OK");
                mobj.CheckForScreen("MSM096B", true);
                //set detail
                mobj.SetFieldValue("STD_TEXT_C2I1", mobj.GetFieldValue("STD_TEXT_C2I1").Trim() + " ");
                mobj.SetFieldValue("STD_TEXT_C2I2", mobj.GetFieldValue("STD_TEXT_C2I2").Trim() + " ");
                mobj.SetFieldValue("STD_TEXT_C2I3", mobj.GetFieldValue("STD_TEXT_C2I3").Trim() + " " + refOrgInvoiceNo);

                mobj.ExecuteCommand("OK");
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Exit");
                }
                if (mobj.CheckForScreen("MSM096B", false))
                {
                    mobj.ExecuteCommand("Revert");
                }
            }

            if (mobj.CheckForScreen("MSM265A", false))
            {
                for (int i = 0; i < 4; i++)
                {
                    if (mobj.GetErrorMessage().Trim().Length > 0)
                    {
                        if (Regex.IsMatch(mobj.GetErrorMessage(), "invoice.*?loaded\\s+(un)?approved",
                            RegexOptions.IgnoreCase | RegexOptions.Singleline))
                        {
                            result.Text = mobj.GetErrorMessage();
                            result.FillingNo = fillingNo;
                            break;
                        }
                        else
                        {
                            throw new Exception("Error: " + mobj.GetErrorMessage());
                        }
                    }
                    else if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }
                    else if (mobj.IsCommand("OK"))
                    {
                        mobj.ExecuteCommand("OK");
                    }
                }//end loop for
            }
            else
            {
                throw new Exception("Error: Invoice not loaded, screen: " + mobj.GetScreenName());
            }
            //            loggerinfo(mobj.DumpFields());

            return result;
        }

        public string CancelInvoice(MincomObject mobj, MincomParameter mp)
        {
            string result = null;

            string[] splitids = job.GetJobSplit("LoadNonOrderInvoice", (dynamic)mp.GetString("invoiceNo"));
            if (splitids == null)
            {
                splitids = new string[] { mp.GetString("invoiceNo") };
            }
            for (int i = 0; i < splitids.Length; i++)
            {
                result = CancelInvoiceDo(mobj, mp.GetString("districtCode"), mp.GetString("supplierNo"), splitids[i]);
            }

            return result;
        }

        /// <summary>
        /// MSO262
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="districtCode"></param>
        /// <param name="supplierNo"></param>
        /// <param name="invoiceNo"></param>
        /// <returns></returns>
        private string CancelInvoiceDo(MincomObject mobj, string districtCode, string supplierNo, string invoiceNo)
        {
            mobj.GoHome();
            //            LoadNonOrderResult result = new LoadNonOrderResult();

            mobj.ExecuteMSO("MSO262");
            mobj.CheckForScreen("MSM262A", true);

            mobj.SetFieldValue("OPTION1I", "1");
            mobj.SetFieldValue("DSTRCT_CODE1I", districtCode);
            mobj.SetFieldValue("SUPPLIER_NO1I", supplierNo);
            mobj.SetFieldValue("INV_NO1I", invoiceNo);
            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM262B", true);
            mobj.SetFieldValue("REMOVE_INV2I", "Y");
            mobj.SetFieldValue("CONFIRM2I", "Y");
            mobj.ExecuteCommand("OK");

            string result = "";
            if (!mobj.CheckForScreen("MSM262A", false))
            {
                throw new Exception(mobj.GetErrorMessage());
            }
            else
            {
                result = mobj.GetErrorMessage().Trim();
                if (result.EndsWith("CANCELLED & REMOVED off file")
                    || result.EndsWith("CANCELLED"))
                {
                    result = "Invoice successfully cancelled: " + invoiceNo;
                    loggerinfo(result);
                }
                else
                {
                    throw new Exception(result);
                }
            }

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO230
        /// </summary>
        /// <param name="mobj"></param>
        /// <returns></returns>
        private string CreatePurchaseOrder(MincomObject mobj, MincomParameter mp)
        {
            //if (operation.Equals("CreatePurchaseOrder"))
            //{
            //    List<string> desc = new List<string>();
            //    List<string> qty = new List<string>();
            //    List<string> price = new List<string>();
            //    try
            //    {
            //        foreach (dynamic item in jsd.items)
            //        {
            //            desc.Add(item["description"]);
            //            qty.Add(item["qty"]);
            //            price.Add(item["price"]);
            //        }
            //    }
            //    catch (Exception ex) { loggerinfo("Error parsing: " + ex.Message); }
            //    result = CreatePurchaseOrderDo(mobj, jsd.purchaseNo, jsd.supplierNo, desc.ToArray(), qty.ToArray(), price.ToArray(), jsd.currency);
            //}

            string purchaseNo = mp.GetString("purchaseNo");
            string supplierNo = mp.GetString("supplierNo");
            string[] desc = mp.GetStringArray("items", "description");
            string[] qty = mp.GetStringArray("items", "qty");
            string[] price = mp.GetStringArray("items", "price");
            string currency = mp.GetString("currency");

            mobj.GoHome();

            mobj.ExecuteMSO("MSO230");
            mobj.CheckForScreen("MSM230A", true);
            mobj.SetFieldValue("OPTION1I", "2");
            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM23BA", true);
            mobj.SetFieldValue("REQUEST_BY1I", "KB13007");
            mobj.SetFieldValue("REQ_BY_DATE1I", string.Format("{0:dd/M/yy}", DateTime.Today));
            mobj.SetFieldValue("DEL_MOD1I", "Y");
            mobj.SetFieldValue("SUPPLIER_NO1I", supplierNo);
            mobj.SetFieldValue("CURRENCY_TYPE1I", currency);
            mobj.SetFieldValue("FREIGHT_CODE1I", "NA");
            mobj.SetFieldValue("DELIV_LOCATION1I", "KPJK");
            mobj.SetFieldValue("ORIGIN_CODE1I", "JK");
            mobj.SetFieldValue("WHOUSE_ID1I", "JKHO");
            mobj.SetFieldValue("PURCH_OFFCER_D1I", "KB13007");
            mobj.SetFieldValue("PO_NO1I", purchaseNo);
            mobj.SetFieldValue("CURR_DUE_DATE1I", string.Format("{0:dd/M/yy}", DateTime.Today));
            mobj.ExecuteCommand("OK");
            //            loggerinfo(string.Join(",", mobj.GetCommands()));
            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM232A", true);
            //mobj.SetFieldValue("COST_CEN_ACCT1I1", "888888000027004");
            //mobj.SetFieldValue("COST_CEN_ACCT1I1", "888888201722252");
            mobj.SetFieldValue("COST_CEN_ACCT1I1", "888888000027001");
            mobj.ExecuteCommand("OK");
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            mobj.CheckForScreen("MSM23CA", true);
            for (int i = 0; i < qty.Length; i++)
            {
                int pos = (i % 3) + 1;
                mobj.SetFieldValue("TYPE1I" + pos, "G");
                mobj.SetFieldValue("QTY_REQD1I" + pos, qty[i]);
                mobj.SetFieldValue("ACT_GROSS_PR1I" + pos, price[i]);
                mobj.SetFieldValue("UOM1I" + pos, "EACH");
                mobj.SetFieldValue("ACTION1I" + pos, "L");
                mobj.SetFieldValue("DESC_LINE_11I" + pos, desc[i]);
                if (pos == 3)
                {
                    mobj.ExecuteCommand("OK");
                    purchaseNo = mobj.GetFieldValue("PO_NO1I1");
                    if (mobj.IsCommand("Confirm"))
                    {
                        mobj.ExecuteCommand("Confirm");
                    }
                }
            }
            //DESC_LINE_21I1
            //DESC_LINE_31I1
            //DESC_LINE_41I1
            mobj.ExecuteCommand("OK");
            if (mobj.IsCommand("Confirm"))
            {
                mobj.ExecuteCommand("Confirm");
            }

            if (mobj.CheckForScreen("MSM23CA", false))
            {
                throw new Exception(string.Format("Error: {0} - Active Control: {1}", mobj.GetErrorMessage(), mobj.GetActiveField()));
            }

            string receive = ReceivePurchaseOrder(mobj, mp);
            string result = purchaseNo + " - " + receive;

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// MSO150
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="purchaseNo"></param>
        /// <returns></returns>
        private string ReceivePurchaseOrder(MincomObject mobj, MincomParameter mp)
        {
            //if (operation.Equals("ReceivePurchaseOrder"))
            //{
            //    result = ReceivePurchaseOrderDo(mobj, jsd.purchaseNo);
            //}

            //string purchaseNo
            mobj.GoHome();

            mobj.ExecuteMSO("MSO150");
            mobj.CheckForScreen("MSM150A", true);

            mobj.SetFieldValue("OPTION1I", "2");
            mobj.SetFieldValue("PO_NO1I", mp.GetString("purchaseNo"));
            mobj.SetFieldValue("STOREMAN1I", "KB13007");
            mobj.SetFieldValue("WHOUSE_ID1I", "JKHO");
            mobj.ExecuteCommand("OK");

            if (mobj.CheckForScreen("MSM004A", false))
            {
                mobj.SetFieldValue("ITEM_NUM1I", "1");
                mobj.ExecuteCommand("OK");
            }

            mobj.CheckForScreen("MSM152B", true);
            mobj.SetFieldValue("RECEIVE_ALL2I", "Y");
            mobj.ExecuteCommand("OK");
            mobj.ExecuteCommand("Confirm");

            int items = 0;
            while (mobj.CheckForScreen("MSM23GD", false))
            {
                items++;
                mobj.ExecuteCommand("OK");
            }
            string result = string.Format("Items received: {0}", items);

            return JsonHelper.GetJsonString(result);
        }

        /// <summary>
        /// items
        /// </summary>
        //class PurchaseRequisitionItem
        //{
        //    public string qty;
        //    public string uom;
        //    public string currency;
        //    public string price;
        //    public string description;
        //    public string accountcode;
        //}

        /// <summary>
        /// create pr
        /// </summary>
        /// <param name="mobj"></param>
        /// <param name="requestby"></param>
        /// <param name="position"></param>
        /// <param name="requestbydate"></param>
        /// <param name="suggestedsupplier"></param>
        /// <param name="accountcode">left empty to input account code per item</param>
        /// <param name="woproject"></param>
        /// <param name="indicator"></param>
        /// <param name="equipref"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        ///        private static string CreatePurchaseRequisition(MincomObject mobj, string requestby, string position, string requestbydate, string suggestedsupplier, string accountcode, string woproject, string indicator, string equipref, PurchaseRequisitionItem[] items)
        private string CreatePurchaseRequisition(MincomObject mobj, MincomParameter mp)
        {
            string result = "";

            mobj.GoHome();

            mobj.ExecuteMSO("MSO230");
            mobj.CheckForScreen("MSM230A", true);
            mobj.SetFieldValue("OPTION1I", "1");
            mobj.ExecuteCommand("OK");

            mobj.CheckForScreen("MSM23BA", true);
            result = mobj.GetFieldValue("PREQ_NO1I").Trim();
            mobj.SetFieldValue("REQUEST_BY1I", mp.GetString("requestby")); //KB15009 / KB13048
            mobj.SetFieldValue("REQ_BY_DATE1I", mp.GetString("requestbydate"));// 7 / 16 / 2020
            if (mp.GetString("indicator") == "P" || mp.GetString("accountcode") != "") // costing per pr
            {
                mobj.SetFieldValue("DEL_MOD1I", "Y"); //create costing alloc
            }
            //mobj.SetFieldValue("SUPPLIER_NO1I", suppliercode);// 88
            mobj.SetFieldValue("SUGGESTED_SUPP1I", mp.GetString("suggestedsupplier"));
            mobj.SetFieldValue("DELIV_INST1I", mp.GetString("docNo"));
            mobj.ExecuteCommand("OK");
            if (mobj.GetScreenName() == "MSM004A")
            {
                //choose position
                //ITEM1I1: 1
                //POSITION_ID1I1: KP1SM0212
                //POS_TITLE1I1    : SM SYSTEM DEVELOPMENT OFFICER
                //ITEM_NUM1I
                //mobj.SetFieldValue("ITEM_NUM1I", "1"); //choose first item if not found
                bool found = false;
                for (int i = 1; i <= 13; i++)
                {
                    if (mobj.GetFieldValue("POSITION_ID1I" + i).Trim().ToUpper() == mp.GetString("position").ToUpper())
                    {
                        mobj.SetFieldValue("ITEM_NUM1I", i.ToString());
                        mobj.ExecuteCommand("OK");
                        found = true;
                        break;
                    }
                }
                if (!found)
                    throw new Exception(string.Format("Position '{0}' not found", mp.GetString("position")));
            }
            if (mobj.GetScreenName() == "MSM23BA")
            {
                mobj.ExecuteCommand("OK");//confirm
            }

            if (mp.GetString("indicator") == "P" || mp.GetString("accountcode") != "") // costing per pr
            {
                mobj.CheckForScreen("MSM232A", true);
                //repeating?
                mobj.SetFieldValue("COST_CEN_ACCT1I1", mp.GetString("accountcode"));// 470020
                mobj.SetFieldValue("WO_PROJECT1I1", mp.GetString("woproject"));// C20CA01A
                mobj.SetFieldValue("PROJECT_IND1I1", mp.GetString("indicator")); //P/ W indicator
                mobj.SetFieldValue("PLANT_NO1I1", mp.GetString("equipref")); // equipment reference
                mobj.SetFieldValue("ALLOC_PC1I1", "100");
                mobj.ExecuteCommand("OK");
                mobj.ExecuteCommand("OK");
                //loggerinfo(mobj.GetErrorMessage());
            }
            for (int i = 0; i < mp.Count("items"); i++)
            {
                MincomParameter mpi = mp.Get("items[" + i + "]");
                mobj.CheckForScreen("MSM23EA", true);
                string itemno = mobj.GetFieldValue("PREQ_ITEM_NO1I");
                if (!itemno.Equals(string.Format("{0:000}", i + 1)))
                {
                    //LogHelper.LogMessage();
                    throw new Exception(string.Format("Invalid item no {0}, expected {1:000}", itemno, i + 1));
                    //not valid
                }
                //repeating
                mobj.SetFieldValue("PREQ_TYPE1I", "G"); // G goods, F field release, Q quote item, S service order
                mobj.SetFieldValue("QTY_REQD1I", mpi.GetString("qty"));
                mobj.SetFieldValue("UOM1I", mpi.GetString("uom")); // - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'UOM'
                mobj.SetFieldValue("CURR_TYPE1I", mpi.GetString("currency"));// - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'CUR_TYPE'
                mobj.SetFieldValue("EST_PRICE1I", mpi.GetString("price"));
                //PART_NO1I
                mobj.SetFieldValue("DESC_LINE_11I", mpi.GetString("description")); //max 40 chars!!
                //mobj.SetFieldValue("DESC_LINE_21I", items[i].description); //max 40 chars!!
                //mobj.SetFieldValue("DESC_LINE_31I", items[i].description); //max 40 chars!!
                //mobj.SetFieldValue("DESC_LINE_41I", items[i].description); //max 40 chars!!
                mobj.SetFieldValue("GROUP_CLASS1I", "0000");
                //* EST_FREIGHT1I
                //mobj.SetFieldValue("SUPPLIER_NO1I", suppliercode);
                //SUPPLIER_NAME1I - output
                //* ACT_GROSS_PR1I
                //* PURCH_OFFICER1I
                //CURRENCY_TYPE1I
                //* FREIGHT_CODE1I -options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'FREIGHT_CODE'
                //* ORIGIN_CODE1I - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'ORIGIN_CODE'
                //* ORIGIN_CODE1I - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'ORIGIN_CODE'
                //EXPED_CODE1I - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'EXPED_CODE'
                //PO_MEDIUM_FLG1I P paper
                //*QUOTE_NO1I
                //PRICE_CODE1I - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'PRICE_CODE'
                //SUPP_LEAD_TIME1I
                //DELIV_LOCATION1I - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'DELIV_LOCATION'
                //WHOUSE_ID1I - options: select opt_value, opt_desc from DB_EllipseHelper.input_options where group_name = 'WHOUSE_ID'
                //WHOLD_TAX_CODE1I
                if (mp.GetString("indicator") == "P" || mp.GetString("accountcode") != "") // costing per pr
                {
                    mobj.ExecuteCommand("OK");
                    if (!mobj.GetErrorMessage().Trim().Equals(""))
                    {
                        throw new Exception(mobj.GetErrorMessage());
                        //LogHelper.LogMessage(mobj.GetActiveField());
                        //mobj.ExecuteCommand("OK");
                        //LogHelper.LogMessage(mobj.DumpFields());
                    }
                }
                else
                {
                    //costing per item
                    mobj.SetFieldValue("ACTION_A1I", "A");
                    mobj.ExecuteCommand("OK");
                    mobj.CheckForScreen("MSM232A", true);
                    //REQ_ITEM1I
                    //repeating?
                    mobj.SetFieldValue("COST_CEN_ACCT1I1", mpi.GetString("accountcode"));// 888888201712003
                    //mobj.SetFieldValue("WO_PROJECT1I1", mp.GetString("woproject")); // tidak diinput
                    //mobj.SetFieldValue("PROJECT_IND1I1", indicator); // tidak diinput
                    mobj.SetFieldValue("PLANT_NO1I1", mp.GetString("equipref")); // equipment reference
                    mobj.SetFieldValue("ALLOC_PC1I1", "100");
                    mobj.ExecuteCommand("OK");
                    mobj.ExecuteCommand("OK");
                    if (mobj.GetErrorMessage().Trim() != "")
                    {
                        throw new Exception(mobj.GetErrorMessage());
                    }
                    //loggerinfo(mobj.GetErrorMessage());
                    mobj.ExecuteCommand("OK");
                }
            }
            mobj.ExecuteCommand("OK");
            loggerinfo("screen: " + mobj.GetScreenName());

            //return JsonHelper.GetJsonString(result);
            return result;
        }

        private string CheckPurchaseRequisition(MincomObject mobj, MincomParameter mp)
        {
            string status = null;
            string reqby = null;
            mobj.GoHome();

            mobj.ExecuteMSO("MSO231");

            mobj.CheckForScreen("MSM231A", true);

            if (mp.GetString("itemno") == "")
            {
                mobj.SetFieldValue("PREQ_NO1I", mp.GetString("prno"));
                mobj.ExecuteCommand("OK");
                if (mobj.CheckForScreen("MSM23GB", false))
                {
                    mobj.SetFieldValue("ACTION_A2I", "H");
                    mobj.ExecuteCommand("OK");
                }
                mobj.CheckForScreen("MSM23GA", true);
                status = mobj.GetFieldValue("AUTHSD_STATUS1I");
                reqby = mobj.GetFieldValue("REQUESTED_BY1I");
            }
            else
            {
                mobj.SetFieldValue("PREQ_NO1I", mp.GetString("prno"));
                mobj.SetFieldValue("PREQ_ITEM_NO1I", mp.GetString("itemno"));
                mobj.ExecuteCommand("OK");
                mobj.CheckForScreen("MSM23GB", true);
                status = mobj.GetFieldValue("ORD_ST_LIT2I");
                reqby = mobj.GetFieldValue("REQUESTED_BY1I");
            }

            JObject res = JObject.FromObject(new
            {
                Status = status,
                ReqBy = reqby
            });
            return res.ToString(Formatting.None);
        }

        private string ApprovePurchaseRequisition(MincomObject mobj, MincomParameter mp)
        {
            //USERAPPR
            //mso877
            //mso877a
            //DISTRICT1I kpho
            //EMPLOYEE_ID1I 196213
            //POSITION_ID1I KP2SD01
            //TRAN_TYPE1I1
            //TRAN_KEY1I1
            //ACTION1I1
            mobj.GoHome();
            mobj.ExecuteMSO("MSO877");
            mobj.SetFieldValue("DISTRICT1I", "KPHO");
            mobj.SetFieldValue("EMPLOYEE_ID1I", "196213");
            mobj.SetFieldValue("POSITION_ID1I", "KP2SD01");
            mobj.ExecuteCommand("OK");

            for (int i = 1; i <= 3; i++)
            {
                loggerinfo(mobj.GetFieldValue("TRAN_TYPE1I" + i));
                loggerinfo(mobj.GetFieldValue("TRAN_KEY1I" + i));
                loggerinfo(mobj.GetFieldValue("ACTION1I" + i));

            }


            return null;
        }

        private string CancelPurchaseRequisition(MincomObject mobj, MincomParameter mp)
        {
            string statjson = CheckPurchaseRequisition(mobj, mp);
            string stat = new MincomParameter(statjson).GetString("Status").Trim();
            //item:
            //Not Authorised
            //Not ordered
            //pr:
            //Unauthorised
            //Unauthor.;Pos.KP2Sxxx
            //Authorised By
            if (!stat.Equals("Not Ordered", StringComparison.OrdinalIgnoreCase)
                && !stat.StartsWith("Authorised By", StringComparison.OrdinalIgnoreCase)
                && !stat.StartsWith("Unauthor", StringComparison.OrdinalIgnoreCase)
                && !stat.Equals("Not Authorised", StringComparison.OrdinalIgnoreCase))
            {
                //ordered!! reject!
                throw new Exception("Error status: " + stat);
            }

            string result = null;

            mobj.GoHome();

            mobj.ExecuteMSO("MSO230");
            mobj.CheckForScreen("MSM230A", true);
            mobj.SetFieldValue("OPTION1I", "4");
            mobj.SetFieldValue("PREQ_NO1I", mp.GetString("prno"));
            mobj.SetFieldValue("PREQ_ITEM_NO1I", mp.GetString("itemno"));
            mobj.ExecuteCommand("OK");

            if (!mobj.GetErrorMessage().Trim().Equals(""))
            {
                //7974 - REQUISITION IS COMPLETE
                //K007 - ITEM HARUS DIISI UNTUK PR CAPITAL
                throw new Exception(mobj.GetErrorMessage().Trim());
            }

            if (mp.GetString("itemno") == "")
            {
                //per pr
                mobj.CheckForScreen("MSM23BA", true);
                mobj.SetFieldValue("DEL_MOD1I", "Y");
                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().Trim() != "")
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                result = "success";
            }
            else
            {
                //per item
                mobj.CheckForScreen("MSM23EA", true);
                mobj.SetFieldValue("PROCESS_ITEM1I", "D");
                mobj.ExecuteCommand("OK");
                if (mobj.GetErrorMessage().Trim() != "")
                {
                    throw new Exception(mobj.GetErrorMessage());
                }
                result = "success";
            }


            return result;
        }

    }
}