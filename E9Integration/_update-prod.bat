@echo off

set svpf=10.14.101.
set svlist=8 9
set svname="E9 Integration"
set src=X:\VS\E9Integration\bin\Debug
set target=c$\E9I
set updateconfig=false

choice /c YN /M "Update Config"
if %ERRORLEVEL% == 2 goto skipconfig
set updateconfig=true
:skipconfig

echo .
echo prepare network
for %%G in (%svlist%) do net use \\%svpf%%%G\c$ /user:kppmining\dmadmin kpp0811
net use
pause

echo .
set dt=%date:~10,4%%date:~4,2%%date:~7,2%
choice /c YN /M "Backup files to %dt% - DO THIS ONLY ONCE"
if %ERRORLEVEL% == 2 goto skipbackup
echo .
echo backup
for %%G in (%svlist%) do robocopy \\%svpf%%%G\%target%\ \\%svpf%%%G\%target%\bk%dt%\ *.exe *.dll *.pdb
pause
:skipbackup

echo .
echo query service
for %%G in (%svlist%) do echo query %%G && sc \\%svpf%%%G query %svname% && timeout 2
rem pause

rem exit

echo .
echo stop service
for %%G in (%svlist%) do echo stop %%G && sc \\%svpf%%%G stop %svname% && timeout 2
pause

echo .
echo updating
for %%G in (%svlist%) do echo updating %%G && robocopy %src% \\%svpf%%%G\%target%\ *.exe *.dll *.pdb && pause

if %updateconfig% neq true goto skipupdateconfig
echo .
echo updating config
for %%G in (%svlist%) do echo updating %%G && robocopy %src%\..\..\_INSTALL \\%svpf%%%G\%target%\ *.config && pause
rem pause
:skipupdateconfig

rem echo .
rem echo first time
rem for %%G in (%svlist%) do echo updating %%G && robocopy %src%\..\..\_INSTALL \\%svpf%%%G\%target%\ *.bat certificates && pause
rem pause

echo .
echo start service
for %%G in (%svlist%) do echo start %%G && sc \\%svpf%%%G start %svname% && timeout 2
rem pause

echo .
echo query service
for %%G in (%svlist%) do echo query %%G && sc \\%svpf%%%G query %svname% && timeout 2
rem pause

echo .
echo check
for %%G in (%svlist%) do echo check %%G && dir \\%svpf%%%G\%target%\*.exe
pause

