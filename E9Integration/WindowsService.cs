﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace E9Integration
{
    public class ErrorHandler : IErrorHandler
    {
        private static readonly ILog logger =
            LogManager.GetLogger(typeof(ErrorHandler));
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {

        }

        public bool HandleError(Exception error)
        {
            logger.Info(error.Message);
            logger.Info(error.StackTrace);
            return false;
        }
    }

    public class ErrorServiceBehavior : IServiceBehavior
    {
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {

        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            ErrorHandler handler = new ErrorHandler();
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(handler);
            }
        }
    }

    class WindowsService : ServiceBase
    {
        public static string VERSION = "E9I-2.20 2022-03-01";

        private static readonly ILog logger =
            LogManager.GetLogger(typeof(WindowsService));

        //private WebServiceHost host;

        public WindowsService()
        {
            //InitializeComponent();
        }

        void host_Closed(object sender, EventArgs e)
        {
            logger.Info("Host closed " + VERSION);
        }

        void host_Opened(object sender, EventArgs e)
        {
            logger.Info("Host opened " + VERSION);
        }

        void host_Faulted(object sender, EventArgs e)
        {
            logger.Info("Host faulted " + VERSION);
        }

        public void Debug()
        {
            //debug - run as admin - delete acl
            OnStart(new string[0]);
        }

        protected override void OnStart(string[] args)
        {
            logger.Info("Starting service...");
            String baseAddress = "http://localhost:10001";

            WebHttpBinding binding = new WebHttpBinding();
            binding.MaxReceivedMessageSize = 4194304;

            WebServiceHost host = new WebServiceHost(typeof(WcfService), new Uri(baseAddress));
            host.AddServiceEndpoint(typeof(IWcfService), binding, "service"); //the problem

            host.Opened += new EventHandler(host_Opened);
            host.Closed += new EventHandler(host_Closed);
            host.Faulted += new EventHandler(host_Faulted);
            host.UnknownMessageReceived += Host_UnknownMessageReceived;
            host.Description.Behaviors.Add(new ErrorServiceBehavior());
            host.Open();
            logger.Info("Service started");
            new WcfService().Initiate(true);
        }

        private void Host_UnknownMessageReceived(object sender, UnknownMessageReceivedEventArgs e)
        {
            logger.Info("Unknown message " + VERSION);
            //throw new NotImplementedException();
        }

        protected override void OnStop()
        {
            logger.Info("Stop");
            //try
            //{
            //    host.Close(new TimeSpan(0, 0, 0, 30));
            //}
            //catch (Exception) { }
            logger.Info("Stopped");
        }
    }
}
