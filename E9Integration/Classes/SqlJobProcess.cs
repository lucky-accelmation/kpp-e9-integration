﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using E9Integration.Libraries;
using System.Data;
using E9Integration.Classes;
using System.Net;
using System.Net.Sockets;
using static E9Integration.Libraries.E9Functions;

namespace E9Integration.Classes
{
    public class SqlJobProcess : JobProcess
    {
        public override int GetJobCount()
        {
            int count = 0;
            SqlConnection conn = null;
            try
            {
                //                logger.Info(ServiceHelper.GetConfig("DefaultSqlConnection"));
                //                logger.Info(ServiceHelper.GetDefaultConnectionString());
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "select count(jobid) from jobdata where state='New'";
                count = (int)QueryHelper.ReadScalarWithParam(conn, q, null);
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return count;
        }

        public override int RestartUnfinishedJob()
        {
            int r = 0;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "update jobdata set state='New' where state=@st ";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("st", "Fetch"));
                r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return r;
        }

        public override string CreateJob(string operation, string input)
        {
            E9Functions mf = new E9Functions();
            if (!mf.IsValidOperation(operation))
            {
                throw new Exception("Invalid operation");
            }
            string jobid = null;
            int priority = mf.GetOperationPriority(operation);
            SqlConnection conn = null;
            try
            {
                jobid = Guid.NewGuid().ToString();
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string machineid = GetMachineID(conn);
                string q = "insert into jobdata (jobid,operation,input,state,added,priority,creator) values (@jobid,@op,@inp,'New',getdate(),@priority,@machid)";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("jobid", jobid));
                param.Add(new SqlParameter("op", operation));
                param.Add(new SqlParameter("inp", input));
                param.Add(new SqlParameter("priority", priority));
                param.Add(new SqlParameter("machid", machineid));
                int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return jobid;
        }

        public override void RemoveJob(string jobid)
        {
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "delete jobdata where jobid=@jobid and state='New'";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("jobid", jobid));
                int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
        }

        public override string GetFetchedJob()
        {
            string jobid = null;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "select top 1 jobid from jobdata where state='Fetch' with (tablockx) order by added";
                jobid = QueryHelper.ReadScalarWithParam(conn, q, null) as string;
                q = "update jobdata set state='Fetch',started=getdate(),lastworker=@machid where jobid=@jobid";
                string machineid = GetMachineID(conn);
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("machid", machineid));
                param.Add(new SqlParameter("jobid", jobid));
                int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return jobid;
        }

        public override string PeekNextJob()
        {
            string jobid = null;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "select top 1 jobid from jobdata where state='New' order by priority,added";
                jobid = QueryHelper.ReadScalarWithParam(conn, q, null) as string;
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return jobid;
        }

        public string GetMachineID(SqlConnection conn)
        {
            string str = System.Environment.MachineName;
            str += "; " + System.Environment.UserDomainName + "\\" + System.Environment.UserName;
            //str += "; " + System.Environment.CurrentDirectory;
            str += "; " + AppDomain.CurrentDomain.BaseDirectory;

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    str += "; " + ip.ToString();
                }
            }

            //require VIEW SERVER STATE permission on login sql server
            //string q = "select top 1 hostname,program_name,nt_domain,nt_username,net_address,net_library,loginame from sys.sysprocesses where spid =@@SPID";
            //require VIEW SERVER STATE permission on login sql server
            //string q = "select top 1 net_transport,protocol_type,auth_scheme,client_net_address from sys.dm_exec_connections where session_id =@@SPID";
            //Dictionary<string, object> res = QueryHelper.ReadRowWithParam(conn, q, new List<SqlParameter>());
            //str += "; client " + res["client_net_address"] as string;
            str += "; " + E9Integration.WindowsService.VERSION;

            return str;
        }

        public override void FetchJob(string jobid)
        {
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string machineid = GetMachineID(conn);
                string q = "update jobdata set state='Fetch',started=getdate(),lastworker=@machid where jobid=@jobid";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("machid", machineid));
                param.Add(new SqlParameter("jobid", jobid));
                int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
        }

        public override void ResetJob(string jobid)
        {
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "update jobdata set state='New',started=getdate() where jobid=@jobid";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("jobid", jobid));
                int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
        }

        public override string GetNextJob()
        {
            string jobid = null;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                string q = "select top 1 jobid from jobdata with (tablockx) where state='New' order by priority,added";
                jobid = QueryHelper.ReadScalarWithParam(conn, q, null) as string;
                if (jobid != null)
                {
                    string machineid = GetMachineID(conn);
                    q = "update jobdata set state='Fetch',started=getdate(),lastworker=@machid where jobid=@jobid";
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("machid", machineid));
                    param.Add(new SqlParameter("jobid", jobid));
                    int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
                }
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return jobid;
        }

        public override JobInfo[] GetJobList()
        {
            int truncateSize = 0;
            int.TryParse(ServiceHelper.GetConfig("InfoTruncateSize"), out truncateSize);
            List<JobInfo> result = new List<JobInfo>();
            JobInfo info = null;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());

                string q = "select top 10 jobid,operation,input,output,state,error,stacktrace,added,started,finished from jobdata order by r_id desc ";
                List<SqlParameter> param = new List<SqlParameter>();
                DataTable dt = QueryHelper.ReadQueryTableParam(conn, q, param);
                if (dt.Rows.Count == 0)
                {
                    info = new JobInfo();
                    info.jobid = "";
                    info.output = "Empty job list";
                    //result.Add(info);
                    logger.Info(info.output);
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow res = dt.Rows[i];
                        info = new JobInfo();
                        info.jobid = res["jobid"] as string;
                        info.operation = res["operation"] as string;
                        info.input = res["input"] as string;
                        info.output = res["output"] as string;
                        info.state = res["state"] as string;
                        info.error = res["error"] as string;
                        info.stacktrace = res["stacktrace"] as string;
                        if (info.output == null) info.output = "";
                        if (info.error == null) info.error = "";
                        if (info.stacktrace == null) info.stacktrace = "";
                        if (truncateSize > 0)
                        {
                            info.input = info.input.Length > truncateSize ? string.Format("{0}... ({1:N0} chars more)", info.input.Substring(0, truncateSize), info.input.Length - truncateSize) : info.input;
                            info.output = info.output.Length > truncateSize ? string.Format("{0}... ({1:N0} chars more)", info.output.Substring(0, truncateSize), info.input.Length - truncateSize) : info.output;
                        }
                        info.added = string.Format("{0}", res["added"]);
                        info.started = string.Format("{0}", res["started"]);
                        info.finished = string.Format("{0}", res["finished"]);
                        result.Add(info);
                    }
                }
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return result.ToArray();
        }

        //protected override string GetJobInfo(string jobid, string field)
        //{
        //    string res = null;
        //    SqlConnection conn = null;
        //    try
        //    {
        //        conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
        //        string q = "select top 1 " + field + " from jobdata where jobid=@jobid";
        //        List<SqlParameter> param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("jobid", jobid));
        //        res = QueryHelper.ReadScalarWithParam(conn, q, param) as string;
        //    }
        //    catch (Exception ex)
        //    {
        //        QueryHelper.RollbackTransaction(conn);
        //        logger.Info(ex.Message);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        QueryHelper.CloseConnection(conn);
        //    }
        //    return res;
        //}

        public override JobInfo GetJobInfo(string jobid, bool truncate = true)
        {
            int truncateSize = 0;
            int.TryParse(ServiceHelper.GetConfig("InfoTruncateSize"), out truncateSize);
            JobInfo info = null;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());

                string q = "select top 1 jobid,operation,input,output,state,error,stacktrace,added,started,finished,lastworker from jobdata where jobid=@jobid";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("jobid", jobid));
                Dictionary<string, object> res = QueryHelper.ReadRowWithParam(conn, q, param);
                if (res.Count == 0)
                {
                    info = new JobInfo();
                    info.jobid = jobid;
                    info.output = "Invalid JobID";
                }
                else
                {
                    info = new JobInfo();
                    info.jobid = res["jobid"] as string;
                    info.operation = res["operation"] as string;
                    info.input = res["input"] as string;
                    info.output = res["output"] as string;
                    if (truncateSize > 0 && truncate)
                    {
                        info.input = info.input.Length > truncateSize ? string.Format("{0}... ({1:N0} chars more)",
                            info.input.Substring(0, truncateSize), info.input.Length - truncateSize) : info.input;
                    }
                    info.state = res["state"] as string;
                    info.error = res["error"] as string;
                    if (info.error != null && truncateSize > 0 && truncate)
                    {
                        info.error = info.error.Length > truncateSize ? string.Format("{0}... ({1:N0} chars more)",
                            info.error.Substring(0, truncateSize), info.error.Length - truncateSize) : info.error;
                    }
                    info.stacktrace = res["stacktrace"] as string;
                    if (info.output == null) info.output = "";
                    if (info.error == null) info.error = "";
                    if (info.stacktrace == null) info.stacktrace = "";
                    info.added = string.Format("{0}", res["added"]);
                    info.started = string.Format("{0}", res["started"]);
                    info.finished = string.Format("{0}", res["finished"]);
                    info.lastworker = string.Format("{0}", res["lastworker"]);
                }
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return info;
        }

        public override void SetJobFinish(string jobid, string result, string error, string stacktrace)
        {
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("jobid", jobid));
                StringBuilder sb = new StringBuilder();
                sb.Append("update jobdata set ");
                if (result != null)
                {
                    sb.Append("output=@ou,state='Finish',finished=getdate() where jobid=@jobid");
                    param.Add(new SqlParameter("ou", result));
                }
                else
                {
                    sb.Append("error=@er,stacktrace=@st,state='Error',finished=getdate() where jobid=@jobid");
                    if (error == null) error = "-";
                    if (stacktrace == null) stacktrace = "-";
                    param.Add(new SqlParameter("er", error));
                    param.Add(new SqlParameter("st", stacktrace));
                }
                int r = QueryHelper.ExecuteQueryWithParam(conn, sb.ToString(), param);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
        }

        public override void CreateJobSplit(string jobid, string operation, string originalid, JobSplit[] jobsplit)
        {
            StringBuilder sbid = new StringBuilder();
            StringBuilder sbamt = new StringBuilder();
            for (int i = 0; i < jobsplit.Length; i++)
            {
                if (i > 0)
                {
                    sbid.Append("^|^");
                    sbamt.Append("^|^");
                }
                sbid.Append(jobsplit[i].id);
                sbamt.Append(jobsplit[i].amount);
            }
            string splitidstr = sbid.ToString();
            string amountstr = sbamt.ToString();
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("jobid", jobid));
                param.Add(new SqlParameter("operation", operation));
                param.Add(new SqlParameter("originalid", originalid));
                param.Add(new SqlParameter("splitids", splitidstr));
                param.Add(new SqlParameter("amounts", amountstr));

                string q = "update splitdata set jobid=@jobid,splitids=@splitids,amounts=@amounts where operation=@operation and originalid=@originalid ";
                int r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
                if (r == 0)
                {
                    param = new List<SqlParameter>();
                    param.Add(new SqlParameter("jobid", jobid));
                    param.Add(new SqlParameter("operation", operation));
                    param.Add(new SqlParameter("originalid", originalid));
                    param.Add(new SqlParameter("splitids", splitidstr));
                    param.Add(new SqlParameter("amounts", amountstr));
                    q = "insert into splitdata (jobid,operation,originalid,splitids,amounts) values (@jobid,@operation,@originalid,@splitids,@amounts) ";
                    r = QueryHelper.ExecuteQueryWithParam(conn, q, param);
                }
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
        }

        public override JobSplit[] GetJobSplit(string operation, string originalid)
        {
            JobSplit[] rs = null;
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString());

                string q = "select top 1 jobid,splitids,amounts from splitdata where operation=@operation and originalid=@originalid";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("operation", operation));
                param.Add(new SqlParameter("originalid", originalid));
                Dictionary<string, object> res = QueryHelper.ReadRowWithParam(conn, q, param);
                if (res.Count > 0)
                {
                    string s = res["splitids"] as string;
                    string a = res["amounts"] as string;
                    if (s != null && a != null)
                    {
                        string[] ids = s.Split(new string[] { "^|^" }, StringSplitOptions.None);
                        string[] amts = a.Split(new string[] { "^|^" }, StringSplitOptions.None);
                        int ln = ids.Length <= amts.Length ? ids.Length : amts.Length;
                        rs = new JobSplit[ln];
                        for (int i = 0; i < ln; i++)
                        {
                            rs[i] = new JobSplit(ids[i], amts[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return rs;
        }

        /// <summary>
        /// get server credentials
        /// baseurl, username, password, position based on logintype
        /// </summary>
        /// <param name="logintype"></param>
        /// <returns></returns>
        public override StructOperation GetServerCredentials(StructOperation op)
        {
            int operationtimeout = int.Parse(ServiceHelper.GetConfig("OperationTimeoutMin")); // minutes
            int delayretry = int.Parse(ServiceHelper.GetConfig("CredentialDelaySec")); // max (random)
            int maxretry = int.Parse(ServiceHelper.GetConfig("CredentialRetry"));
            bool success = false;
            StructOperation ret = op.Clone();
            SqlConnection conn = null;

            for (int retry = 0; retry < maxretry; retry++)
            {
                try
                {
                    conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString(), true);

                    List<SqlParameter> param = new List<SqlParameter>();
                    string q = "select top 1 baseurl,username,password,position,status,datediff(minute,started,getdate()) as tm " +
                        "from servercredentials with (rowlock,updlock)" +
                        "where (status!='busy' " +
                        "or datediff(minute,started,getdate())>" + operationtimeout + ") " +
                        "and logintype=@logintype ";
                    if (ServiceHelper.GetConfig("CredentialServerSpecific") == "true")
                    {
                        logger.Info("Getting server credentials for "+ System.Environment.MachineName);
                        q += "and server=@server ";
                        param.Add(new SqlParameter("server", System.Environment.MachineName));
                    }
                    q += "order by priority ";
                    param.Add(new SqlParameter("logintype", op.login));

                    Dictionary<string, object> res = QueryHelper.ReadRowWithParam(conn, q, param);
                    if (res.Count > 0)
                    {
                        ret.baseurl = res["baseurl"] as string;
                        ret.username = res["username"] as string;
                        ret.password = res["password"] as string;
                        ret.position = res["position"] as string;
                        success = true;
                        logger.Info("Login using " + ret.baseurl + " - " + ret.username + " - " + ret.position + " " + res["status"] + " " + res["tm"]);

                        q = "update servercredentials set status='busy',started=getdate() where baseurl=@baseurl and username=@username ";
                        List<SqlParameter> uparam = new List<SqlParameter>();
                        uparam.Add(new SqlParameter("baseurl", ret.baseurl));
                        uparam.Add(new SqlParameter("username", ret.username));
                        QueryHelper.ExecuteQueryWithParam(conn, q, uparam);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    QueryHelper.RollbackTransaction(conn);
                    logger.Info(ex.Message);
                    throw ex;
                }
                finally
                {
                    QueryHelper.CloseConnection(conn);
                }
                int delay = new Random((int)DateTime.Now.Ticks).Next(delayretry);
                logger.Info("Waiting credential, delay " + delay + "s retry " + retry);
                System.Threading.Thread.Sleep(delay * 1000);
            }
            if (!success)
            {
                throw new Exception("Server credentials not found");
            }

            return ret;
        }

        public override void UnlockCredential(StructOperation op)
        {
            SqlConnection conn = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString(), true);

                string q = "update servercredentials set status='ready' where baseurl=@baseurl and username=@username ";
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("baseurl", op.baseurl));
                param.Add(new SqlParameter("username", op.username));
                QueryHelper.ExecuteQueryWithParam(conn, q, param);
                logger.Info("Unlock " + op.baseurl + " - " + op.username + " - " + op.position);
                System.Threading.Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
        }

        public override DataTable QueryDB(string query, List<SqlParameter> prm)
        {
            SqlConnection conn = null;
            DataTable result = null;
            try
            {
                conn = QueryHelper.CreateConnection(ServiceHelper.GetDefaultConnectionString(), true);
                result = QueryHelper.ReadQueryTableParam(conn, query, prm);
                logger.Info("Query DB: " + query);
                logger.Info("Result: " + result.Rows.Count);
            }
            catch (Exception ex)
            {
                QueryHelper.RollbackTransaction(conn);
                logger.Info(ex.Message);
                throw ex;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            return result;
        }
    }
}