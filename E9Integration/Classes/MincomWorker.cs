﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Collections.Specialized;
using System.IO;
using E9Integration.Libraries;
using log4net;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;

namespace E9Integration.Classes
{
    public class MincomWorker : BaseWorker
    {
        public MincomWorker(JobProcess jobData) : base(jobData)
        {
        }

        public static MincomObject DoLoginTimeout(object threadid, string connectionName, string overrideDistrict)
        {
            int timeout = 30;
            int retry = 5;
            while (retry > 0)
            {
                retry--;
                Thread t = new Thread(new ParameterizedThreadStart(DoLoginProcess));
                List<object> retvalue = new List<object>();
                DateTime start = DateTime.Now;
                loggerinfo(threadid, "Starting login process with timeout");

                t.Start(new object[]{
                    threadid,
                    connectionName,
                    overrideDistrict,
                    retvalue
                });

                int logtime = 0;
                while (t.IsAlive && DateTime.Now.Subtract(start).TotalSeconds < timeout)
                {
                    if (DateTime.Now.Subtract(start).TotalSeconds > logtime + 5)
                    {
                        logtime += 5;
                        loggerinfo(threadid, string.Format("Waiting to login, timeout in {0} seconds", timeout - logtime));
                    }
                    Thread.Sleep(200);
                }
                if (t.IsAlive)
                {
                    loggerinfo(threadid, "Connect timeout, retry: " + retry);
                    //t.Abort(); //cannot abort unmanaged code
                }
                else
                {
                    //done
                    object r = retvalue[0];
                    if (r is MincomObject)
                    {
                        return (MincomObject)r;
                    }
                    else if (r is Exception)
                    {
                        throw (Exception)r;
                    }
                }
            }
            loggerinfo(threadid, "Restarting system...");
            ConnectTimeout(null);
            throw new Exception("Connect failed, reason: timeout");
        }

        private static void ConnectTimeout(object state)
        {
//            Process.Start("shutdown", "/r /f /t 5");

            //ProcessStartInfo proc = new ProcessStartInfo();
            //proc.FileName = "cmd";
            //proc.WindowStyle = ProcessWindowStyle.Hidden;
            //proc.Arguments = "/C shutdown /r /f /t 10";
            //Process.Start(proc);

            ServiceHelper.DoExitWin(ServiceHelper.EWX_REBOOT);
        }

        public static void DoLoginProcess(object o)
        {
            object[] p = (object[])o;
            List<object> retvalue = (List<object>)p[3];
            try
            {
                MincomObject mobj = DoLogin(p[0], (string)p[1], (string)p[2]);
                retvalue.Add(mobj);
            }
            catch (Exception ex)
            {
                retvalue.Add(ex);
            }
        }

        public static MincomObject DoLogin(object threadid, string connectionName)
        {
            return DoLogin(threadid, connectionName, null);
        }

        public static MincomObject DoLogin(object threadid, string connectionName, string overrideDistrict)
        {
//            Thread.Sleep(120000);
            NameValueCollection MincomLogin = null;
            MincomObject MObj = new MincomObject();
            MObj.threadid = threadid;
            MincomLogin = ServiceHelper.GetMincomLogin(connectionName);
            loggerinfo(threadid, string.Format("Login to {0} with user {1}{2}.", MincomLogin["CH"], MincomLogin["MU"], MincomLogin["CU"]));
            if (overrideDistrict != null)
            {
                MincomLogin["MD"] = overrideDistrict;
                loggerinfo(threadid, string.Format("Override district {0}", overrideDistrict));
            }
            MObj.Connect(MincomLogin["CH"], MincomLogin["CP"], MincomLogin["CU"], MincomLogin["CW"],
                MincomLogin["MU"], MincomLogin["MW"], MincomLogin["MD"], MincomLogin["MP"]);
            //                logger.Info("Logged in");
            //CurrentLogin = connectionName;
            return MObj;
        }

        public static void DoDisconnect(MincomObject MObj)
        {
//            logger.Info("Disconnecting");
            if (MObj != null)
            {
                MObj.Disconnect();
            }
//            logger.Info("Disconnected");
        }

        //called as spawn child do specific job
        public override void DoProcessWorker(string jobid)
        {
            MincomObject MObj = null;
            try
            {
                loggerinfo(jobid, "Process Worker Start");
                string joboperation = JobData.GetJobOperation(jobid);
                string jobinput = JobData.GetJobInput(jobid);
                MincomFunctions mf = new MincomFunctions();
                string cn = mf.GetLoginJob(joboperation);
                string ovrDistrict = mf.GetOverrideDistrict(joboperation, jobinput);
                //JobData.FetchJob(jobid);
                try
                {
//                    MObj = DoLogin(jobid, cn, ovrDistrict);
                    MObj = DoLoginTimeout(jobid, cn, ovrDistrict);
                }
                catch
                {
                    JobData.ResetJob(jobid);
                    throw;
                }
                try
                {
                    loggerinfo(jobid, string.Format("Executing mincom function: {0}", joboperation));
                    string result = mf.ProcessJob(MObj, jobid, joboperation, jobinput);
                    loggerinfo(jobid, "Finished executing mincom function");
                    JobData.SetJobFinish(jobid, result, null, null);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Trim().Contains("No screen map is available. Restart and try again.") 
                        || ex.Message.Trim().Equals("Screen not match: "))
                    {
                        //retry
                        loggerinfo(jobid, "No screen map/screen empty, retry job");
                        JobData.ResetJob(jobid);
                    }
                    else
                    {
                        JobData.SetJobFinish(jobid, null, ex.Message, ex.StackTrace);
                        loggerinfo(jobid, string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace));

                        if (ex.Message.Trim().Contains("Error in extracting XML screen fields."))
                        {
                            loggerinfo(jobid, "Error XML, Restarting system...");
                            ConnectTimeout(null);
                        }
                    }
                }
                DoDisconnect(MObj);
            }
            catch (Exception ex)
            {
                string errorMsg = string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace);
                loggerinfo(jobid, errorMsg);

                try
                {
                    ServiceHelper.SendReport(errorMsg);
                    loggerinfo(jobid, "Email report sent.");
                }
                catch (Exception exc)
                {
                    errorMsg = string.Format("Error: {0}\r\n{1}", exc.Message, exc.StackTrace);
                    loggerinfo(jobid, errorMsg);
                }
            }
            finally
            {
                DoDisconnect(MObj);
            }

            loggerinfo(jobid, "Thread finished");
        }

        //the new way, using spawn, cannot debugged
        private void DoProcessSpawn(object param)
        {
            object[] p = (object[])param;
            int pnum = (int)p[0];
            //if (param == null)
            //{
            //    return;
            //}
            loggerinfo(pnum, "Thread running by " + GetComputerId());
            try
            {
                string jobid = null;
                while ((jobid = JobData.GetNextJob()) != null)
                {
                    loggerinfo(pnum, "Thread process spawn started: " + jobid);
                    //spawning process, monitoring
                    int timeout = 300; //seconds
                    int.TryParse(ServiceHelper.GetConfig("SpawnProcessTimeout"), out timeout);

//                    string fn = Process.GetCurrentProcess().MainModule.FileName;
                    string fn = System.Reflection.Assembly.GetExecutingAssembly().Location;

                    ProcessStartInfo procinfo = new ProcessStartInfo(fn, jobid);
                    procinfo.RedirectStandardInput = false;
                    procinfo.RedirectStandardOutput = true;
                    procinfo.RedirectStandardError = false;
                    procinfo.UseShellExecute = false;
                    procinfo.CreateNoWindow = true;

                    Process proc = Process.Start(procinfo);
                    ProcessOutputData procdata = new ProcessOutputData();
                    proc.OutputDataReceived += new DataReceivedEventHandler(procdata.proc_OutputDataReceived);
                    proc.BeginOutputReadLine();
                    while (!proc.HasExited)
                    {
                        if (procdata.sb.Length > 0)
                        {
                            Console.WriteLine("== Worker output start ==");
                            Console.WriteLine(procdata.sb.ToString().TrimEnd());
                            Console.WriteLine("== Worker output end ==");
                            procdata.sb.Clear();
                        }
                        if (DateTime.Now.Subtract(procdata.lastresponse).TotalSeconds < timeout)
                        {
//                            loggerinfo(pnum, "Process thread alive");
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            loggerinfo(pnum, "Process thread kill, inactive for " + timeout + " seconds");
                            proc.Kill();
                            JobData.ResetJob(jobid);
                        }
                    }
                    loggerinfo(pnum, "Thread process spawn finished: " + jobid);

                    Start(); //always start another thread
                }
            }
            catch (Exception ex)
            {
                string errorMsg = string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace);
                loggerinfo(pnum, errorMsg);

                try
                {
                    ServiceHelper.SendReport(errorMsg);
                    loggerinfo(pnum, "Email report sent.");
                }
                catch (Exception exc)
                {
                    errorMsg = string.Format("Error: {0}\r\n{1}", exc.Message, exc.StackTrace);
                    loggerinfo(pnum, errorMsg);
                }
            }

            loggerinfo(pnum, "Thread finished");
        }

        class ProcessOutputData
        {
            public DateTime lastresponse = DateTime.Now;
            public StringBuilder sb = new StringBuilder();
            public void proc_OutputDataReceived(object sender, DataReceivedEventArgs e)
            {
                sb.AppendLine(e.Data);
                lastresponse = DateTime.Now;
            }
        }

        //the old way (thread), used for debugging
        protected override void DoProcess(object param)
        {
            object[] p = (object[])param;
            int pnum = (int)p[0];
            //if (param == null)
            //{
            //    return;
            //}
            loggerinfo(pnum, "Thread running");
            MincomObject MObj = null;
            try
            {
                string jobid = null;
                while ((jobid = JobData.GetNextJob()) != null)
                {
                    loggerinfo(pnum, "Thread process: " + jobid);
                    string joboperation = JobData.GetJobOperation(jobid);
                    string jobinput = JobData.GetJobInput(jobid);
                    MincomFunctions mf = new MincomFunctions();
                    string cn = mf.GetLoginJob(joboperation);
                    string ovrDistrict = mf.GetOverrideDistrict(joboperation, jobinput);
                    //JobData.FetchJob(jobid);
                    try
                    {
                        MObj = DoLoginTimeout(pnum, cn, ovrDistrict);
                    }
                    catch
                    {
                        JobData.ResetJob(jobid);
                        throw;
                    }
                    try
                    {
                        loggerinfo(pnum, string.Format("Executing mincom function: {0}", joboperation));
                        string result = mf.ProcessJob(MObj, pnum, joboperation, jobinput);
                        loggerinfo(pnum, "Finished executing mincom function");
                        JobData.SetJobFinish(jobid, result, null, null);
                    }
                    catch (Exception ex)
                    {
                        JobData.SetJobFinish(jobid, null, ex.Message, ex.StackTrace);
                        loggerinfo(pnum, string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace));

                        if (ex.Message.Trim().Contains("Error in extracting XML screen fields."))
                        {
                            loggerinfo(pnum, "Error XML, Restarting system...");
                            ConnectTimeout(null);
                        }
                    }
                    DoDisconnect(MObj);
                    Start(); //always start another thread
                }
            }
            catch (Exception ex)
            {
                string errorMsg = string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace);
                loggerinfo(pnum, errorMsg);

                try
                {
                    ServiceHelper.SendReport(errorMsg);
                    loggerinfo(pnum, "Email report sent.");
                }
                catch (Exception exc)
                {
                    errorMsg = string.Format("Error: {0}\r\n{1}", exc.Message, exc.StackTrace);
                    loggerinfo(pnum, errorMsg);
                }
            }
            finally
            {
                DoDisconnect(MObj);
            }

            loggerinfo(pnum, "Thread finished");
        }

        public override bool Start()
        {
            int cnt = JobData.GetJobCount();
            if (cnt == 0)
            {
//                logger.Info("No job available");
                return false;
            }
            else
            {
                for (int i = 0; i < MAX_THREADS; i++)
                {
                    if (WorkerThreads[i] == null || !WorkerThreads[i].IsAlive)
                    {
                        object p = new object[] { 
                            i+1 //thread number
                        };
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            //do the old way, for debugging purpose
                            WorkerThreads[i] = new Thread(new ParameterizedThreadStart(DoProcess));
                        }
                        else
                        {
                            //anti hang, but cannot debugged
                            WorkerThreads[i] = new Thread(new ParameterizedThreadStart(DoProcessSpawn));
                        }
                        WorkerThreads[i].Start(p);
                        cnt--;
                    }
                    if (cnt == 0) break;
                }
            }
            return true;
        }


    }
}