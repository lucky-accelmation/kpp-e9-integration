﻿using E9Integration.Libraries;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace E9Integration.Classes
{
    public abstract class BaseWorker
    {
        protected Thread[] WorkerThreads;
        protected JobProcess JobData;
        protected int MAX_THREADS = 3;
        protected static readonly ILog logger =
            LogManager.GetLogger(typeof(MincomWorker));

        protected static void loggerinfo(object threadid, string message)
        {
            string stringid = threadid.ToString();
            if (stringid != null && stringid.Length > 8) stringid = stringid.Substring(0, 8);
            logger.InfoFormat("{0}#{1}: {2}", E9Integration.WindowsService.VERSION, stringid, message);
        }

        public BaseWorker(JobProcess jobData)
        {
            // prepare worker threads
            int.TryParse(ServiceHelper.GetConfig("MainThreadMaxCount"), out MAX_THREADS);
            WorkerThreads = new Thread[MAX_THREADS];
            for (int i = 0; i < MAX_THREADS; i++)
            {
                WorkerThreads[i] = new Thread(new ParameterizedThreadStart(DoProcess));
            }
            this.JobData = jobData;
        }

        protected string GetComputerId()
        {
            string cid = Environment.MachineName;
            IPAddress[] caddr = Dns.GetHostAddresses(Environment.MachineName);
            List<string> addrl = new List<string>();
            for (int i = 0; i < caddr.Length; i++)
            {
                addrl.Add(caddr[i].ToString());
            }
            cid += " [" + string.Join(",", addrl.ToArray()) + "]";
            return cid;
        }

        //standard do process
        protected abstract void DoProcess(object param);

        public abstract bool Start();

        //called as spawn child, to do specific job
        public abstract void DoProcessWorker(string jobid);

        public int RunningCount()
        {
            int cnt = 0;
            for (int i = 0; i < MAX_THREADS; i++)
            {
                if (WorkerThreads[i] == null)
                    continue;
                if (WorkerThreads[i].IsAlive) cnt++;
            }
            return cnt;
        }

    }
}
