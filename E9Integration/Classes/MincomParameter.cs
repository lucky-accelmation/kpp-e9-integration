﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E9Integration.Classes
{
    public class MincomParameter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(MincomParameter));

        public JToken token = null;
        public MincomParameter(string json)
        {
            try
            {
                if (json.Equals(""))
                {
                    json = "\"\"";
                    //json = "null";
                    //json = "{}";
                }

                token = JToken.Parse(json);
            }
            catch //(Exception ex)
            {
                throw;
            }
        }

        public MincomParameter(JToken token)
        {
            this.token = token;
        }

        public string[] GetStringChunk(string path, int chunk, int count)
        {
            string content = GetString(path);
            IEnumerable<string> res = Enumerable.Range(0, (int)Math.Ceiling(((decimal)content.Length / chunk)))
                .Select(ix => content.Substring((ix * chunk) < content.Length ? (ix * chunk) : content.Length,
                (ix * chunk + chunk) < content.Length ? chunk : content.Length - (ix * chunk)));
            if (res.Count() < count)
            {
                res = res.Concat(Enumerable.Repeat("", count - res.Count()));
            }
            return res.ToArray();
        }

        public string GetStringTruncate(string path, int maxlength)
        {
            string str = GetString(path);
            if (str == null) return str;
            if (str.Length > maxlength) logger.Info("field " + path + " value truncated " + maxlength + " chars");
            return str.Length > maxlength ? str.Substring(0, maxlength) : str;
        }

        public string GetString(string path)
        {
            try
            {
                return (string)token.SelectToken(path, true);
            }
            catch //(Exception ex)
            {
                logger.Info("parameter " + path + " not found");
                return null; //just return null instead throw
                //throw;
            }
        }

        public void SetString(string path, string value)
        {
            try
            {
                JToken prop = token.SelectToken(path, false) as JToken;
                if (prop == null)
                {
                    ((JObject)token).Add(path, value);
                }
                else
                {
                    prop.Replace(new JValue(value));
                }
            }
            catch //(Exception ex)
            {
                throw;
            }
        }

        public string[] GetStringArray(string path, string parameterName)
        {
            List<string> items = new List<string>();
            for (int i = 0; i < Count(path); i++)
            {
                if (parameterName != null)
                {
                    MincomParameter mpi = Get(path + "[" + i + "]");
                    items.Add(mpi.GetString(parameterName));
                }
                else
                {
                    items.Add(GetString(path + "[" + i + "]"));
                }
            }
            return items.ToArray();
        }

        public MincomParameter[] GetArray(string path)
        {
            List<MincomParameter> items = new List<MincomParameter>();
            for (int i = 0; i < Count(path); i++)
            {
                items.Add(Get(path + "[" + i + "]"));
            }
            return items.ToArray();
        }

        public MincomParameter Get(string path)
        {
            try
            {
                return new MincomParameter(token.SelectToken(path, true));
            }
            catch //(Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// check property exists
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public bool IsExist(string prop)
        {
            return token.Where(a => (a as JProperty) != null && (a as JProperty).Name.Equals(prop)).Count() > 0;
        }

        /// <summary>
        /// check property
        /// not exist => false
        /// empty string => false
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public bool IsNotEmpty(string prop)
        {
            var elm = token.Where(a => (a as JProperty) != null && (a as JProperty).Name.Equals(prop));
            return elm.Count() > 0 && (string)elm.First() != null && (string)elm.First() != "";
        }

        public bool IsArray(string path)
        {
            return token.SelectToken(path, true) is JArray;
        }

        public void CheckRequired(string[] fields, bool nonempty)
        {
            List<string> missingfields = new List<string>();
            foreach (string f in fields)
                if (!IsExist(f))
                    missingfields.Add(f);
                else if (nonempty && GetString(f) == "")
                    missingfields.Add(f);
            if (missingfields.Count > 0)
                throw new Exception("These fields are missing: " + string.Join(", ", missingfields.ToArray()));
        }

        public int Count(string path)
        {
            try
            {
                return ((JArray)token.SelectToken(path, true)).Count;
            }
            catch //(Exception ex)
            {
                throw;
            }
        }
    }
}
