﻿
using E9Integration.Libraries;
using log4net;
using static E9Integration.Libraries.E9Functions;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace E9Integration.Classes
{
    public abstract class JobProcess
    {
        protected static readonly ILog logger =
            LogManager.GetLogger(typeof(JobProcess));

        BaseWorker worker;

        public JobProcess()
        {
            worker = new E9Worker(this);
        }

        public static JobProcess GetJobProcess()
        {
            if (ServiceHelper.GetConnectionString("Server") == "CacheJobProcess")
            {
                logger.Info("Use CacheJobProcess");
                return new CacheJobProcess();
            }
            else
            {
                string svr = ServiceHelper.GetConnectionString("Server").Split(';').Where(s => s.StartsWith("Server")).FirstOrDefault();
                logger.Info("Use SqlJobProcess " + svr);
                return new SqlJobProcess();
            }
        }

        //called as master
        public bool StartJob()
        {
            return worker.Start();
        }

        //called as spawn child
        public void DoProcessWorker(string jobid)
        {
            worker.DoProcessWorker(jobid);
        }

        public int GetRunningCount()
        {
            return worker.RunningCount();
        }

        public abstract int RestartUnfinishedJob();

        public abstract int GetJobCount();

        public abstract string CreateJob(string operation, string input);

        public abstract void RemoveJob(string jobid);

        public abstract string GetFetchedJob();

        public abstract string GetNextJob();

        public abstract string PeekNextJob();

        public abstract void FetchJob(string jobid);

        public abstract void ResetJob(string jobid);

        public string GetJobOperation(string jobid)
        {
            return GetJobInfo(jobid).operation;
            //return GetJobInfo(jobid, "operation");
        }

        public string GetJobInput(string jobid)
        {
            return GetJobInfo(jobid, false).input;
            //return GetJobInfo(jobid, "input");
        }

        //public string GetJobStatus(string jobid)
        //{
        //    return GetJobInfo(jobid, "state");
        //}

        //public string GetJobOutput(string jobid)
        //{
        //    return GetJobInfo(jobid, "output");
        //}

        //public string GetJobError(string jobid)
        //{
        //    return GetJobInfo(jobid, "error");
        //}

        //protected abstract string GetJobInfo(string jobid, string field);

        public abstract JobInfo GetJobInfo(string jobid, bool truncate = true);

        public abstract JobInfo[] GetJobList();

        public abstract void SetJobFinish(string jobid, string result, string error, string stacktrace);

        public abstract void CreateJobSplit(string jobid, string operation, string originalid, JobSplit[] jobsplit);

        public abstract JobSplit[] GetJobSplit(string operation, string originalid);

        public abstract StructOperation GetServerCredentials(StructOperation op);

        public abstract void UnlockCredential(StructOperation op);

        public abstract DataTable QueryDB(string query, List<SqlParameter> prm);
    }
}