﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E9Integration.Classes
{
    public class ServiceResult
    {
        public ServiceResult()
        {
            this.jobid = "";
            this.message = "";
        }

        public ServiceResult(string jobid, string message)
        {
            this.jobid = jobid;
            this.message = message;
        }

        public string jobid;
        public string message;
    }
}