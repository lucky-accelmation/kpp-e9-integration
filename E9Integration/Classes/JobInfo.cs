﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E9Integration.Classes
{
    public class JobInfo
    {
        public JobInfo()
        {
            this.jobid = "";
            this.operation = "";
            this.input = "";
            this.output = "";
            this.state = "";
            this.error = "";
            this.stacktrace = "";
            this.added = "";
            this.started = "";
            this.finished = "";
            this.lastworker = "";
        }

        public string jobid;
        public string operation;
        public string input;
        public string output;
        public string state;
        public string error;
        public string stacktrace;
        public string added;
        public string started;
        public string finished;
        public string lastworker;
    }
}