﻿using E9Integration.Libraries;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace E9Integration.Classes
{
    public class E9Worker : BaseWorker
    {
        public E9Worker(JobProcess jobData) : base(jobData)
        {
        }

        public override void DoProcessWorker(string jobid)
        {
            //spawn not implemented anymore
            throw new NotImplementedException();
        }

        protected override void DoProcess(object param)
        {
            object[] p = (object[])param;
            int pnum = (int)p[0];
            loggerinfo(pnum, "Thread running");
            try
            {
                string jobid = null;
                while ((jobid = JobData.GetNextJob()) != null)
                {
                    string id = jobid.Replace(" ", "");
                    if (id.Length > 8)
                    {
                        id = id.Substring(0, 8);
                    }
                    ThreadContext.Properties["id"] = id;
                    log4net.Config.XmlConfigurator.Configure();

                    loggerinfo(pnum, "Thread process: " + jobid);
                    string joboperation = JobData.GetJobOperation(jobid);
                    string jobinput = JobData.GetJobInput(jobid);
                    E9Functions e9f = new E9Functions();
                    try
                    {
                        loggerinfo(pnum, string.Format("Executing mincom function: {0}", joboperation));
                        string result = e9f.ProcessJob(pnum, joboperation, jobinput);
                        loggerinfo(pnum, "Finished executing mincom function");
                        JobData.SetJobFinish(jobid, result, null, null);
                    }
                    catch (Exception ex)
                    {
                        JobData.SetJobFinish(jobid, null, ex.Message, ex.StackTrace);
                        loggerinfo(pnum, string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace));

                        if (ex.Message.Trim().Contains("Error in extracting XML screen fields."))
                        {
                            loggerinfo(pnum, "Error XML, Restarting system...");
                            //ConnectTimeout(null);
                        }
                    }
                    //DoDisconnect(MObj);
                    Start(); //always start another thread
                }
                ThreadContext.Properties.Clear();
                log4net.Config.XmlConfigurator.Configure();
            }
            catch (Exception ex)
            {
                string errorMsg = string.Format("Error: {0}\r\n{1}", ex.Message, ex.StackTrace);
                loggerinfo(pnum, errorMsg);

                try
                {
                    ServiceHelper.SendReport(errorMsg);
                    loggerinfo(pnum, "Email report sent.");
                }
                catch (Exception exc)
                {
                    errorMsg = string.Format("Error: {0}\r\n{1}", exc.Message, exc.StackTrace);
                    loggerinfo(pnum, errorMsg);
                }
            }
            finally
            {
                //DoDisconnect(MObj);
            }

            loggerinfo(pnum, "Thread finished");
        }

        public override bool Start()
        {
            int cnt = JobData.GetJobCount();
            if (cnt == 0)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < MAX_THREADS; i++)
                {
                    if (WorkerThreads[i] == null || !WorkerThreads[i].IsAlive)
                    {
                        object p = new object[] {
                            i+1 //thread number
                        };
                        //do the old way, for debugging purpose
                        WorkerThreads[i] = new Thread(new ParameterizedThreadStart(DoProcess));
                        WorkerThreads[i].Start(p);
                        cnt--;
                    }
                    if (cnt == 0) break;
                }
            }
            return true;
        }

    }
}
