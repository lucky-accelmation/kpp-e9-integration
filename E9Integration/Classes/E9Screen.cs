﻿using E9Integration.Libraries;
using EllipseWebServicesClient;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace E9Integration.Classes
{
    public class E9Screen
    {
        public E9Screen(ScreenService.OperationContext context, string baseurl)
        {
            ctx = context;
            svc = new ScreenService.ScreenService();
            if (baseurl != null)
            {
                svc.Url = Regex.Replace(svc.Url, @"http://.*?(?=/)", baseurl);
            }
            int timeout = int.Parse(ServiceHelper.GetConfig("ServiceTimeout"));
            if (timeout > 0)
            {
                svc.Timeout = timeout * 1000;
                logger.Info("screenservice timeout " + timeout);
            }
        }
        Random random = new Random();

        public ScreenService.OperationContext ctx;
        ScreenService.ScreenService svc;
        ScreenService.ScreenDTO scr;
        List<ScreenService.ScreenNameValueDTO> fields;
        private readonly ILog logger =
            LogManager.GetLogger(typeof(E9Screen));
        private static readonly ILog dumper =
            LogManager.GetLogger("dumpfields");

        public void Connect(string username, string password, string district, string position)
        {
            //do nothing
        }

        public void Disconnect()
        {
            //do nothing
        }

        public bool IsIdle()
        {
            return scr.idle;
        }

        public bool IsConnected()
        {
            return svc != null;
        }

        public string GetScreenName()
        {
            return scr == null ? null : scr.mapName;
        }

        public string GetErrorMessage()
        {
            return scr.message;
        }

        public bool CheckForScreen(string screen, bool handleError)
        {
            if (svc == null || scr == null)
                return false;

            if (scr.idle || scr.mapName != screen)
            {
                if (handleError)
                {
                    if (scr.message != null && scr.message != "")
                    {
                        throw new Exception("E9S: Expected screen " + screen + " encountered error " + scr.message);
                    }
                    else if (IsIdle())
                    {
                        throw new Exception("E9S: Expected screen " + screen + ", encountered idle screen");
                    }
                    else
                    {
                        throw new Exception("E9S: Expected screen " + screen + ", encountered " + scr.mapName);
                    }
                }
                return false;
            }
            return true;
        }

        public string DumpFields()
        {
            return DumpFields(true);
        }

        public string DumpFields(bool includeEmptyFields)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("---Dump Fields---\r\n");
            sb.Append("Screen\t: ").Append(GetScreenName()).Append("\r\n");
            sb.Append(string.Join("\r\n", GetFieldValues(includeEmptyFields)));
            sb.Append("\r\nCommands:\r\n");
            sb.Append(string.Join("\r\n", GetCommands()));
            sb.Append("\r\n---End Dump---");
            string result = sb.ToString();
            return result;
        }

        public string[] GetFieldValues()
        {
            return GetFieldValues(true);
        }

        public string[] GetFieldValues(bool includeEmptyFields)
        {
            string[] fields = scr.screenFields
                .Where(f => f.value.Trim().Length > 0 || includeEmptyFields || this.fields.FirstOrDefault(tf => tf.fieldName == f.fieldName) != null)
                .Select(f => string.Format("{0}{1}\t: {2}{3}", f.fieldProtected ? "*" : "",
                f.fieldName, f.value, string.Join("", this.fields.Where(tf => tf.fieldName == f.fieldName)
                .Select(tf => " => [" + tf.value + "]").ToArray()))).ToArray();
            return fields;
        }

        public string[] GetFields()
        {
            string[] fields = scr.screenFields
                .Select(f => string.Format("{0}{1}", f.fieldProtected ? "*" : "", f.fieldName)).ToArray();
            return fields;
        }

        public string[] GetCommands()
        {
            if (IsIdle()) return new string[0];
            return scr.functionKeys
                .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .Select(cmd => cmd.Trim()).ToArray();
        }

        public void ExecuteMSO(string msoCode)
        {
            logger.Info("Executing MSO " + msoCode);
            scr = svc.executeScreen(ctx, msoCode);
            fields = new List<ScreenService.ScreenNameValueDTO>();
            logger.Info("Current Screen " + GetScreenName());
        }

        public bool IsCommand(string commandName)
        {
            //example XMIT-Update,F8-Match with order
            return GetCommands().Where(cmd => cmd.EndsWith(commandName)).Count() == 1; //if found>1 then something wrong
        }

        public void ScreenKey(string screenKey)
        {
            var dto = new ScreenService.ScreenSubmitRequestDTO()
            {
                screenKey = screenKey,
                screenFields = fields.ToArray(),
            };
            scr = svc.submit(ctx, dto);
            fields = new List<ScreenService.ScreenNameValueDTO>();
        }

        public void ExecuteCommand(string commandName)
        {
            var cmds = GetCommands();
            string[] map = { "", "XMIT", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10" };
            string cmdkey = cmds.Where(cmd => cmd.EndsWith(commandName)).FirstOrDefault();
            string key = "";
            if (commandName == "OK" || commandName == "Confirm")
            {
                cmdkey = commandName;
                key = "1";
            }
            else if (commandName == "Home")
            {
                GoHome();
                return;
            }
            else if (cmdkey != null)
            {
                key = cmdkey.Split('-')[0];
                if (map.Contains(key))
                {
                    key = Array.IndexOf(map, key).ToString();
                }
                else
                {
                    throw new Exception("E9S: Command key invalid [" + key + "] in [" + string.Join(",", cmds) + "]");
                }
            }
            else
            {
                throw new Exception("E9S: Command not found [" + commandName + "] in [" + string.Join(",", cmds) + "]");
            }
            var dto = new ScreenService.ScreenSubmitRequestDTO()
            {
                screenKey = key,
                screenFields = fields.ToArray(),
            };
            StringBuilder sb = new StringBuilder();
            if (ServiceHelper.GetConfig("DumpFieldsOnCommand").Equals("true"))
            {
                sb.Append(DumpFields(false));
                sb.Append("\r\nExecute Command: ").Append(commandName);
                sb.Append("\r\nExecuting command " + cmdkey + " key " + key);
            }
            logger.Info("Executing command " + cmdkey + " key " + key);
            for (int retry = 3; retry > 0; retry--)
            {
                try
                {
                    scr = svc.submit(ctx, dto);
                    break;
                }
                catch (System.Web.Services.Protocols.SoapHeaderException ex)
                {
                    // E9 specific bug
                    string[] e9bugs = {
                        "Fault: com.mincom.ellipse.ejra.jca.EllipseProgramExecutionException",
                        "Cannot invoke method getFullMessage() on null object",
                        "Fault: java.lang.NullPointerException",
                        "Fault: java.lang.reflect.UndeclaredThrowableException"
                    };
                    if (e9bugs.Contains(ex.Message))

                    {
                        if (retry == 0)
                            throw ex;
                    }
                    else
                    {
                        try
                        {
                            logger.Info("Current Screen " + GetScreenName());
                        }
                        catch (Exception) { }
                        throw ex;
                    }
                    System.Threading.Thread.Sleep(5000);

                    //com.mincom.enterpriseservice.exception.EnterpriseServiceException: com.mincom.ellipse.ejra.mso.ScreenFieldNotFoundException: Could not find the field: PO_NO1I
                }
            }
            logger.Info("Current Screen " + GetScreenName());
            fields = new List<ScreenService.ScreenNameValueDTO>(); //clear set fields
            if (ServiceHelper.GetConfig("DumpFieldsOnCommand").Equals("true"))
            {
                string err = GetErrorMessage().Trim();
                sb.Append("\r\nScreen: ").Append(GetScreenName());
                sb.Append("\r\nResult Message: ").Append(err);
                if (!err.Equals(""))
                {
                    sb.Append("\r\nActive Field: ").Append(GetActiveField());
                }
                sb.Append("\r\n---End Dump Response---");
                dumper.Debug(sb.ToString());
            }
        }

        public bool SetFieldValue(string fieldName, string value)
        {
            if (GetFields().Contains("*" + fieldName))
            {
                throw new Exception("E9S: Field not writable " + fieldName);
            }
            if (!GetFields().Contains(fieldName))
            {
                throw new Exception("E9S: Field not found " + fieldName);
            }
            fields.Add(new ScreenService.ScreenNameValueDTO()
            {
                fieldName = fieldName,
                value = value
            });
            return true;
        }

        public bool IsFieldProtected(string fieldName)
        {
            return scr.screenFields.Where(f => f.fieldName == fieldName).First().fieldProtected;
        }

        public string GetFieldValue(string fieldName)
        {
            try
            {
                return scr.screenFields.Where(f => f.fieldName == fieldName).First().value;
            }
            catch
            {
                throw new Exception("E9S: field not found " + fieldName);
            }
        }

        public string GetActiveField()
        {
            return scr.currentCursorFieldName;
        }

        public void GoHome(int retry = 0)
        {
            if (ServiceHelper.GetConfig("DisablePositionToMenu").ToLower() == "true")
            {
                return;
            }
            try
            {
                svc.positionToMenu(ctx);
            }
            catch (System.Web.Services.Protocols.SoapHeaderException ex)
            {
                // E9 specific bug
                string[] e9bugs = {
                    "Fault: com.mincom.ellipse.ejra.jca.EllipseProgramExecutionException",
                    "Cannot invoke method getFullMessage() on null object",
                    "Fault: java.lang.NullPointerException",
                    "Fault: java.lang.reflect.UndeclaredThrowableException",
                    "Cannot invoke method getPrimaryKey() on null object",
                    "Fault:",
                    "Cannot invoke"
                };
                if (e9bugs.Contains(ex.Message))
                {
                    if (retry >= 20)
                    {
                        logger.Info("GoHome Error retry max reached");
                        throw ex;
                    }
                    System.Threading.Thread.Sleep(15000);
                    string[] msochoices = new string[] { "MSO011", "MSO012", "MSO00A", "MSO080" };
                    string msorandom = msochoices[random.Next(0, msochoices.Length)];
                    logger.Info("GoHome Error " + ex.Message + " retry " + retry + " msorandom " + msorandom);
                    // execute random screen
                    try
                    {
                        ScreenService.ScreenDTO screen = svc.executeScreen(ctx, msorandom);
                        logger.Info("Workarround screen: " + screen.mapName);
                    }
                    catch (Exception ex1)
                    {
                        logger.Info("GoHome Error workarround " + ex1.Message);
                    }
                    GoHome(retry + 1);
                    return;
                }
                //else if (ex.Message == "Fault: java.lang.NullPointerException")
                //{
                //    logger.Info("GoHome error NullPointer ignore workarround");
                //    return;
                //}
                throw ex;
            }
        }
    }
}
