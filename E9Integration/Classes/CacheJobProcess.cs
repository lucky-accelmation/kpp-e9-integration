﻿using E9Integration.Libraries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static E9Integration.Libraries.E9Functions;

namespace E9Integration.Classes
{
    class CacheJobProcess : JobProcess
    {
        //not support multi process multi server

        List<JobInfo> joblist = new List<JobInfo>();

        public override string CreateJob(string operation, string input)
        {
            E9Functions mf = new E9Functions();
            if (!mf.IsValidOperation(operation))
            {
                throw new Exception("Invalid operation");
            }

            string jobid = Guid.NewGuid().ToString();
            //string machineid = GetMachineID(conn);
            //string q = "insert into jobdata (jobid,operation,input,state,added,priority,creator) values (@jobid,@op,@inp,'New',getdate(),@priority,@machid)";
            joblist.Add(new JobInfo()
            {
                jobid = jobid,
                operation = operation,
                input = input,
                state = "New",
                //priority=priority,
                //machineid=machineid,
            });
            return jobid;
        }

        public override void CreateJobSplit(string jobid, string operation, string originalid, JobSplit[] jobsplit)
        {
            string jobidwrap = Guid.NewGuid().ToString();
            string payload = JsonHelper.GetJsonString(jobsplit);

            joblist.Add(new JobInfo()
            {
                jobid = jobidwrap,
                operation = ":split." + operation,
                input = originalid,
                output = payload,
            });
        }

        public override void FetchJob(string jobid)
        {
            joblist.Where(j => j.jobid == jobid).First().state = "Fetch";
        }

        public override string GetFetchedJob()
        {
            JobInfo info = joblist.Where(j => j.state == "Fetch").FirstOrDefault();
            if (info != null)
            {
                return info.jobid;
            }
            else
            {
                return null;
            }
        }

        public override int GetJobCount()
        {
            return joblist.Where(j => j.state == "New").Count();
        }

        public override JobInfo GetJobInfo(string jobid, bool truncate = true)
        {
            int truncateSize = 0;
            int.TryParse(ServiceHelper.GetConfig("InfoTruncateSize"), out truncateSize);
            try
            {
                JobInfo info = joblist.Where(j => j.jobid == jobid).First();
                if (truncateSize > 0 && truncate)
                {
                    info.input = info.input.Length > truncateSize ? string.Format("{0}... ({1:N0} chars more)", info.input.Substring(0, truncateSize), info.input.Length - truncateSize) : info.input;
                }
                return info;
            }
            catch
            {
                return new JobInfo()
                {
                    jobid = jobid,
                    output = "Invalid JobID"
                };
            }
        }

        public override JobInfo[] GetJobList()
        {
            return joblist.TakeWhile((j, i) => i <= 10).ToArray();
        }

        public override JobSplit[] GetJobSplit(string operation, string originalid)
        {
            JobInfo info = joblist.Where(j => j.operation == ":split." + operation && j.input == originalid).FirstOrDefault();
            if (info != null)
            {
                return JsonHelper.GetJsonObject(
                    info.output) as JobSplit[];
            }
            else
            {
                return null;
            }
        }

        public override string GetNextJob()
        {
            JobInfo info = joblist.Where(j => j.state == "New").FirstOrDefault();
            if (info != null)
            {
                info.state = "Fetch";
                return info.jobid;
            }
            else
            {
                return null;
            }
        }

        public override string PeekNextJob()
        {
            JobInfo info = joblist.Where(j => j.state == "New").FirstOrDefault();
            if (info != null)
            {
                return info.jobid;
            }
            else
            {
                return null;
            }
        }

        public override void RemoveJob(string jobid)
        {
            JobInfo info = joblist.Where(j => j.jobid == jobid).FirstOrDefault();
            joblist.Remove(info);
        }

        public override void ResetJob(string jobid)
        {
            joblist.Where(j => j.jobid == jobid).First().state = "New";
        }

        public override int RestartUnfinishedJob()
        {
            int cnt = 0;
            foreach (var job in joblist.Where(j => j.state == "Fetch"))
            {
                job.state = "New";
                cnt++;
            }
            return cnt;
        }

        public override void SetJobFinish(string jobid, string result, string error, string stacktrace)
        {
            JobInfo info = joblist.Where(j => j.jobid == jobid).First();
            if (result != null)
            {
                info.output = result;
                info.state = "Finish";
            }
            else
            {
                info.error = error;
                info.stacktrace = stacktrace;
                info.state = "Error";
            }
        }

        List<string> locked = new List<string>();

        /// <summary>
        /// get server credentials
        /// baseurl, username, password, position based on logintype
        /// </summary>
        /// <param name="logintype"></param>
        /// <returns></returns>
        public override StructOperation GetServerCredentials(StructOperation op)
        {
            StructOperation ret = op.Clone();
            //op.baseurl = "http://ews-kpd.kppmining.net";
            ret.baseurl = "https://ellipse-kpd.kppmining.net";
            switch (op.login)
            {
                case "Admin":
                    ret.username = "dailykpp";
                    ret.password = "kpp12345";
                    ret.position = "KP2SD03";
                    break;
                case "User":
                    ret.username = "kppcost";
                    ret.password = "kpp1234";
                    ret.position = "KP2SD03";
                    break;
                case "Creator":
                    ret.username = "bpm001";
                    ret.password = "kpp0811";
                    ret.position = "KP2SD021";
                    //ret.position = "KP2AC014";
                    break;
                default:
                    throw new Exception("Invalid login type " + op.login);
            }
            bool success = false;
            for (int retry = 100; retry > 0; retry--)
            {
                lock (locked)
                {
                    if (!locked.Contains(ret.baseurl + ret.username + ret.position))
                    {
                        locked.Add(ret.baseurl + ret.username + ret.position);
                        success = true;
                        break;
                    }
                }
                logger.Info("waiting for available credentials");
                System.Threading.Thread.Sleep(5000);
            }
            if (!success) throw new Exception("CacheJobProcess: cant get available credentials");
            logger.Info("Login using " + ret.baseurl + " - " + ret.username + " - " + ret.position);
            return ret;
        }

        public override void UnlockCredential(StructOperation op)
        {
            lock (locked)
            {
                if (locked.Contains(op.baseurl + op.username + op.position))
                {
                    locked.Remove(op.baseurl + op.username + op.position);
                }
            }
        }

        public override DataTable QueryDB(string query, List<SqlParameter> prm)
        {
            throw new NotImplementedException();
        }

    }
}
