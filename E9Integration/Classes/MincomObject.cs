﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using MIMSX;
using System.Text;
using MincomWCF.Libraries;
using System.Threading;
using log4net;
using System.Diagnostics;

namespace E9Integration.Classes
{
    public class MincomObject
    {
        public class MincomException : Exception
        {
            public MincomException(string message)
                : base(message)
            {
            }
        }

        public object threadid = 0;

        private static readonly ILog logger =
            LogManager.GetLogger(typeof(MincomObject));
        private static readonly ILog dumper =
            LogManager.GetLogger("dumpfields");

        private void loggerinfo(string message)
        {
            string stringid = threadid.ToString();
            if (stringid != null && stringid.Length > 8) stringid = stringid.Substring(0, 8);
            logger.InfoFormat("#{0}: {1}", stringid, message);
        }

        //private MIMSXServer mServer = null;

        //private void LogMessage(string message)
        //{
        //    LogHelper.LogMessage(message);
        //}

        public void Connect()
        {
            Connect(null, null, null, null, null, null, null, null);
        }

        public void Connect(string host, string port, string username, string password,
            string mimsuser, string mimspassword, string district, string position)
        {
            //if (IsConnected())
            //{
            //    //already connect
            //    return;
            //}
            //bool askLogin = false;

            //loggerinfo("Connecting");

            ////            Thread.Sleep(120000);

            //mServer = new MIMSXServer();
            //if (!mServer.Initialise())
            //{
            //    //cannot connect
            //    throw new MincomException("Unable to initialise the Ellipse Server");
            //}

            //if (host != null && port != null && username != null && password != null)
            //{
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_HOST, host);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_PORT, port);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_USERNAME, username);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_PLAIN_TXT_PWD, password);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_MIMS_USER, mimsuser);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_MIMS_PASSWORD, mimspassword);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_MIMS_DISTRICT, district);
            //    mServer.SetParameter(MIMSX.MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_MIMS_POSITION, position);
            //}
            //else
            //{
            //    askLogin = true;
            //}
            //string paramTimeout = ServiceHelper.GetConfig("MincomConnectionTimeout");
            //if (paramTimeout == null || paramTimeout.Equals(""))
            //{
            //    paramTimeout = "999";
            //}
            //mServer.SetParameter(MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_TIMEOUT, paramTimeout);

            //int retry = 20;
            //int rc = retry;
            //while (rc > 0)
            //{
            //    if (mServer.Connect(!askLogin))
            //    {
            //        //connected
            //        mServer.SetParameter(MIMSX_PARAMETER_CONSTANTS.MIMSX_PARAM_TIMEOUT, "6000");
            //        loggerinfo("Connected");
            //        break;
            //    }
            //    else
            //    {
            //        rc--;
            //        //failed
            //        Thread.Sleep(10000);
            //        StringBuilder sb = new StringBuilder();
            //        if (mServer.Errors.Count > 0)
            //        {
            //            for (int i = 1; i < mServer.Errors.Count + 1; i++)
            //            {
            //                sb.Append(mServer.Errors.Item(i).ErrorMessage);
            //            }
            //        }
            //        string msg = sb.ToString().ToLower();
            //        if (msg.Contains("invalid") && msg.Contains("password"))
            //        {
            //            rc = -1;
            //        }
            //        else if (msg.Contains("ellipse user profile locked, contact sysadmin"))
            //        {
            //            rc = -1;
            //        }
            //        if (rc < 0)
            //        {
            //            throw new MincomException("Failed to connect to the MIMS Server: " + sb.ToString());
            //        }
            //        else
            //        {
            //            loggerinfo("Unable to connect to the MIMS Server: " + sb.ToString());
            //            loggerinfo("Retry connecting: " + (retry - rc) + " of " + retry);
            //        }
            //    }
            //}
        }

        public void Disconnect()
        {
            //if (IsConnected())
            //{
            //    loggerinfo("Disconnecting");
            //    if (mServer != null)
            //    {
            //        mServer.Disconnect();
            //        mServer = null;
            //    }
            //    loggerinfo("Disconnected");
            //}
            //else
            //{
            //    //not connected
            //    //throw new MincomException("Not currently connected.");
            //}
        }

        public bool IsIdle()
        {
            return false;
            //return mServer.Screen.Idle;
        }

        public bool IsConnected()
        {
            //if (mServer != null)
            //{
            //    return mServer.Connected;
            //}
            return false;
        }

        public string GetScreenName()
        {
            //if (!IsConnected() || IsIdle())
            //{
            //    return null;
            //}
            //return mServer.Screen.MSO.Name;
            return null;
        }

        public string GetErrorMessage()
        {
            //string errmsg = mServer.Screen.MSO.Error.Trim();
            //if (errmsg == null)
            //{
            //    errmsg = GetFieldValue("ERRMESS1I");
            //}
            //return errmsg;
            return null;
        }

        public bool CheckForScreen(string screen, bool handleError)
        {
            //if (mServer == null)
            //{
            //    return false;
            //}
            //if (mServer.Screen.Idle || mServer.Screen.MSO.Name != screen)
            //{
            //    if (handleError)
            //    {
            //        if (mServer.Screen.MSO.Fields.Count != 0)
            //        {
            //            if (!mServer.Screen.MSO.Error.Trim().Equals(""))
            //            {
            //                throw new MincomException("Unexpected MIMS Error: " + Environment.NewLine
            //                    + mServer.Screen.MSO.Error.Trim());// + " - Active Field: " + mServer.Screen.MSO.ActiveField.Name);
            //            }
            //            else
            //            {
            //                string errorMessage = GetFieldValue("ERRMESS1I");
            //                if (errorMessage != null && !errorMessage.Trim().Equals(""))
            //                {
            //                    throw new MincomException("Unexpected MIMS Error: " + Environment.NewLine
            //                        + errorMessage);
            //                }
            //                else
            //                {
            //                    throw new MincomException("Expected screen: " + screen + Environment.NewLine
            //                        + "Encountered screen: " + mServer.Screen.MSO.Name);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            throw new MincomException("Expected screen: " + screen + Environment.NewLine
            //                + "No screen map is available. Restart and try again."
            //                        + "Encountered screen: " + mServer.Screen.MSO.Name);
            //        }
            //    }
            //    return false;
            //}
            return true;
        }

        public string DumpFields()
        {
            return DumpFields(true);
        }

        public string DumpFields(bool includeEmptyFields)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("---Dump Fields---\r\n");
            sb.Append("Screen\t: ").Append(GetScreenName()).Append("\r\n");
            sb.Append(string.Join("\r\n", GetFieldValues(includeEmptyFields)));
            sb.Append("\r\nCommands:\r\n");
            sb.Append(string.Join("\r\n", GetCommands()));
            sb.Append("\r\n---End Dump---");
            string result = sb.ToString();
            return result;
        }

        public string[] GetFieldValues()
        {
            return GetFieldValues(true);
        }

        public string[] GetFieldValues(bool includeEmptyFields)
        {
            //List<string> fields = new List<string>();
            //for (int i = 1; i < mServer.Screen.MSO.Fields.Count + 1; i++)
            //{
            //    if (includeEmptyFields || mServer.Screen.MSO.Fields.Item(i).Value.Trim().Length > 0)
            //    {
            //        if (mServer.Screen.MSO.Fields.Item(i).Protected > 0)
            //        {
            //            fields.Add(string.Format("*{0}\t: {1}", mServer.Screen.MSO.Fields.Item(i).Name,
            //                mServer.Screen.MSO.Fields.Item(i).Value));
            //        }
            //        else
            //        {
            //            fields.Add(string.Format("{0}\t: {1}", mServer.Screen.MSO.Fields.Item(i).Name,
            //                mServer.Screen.MSO.Fields.Item(i).Value));
            //        }
            //    }
            //}
            //return fields.ToArray();
            return null;
        }

        public string[] GetFields()
        {
            //List<string> fields = new List<string>();
            //for (int i = 1; i < mServer.Screen.MSO.Fields.Count + 1; i++)
            //{
            //    if (mServer.Screen.MSO.Fields.Item(i).Protected > 0)
            //    {
            //        fields.Add(string.Format("*{0}", mServer.Screen.MSO.Fields.Item(i).Name));
            //    }
            //    else
            //    {
            //        fields.Add(string.Format("{0}", mServer.Screen.MSO.Fields.Item(i).Name));
            //    }
            //}
            //return fields.ToArray();
            return null;
        }

        public string[] GetCommands()
        {
            //List<string> commands = new List<string>();
            //for (int i = 1; i < mServer.Screen.MSO.Commands.Count + 1; i++)
            //{
            //    commands.Add(string.Format("{0}", mServer.Screen.MSO.Commands.Item(i).Name));
            //}
            //return commands.ToArray();
            return null;
        }

        public void ExecuteMSO(string msoCode)
        {
            //try
            //{
            //    if (IsConnected() && IsIdle())
            //    {
            //        mServer.Screen.ExecuteMSO(msoCode);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message + " on executing " + msoCode, ex);
            //}
        }

        public bool IsCommand(string commandName)
        {
            //bool found = false;
            //for (int i = 1; i < mServer.Screen.MSO.Commands.Count + 1; i++)
            //{
            //    if (mServer.Screen.MSO.Commands.Item(i).Name.Equals(commandName))
            //    {
            //        found = true;
            //        break;
            //    }
            //}
            //return found;
            return false;
        }

        public void ExecuteCommand(string commandName)
        {
            //StringBuilder sb = new StringBuilder();
            //bool execute = false;
            //if (ServiceHelper.GetConfig("DumpFieldsOnCommand").Equals("true"))
            //{
            //    sb.Append(DumpFields(false));
            //    sb.Append("\r\nExecute Command: ").Append(commandName);
            //}
            //for (int i = 1; i < mServer.Screen.MSO.Commands.Count + 1; i++)
            //{
            //    if (mServer.Screen.MSO.Commands.Item(i).Name.Equals(commandName))
            //    {
            //        mServer.Screen.MSO.Commands.Item(i).Execute();
            //        execute = true;
            //        break;
            //    }
            //}
            //if (ServiceHelper.GetConfig("DumpFieldsOnCommand").Equals("true"))
            //{
            //    string err = GetErrorMessage().Trim();
            //    sb.Append("\r\nScreen: ").Append(GetScreenName());
            //    sb.Append("\r\nResult Message: ").Append(err);
            //    if (!err.Equals(""))
            //    {
            //        sb.Append("\r\nActive Field: ").Append(GetActiveField());
            //    }
            //    sb.Append("\r\n---End Dump Response---");
            //    dumper.Debug(sb.ToString());
            //}
            //if (!execute) throw new MincomException("Command failed to execute: " + commandName);
        }

        public bool SetFieldValue(string fieldName, string value)
        {
            //bool succeed = false;
            //for (int i = 1; i < mServer.Screen.MSO.Fields.Count + 1; i++)
            //{
            //    if (mServer.Screen.MSO.Fields.Item(i).Name.Equals(fieldName))
            //    {
            //        succeed = true;
            //        mServer.Screen.MSO.Fields.Item(i).Value = value;
            //        break;
            //    }
            //}
            //return succeed;
            return false;
        }

        public bool IsFieldProtected(string fieldName)
        {
            //bool value = true;
            //for (int i = 1; i < mServer.Screen.MSO.Fields.Count + 1; i++)
            //{
            //    if (mServer.Screen.MSO.Fields.Item(i).Name.Equals(fieldName))
            //    {
            //        value = (mServer.Screen.MSO.Fields.Item(i).Protected > 0);
            //        break;
            //    }
            //}
            //return value;
            return false;
        }

        public string GetFieldValue(string fieldName)
        {
            //string value = null;
            //for (int i = 1; i < mServer.Screen.MSO.Fields.Count + 1; i++)
            //{
            //    if (mServer.Screen.MSO.Fields.Item(i).Name.Equals(fieldName))
            //    {
            //        value = mServer.Screen.MSO.Fields.Item(i).Value;
            //        break;
            //    }
            //}
            //return value;
            return null;
        }

        public string GetActiveField()
        {
            //return mServer.Screen.MSO.ActiveField.Name;
            return null;
        }

        public void GoHome()
        {
            if (!IsIdle())
            {
                ExecuteCommand("Home");
                if (!IsIdle())
                {
                    throw new Exception("Not in idle state.");
                }
            }
        }

    }
}