﻿using E9Integration.Classes;
using E9Integration.Libraries;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace E9Integration
{
    class Service
    {
        private static readonly ILog logger =
            LogManager.GetLogger(typeof(Service));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            log4net.GlobalContext.Properties["id"] = "ws";

            if (System.Diagnostics.Debugger.IsAttached)
            {
                if (new E9Functions().testfunction())
                {
                    return;
                }
            }

            if (args.Length == 1 && args[0] != "--standalone")
            {
                //spawn executor
                string id = args[0];
                id.Replace(" ", "");
                if (id.Length > 8)
                {
                    id = id.Substring(0, 8);
                }
                log4net.GlobalContext.Properties["id"] = id;
                log4net.Config.XmlConfigurator.Configure();
                //run worker
                JobProcess jp = JobProcess.GetJobProcess();
                jp.DoProcessWorker(args[0]);
            }
            else
            {
                log4net.Config.XmlConfigurator.Configure();
                //run service
                try
                {
                    ServiceBase service = new WindowsService();
                    if (System.Diagnostics.Debugger.IsAttached)
                    //if (Environment.UserInteractive)
                    {
                        //should run with admin account to debug this
                        //set output type window to see output texts in VS window
                        logger.Info("Debug mode");
                        Console.WriteLine("Starting service...");
                        ((WindowsService)service).Debug();
                        Console.WriteLine("Service is running.");
                        System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
                    }
                    else if (args.Length == 1 && args[0] == "--standalone")
                    {
                        //set output type console in "Project Properties" to see output texts
                        logger.Info("Standalone mode");
                        Console.WriteLine("Starting service...");
                        ((WindowsService)service).Debug();
                        Console.WriteLine("Service is running.");
                        System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
                    }
                    else
                    {
                        logger.Info("Service mode");
                        ServiceBase.Run(service);
                    }

                }
                catch (Exception ex)
                {
                    logger.Error("Error occured " + ex.Message, ex);
                    //netsh http add urlacl url=http://+:10001/ user="dev"
                    throw;
                }
            }

        }
    }
}