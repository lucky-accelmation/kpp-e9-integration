﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Windows.Forms;

namespace EIS_Monitor.Libraries
{
    public class QueryHelper
    {
        private static object thelock = new object();

        public static string[] GetConnectionStrings()
        {
            List<string> conns = new List<string>();
            foreach (ConnectionStringSettings conn in ConfigurationManager.ConnectionStrings)
            {
                if (conn.Name == "LocalSqlServer") continue;
                conns.Add(conn.Name);
            }
            return conns.ToArray();
        }

        public static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public static DataRow GetDataBoundItem(DataGridViewRow dgvRow)
        {
            if (dgvRow == null) return null;
            if (!(dgvRow.DataBoundItem is DataRowView))
            {
                return null;
            }
            return ((DataRowView)dgvRow.DataBoundItem).Row;
        }


        public static SqlConnection CreateConnection(string connectionString, bool createTransaction)
        {
            //            connectionString += ";MultipleActiveResultSets=true";
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionString;
            connection.Open();
            SqlTransaction transaction = GetTransaction(connection, createTransaction);

            return connection;
        }

        public static SqlConnection CreateConnection(string connectionString)
        {
            return CreateConnection(connectionString, true);
        }

        public static SqlTransaction GetTransaction(SqlConnection connection, bool create)
        {
            lock (thelock)
            {
                if (GlobalVariables.SqlTransactions == null)
                {
                    GlobalVariables.SqlTransactions = new Dictionary<SqlConnection, SqlTransaction>();
                    return GetTransaction(connection, create);
                }
                else if (connection == null)
                {
                    return null;
                }
                else if (GlobalVariables.SqlTransactions.ContainsKey(connection))
                {
                    return (SqlTransaction)GlobalVariables.SqlTransactions[connection];
                }
                else if (create)
                {
                    SqlTransaction transaction = connection.BeginTransaction();
                    GlobalVariables.SqlTransactions.Add(connection, transaction);
                    return transaction;
                }
                else
                {
                    return null;
                }
            }
        }

        public static void CommitTransaction(SqlConnection connection)
        {
            SqlTransaction transaction = GetTransaction(connection, false);
            if (transaction != null)
            {
                transaction.Commit();
                lock (thelock)
                {
                    GlobalVariables.SqlTransactions.Remove(connection);
                }
            }
        }

        public static void RollbackTransaction(SqlConnection connection)
        {
            SqlTransaction transaction = GetTransaction(connection, false);
            if (transaction != null)
            {
                transaction.Rollback();
                lock (thelock)
                {
                    GlobalVariables.SqlTransactions.Remove(connection);
                }
            }
        }

        public static void CloseConnection(SqlConnection connection)
        {
            if (connection != null)
            {
                //commit transaction
                CommitTransaction(connection);
                connection.Close();
            }
        }

        public static Dictionary<string, object> ReadRowWithParam(SqlConnection connection, string sql, List<SqlParameter> parameters)
        {
            Dictionary<string, object> res = new Dictionary<string, object>();
            if (connection != null)
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlTransaction trans = GetTransaction(connection, false);
                if (trans != null)
                    cmd.Transaction = trans;
                //                System.Diagnostics.Debug.Assert(trans != null);
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        if (reader.IsDBNull(i))
                        {
                            res.Add(reader.GetName(i), null);
                        }
                        else
                        {
                            res.Add(reader.GetName(i), reader.GetValue(i));
                        }
                    }
                }
                reader.Close();
            }
            return res;
        }

        public static Object ReadScalarWithParam(SqlConnection connection, string sql, List<SqlParameter> parameters)
        {
            Object res = null;
            if (connection != null)
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlTransaction trans = GetTransaction(connection, false);
                if (trans != null)
                    cmd.Transaction = trans;
                //                System.Diagnostics.Debug.Assert(trans != null);
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                res = cmd.ExecuteScalar();
            }
            if (res == DBNull.Value)
            {
                res = null;
            }
            return res;
        }

        public static DataTable ReadQueryTableParam(SqlConnection connection, string sql, List<SqlParameter> parameters)
        {
            DataTable result = new DataTable();

            if (connection != null)
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 300;

                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                SqlTransaction trans = GetTransaction(connection, false);
                if (trans != null)
                    cmd.Transaction = trans;
                //                System.Diagnostics.Debug.Assert(trans != null);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(result);
            }

            return result;
        }

        public static int ExecuteQueryWithParam(SqlConnection connection, string sql, List<SqlParameter> parameters)
        {
            int result = -1;
            if (connection != null)
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                SqlTransaction trans = GetTransaction(connection, false);
                if (trans != null)
                    cmd.Transaction = trans;
                //                System.Diagnostics.Debug.Assert(trans != null);
                result = cmd.ExecuteNonQuery();
            }
            return result;
        }
    }
}