﻿using EIS_Monitor.Libraries;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIS_Monitor
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }
        int[] ints = new int[0];
        BackgroundWorker bw = new BackgroundWorker();
        BackgroundWorker bwsc = new BackgroundWorker();
        bool busy = false;

        private void FormMain_Load(object sender, EventArgs e)
        {
            this.Text += " v.3.0 20220124";

            dgView.AllowUserToAddRows = false;
            dgView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dgView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgView.RowHeadersVisible = false;
            dgView.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgView.ReadOnly = true;
            dgView.AllowUserToResizeRows = false;

            cbAction.Items.AddRange(new string[] {
                "--Action--",
                "Copy JobID",
                "Copy 8-JobID",
                "Copy Input",
                "Copy Input-Escaped",
                "Copy Output",
                "Copy Error",
                "Copy Stacktrace",
                "Open Log",
                "Open Dump"
            });
            cbAction.SelectedIndex = 0;
            cbAction.DropDownStyle = ComboBoxStyle.DropDownList;

            cbInterval.Items.AddRange(new string[]
            {
                "1s",
                "5s",
                "10s",
                "30s"
            });
            ints = new int[] { 1000, 5000, 10000, 30000 };
            cbInterval.SelectedIndex = 1;
            cbInterval.DropDownStyle = ComboBoxStyle.DropDownList;

            cbLimitRow.Items.AddRange(new string[]
            {
                "35",
                "50",
                "100",
                "1000"
            });
            cbLimitRow.SelectedIndex = 0;

            cbOperationFilter.Items.Add("--ALL Operation--");
            string path = System.Configuration.ConfigurationManager.AppSettings["presetpath"];
            StreamReader reader = new StreamReader(new FileStream(path + "Operations.txt", FileMode.Open, FileAccess.Read));
            string[] ops = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            Array.Sort(ops);
            reader.Close();
            cbOperationFilter.Items.AddRange(ops);
            cbOperationFilter.SelectedIndex = 0;

            //chStartDate.Checked = true;
            //dpStartDate.Value = new DateTime(2021, 3, 1);
            dpStartDate.Format = DateTimePickerFormat.Custom;
            dpStartDate.CustomFormat = "ddd, dd-MMM-yyyy";

            dgView.CellFormatting += DgView_CellFormatting;
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;

            bwsc.DoWork += Bwsc_DoWork;
            bwsc.RunWorkerCompleted += Bw_RunWorkerCompleted;

            cbServer.Items.AddRange(QueryHelper.GetConnectionStrings());
            cbServer.SelectedIndex = 0;
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            busy = false;
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            busy = true;
            Dictionary<string, object> args = (Dictionary<string, object>)e.Argument;
            SqlConnection conn = null;
            try
            {
                string limit = args.ContainsKey("limitrow") ? args["limitrow"] as string : "35";
                string operation = args.ContainsKey("operation") ? args["operation"] as string : null;
                string server = args["server"] as string;
                conn = QueryHelper.CreateConnection(QueryHelper.GetConnectionString(server));
                DateTime startdate = args.ContainsKey("startdate") ? (DateTime)args["startdate"] : DateTime.MinValue;
                string q = "SELECT top " + limit + " r_id,jobid,operation,priority,input,datalength(input) as inputlen,output,datalength(output) as outputlen,"
                    + "state,error,stacktrace,added,started,finished,datediff(minute,started,getdate()) as dif,lastworker "
                    + "FROM[dbo].[jobdata] where 1=1";
                List<SqlParameter> p = new List<SqlParameter>();
                if (args.ContainsKey("filterfetch") && (args["filterfetch"] as string).Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    //q += " and state='Fetch' ";
                    q += " and (state='Fetch' or datediff(second,finished,getdate())<30) ";
                }
                if (operation != null)
                {
                    q += " and operation=@op ";
                    p.Add(new SqlParameter("op", operation));
                }
                if (args.ContainsKey("ridfrom"))
                {
                    if (args.ContainsKey("ridto") && !args["ridto"].Equals(""))
                    {
                        q += " and r_id>=@ridfrom ";
                        p.Add(new SqlParameter("ridfrom", args["ridfrom"]));
                        q += " and r_id<=@ridto ";
                        p.Add(new SqlParameter("ridto", args["ridto"]));
                    }
                    else
                    {
                        q += " and r_id=@ridfrom ";
                        p.Add(new SqlParameter("ridfrom", args["ridfrom"]));
                    }
                }
                if (args.ContainsKey("search"))
                {
                    q += " and input like @search ";
                    p.Add(new SqlParameter("search", args["search"]));
                }
                if (startdate != DateTime.MinValue)
                {
                    q += " and added>=@sd ";
                    p.Add(new SqlParameter("sd", startdate));
                    q += "order by r_id asc ";
                }
                else
                {
                    q += "order by r_id desc ";
                }
                DataTable dt = QueryHelper.ReadQueryTableParam(conn, q, p);

                //q = "select count(r_id) as fetchcnt,datediff(minute,min(added),getdate()) as oldest from jobdata where state='Fetch' ";
                //Dictionary<string, object> res = QueryHelper.ReadRowWithParam(conn, q, null);
                //dgView.BeginInvoke(new UpdateDataDelegate(UpdateData), dt,
                //    string.Format("Last Update {0:dd-MMM HH:mm:ss}; {1} Fetch; {2} (min) Oldest", DateTime.Now, res["fetchcnt"], res["oldest"]));


                // ---- STATS ----
                q = "select state,count(state) as cnt from jobdata group by state";
                DataTable dtstate = QueryHelper.ReadQueryTableParam(conn, q, null);
                List<string> states = new List<string>();
                states.AddRange(dtstate.AsEnumerable().Select(r => string.Format("{0}: {1:N0}", r["state"], r["cnt"])));
                string str = string.Format("Last Update {0:dd-MMM HH:mm:ss};\r\n{1}", DateTime.Now, string.Join("\r\n", states.ToArray()));

                q = "select convert(float,count(case when datediff(minute,started,getdate())<=15 then 1 end))/15 as avg15,";
                q += "convert(float,count(case when datediff(minute,started,getdate())<=60 then 1 end))/60 as avg60,";
                q += "convert(float,count(1))/180 as avg180 ";
                q += "from jobdata where datediff(minute,started,getdate())<=180";
                Dictionary<string, object> stats = QueryHelper.ReadRowWithParam(conn, q, null);
                str += "\r\n" + string.Join(" -- ", stats.Select(kv => string.Format("[{0}: {1:N1}]", kv.Key, kv.Value)).ToArray());
                dgView.BeginInvoke(new UpdateDataDelegate(UpdateData), dt, str);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                msg += "\r\n" + ex.StackTrace;
                dgView.BeginInvoke(new UpdateDataDelegate(UpdateData), null, msg);
                busy = false;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }

        }

        private delegate void UpdateDataDelegate(DataTable dt, string tx);

        private void UpdateData(DataTable dt, string tx)
        {
            if (dt != null)
            {
                dgView.DataSource = dt;
                dgView.Columns["jobid"].Visible = false;
                dgView.Columns["input"].Visible = false;
                dgView.Columns["output"].Visible = false;
                dgView.Columns["error"].Visible = false;
                dgView.Columns["stacktrace"].Visible = false;
                dgView.Columns["dif"].HeaderText = "age (minute)";
            }
            else
            {
                chAutoUpdate.Checked = false;
            }
            txOutput.Text = tx;

        }

        private void DgView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgView.Columns[e.ColumnIndex].DataPropertyName.Equals("state"))
            {
                if (dgView[e.ColumnIndex, e.RowIndex].Value.Equals("Finish"))
                {
                    e.CellStyle.BackColor = Color.Lime;
                }
                else if (dgView[e.ColumnIndex, e.RowIndex].Value.Equals("Fetch"))
                {
                    e.CellStyle.BackColor = Color.Yellow;
                }
                else
                {
                    e.CellStyle.BackColor = Color.Red;
                }
            }
        }

        private void BtUpdate_Click(object sender, EventArgs e)
        {
            if (!busy)
            {
                Dictionary<string, object> args = new Dictionary<string, object>();
                args.Add("filterfetch", chFilterFetch.Checked.ToString());
                args.Add("limitrow", cbLimitRow.SelectedItem as string);
                args.Add("operation", cbOperationFilter.SelectedIndex <= 0 ? null : cbOperationFilter.SelectedItem as string);
                args.Add("startdate", chStartDate.Checked ? dpStartDate.Value : DateTime.MinValue);
                args.Add("server", cbServer.SelectedItem as string);
                txOutput.Text = string.Format("Updating {0:dd-MMM HH:mm:ss}", DateTime.Now);
                bw.RunWorkerAsync(args);
            }
            else
            {
                txOutput.Text = string.Format("Busy {0:dd-MMM HH:mm:ss}", DateTime.Now);
                chAutoUpdate.Checked = false;
            }

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            BtUpdate_Click(sender, e);
        }

        private void ChAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            if (chAutoUpdate.Checked)
                timer.Enabled = true;
            else
                timer.Enabled = false;
        }

        private void CbInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            timer.Interval = ints[cbInterval.SelectedIndex];
        }

        private void copytext(string rowname)
        {
            copytext(rowname, false);
        }

        private void copytext(string rowname, bool escaped)
        {
            if (dgView.SelectedRows.Count > 0)
            {
                try
                {
                    string rownametx = rowname;
                    if (rowname == "8-jobid") rownametx = "jobid";
                    DataRow row = QueryHelper.GetDataBoundItem(dgView.SelectedRows[0]);
                    string tx = row[rownametx] as string;
                    if (escaped)
                        tx = "\"" + tx.Replace("\"", "\\\"") + "\"";
                    if (rowname == "8-jobid") tx = tx.Substring(0, 8);
                    Clipboard.SetText(tx);
                    txOutput.Text = string.Format("copied {0} {1:dd-MMM HH:mm:ss}: {2}", rowname, DateTime.Now, tx);
                }
                catch (Exception ex)
                {
                    txOutput.Text = ex.Message;
                }
            }
        }

        string lastaction = null;

        private void doaction(string action)
        {
            if (action == null && lastaction == null)
            {
                return;
            }
            if (action == null)
            {
                action = lastaction;
            }
            if (action.Equals("copy jobid"))
            {
                copytext("jobid");
            }
            else if (action.Equals("copy 8-jobid"))
            {
                copytext("8-jobid");
            }
            else if (action.Equals("copy input"))
            {
                copytext("input");
            }
            else if (action.Equals("copy input-escaped"))
            {
                copytext("input", true);
            }
            else if (action.Equals("copy output"))
            {
                copytext("output");
            }
            else if (action.Equals("copy error"))
            {
                copytext("error");
            }
            else if (action.Equals("copy stacktrace"))
            {
                copytext("stacktrace");
            }
            else if (action.Equals("open log"))
            {
                openlog("eis");
            }
            else if (action.Equals("open dump"))
            {
                openlog("dump");
            }
            lastaction = action;
        }

        private void CbAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAction.SelectedIndex != 0)
            {
                string action = cbAction.SelectedItem.ToString().ToLower();
                doaction(action);
                cbAction.SelectedIndex = 0;
            }
        }

        private void openlog(string logtype, int retry = 3)
        {
            string template = @"\\{0}\c$\EIS\logs\{1:yyyyMMdd}\{2}_{3}.log";
            if (((string)cbServer.SelectedItem).Contains("E9"))
            {
                template = @"\\{0}\c$\E9I\logs\{1:yyyyMMdd}\{2}_{3}.log";
                if (logtype == "eis") logtype = "e9";
                if (logtype == "dump") logtype = "dmp";
            }
            if (dgView.SelectedRows.Count > 0)
            {
                string server = null;
                try
                {
                    DataRow row = QueryHelper.GetDataBoundItem(dgView.SelectedRows[0]);
                    string[] infosplit = (row["lastworker"] as string).Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    string[] jobidsplit = (row["jobid"] as string).Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                    //System.Environment.MachineName
                    server = infosplit[3].Trim();
                    string path = string.Format(template, server, row["started"], logtype, jobidsplit[0]);
                    if (infosplit[0] == System.Environment.MachineName)
                    {
                        template = @"{0}logs\{1:yyyyMMdd}\{2}_{3}.log";
                        path = string.Format(template, infosplit[2].Trim(), row["started"], logtype, jobidsplit[0]);
                    }
                    txOutput.Text = string.Format("open log {0} {1:dd-MMM HH:mm:ss}", path, DateTime.Now);
                    //System.Security.SecureString pass = new System.Security.SecureString();
                    //"kpp0811".ToList().ForEach(p => pass.AppendChar(p));
                    //Process.Start(path, "dmadmin", pass, "kppmining.net");
                    Process.Start(path);
                }
                catch (Exception ex)
                {
                    txOutput.Text = "Error: " + ex.Message;
                    //if (ex.Message == "The specified network password is not correct")
                    //{
                    //    if (retry <= 0) return;
                    //    if (server != null) opennetwork(server);
                    //    openlog(logtype, retry - 1);
                    //}
                }
            }
        }

        private void opennetwork(string server)
        {
            string cmd = @"c:\windows\system32\net.exe";
            string arg = @"use \\" + server + @"\c$ / user:kppmining.net\dmadmin kpp0811";
            Process p = Process.Start(cmd, arg);
            p.WaitForExit();
            //string s=p.StandardOutput.ReadToEnd();
            //s += p.StandardError.ReadToEnd();
            //txOutput.Text = cmd+s;
        }

        private void DgView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgView.SelectedRows.Count > 0)
            {
                try
                {
                    DataRow row = QueryHelper.GetDataBoundItem(dgView.SelectedRows[0]);
                    if (chFormat.Checked)
                    {
                        txInput.Text = JsonConvert.SerializeObject(JsonConvert.DeserializeObject(row["input"] as string), Formatting.Indented);
                    }
                    else
                    {
                        txInput.Text = row["input"] as string;
                    }
                    if (row["state"].Equals("Error"))
                    {
                        txOutputError.Text = row["error"] as string;
                    }
                    else
                    {
                        txOutputError.Text = row["output"] as string;
                    }
                }
                catch (Exception ex)
                {
                    txOutput.Text = ex.Message;
                }
            }
        }

        private void btRepeatAction_Click(object sender, EventArgs e)
        {
            doaction(null);
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            if (!busy)
            {
                Dictionary<string, object> args = new Dictionary<string, object>();
                //args.Add("filterfetch", chFilterFetch.Checked.ToString());
                //args.Add("limitrow", cbLimitRow.SelectedItem as string);
                //args.Add("operation", cbOperationFilter.SelectedIndex <= 0 ? null : cbOperationFilter.SelectedItem as string);
                //args.Add("startdate", chStartDate.Checked ? dpStartDate.Value : DateTime.MinValue);
                args.Add("server", cbServer.SelectedItem as string);
                args.Add("search", "%" + txSearch.Text + "%");
                txOutput.Text = string.Format("Updating {0:dd-MMM HH:mm:ss}", DateTime.Now);
                bw.RunWorkerAsync(args);
            }
            else
            {
                txOutput.Text = string.Format("Busy {0:dd-MMM HH:mm:ss}", DateTime.Now);
                chAutoUpdate.Checked = false;
            }
            //DataTable dt = (DataTable)dgView.DataSource;
            //if (dt != null)
            //{
            //    dt.DefaultView.RowFilter = "input like '%" + txSearch.Text + "%'";
            //}
        }

        private void btSearchId_Click(object sender, EventArgs e)
        {
            if (!busy)
            {
                Dictionary<string, object> args = new Dictionary<string, object>();
                //args.Add("filterfetch", chFilterFetch.Checked.ToString());
                //args.Add("limitrow", cbLimitRow.SelectedItem as string);
                //args.Add("operation", cbOperationFilter.SelectedIndex <= 0 ? null : cbOperationFilter.SelectedItem as string);
                //args.Add("startdate", chStartDate.Checked ? dpStartDate.Value : DateTime.MinValue);
                args.Add("server", cbServer.SelectedItem as string);
                args.Add("ridfrom", txIdFrom.Text);
                args.Add("ridto", txIdTo.Text);
                txOutput.Text = string.Format("Updating {0:dd-MMM HH:mm:ss}", DateTime.Now);
                bw.RunWorkerAsync(args);
            }
            else
            {
                txOutput.Text = string.Format("Busy {0:dd-MMM HH:mm:ss}", DateTime.Now);
                chAutoUpdate.Checked = false;
            }

        }

        private void chFormat_CheckedChanged(object sender, EventArgs e)
        {
            DgView_SelectionChanged(sender, e);
        }

        private void btSpecialCase_Click(object sender, EventArgs e)
        {
            dgView.DataSource = null;
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("server", cbServer.SelectedItem as string);
            bwsc.RunWorkerAsync(args);
        }

        private void Bwsc_DoWork(object sender, DoWorkEventArgs e)
        {
            busy = true;
            Dictionary<string, object> args = (Dictionary<string, object>)e.Argument;
            SqlConnection conn = null;
            try
            {
                int rid = 12000;
                int start = 282;
                int end = 386;
                end = 450;
                string search = "";
                string server = args["server"] as string;
                conn = QueryHelper.CreateConnection(QueryHelper.GetConnectionString(server));

                DataTable dt = null;
                for (int i = start; i <= end; i++)
                {
                    search = string.Format("PR-22-{0:000}", i);
                    string q = "SELECT r_id,'PRNO' as PRNO,'DISTRICT' as DISTRICT,jobid,operation,input,output,state,error,added,started,finished,datediff(minute,started,getdate()) as dif,lastworker "
                        + "FROM[dbo].[jobdata] where 1=1";
                    q += " and r_id>@rid ";
                    q += " and input like @search ";
                    q += " and state='Finish' ";
                    q += "order by r_id desc ";

                    List<SqlParameter> p = new List<SqlParameter>();
                    p.Add(new SqlParameter("rid", rid));
                    p.Add(new SqlParameter("search", "%" + search + "%"));
                    DataTable dtn = QueryHelper.ReadQueryTableParam(conn, q, p);
                    for (int j = 0; j < dtn.Rows.Count; j++)
                    {
                        JObject obj = (JObject)JsonConvert.DeserializeObject(dtn.Rows[j]["input"] as string);
                        dtn.Rows[j]["PRNO"] = search;
                        dtn.Rows[j]["DISTRICT"] = obj["overrideDistrict"].ToString();
                    }
                    if (dt == null)
                    {
                        dt = dtn;
                        dgView.BeginInvoke((MethodInvoker)delegate
                        {
                            dgView.DataSource = dt;
                            dgView.Columns["jobid"].Visible = false;
                            dgView.Columns["input"].Visible = false;
                            //dgView.Columns["output"].Visible = false;
                            dgView.Columns["error"].Visible = false;
                            dgView.Columns["dif"].HeaderText = "age (minute)";
                        });
                    }
                    else if (dtn.Rows.Count > 1)
                    {
                        dgView.BeginInvoke((MethodInvoker)delegate
                        {
                            for (int j = 0; j < dtn.Rows.Count; j++)
                            {
                                dt.ImportRow(dtn.Rows[j]);
                            }
                        });
                    }
                    else
                    {
                        dgView.BeginInvoke((MethodInvoker)delegate
                        {
                            txOutput.Text = string.Format("Search {0} {1}", search, dtn.Rows.Count);
                        });
                    }
                    Console.WriteLine(search);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                msg += "\r\n" + ex.StackTrace;
                Console.WriteLine(msg);
                busy = false;
            }
            finally
            {
                QueryHelper.CloseConnection(conn);
            }
            dgView.BeginInvoke((MethodInvoker)delegate
            {
                txOutput.Text = "Done";
            });

            Console.WriteLine("Done");
        }
    }
}
