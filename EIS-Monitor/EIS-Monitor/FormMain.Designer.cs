﻿namespace EIS_Monitor
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btUpdate = new System.Windows.Forms.Button();
            this.dgView = new System.Windows.Forms.DataGridView();
            this.txOutput = new System.Windows.Forms.TextBox();
            this.chAutoUpdate = new System.Windows.Forms.CheckBox();
            this.cbAction = new System.Windows.Forms.ComboBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.cbInterval = new System.Windows.Forms.ComboBox();
            this.txInput = new System.Windows.Forms.TextBox();
            this.txOutputError = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chFilterFetch = new System.Windows.Forms.CheckBox();
            this.cbLimitRow = new System.Windows.Forms.ComboBox();
            this.cbOperationFilter = new System.Windows.Forms.ComboBox();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.chStartDate = new System.Windows.Forms.CheckBox();
            this.btRepeatAction = new System.Windows.Forms.Button();
            this.cbServer = new System.Windows.Forms.ComboBox();
            this.txSearch = new System.Windows.Forms.TextBox();
            this.btSearch = new System.Windows.Forms.Button();
            this.txIdFrom = new System.Windows.Forms.TextBox();
            this.btSearchId = new System.Windows.Forms.Button();
            this.chFormat = new System.Windows.Forms.CheckBox();
            this.btSpecialCase = new System.Windows.Forms.Button();
            this.txIdTo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.SuspendLayout();
            // 
            // btUpdate
            // 
            this.btUpdate.Location = new System.Drawing.Point(12, 12);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(101, 48);
            this.btUpdate.TabIndex = 0;
            this.btUpdate.Text = "Update";
            this.btUpdate.UseVisualStyleBackColor = true;
            this.btUpdate.Click += new System.EventHandler(this.BtUpdate_Click);
            // 
            // dgView
            // 
            this.dgView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Location = new System.Drawing.Point(12, 66);
            this.dgView.Name = "dgView";
            this.dgView.Size = new System.Drawing.Size(1394, 394);
            this.dgView.TabIndex = 17;
            this.dgView.SelectionChanged += new System.EventHandler(this.DgView_SelectionChanged);
            // 
            // txOutput
            // 
            this.txOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txOutput.Location = new System.Drawing.Point(12, 550);
            this.txOutput.Multiline = true;
            this.txOutput.Name = "txOutput";
            this.txOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txOutput.Size = new System.Drawing.Size(1394, 73);
            this.txOutput.TabIndex = 23;
            // 
            // chAutoUpdate
            // 
            this.chAutoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chAutoUpdate.AutoSize = true;
            this.chAutoUpdate.Location = new System.Drawing.Point(1219, 14);
            this.chAutoUpdate.Name = "chAutoUpdate";
            this.chAutoUpdate.Size = new System.Drawing.Size(86, 17);
            this.chAutoUpdate.TabIndex = 14;
            this.chAutoUpdate.Text = "Auto Update";
            this.chAutoUpdate.UseVisualStyleBackColor = true;
            this.chAutoUpdate.CheckedChanged += new System.EventHandler(this.ChAutoUpdate_CheckedChanged);
            // 
            // cbAction
            // 
            this.cbAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAction.FormattingEnabled = true;
            this.cbAction.Location = new System.Drawing.Point(1142, 39);
            this.cbAction.Name = "cbAction";
            this.cbAction.Size = new System.Drawing.Size(163, 21);
            this.cbAction.TabIndex = 15;
            this.cbAction.SelectedIndexChanged += new System.EventHandler(this.CbAction_SelectedIndexChanged);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // cbInterval
            // 
            this.cbInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbInterval.FormattingEnabled = true;
            this.cbInterval.Location = new System.Drawing.Point(1142, 12);
            this.cbInterval.Name = "cbInterval";
            this.cbInterval.Size = new System.Drawing.Size(71, 21);
            this.cbInterval.TabIndex = 13;
            this.cbInterval.SelectedIndexChanged += new System.EventHandler(this.CbInterval_SelectedIndexChanged);
            // 
            // txInput
            // 
            this.txInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txInput.Location = new System.Drawing.Point(42, 466);
            this.txInput.Multiline = true;
            this.txInput.Name = "txInput";
            this.txInput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txInput.Size = new System.Drawing.Size(1300, 52);
            this.txInput.TabIndex = 19;
            // 
            // txOutputError
            // 
            this.txOutputError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txOutputError.Location = new System.Drawing.Point(42, 524);
            this.txOutputError.Name = "txOutputError";
            this.txOutputError.Size = new System.Drawing.Size(1364, 20);
            this.txOutputError.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 469);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "I";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 527);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "O/E";
            // 
            // chFilterFetch
            // 
            this.chFilterFetch.AutoSize = true;
            this.chFilterFetch.Location = new System.Drawing.Point(291, 13);
            this.chFilterFetch.Name = "chFilterFetch";
            this.chFilterFetch.Size = new System.Drawing.Size(102, 17);
            this.chFilterFetch.TabIndex = 3;
            this.chFilterFetch.Text = "Filter Fetch Only";
            this.chFilterFetch.UseVisualStyleBackColor = true;
            // 
            // cbLimitRow
            // 
            this.cbLimitRow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLimitRow.FormattingEnabled = true;
            this.cbLimitRow.Location = new System.Drawing.Point(187, 10);
            this.cbLimitRow.Name = "cbLimitRow";
            this.cbLimitRow.Size = new System.Drawing.Size(98, 21);
            this.cbLimitRow.TabIndex = 2;
            // 
            // cbOperationFilter
            // 
            this.cbOperationFilter.FormattingEnabled = true;
            this.cbOperationFilter.Location = new System.Drawing.Point(119, 39);
            this.cbOperationFilter.Name = "cbOperationFilter";
            this.cbOperationFilter.Size = new System.Drawing.Size(274, 21);
            this.cbOperationFilter.TabIndex = 4;
            // 
            // dpStartDate
            // 
            this.dpStartDate.CustomFormat = "ddd, dd-MMM-yyyy";
            this.dpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpStartDate.Location = new System.Drawing.Point(485, 12);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(136, 20);
            this.dpStartDate.TabIndex = 6;
            // 
            // chStartDate
            // 
            this.chStartDate.AutoSize = true;
            this.chStartDate.Location = new System.Drawing.Point(431, 15);
            this.chStartDate.Name = "chStartDate";
            this.chStartDate.Size = new System.Drawing.Size(48, 17);
            this.chStartDate.TabIndex = 5;
            this.chStartDate.Text = "Start";
            this.chStartDate.UseVisualStyleBackColor = true;
            // 
            // btRepeatAction
            // 
            this.btRepeatAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btRepeatAction.Location = new System.Drawing.Point(1311, 12);
            this.btRepeatAction.Name = "btRepeatAction";
            this.btRepeatAction.Size = new System.Drawing.Size(95, 48);
            this.btRepeatAction.TabIndex = 16;
            this.btRepeatAction.Text = "Repeat Action";
            this.btRepeatAction.UseVisualStyleBackColor = true;
            this.btRepeatAction.Click += new System.EventHandler(this.btRepeatAction_Click);
            // 
            // cbServer
            // 
            this.cbServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbServer.FormattingEnabled = true;
            this.cbServer.Location = new System.Drawing.Point(119, 10);
            this.cbServer.Name = "cbServer";
            this.cbServer.Size = new System.Drawing.Size(62, 21);
            this.cbServer.TabIndex = 1;
            // 
            // txSearch
            // 
            this.txSearch.Location = new System.Drawing.Point(666, 13);
            this.txSearch.Name = "txSearch";
            this.txSearch.Size = new System.Drawing.Size(138, 20);
            this.txSearch.TabIndex = 7;
            // 
            // btSearch
            // 
            this.btSearch.Location = new System.Drawing.Point(810, 13);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(92, 20);
            this.btSearch.TabIndex = 8;
            this.btSearch.Text = "Search Input";
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // txIdFrom
            // 
            this.txIdFrom.Location = new System.Drawing.Point(666, 38);
            this.txIdFrom.Name = "txIdFrom";
            this.txIdFrom.Size = new System.Drawing.Size(63, 20);
            this.txIdFrom.TabIndex = 9;
            // 
            // btSearchId
            // 
            this.btSearchId.Location = new System.Drawing.Point(810, 37);
            this.btSearchId.Name = "btSearchId";
            this.btSearchId.Size = new System.Drawing.Size(92, 20);
            this.btSearchId.TabIndex = 11;
            this.btSearchId.Text = "Search ID";
            this.btSearchId.UseVisualStyleBackColor = true;
            this.btSearchId.Click += new System.EventHandler(this.btSearchId_Click);
            // 
            // chFormat
            // 
            this.chFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chFormat.AutoSize = true;
            this.chFormat.Location = new System.Drawing.Point(1348, 468);
            this.chFormat.Name = "chFormat";
            this.chFormat.Size = new System.Drawing.Size(58, 17);
            this.chFormat.TabIndex = 20;
            this.chFormat.Text = "Format";
            this.chFormat.UseVisualStyleBackColor = true;
            this.chFormat.CheckedChanged += new System.EventHandler(this.chFormat_CheckedChanged);
            // 
            // btSpecialCase
            // 
            this.btSpecialCase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSpecialCase.Location = new System.Drawing.Point(1041, 12);
            this.btSpecialCase.Name = "btSpecialCase";
            this.btSpecialCase.Size = new System.Drawing.Size(95, 48);
            this.btSpecialCase.TabIndex = 12;
            this.btSpecialCase.Text = "Special Case";
            this.btSpecialCase.UseVisualStyleBackColor = true;
            this.btSpecialCase.Click += new System.EventHandler(this.btSpecialCase_Click);
            // 
            // txIdTo
            // 
            this.txIdTo.Location = new System.Drawing.Point(741, 38);
            this.txIdTo.Name = "txIdTo";
            this.txIdTo.Size = new System.Drawing.Size(63, 20);
            this.txIdTo.TabIndex = 10;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1418, 635);
            this.Controls.Add(this.txIdTo);
            this.Controls.Add(this.btSpecialCase);
            this.Controls.Add(this.chFormat);
            this.Controls.Add(this.btSearchId);
            this.Controls.Add(this.txIdFrom);
            this.Controls.Add(this.btSearch);
            this.Controls.Add(this.txSearch);
            this.Controls.Add(this.cbServer);
            this.Controls.Add(this.btRepeatAction);
            this.Controls.Add(this.chStartDate);
            this.Controls.Add(this.dpStartDate);
            this.Controls.Add(this.cbOperationFilter);
            this.Controls.Add(this.cbLimitRow);
            this.Controls.Add(this.chFilterFetch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txOutputError);
            this.Controls.Add(this.txInput);
            this.Controls.Add(this.cbInterval);
            this.Controls.Add(this.cbAction);
            this.Controls.Add(this.chAutoUpdate);
            this.Controls.Add(this.txOutput);
            this.Controls.Add(this.dgView);
            this.Controls.Add(this.btUpdate);
            this.Name = "FormMain";
            this.Text = "EIS Monitor";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.TextBox txOutput;
        private System.Windows.Forms.CheckBox chAutoUpdate;
        private System.Windows.Forms.ComboBox cbAction;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ComboBox cbInterval;
        private System.Windows.Forms.TextBox txInput;
        private System.Windows.Forms.TextBox txOutputError;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chFilterFetch;
        private System.Windows.Forms.ComboBox cbLimitRow;
        private System.Windows.Forms.ComboBox cbOperationFilter;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.CheckBox chStartDate;
        private System.Windows.Forms.Button btRepeatAction;
        private System.Windows.Forms.ComboBox cbServer;
        private System.Windows.Forms.TextBox txSearch;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.TextBox txIdFrom;
        private System.Windows.Forms.Button btSearchId;
        private System.Windows.Forms.CheckBox chFormat;
        private System.Windows.Forms.Button btSpecialCase;
        private System.Windows.Forms.TextBox txIdTo;
    }
}

